<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once 'init.php';

$smarty->assign('mobile_brands', allBrands());

$permalink = $_GET["permalink"];
$permalink = Secure($permalink);

// paget details
$query = "SELECT id, title, description, meta_title, meta_description, meta_keywords, custom_tpl_file, custom_php_file, redirect_url FROM ".$database_table_prefix."pages WHERE permalink = '$permalink' AND active = '1' LIMIT 1";
$rs = $conn->query($query); 
$valid_page = $rs->num_rows; 
$row = $rs->fetch_assoc();

if($valid_page==0)
	{
		$smarty->display('404.tpl');
		exit;
	}
$content_id = $row['id'];	
$content_title = stripslashes($row['title']);
$content_content = stripslashes(html_entity_decode($row['description']));
$content_meta_title = stripslashes($row['meta_title']);
$content_meta_description = stripslashes($row['meta_description']);
$content_meta_keywords = stripslashes($row['meta_keywords']);
$custom_tpl_file = stripslashes($row['custom_tpl_file']);
$custom_php_file = $row['custom_php_file'];
$redirect_url = $row['redirect_url'];

if($redirect_url!='')
	{
	header("Location: $redirect_url");	
	exit;
	}

$content_content2 = strip_tags(html_entity_decode($content_content, ENT_QUOTES));
$content_content2 = substr($content_content2, 0, 250);
		
if($content_meta_title!="") $content_meta_title = $content_meta_title; else $content_meta_title = $content_title;
if($content_meta_description!="") $content_meta_description = $content_meta_description; else $content_meta_description = $content_content2;

$smarty->assign('META_TITLE',$content_meta_title);
$smarty->assign('META_DESCRIPTION',$content_meta_description);
$smarty->assign('META_KEYWORDS',$content_meta_keywords);
$smarty->assign('CONTENT_PERMALINK',$permalink);
$smarty->assign('CONTENT_TITLE',$content_title);
$smarty->assign('CONTENT_CONTENT',$content_content);


// ****************************************************************************************************
// PAGE MEDIA - IMAGES
// ****************************************************************************************************
$page_media_images = array();
$query = "SELECT id, title, description, file, url_redirect FROM ".$database_table_prefix."media WHERE source = 'page' AND content_id = '$content_id' AND type = 'image' ORDER BY id DESC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{		
	$media_id = $row['id'];	
	$media_title = stripslashes($row['title']);
	$media_description = stripslashes($row['description']);
	$media_file = $row['file'];	
	$url_redirect = stripslashes($row['url_redirect']);
	
	$media_title_summary = substr($media_title, 0, 80);

	$page_media_images[] = array("id" => $media_id, "title" => $media_title, "title_summary" => $media_title_summary, "description" => $media_description, "file" => $media_file, "url_redirect" => $url_redirect);
	}
$smarty->assign('page_media_images', $page_media_images);

// ****************************************************************************************************
// PAGE MEDIA - VIDEOS
// ****************************************************************************************************
$page_media_videos = array();
$query = "SELECT id, title, description, embed_code, url_redirect FROM ".$database_table_prefix."media WHERE source = 'page' AND content_id = '$content_id' AND type = 'video' ORDER BY id DESC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{		
	$media_id = $row['id'];	
	$media_title = stripslashes($row['title']);
	$media_description = stripslashes($row['description']);
	$url_redirect = stripslashes($row['url_redirect']);
	$embed_code = $row['embed_code'];	
	
	$media_title_summary = substr($media_title, 0, 80);
	
	$page_media_videos[] = array("id" => $media_id, "title" => $media_title, "title_summary" => $media_title_summary, "description" => $media_description, "embed_code" => $embed_code, "url_redirect" => $url_redirect);
	}
$smarty->assign('page_media_videos', $page_media_videos);

// ****************************************************************************************************
// SIMILAR PAGES
// ****************************************************************************************************
$similar_pages = array();
$query = "SELECT id, title, permalink FROM ".$database_table_prefix."pages WHERE active = '1' AND permalink != '$permalink' ORDER BY id DESC LIMIT 8";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{		
	$page_id = $row['id'];	
	$page_title = stripslashes($row['title']);
	$permalink = $row['permalink'];	

	$similar_pages[] = array("id" => $page_id, "title" => $page_title, "permalink" => $permalink);
	}
$smarty->assign('similar_pages', $similar_pages);


if($custom_php_file)
	include_once "$custom_php_file";

if($custom_tpl_file)
	$smarty->display($custom_tpl_file);
else
	$smarty->display('page.tpl');	
?>