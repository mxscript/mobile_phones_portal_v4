<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once 'init.php';

$cf_group_id = $mobiles_cf_group_id; // setup in global.php

$content_id_1 = isset($_REQUEST['id_product1']) ? $_REQUEST['id_product1'] : '';
$content_id_1 = Secure($content_id_1);

$content_id_2 = isset($_REQUEST['id_product2']) ? $_REQUEST['id_product2'] : '';
$content_id_2 = Secure($content_id_2);

$content_id_3 = isset($_REQUEST['id_product3']) ? $_REQUEST['id_product3'] : '';
$content_id_3 = Secure($content_id_3);

$number_of_items_to_compare = 0;

$msg = isset($_GET['msg']) ? $_GET['msg'] : '';
$msg = Secure($msg);
$smarty->assign('MSG',$msg);

$active_status_id = getActiveContentStatusID();

// ***********************************************************************************************************************
// details product 1
// ***********************************************************************************************************************
$query = "SELECT id, permalink, title, image, categ_id, content_short, content, cf_array FROM ".$database_table_prefix."content WHERE id = '$content_id_1' AND status_id = '$active_status_id' LIMIT 1";
$rs = $conn->query($query); $exist = $rs->num_rows; $row = $rs->fetch_assoc();
if($exist!=0) $number_of_items_to_compare = $number_of_items_to_compare + 1;

$content_id_1 = $row['id'];	
$content_permalink_1 = $row['permalink'];	
$content_title_1 = stripslashes($row['title']);
$content_image_1 = $row['image'];	
$content_categ_id_1 = $row['categ_id'];
$content_content_short_1 = stripslashes($row['content_short']);
$content_content_1 = stripslashes(html_entity_decode($row['content']));
$cf_array_1 = $row['cf_array'];

$query_categ = "SELECT title, permalink, parent_id FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id_1' LIMIT 1";
$rs_categ = $conn->query($query_categ);
$row = $rs_categ->fetch_assoc();
$content_categ_title_1 = stripslashes($row['title']);	
$content_categ_parent_id_1 = stripslashes($row['parent_id']);	
$content_categ_permalink_1 = stripslashes($row['permalink']);

if($content_categ_parent_id_1 == $mobiles_categ_id)
	{
		$content_title_1 = $content_categ_title_1." ".$content_title_1;
	}

$smarty->assign('VALID_CONTENT_1',$exist);
$smarty->assign('CONTENT_ID_1',$content_id_1);
$smarty->assign('CONTENT_PERMALINK_1',$content_permalink_1);
$smarty->assign('CONTENT_CATEG_PERMALINK_1',$content_categ_permalink_1);
$smarty->assign('CONTENT_TITLE_1',$content_title_1);
$smarty->assign('CONTENT_IMAGE_1',$content_image_1);
$smarty->assign('CONTENT_CONTENT_SHORT_1',$content_content_short_1);
$smarty->assign('CONTENT_CONTENT_1',$content_content_1);


// ***********************************************************************************************************************
// details product 2
// ***********************************************************************************************************************
$query = "SELECT id, permalink, title, image, categ_id, content_short, content, cf_array FROM ".$database_table_prefix."content WHERE id = '$content_id_2' AND status_id = '$active_status_id' LIMIT 1";
$rs = $conn->query($query); $exist = $rs->num_rows; $row = $rs->fetch_assoc();
if($exist!=0) $number_of_items_to_compare = $number_of_items_to_compare + 1;

$content_id_2 = $row['id'];	
$content_permalink_2 = $row['permalink'];	
$content_title_2 = stripslashes($row['title']);
$content_image_2 = $row['image'];	
$content_categ_id_2 = $row['categ_id'];
$content_content_short_2 = stripslashes($row['content_short']);
$content_content_2 = stripslashes(html_entity_decode($row['content']));
$cf_array_2 = $row['cf_array'];

$query_categ = "SELECT title, permalink, parent_id FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id_2' LIMIT 1";
$rs_categ = $conn->query($query_categ);
$row = $rs_categ->fetch_assoc();
$content_categ_title_2 = stripslashes($row['title']);	
$content_categ_parent_id_2 = stripslashes($row['parent_id']);	
$content_categ_permalink_2 = stripslashes($row['permalink']);

if($content_categ_parent_id_2 == $mobiles_categ_id)
	{
		$content_title_2 = $content_categ_title_2." ".$content_title_2;
	}

$smarty->assign('VALID_CONTENT_2',$exist);
$smarty->assign('CONTENT_ID_2',$content_id_2);
$smarty->assign('CONTENT_PERMALINK_2',$content_permalink_2);
$smarty->assign('CONTENT_CATEG_PERMALINK_2',$content_categ_permalink_2);
$smarty->assign('CONTENT_TITLE_2',$content_title_2);
$smarty->assign('CONTENT_IMAGE_2',$content_image_2);
$smarty->assign('CONTENT_CONTENT_SHORT_2',$content_content_short_2);
$smarty->assign('CONTENT_CONTENT_2',$content_content_2);


// ***********************************************************************************************************************
// details product 3
// ***********************************************************************************************************************
$query = "SELECT id, permalink, title, image, categ_id, content_short, content, cf_array FROM ".$database_table_prefix."content WHERE id = '$content_id_3' AND status_id = '$active_status_id' LIMIT 1";
$rs = $conn->query($query); $exist = $rs->num_rows; $row = $rs->fetch_assoc();
if($exist!=0) $number_of_items_to_compare = $number_of_items_to_compare + 1;

$content_id_3 = $row['id'];	
$content_permalink_3 = $row['permalink'];	
$content_title_3 = stripslashes($row['title']);
$content_image_3 = $row['image'];	
$content_categ_id_3 = $row['categ_id'];
$content_content_short_3 = stripslashes($row['content_short']);
$content_content_3 = stripslashes(html_entity_decode($row['content']));
$cf_array_3 = $row['cf_array'];

$query_categ = "SELECT title, permalink, parent_id FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id_3' LIMIT 1";
$rs_categ = $conn->query($query_categ);
$row = $rs_categ->fetch_assoc();
$content_categ_title_3 = stripslashes($row['title']);	
$content_categ_parent_id_3 = stripslashes($row['parent_id']);	
$content_categ_permalink_3 = stripslashes($row['permalink']);

if($content_categ_parent_id_3 == $mobiles_categ_id)
	{
		$content_title_3 = $content_categ_title_3." ".$content_title_3;
	}

$smarty->assign('VALID_CONTENT_3',$exist);
$smarty->assign('CONTENT_ID_3',$content_id_3);
$smarty->assign('CONTENT_PERMALINK_3',$content_permalink_3);
$smarty->assign('CONTENT_CATEG_PERMALINK_3',$content_categ_permalink_3);
$smarty->assign('CONTENT_TITLE_3',$content_title_3);
$smarty->assign('CONTENT_IMAGE_3',$content_image_3);
$smarty->assign('CONTENT_CONTENT_SHORT_3',$content_content_short_3);
$smarty->assign('CONTENT_CONTENT_3',$content_content_3);



if($number_of_items_to_compare>=2)
	{
	$query = "SELECT id FROM ".$database_table_prefix."compares WHERE product1_id = '$content_id_1' AND product2_id = '$content_id_2' AND  product3_id = '$content_id_3' ORDER BY id DESC LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc(); 
	$duplicate_compare_id = $row['id'];
	$exist_compare = $rs->num_rows;
	$now = date("Y-m-d H:i:s");	
	
	if($exist_compare==0)
		$query = "INSERT INTO ".$database_table_prefix."compares(id, product1_id, product2_id, product3_id, last_compare, items_compared, counter) VALUES (NULL, '$content_id_1', '$content_id_2', '$content_id_3', '$now', '$number_of_items_to_compare', 0)";
	else
		$query = "UPDATE ".$database_table_prefix."compares SET counter = counter + 1 WHERE id = '$duplicate_compare_id' LIMIT 1";

	$rs = $conn->query($query);
	}


// ****************************************
// SPECS 1
// ****************************************	
$specs_array_content1 = unserialize($cf_array_1);
$smarty->assign('specs_array_content1', $specs_array_content1);
// ***********************************************************************************************************************

// ****************************************
// SPECS 2
// ****************************************	
$specs_array_content2 = unserialize($cf_array_2);
$smarty->assign('specs_array_content2', $specs_array_content2);
// ***********************************************************************************************************************

// ****************************************
// SPECS 3
// ****************************************	
$specs_array_content3 = unserialize($cf_array_3);
$smarty->assign('specs_array_content3', $specs_array_content3);
// ***********************************************************************************************************************




// ****************************************************************************************************
// TOP COMPARES (2 ITEMS)
// ****************************************************************************************************
$top_compares = array();

$query = "SELECT product1_id, product2_id FROM ".$database_table_prefix."compares WHERE items_compared = 2 AND product1_id != product2_id ORDER BY counter DESC LIMIT 10";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
	$product1_id = $row['product1_id'];	
	$product2_id = $row['product2_id'];	
	
	$query_product1 = "SELECT title, permalink, image FROM ".$database_table_prefix."content WHERE id = '$product1_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 1";
	$rs1 = $conn->query($query_product1);
	$row = $rs1->fetch_assoc();
	$product1_title = stripslashes($row['title']);	
	$product1_permalink = stripslashes($row['permalink']);
	$product1_image = $row['image'];
	if(strlen($product1_title)>25) $product1_title = substr($product1_title, 0, 25)."...";	
	
	$query_product2 = "SELECT title, permalink, image FROM ".$database_table_prefix."content WHERE id = '$product2_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 1";
	$rs2 = $conn->query($query_product2);
	$row = $rs2->fetch_assoc();
	$product2_title = stripslashes($row['title']);	
	$product2_permalink = stripslashes($row['permalink']);
	$product2_image = $row['image'];
	if(strlen($product2_title)>25) $product2_title = substr($product2_title, 0, 25)."...";	
	
	$top_compares[] = array("product1_id" => $product1_id, "product2_id" => $product2_id, "product1_title" => $product1_title, "product2_title" => $product2_title, "product1_permalink" => $product1_permalink, "product2_permalink" => $product2_permalink, "product1_image" => $product1_image, "product2_image" => $product2_image);
	}

$smarty->assign('top_compares', $top_compares);


// ****************************************************************************************************
// LATEST COMPARES (2 ITEMS)
// ****************************************************************************************************
$latest_compares = array();

$query = "SELECT product1_id, product2_id FROM ".$database_table_prefix."compares WHERE items_compared = 2 AND product1_id != product2_id ORDER BY id DESC LIMIT 10";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
	$product1_id = $row['product1_id'];	
	$product2_id = $row['product2_id'];	
	
	$query_product1 = "SELECT title, permalink, image FROM ".$database_table_prefix."content WHERE id = '$product1_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 1";
	$rs1 = $conn->query($query_product1);
	$row = $rs1->fetch_assoc();
	$product1_title = stripslashes($row['title']);	
	$product1_permalink = stripslashes($row['permalink']);
	$product1_image = $row['image'];

	$query_product2 = "SELECT title, permalink, image FROM ".$database_table_prefix."content WHERE id = '$product2_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 1";
	$rs2 = $conn->query($query_product2);
	$row = $rs2->fetch_assoc();
	$product2_title = stripslashes($row['title']);	
	$product2_permalink = stripslashes($row['permalink']);
	$product2_image = $row['image'];

	$latest_compares[] = array("product1_id" => $product1_id, "product2_id" => $product2_id, "product1_title" => $product1_title, "product2_title" => $product2_title, "product1_permalink" => $product1_permalink, "product2_permalink" => $product2_permalink, "product1_image" => $product1_image, "product2_image" => $product2_image);
	}

$smarty->assign('latest_compares', $latest_compares);



$smarty->assign('NUMBER_OF_ITEMS_TO_COMPARE', $number_of_items_to_compare);

$smarty->display('compare.tpl');	
