<?php
require_once 'init.php';

if ($config_enable_contact_page=='0')
	{
	echo "Contact page disabled";
	exit;	
	}

$msg = isset($_GET['msg']) ? $_GET['msg'] : '';
$msg = Secure($msg);

$captcha_random_code = rand(0,9999);

$smarty->assign("CAPTCHA_RANDOM_CODE",$captcha_random_code);
$smarty->assign("MSG",$msg);

$smarty->assign("CONTACT_PAGE_SHOW_FORM",$config_enable_contact_form);
$smarty->assign("CONTACT_PAGE_SHOW_MAP",$config_contact_page_show_map);
$smarty->assign("CONTACT_PAGE_MAP_LATITUDE",$config_contact_page_map_latitude);
$smarty->assign("CONTACT_PAGE_MAP_LONGITUDE",$config_contact_page_map_longitude);
$smarty->assign("CONTACT_PAGE_MAP_ZOOM",$config_contact_page_map_zoom);
$smarty->assign("CONTACT_PAGE_CUSTOM_TEXT",html_entity_decode($config_contact_page_custom_text, ENT_QUOTES));

$smarty->display('contact.tpl');
?>