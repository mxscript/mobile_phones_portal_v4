<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once 'init.php';

$section_permalink = isset($_GET['section_permalink']) ? $_GET['section_permalink'] : '';
$section_permalink = Secure($section_permalink);

$categ_permalink = isset($_GET['categ_permalink']) ? $_GET['categ_permalink'] : '';
$categ_permalink = Secure($categ_permalink);

$subcateg_permalink = isset($_GET['subcateg_permalink']) ? $_GET['subcateg_permalink'] : '';
$subcateg_permalink = Secure($subcateg_permalink);

$subcateg_id = isset($_GET['subcateg_id']) ? $_GET['subcateg_id'] : '';
$subcateg_id = Secure($subcateg_id);

$pagenum = isset($_GET['pagenum']) ? $_GET['pagenum'] : '';
$pagenum = Secure($pagenum);
$pagenum = (int)$pagenum;

// is main section (level 1)
if($section_permalink!="" and $categ_permalink=="" and $subcateg_permalink=="") 
	{
		$query = "SELECT id, parent_id, title, description, meta_title, meta_description, meta_keywords, custom_tpl_file, slider_id FROM ".$database_table_prefix."categories WHERE permalink = '$section_permalink' AND active = 1 LIMIT 1";
		$is_section = 1;
		$smarty->assign(IS_SECTION,$is_section);
		$smarty->assign(SECTION_PERMALINK,$section_permalink);
	}

// if is category (level 2)
if($categ_permalink!="") 
	{
		$query_cat = "SELECT id FROM ".$database_table_prefix."categories WHERE permalink = '$categ_permalink' LIMIT 1";
		$rs_cat = $conn->query($query_cat); 
		$row = $rs_cat->fetch_assoc();
		$categ_id = $row['id'];	

		$is_category = 1;
		$smarty->assign(IS_CATEG,$is_category);
		$smarty->assign(CATEG_PERMALINK,$categ_permalink);
		$smarty->assign(SECTION_PERMALINK,$section_permalink);
		
		$query = "SELECT id, parent_id, title, description, meta_title, meta_description, meta_keywords, custom_tpl_file, slider_id FROM ".$database_table_prefix."categories WHERE permalink = '$categ_permalink' AND id = '$categ_id' AND active = 1 LIMIT 1";
	}

// if is subcategory (level 3, 4.....)
if($subcateg_permalink!="") 
	{
		$is_subcategory = 1;
		$smarty->assign(IS_SUBCATEG,$is_subcategory);
		$smarty->assign(SUBCATEG_PERMALINK,$subcateg_permalink);
		$smarty->assign(SECTION_PERMALINK,$section_permalink);
		$smarty->assign(SUBCATEG_ID,$subcateg_id);		
		
		$query = "SELECT id, parent_id, title, description, meta_title, meta_description, meta_keywords, custom_tpl_file, slider_id FROM ".$database_table_prefix."categories WHERE permalink = '$subcateg_permalink' AND id = '$subcateg_id' AND active = 1 LIMIT 1";
	}


$rs = $conn->query($query); $exist = $rs->num_rows; $row = $rs->fetch_assoc();

if($exist==0)
	{		
	$smarty->display('404.tpl');
	exit;
	}

$categ_id = $row['id'];	
$parent_id = $row['parent_id'];	
$title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
$description = $row['description'];
$meta_title = stripslashes($row['meta_title']);
$meta_description = stripslashes($row['meta_description']);
$meta_keywords = stripslashes($row['meta_keywords']);
$custom_tpl_file = $row['custom_tpl_file'];	
$slider_id = $row['slider_id'];	
				
if($meta_title=="") $meta_title = $title;
if($meta_description=="") $meta_description = $description;

if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

$status_id_active = getActiveContentStatusID();
$subcategories_list = createSubcategories_string($categ_id);

$query = "SELECT id FROM ".$database_table_prefix."content WHERE categ_id IN ($subcategories_list) AND status_id = '$status_id_active'";
$rs = $conn->query($query); $rows = $rs->num_rows; 

$content_this_page = array();

if ($rows!=0)
	{
		$page_rows = 100; // number of items per page
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1) { $pagenum = 1; } elseif ($pagenum > $last) { $pagenum = $last; }
		
		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;

		if($sort=="popular") 
			$sort_criteria = "hits";
		else	
			$sort_criteria = "id";
		
		$query = "SELECT id, categ_id, title, permalink, image, content_short, DATE(datetime_added) AS date_added FROM ".$database_table_prefix."content WHERE categ_id IN ($subcategories_list) AND status_id = '$status_id_active' ORDER BY sticky DESC, mobile_release_date DESC $max";
		$rs = $conn->query($query); $exist = $rs->num_rows; 
		while ($row = $rs->fetch_assoc())
			{
			$content_id = $row['id'];	
			$content_categ_id = $row['categ_id'];
			$content_title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
			$content_permalink = $row['permalink'];
			$content_image = $row['image'];
			$content_short = stripslashes(htmlentities($row['content_short'], ENT_QUOTES));
			$content_date = $row['date_added'];

			$query_categ = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id' LIMIT 1";
			$rs_categ = $conn->query($query_categ);
			$row = $rs_categ->fetch_assoc();
			$content_categ_title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
			$content_categ_permalink = $row['permalink'];
			
			$content_this_page[] = array("id" => $content_id, "title" => $content_title, "content_short" => $content_short, "permalink" => $content_permalink, "image" => $content_image, "date" => DateFormat($content_date), "categ_permalink" => $content_categ_permalink, "categ_title" => $content_categ_title);
			}
	}


$smarty->assign('META_TITLE',$meta_title);
$smarty->assign('META_DESCRIPTION',$meta_description);
$smarty->assign('META_KEYWORDS',$meta_keywords);
$smarty->assign('TITLE',$title);
$smarty->assign('PAGENUM',$pagenum);
$smarty->assign('NUMBER_OF_ITEMS',$rows);
$smarty->assign('NUMBER_OF_PAGES',$last);
$smarty->assign('ITEMS_PER_PAGE',$config_items_on_page);

$smarty->assign('content_this_page', $content_this_page);

if ($content_categ_id==$news_categ_id or $content_categ_id==$reviews_categ_id)
	$custom_tpl_file = 'categ_articles.tpl';

if($custom_tpl_file)
	$smarty->display($custom_tpl_file);
else
	$smarty->display('categ.tpl');	
?>