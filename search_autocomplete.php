<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<p id="searchresults">
<?php
require "core/core.php";
$status_id_active = getActiveContentStatusID();
	
if(isset($_POST['queryString'])) 
{
	$queryString = Secure($_POST['queryString']);
			
	if(strlen($queryString) >0)
	{	
		//$query_ls_all = "SELECT '".$database_table_prefix."content.id', '".$database_table_prefix."content.title', '".$database_table_prefix."content.permalink', '".$database_table_prefix."content.categ_id', '".$database_table_prefix."content.image', '".$database_table_prefix."categories.title', CONCAT ('".$database_table_prefix."content.title', '".$database_table_prefix."categories.title') AS '".$database_table_prefix."content.terms' FROM ".$database_table_prefix."content AS cont INNER JOIN ".$database_table_prefix."categories AS cat ON '".$database_table_prefix."content.categ_id' = '".$database_table_prefix."categories.id' WHERE MATCH(mx_content.terms) AGAINST ('$queryString' IN BOOLEAN MODE)";	
		if(strlen($queryString)>3)	
			$query_ls_all = "SELECT id FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND MATCH (search_terms) AGAINST ('".$queryString."' IN BOOLEAN MODE)";
		else	
			$query_ls_all = "SELECT id FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND search_terms LIKE '%$queryString%'";
				
		$rs_ls_all = $conn->query($query_ls_all);		
		$ls_numb_results = $rs_ls_all->num_rows;
		
		// display first 6 results
		if(strlen($queryString)>3)	
			$query_ls = "SELECT id, title, permalink, categ_id, image, MATCH (search_terms) AGAINST ('".$queryString."' IN BOOLEAN MODE) AS relevance FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND MATCH (search_terms) AGAINST ('".$queryString."' IN BOOLEAN MODE) ORDER BY relevance DESC, mobile_release_date DESC, rating_value DESC, id DESC LIMIT 6";
		else			
			$query_ls = "SELECT id, title, permalink, categ_id, image FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND search_terms LIKE '%$queryString%' ORDER BY mobile_release_date DESC, rating_value DESC, id DESC LIMIT 6";		
	
		$rs_ls = $conn->query($query_ls);

		if($conn->query($query_ls) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
		
		while ($row_ls = $rs_ls->fetch_assoc())
			{
				$ls_id = stripslashes($row_ls['id']);
				$ls_title = stripslashes($row_ls['title']);
				$ls_permalink = stripslashes($row_ls['permalink']);
				$ls_categ_id = stripslashes($row_ls['categ_id']);	
				$ls_image = stripslashes($row_ls['image']);	
				
				$query_ls_categ = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$ls_categ_id' LIMIT 1";
				$query_ls_categ = $conn->query($query_ls_categ);
				$row_ls = $query_ls_categ->fetch_assoc();
				$ls_categ_title = stripslashes($row_ls['title']);
				$ls_categ_permalink = stripslashes($row_ls['permalink']);
				
				echo '<a id="'.$queryString.'" results="44" onclick="track(this.id, '.$ls_numb_results.');" href="'.$config_site_url.'/'.$ls_categ_permalink.'/'.$ls_permalink.'-'.$ls_id.'.html">';
	         	echo '<img src="'.$config_site_media.'/thumbs/'.$ls_image.'"/>';
	         			
	         	if(strlen($ls_title) > 45) 
					{
					$ls_title = substr($ls_title, 0, 45) . "...";
	         		}	         			
	         	echo '<span class="searchheading">'.$ls_categ_title.' '.$ls_title.'</span>';
	         			
	         	echo '<span></span></a>';
	         } // end while
   		echo '<span class="seperator"><a href="'.$config_site_url.'/search.php?search='.$queryString.'">See all '.$ls_numb_results.' results</a></span><div class="clear"></div>';
	}
} 
else 
{
	echo 'There should be no direct access to this script!';
}
?>
</p>