<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$META_DESCRIPTION}">
    <meta name="keywords" content="{$META_KEYWORDS}">
    <title>{$META_TITLE}</title>
{include file='global_head_code.tpl'} 
</head>

<body>
	
    {include file='header.tpl'}     

    <section id="about-us">
        <div class="container">
			<div class="center wow fadeInDown">
				<h2>{$CONTENT_TITLE}</h2>
			</div>

			<div class="wow fadeInDown">
				{$CONTENT_CONTENT}
			</div>  
		</div><!--/.container-->
    </section><!--/about-us-->
	
{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 

</body>
</html>