<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$META_DESCRIPTION}">
    <meta name="keywords" content="{$META_KEYWORDS}">
    <title>{$META_TITLE}</title>
{include file='global_head_code.tpl'} 
</head>

<body>
	
    {include file='header.tpl'}     

    <section>
        <div class="container">
			<div class="center wow fadeInDown">
				<h2>{$CONTENT_TITLE}</h2>
			</div>

			<div class="wow fadeInDown">
				{$CONTENT_CONTENT}
                
                <div class="brands_list">
                <ul class="list-unstyled two_cols">
                {foreach $mobile_brands as $brand}
			    <li>{$brand.image|unescape:"html"} <a title="{$brand.title} {lang}phones{/lang}" href="{$CONFIG_SITE_URL}/{$brand.permalink}/">{$brand.title} {lang}phones{/lang} ({$brand.number_items})</a></li>
				{/foreach}
				</ul>
                </div>
                
			</div>  
		</div><!--/.container-->
    </section><!--/about-us-->
	
{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 

</body>
</html>