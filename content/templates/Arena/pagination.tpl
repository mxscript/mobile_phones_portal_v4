<div class="clear"></div>

{if $PAGENUM!=0 && $NUMBER_OF_ITEMS!=0}
{lang}Page{/lang} <strong>{$PAGENUM}</strong> {lang}of{/lang} <strong>{$NUMBER_OF_PAGES}</strong>
<br />
{/if}
     

{if $NUMBER_OF_ITEMS!=0}
{if $IS_SECTION==1} 	
    <ul class="pagination">
        {if $PAGENUM > 1}
        <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/">{lang}First{/lang}</a></li>
        {/if}
        
        {if $PAGENUM == 1 || $NUMBER_OF_ITEMS <= $ITEMS_PER_PAGE}
                
        {else}
            {$PREVIOUS = $PAGENUM-1}
            <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/page-{$PREVIOUS}">{lang}Back{/lang}</a></li>
        {/if}    
            
        {for $i=1 to 6}    
            {$p = $PAGENUM + $i}
            {if $p != $PAGENUM && $p<= $NUMBER_OF_PAGES && $NUMBER_OF_PAGES > 1}
                <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/page-{$p}">{$p}</a></li>
            {/if}    
        {/for}            
            
    
        {if $PAGENUM == $NUMBER_OF_PAGES}
            {for $i=6 to 1}    
            {$p = $NUMBER_OF_PAGES - $i}
            {if $p!=$PAGENUM && $p<=$NUMBER_OF_PAGES && $NUMBER_OF_PAGES>1 && $p>0}
                <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/page-{$p}">{$p}</a></li>
            {/if}    
            {/for}      
         
         {else}   
            {$next = $PAGENUM+1}
            <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/page-{$next}">{lang}Next{/lang}</a></li>
         {/if}          
    
            
        {if $PAGENUM > ITEMS_PER_PAGE && $PAGENUM != $NUMBER_OF_PAGES}
            <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/page-{$NUMBER_OF_PAGES}">{lang}Last{/lang}</a></li>    
        {/if}    
    </ul>
{/if}

{if $IS_CATEG==1} 
    <ul class="pagination">
        {if $PAGENUM > 1}
        <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/{$CATEG_PERMALINK}/">{lang}First{/lang}</a></li>
        {/if}
        
        {if $PAGENUM == 1 || $NUMBER_OF_ITEMS <= $ITEMS_PER_PAGE}
                
        {else}
            {$PREVIOUS = $PAGENUM-1}
            <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/{$CATEG_PERMALINK}/page-{$PREVIOUS}">{lang}Back{/lang}</a></li>
        {/if}    
            
        {for $i=1 to 6}    
            {$p = $PAGENUM + $i}
            {if $p != $PAGENUM && $p<= $NUMBER_OF_PAGES && $NUMBER_OF_PAGES > 1}
                <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/{$CATEG_PERMALINK}/page-{$p}">{$p}</a></li>
            {/if}    
        {/for}            
            
    
        {if $PAGENUM == $NUMBER_OF_PAGES}
            {for $i=6 to 1}    
            {$p = $NUMBER_OF_PAGES - $i}
            {if $p!=$PAGENUM && $p<=$NUMBER_OF_PAGES && $NUMBER_OF_PAGES>1 && $p>0}
                <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/{$CATEG_PERMALINK}/page-{$p}">{$p}</a></li>
            {/if}    
            {/for}      
         
         {else}   
            {$next = $PAGENUM+1}
            <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/{$CATEG_PERMALINK}/page-{$next}">{lang}Next{/lang}</a></li>
         {/if}          
    
            
        {if $PAGENUM > ITEMS_PER_PAGE && $PAGENUM != $NUMBER_OF_PAGES}
            <li><a href="{$CONFIG_SITE_URL}/{$SECTION_PERMALINK}/{$CATEG_PERMALINK}/page-{$NUMBER_OF_PAGES}">{lang}Last{/lang}</a></li>    
        {/if}    
    </ul>
{/if}

{if $IS_SUBCATEG==1} 	
    <ul class="pagination">
        {if $PAGENUM > 1}
        <li><a href="{$CONFIG_SITE_URL}/{$SUBCATEG_ID}-{$SUBCATEG_PERMALINK}/">{lang}First{/lang}</a></li>
        {/if}
        
        {if $PAGENUM == 1 || $NUMBER_OF_ITEMS <= $ITEMS_PER_PAGE}
                
        {else}
            {$PREVIOUS = $PAGENUM-1}
            <li><a href="{$CONFIG_SITE_URL}/{$SUBCATEG_ID}-{$SUBCATEG_PERMALINK}/page-{$PREVIOUS}">{lang}Back{/lang}</a></li>
        {/if}    
            
        {for $i=1 to 6}    
            {$p = $PAGENUM + $i}
            {if $p != $PAGENUM && $p<= $NUMBER_OF_PAGES && $NUMBER_OF_PAGES > 1}
                <li><a href="{$CONFIG_SITE_URL}/{$SUBCATEG_ID}-{$SUBCATEG_PERMALINK}/page-{$p}">{$p}</a></li>
            {/if}    
        {/for}            
            
    
        {if $PAGENUM == $NUMBER_OF_PAGES}
            {for $i=6 to 1}    
            {$p = $NUMBER_OF_PAGES - $i}
            {if $p!=$PAGENUM && $p<=$NUMBER_OF_PAGES && $NUMBER_OF_PAGES>1 && $p>0}
                <li><a href="{$CONFIG_SITE_URL}/{$SUBCATEG_ID}-{$SUBCATEG_PERMALINK}/page-{$p}">{$p}</a></li>
            {/if}    
            {/for}      
         
         {else}   
            {$next = $PAGENUM+1}
            <li><a href="{$CONFIG_SITE_URL}/{$SUBCATEG_ID}-{$SUBCATEG_PERMALINK}/page-{$next}">{lang}Next{/lang}</a></li>
         {/if}          
    
            
        {if $PAGENUM > ITEMS_PER_PAGE && $PAGENUM != $NUMBER_OF_PAGES}
            <li><a href="{$CONFIG_SITE_URL}/{$SUBCATEG_ID}-{$SUBCATEG_PERMALINK}/page-{$NUMBER_OF_PAGES}">{lang}Last{/lang}</a></li>    
        {/if}    
    </ul>

{/if}
{/if}