<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$CONFIG_SITE_DESCRIPTION}">
    <meta name="keywords" content="{$CONFIG_SITE_KEYWORDS}">
    <title>{$CONFIG_SITE_TITLE}</title>    
    {include file='global_head_code.tpl'}   
</head><!--/head-->

<body class="homepage">

	{include file='header.tpl'} 
    

    <section class="container">

            <div class="row">
                 
            
                <div class="col-md-12">
                <h1>Website offline</h1> 	
                                                                                        
                </div><!--/.col-md-12-->
                
            </div><!--/.row-->

    </section>

	{include file='footer.tpl'} 
    
	{include file='global_footer_code.tpl'} 
</body>
</html>