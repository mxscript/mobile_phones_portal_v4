{literal}
    <script type="text/javascript">

		$('body').click(function(e) {
			if (!$(e.target).closest('#suggestions_compare1').length){
				$("#suggestions_compare1").fadeOut();
			}
		});

		google.load("jquery", "1.3.1");		
		google.setOnLoadCallback(function()
		{			
			// Fade out the suggestions box when not active
			 $("input").blur(function(){
				$('#suggestions_compare1').fadeOut();
			 });

			// Fade out the suggestions box when not active
			 $("input").blur(function(){
				$('#suggestions_compare2').fadeOut();
			 });

			// Fade out the suggestions box when not active
			 $("input").blur(function(){
				$('#suggestions_compare3').fadeOut();
			 });
			 
		});
		
		function lookup_compare1(inputString_compare1) {
			if(inputString_compare1.length == 0) {
				$('#suggestions_compare1').fadeOut(); // Hide the suggestions box
			} else {
				$.post("{/literal}{$CONFIG_SITE_URL}{literal}/search_autocomplete_compare.php?product_number=1&id_product2={/literal}{$CONTENT_ID_2}{literal}&id_product3={/literal}{$CONTENT_ID_3}{literal}", {queryString: ""+inputString_compare1+""}, function(data) { // Do an AJAX call
					$('#suggestions_compare1').fadeIn(); // Show the suggestions box
					$('#suggestions_compare1').html(data); // Fill the suggestions box
				});
			}
		}   

		function lookup_compare2(inputString_compare2) {
			if(inputString_compare2.length == 0) {
				$('#suggestions_compare2').fadeOut(); // Hide the suggestions box
			} else {
				$.post("{/literal}{$CONFIG_SITE_URL}{literal}/search_autocomplete_compare.php?product_number=2&id_product1={/literal}{$CONTENT_ID_1}{literal}&id_product3={/literal}{$CONTENT_ID_3}{literal}", {queryString: ""+inputString_compare2+""}, function(data) { // Do an AJAX call
					$('#suggestions_compare2').fadeIn(); // Show the suggestions box
					$('#suggestions_compare2').html(data); // Fill the suggestions box
				});
			}
		}   

		function lookup_compare3(inputString_compare3) {
			if(inputString_compare3.length == 0) {
				$('#suggestions_compare3').fadeOut(); // Hide the suggestions box
			} else {
				$.post("{/literal}{$CONFIG_SITE_URL}{literal}/search_autocomplete_compare.php?product_number=3&id_product1={/literal}{$CONTENT_ID_1}{literal}&id_product2={/literal}{$CONTENT_ID_2}{literal}", {queryString: ""+inputString_compare3+""}, function(data) { // Do an AJAX call
					$('#suggestions_compare3').fadeIn(); // Show the suggestions box
					$('#suggestions_compare3').html(data); // Fill the suggestions box
				});
			}
		}   

	</script>
{/literal}

<div class="row">

<table class="table table-condensed" id="specs">	    

	<tr>    
    <form action="{$CONFIG_SITE_URL}/compare/" method="post">
    
    <td align="left" style="min-width:160px;">
    
   
    </td>  
    
    <td align="left" width="33%">
    {if !$VALID_CONTENT_1} 
        <label>{lang}Product 1{/lang}</label><br />
        <input type="text" class="form-control" placeholder="{lang}Search device{/lang}" name="id_product1" id="inputString_compare1" onkeyup="lookup_compare1(this.value);" autocomplete="off"> 
        <div id="suggestions_compare1"></div>
    {else}   
    	<h4><a title="{$CONTENT_TITLE_1}" href="{$CONFIG_SITE_URL}/{$CONTENT_CATEG_PERMALINK_1}/{$CONTENT_PERMALINK_1}-{$CONTENT_ID_1}.html"><strong>{$CONTENT_TITLE_1}</strong></a></h4>
        <i class="fa fa-times" aria-hidden="true"></i> <a href="{$CONFIG_SITE_URL}/compare.html?id_product1=&id_product2={$CONTENT_ID_2}&id_product3={$CONTENT_ID_3}">{lang}Remove{/lang}</a>
        <input type="hidden" name="id_product1" value="{$CONTENT_ID_1}" />           
    {/if}
    </td> 
    <td align="left" width="33%">
    {if !$VALID_CONTENT_2} 
        <label>{lang}Product 2{/lang}</label><br />
        <input type="text" class="form-control" placeholder="{lang}Search device{/lang}" name="id_product2" id="inputString_compare2" onkeyup="lookup_compare2(this.value);" autocomplete="off"> 
        <div id="suggestions_compare2"></div>
    {else}   
    	<h4><a title="{$CONTENT_TITLE_2}" href="{$CONFIG_SITE_URL}/{$CONTENT_CATEG_PERMALINK_2}/{$CONTENT_PERMALINK_2}-{$CONTENT_ID_2}.html"><strong>{$CONTENT_TITLE_2}</strong></a></h4>
        <i class="fa fa-times" aria-hidden="true"></i> <a href="{$CONFIG_SITE_URL}/compare.html?id_product1={$CONTENT_ID_1}&id_product2=&id_product3={$CONTENT_ID_3}">{lang}Remove{/lang}</a>
        <input type="hidden" name="id_product2" value="{$CONTENT_ID_2}" />
    {/if}
    </td>
    <td align="left" width="33%">
    {if !$VALID_CONTENT_3} 
        <label>{lang}Product 3{/lang}</label><br />
        <input type="text" class="form-control" placeholder="{lang}Search device{/lang}" name="id_product3" id="inputString_compare3" onkeyup="lookup_compare3(this.value);" autocomplete="off"> 
        <div id="suggestions_compare3"></div>

    {else}   
    	<h4><a title="{$CONTENT_TITLE_3}" href="{$CONFIG_SITE_URL}/{$CONTENT_CATEG_PERMALINK_3}/{$CONTENT_PERMALINK_3}-{$CONTENT_ID_3}.html"><strong>{$CONTENT_TITLE_3}</strong></a></h4>
        <i class="fa fa-times" aria-hidden="true"></i> <a href="{$CONFIG_SITE_URL}/compare.html?id_product1={$CONTENT_ID_1}&id_product2={$CONTENT_ID_2}&id_product3=">{lang}Remove{/lang}</a>
        <input type="hidden" name="id_product3" value="{$CONTENT_ID_3}" />
    {/if}
    </td>    
    </form>  
    </tr>
    
    <tr>
      <td>&nbsp;</td>
      <td>
      {if $VALID_CONTENT_1}<img class="img-responsive" style="max-height:180px; width:auto;" alt="{$CONTENT_TITLE_1}" title="{$CONTENT_TITLE_1}" src="{$CONFIG_SITE_MEDIA}/thumbs/{$CONTENT_IMAGE_1}" /> {/if} 
      </td>
      <td>
      {if $VALID_CONTENT_2}<img class="img-responsive" style="max-height:180px; width:auto;" alt="{$CONTENT_TITLE_2}" title="{$CONTENT_TITLE_2}" src="{$CONFIG_SITE_MEDIA}/thumbs/{$CONTENT_IMAGE_2}" /> {/if}
      </td>
      <td>
      {if $VALID_CONTENT_3}<img class="img-responsive" style="max-height:180px; width:auto;" alt="{$CONTENT_TITLE_3}" title="{$CONTENT_TITLE_3}" src="{$CONFIG_SITE_MEDIA}/thumbs/{$CONTENT_IMAGE_3}" /> {/if}     
      </td>
    </tr>



	{if $NUMBER_OF_ITEMS_TO_COMPARE==1}
    
        {foreach from=$specs_array_content1 item=specs_section_content1 key=section_content1}	        
        <tr>
            <td colspan="5"><h3>{$section_content1}</h3></td>    
        </tr>					                
                {foreach from=$specs_section_content1 item=value_content1 key=spec_content1}
                    <tr>
                        <td><strong>{$spec_content1}</strong></td>
                        <td>{$value_content1}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>  
                 {/foreach}   
        {/foreach}
    
    {/if}




	{if $NUMBER_OF_ITEMS_TO_COMPARE==2}

        {foreach from=$specs_array_content1 item=specs_section_content1 key=section_content1}	        
        {foreach from=$specs_array_content2 item=specs_section_content2 key=section_content2}
        {if $section_content1==$section_content2}
        <tr>
            <td colspan="5"><h3>{$section_content1}</h3></td>    
        </tr>					                
                {foreach from=$specs_section_content1 item=value_content1 key=spec_content1}            
                {foreach from=$specs_section_content2 item=value_content2 key=spec_content2}    
                    {if $spec_content1==$spec_content2}             
                    <tr>
                        <td><strong>{$spec_content1}</strong></td>
                        <td>{$value_content1}</td>
                        <td>{$value_content2}</td>
                        <td></td>
                        <td></td>
                    </tr>  
                    {/if}                    
                 {/foreach}
                 {/foreach}	
        {/if}		
        {/foreach}    
        {/foreach}
    
    {/if}
    
    
    
	{if $NUMBER_OF_ITEMS_TO_COMPARE==3}

        {foreach from=$specs_array_content1 item=specs_section_content1 key=section_content1}	        
        {foreach from=$specs_array_content2 item=specs_section_content2 key=section_content2}
        {foreach from=$specs_array_content3 item=specs_section_content3 key=section_content3}
        {if $section_content1==$section_content2 && $section_content1==$section_content3}
        <tr>
            <td colspan="5"><h3>{$section_content1}</h3></td>    
        </tr>					                
                {foreach from=$specs_section_content1 item=value_content1 key=spec_content1}            
                {foreach from=$specs_section_content2 item=value_content2 key=spec_content2}  
                {foreach from=$specs_section_content3 item=value_content3 key=spec_content3}   
                	{if $spec_content1==$spec_content2 && $spec_content1==$spec_content3}             
                    <tr>
                        <td><strong>{$spec_content1}</strong></td>
                        <td>{$value_content1}</td>
                        <td>{$value_content2}</td>
                        <td>{$value_content3}</td>
                        <td></td>
                    </tr>  
                    {/if}	                  
                 {/foreach}
                 {/foreach}	
                 {/foreach}
        {/if}		
        {/foreach}    
        {/foreach}
        {/foreach}
    
    {/if}
    
</table>

</div>


{if !$VALID_CONTENT_1 && !$VALID_CONTENT_2 && !$VALID_CONTENT_3}

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <h2 style="margin-top:0;">{lang}Top compares{/lang}</h2>
       	{$numb=1}
        {foreach from=$top_compares item=product name=foo}        	
            {if $smarty.foreach.foo.index == 10} <!--  show 10 compares -->
            {break}
            {/if}
        
			<strong>{$numb}.</strong> <a href="{$CONFIG_SITE_URL}/compare.html?id_product1={$product.product1_id}&id_product2={$product.product2_id}"><strong>{$product.product1_title} {lang}vs{/lang} {$product.product2_title}</strong></a><br />
            {$numb=$numb+1}         
        {/foreach}
	</div>
    
    
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <h2 style="margin-top:0;">{lang}Latest compares{/lang}</h2>
        {$numb=1}
        {foreach from=$latest_compares item=product name=foo}
            {if $smarty.foreach.foo.index == 10} <!--  show 10 compares -->
            {break}
            {/if}
        
            <strong>{$numb}.</strong> <a href="{$CONFIG_SITE_URL}/compare.html?id_product1={$product.product1_id}&id_product2={$product.product2_id}"><strong>{$product.product1_title} {lang}vs{/lang} {$product.product2_title}</strong></a><br />
            {$numb=$numb+1}             
        {/foreach}
	</div>

{/if}
<div class="clear"></div>
