<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{lang}Contact us{/lang}">
    <title>{lang}Contact us{/lang}</title>    
    {include file='global_head_code.tpl'}   
</head><!--/head-->

<body>
	
    {include file='header.tpl'} 
    
    {if $CONTACT_PAGE_SHOW_MAP}    
    <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="gmap">
                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q={$CONTACT_PAGE_MAP_LATITUDE},{$CONTACT_PAGE_MAP_LONGITUDE}&amp;z={$CONTACT_PAGE_MAP_ZOOM}&amp;output=embed"></iframe>
                        </div>
                    </div>
                </div>
            </div>       
    </div>
	{/if}
    
    <section id="contact-page">
        <div class="container">
            {if $CONTACT_PAGE_CUSTOM_TEXT}
            <div class="col-sm-12 map-content" align="center">
            	<div class="clear"></div>
	            {$CONTACT_PAGE_CUSTOM_TEXT}
                <div class="clear"></div>
            </div>
            {/if}

            <h2>{lang}Connect with us using Facebook{/lang}</h2>
            
             <a target="_blank" href="https://www.facebook.com/mxscripts"><button class="btn btn-facebook"><i class="fa fa-facebook"></i> | Facebook page</button></a>
           
            <hr>
            
			{if $CONTACT_PAGE_SHOW_FORM}                       
            <h2>{lang}Contact Form{/lang}</h2>
            <div class="row"> 
            	<a name="form"></a>
                {if $MSG == 'error_name'}
				<div class="col-md-12"><div class="alert alert-danger" role="alert">{lang}Error{/lang}! {lang}Please input your name{/lang}</div></div>
				{/if}      

                {if $MSG == 'error_email'}
				<div class="alert alert-danger" role="alert">{lang}Error{/lang}! {lang}Please input your email{/lang}</div>
				{/if}      

                {if $MSG == 'error_captcha'}
				<div class="alert alert-danger" role="alert">{lang}Error{/lang}! {lang}Please input captcha code{/lang}</div>
				{/if}      

                {if $MSG == 'ok'}
				<div class="col-md-12"><div class="alert alert-success" role="alert">{lang}Message sent{/lang}</div></div>
				{/if}      

                <form class="contact-form" name="contact-form" method="post" action="{$CONFIG_SITE_URL}/core/ContactSubmit.php">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{lang}Your name{/lang} ({lang}required{/lang})</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>{lang}Your email{/lang} ({lang}required{/lang})</label>
                            <input type="email" name="email" class="form-control" required>
                        </div>    
                        <div class="form-group">
                            <label>{lang}Your Skype{/lang} ({lang}optional{/lang})</label>
                            <input type="text" name="skype" class="form-control">
                        </div>                                              
                        <div class="form-group">
                            <label>{lang}Antispam code{/lang} ({lang}required{/lang})</label>
                            <input type="text" name="captcha_box" class="form-control" required>
                            <img src="{$CONFIG_SITE_URL}/core/plugins/captchaimage.php" align="bottom" /><br />
                        </div>                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{lang}Your message{/lang} ({lang}required{/lang})</label>
                            <textarea name="message" id="message" required class="form-control" rows="12"></textarea>
                        </div>                        
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">{lang}Send message{/lang}</button>
                        </div>
                    </div>
                </form> 
            </div><!--/.row-->
            {/if}
            
            <hr>
            
                            
        </div><!--/.container-->
    </section><!--/#contact-page-->

	{include file='footer.tpl'} 
    
	{include file='global_footer_code.tpl'} 

</body>
</html>