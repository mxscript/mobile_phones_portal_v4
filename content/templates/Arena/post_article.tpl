<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$META_DESCRIPTION}">
    <meta name="keywords" content="{$META_KEYWORDS}">
    <title>{$META_TITLE}</title>
{include file='global_head_code.tpl'}     

    <meta property="og:title" content="{$META_TITLE}" />
    <meta property="og:image" content="{$CONFIG_SITE_URL}/content/media/large/{$CONTENT_IMAGE}" />
    <meta property="og:image:width" content="160" />
    <meta property="og:image:height" content="212" />        
    <link rel="image_src" href="{$CONFIG_SITE_URL}/content/media/large/{$CONTENT_IMAGE}" />
    <meta property="og:site_name" content="{$CONFIG_SITE_TITLE}" />
    <meta property="og:description" content="{if !$CONTENT_SHORT}{$META_TITLE}{else} {$CONTENT_SHORT}{/if}" />
    <meta property="fb:app_id" content="{$CONFIG_FB_APP_ID}" />
    <meta property="og:type" content="article" />

</head>

<body>

    {include file='header.tpl'}     

    <section class="container">

            <div class="row">

                <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs">
					{include file='sidebar.tpl'}     				
    			</aside>  

            
                 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                 	
                    <div class="title_image">
				    <img src="{$CONFIG_SITE_MEDIA}/large/{$CONTENT_IMAGE}" class="img-responsive" />
      				<h1>{$CONTENT_TITLE}</h1>
                    <p>{$CONTENT_SHORT}</p>
					</div>

                    <div class="clear"></div>

                    <span style="float:right">
                        <span style="float:right" class="header_google_button">
                        <a target="_blank" href="https://plusone.google.com/_/+1/confirm?hl=en&url={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}.html"  title="Google plus"><i class="fa fa-google-plus"></i></a>
                        </span> 
                        <span style="float:right" class="header_twitter_button">
                        <a target="_blank" href="https://twitter.com/intent/tweet?text={$CONTENT_TITLE}&url={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}.html" title="Twitter"><i class="fa fa-twitter"></i></a>
                        </span>                    
                        <span style="float:right" class="header_facebook_button">
                        <a target="_blank" href="http://www.facebook.com/sharer.php?u={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}.html" title="Facebook"><i class="fa fa-facebook"></i></a>
                        </span>
                    </span>
                    					
                    {if $DISABLE_RATINGS==0}
                    <strong>Rate this article:</strong><br>
                    <span style="float:left;"><input id="star-rating" type="text" class="rating-loading" data-size="xs" value="{$STAR_RATING_VALUE}" ></span>                    <span style="font-size: 20px; margin-left:10px; bottom:0; line-height:30px;">{if $STAR_RATING_NUMB_RATINGS==1}({lang}One vote{/lang}){/if} {if $STAR_RATING_NUMB_RATINGS>1}({$STAR_RATING_NUMB_RATINGS} {lang}votes{/lang}){/if}</span>                                         
                    <div class="rating_click_result" style="font-weight:bold"></div>
                    <div class="clear"></div>
                    {/if}                    
                    
                    <div class="clear"></div>
                    
					<div class="article_info">
                    <strong>{$AUTHOR_NAME}</strong> {lang}at{/lang} {$CONTENT_DATETIME} | {$CONTENT_HITS} {lang}hits{/lang}					
                    </div>
					
        		    <div class="article_text">{$CONTENT}</div>                    					                    	

					<div class="clear"></div> 
                    
					{if $CONTENT_NUMBER_OF_MEDIA_IMAGES>0}
                    <h3>Image gallery</h3>
                    {foreach $content_media_images as $image}
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <a href="{$CONFIG_SITE_MEDIA}/large/{$image.file}" data-toggle="lightbox" data-gallery="multiimages" data-title="{$image.title}" data-footer="{$image.description}"><img class="device_image img-responsive" src="{$CONFIG_SITE_MEDIA}/small/{$image.file}" alt="{$image.title}"></a>
                    </div>
					{/foreach}
                    <div class="clear"></div>
                    {/if}
		        
                    
					
                    {if $CONTENT_NUMBER_OF_MEDIA_VIDEOS>0}
					<div class="col-md-12">
                    {foreach $content_media_videos as $video}                    
	                    {$video.embed_code|unescape}
                        <div class="clear"></div>
					{/foreach}
					</div>                    
                    <div class="clear"></div> 
                    {/if}
                    
					<div class="article_header">
                    <h2>{lang}Related articles{/lang} <span class="more"><i class="fa fa-mobile grey"></i> <a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/">{lang}All{/lang} {$CATEG_TITLE}</a></span></h2>
                    </div>
                    
                    {foreach from=$similar_items item=item name=foo}
                            {if $smarty.foreach.foo.index < 8}                        	                    		
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 articles_list">
                            <a title="{$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html"><img class="img-responsive" src="{$CONFIG_SITE_MEDIA}/large/{$item.image}" alt="{$item.title}" title="{$item.title}" /></a>
							<a title="{$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html">{$item.title}</a>
                    		</div>   
                            {/if}                                             
                    {/foreach}
					
                    <div class="clear"></div>                    
                                                                                        
                </div><!--/.col-md-8-->

                <aside class="hidden-lg hidden-md hidden-sm col-xs-12">
					{include file='sidebar.tpl'}     				
    			</aside>  
                
            </div><!--/.row-->

    </section>

{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 
</body>
</html>