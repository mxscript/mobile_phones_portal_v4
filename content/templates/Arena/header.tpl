<header id="header" class="header1">
        <nav class="navbar navbar-inverse yamm header1" role="banner">

            <div class="container">

        		<div class="navbar-header">
				<a class="navbar-brand home" href="{$CONFIG_SITE_URL}" title="{$CONFIG_SITE_TITLE}"><img src="{$CONFIG_SITE_MEDIA}/img/{$CONFIG_SITE_LOGO}" alt="{$CONFIG_SITE_TITLE}" class="hidden-xs hidden-sm hidden-md"></a>
                </div><!--/.navbar-header -->
                
						<a class="navbar-brand home" href="{$CONFIG_SITE_URL}" title="{$CONFIG_SITE_TITLE}"><img src="{$CONFIG_SITE_MEDIA}/img/{$CONFIG_SITE_LOGO}" alt="{$CONFIG_SITE_TITLE}" class="hidden-sm hidden-md hidden-lg"></a>

                <div class="navbar-header">
                	
                    <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".menu1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>-->

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span>
               			 <span class="icon-bar"></span>
			             <span class="icon-bar"></span>
                     </button>                                                    
                </div>
				 

                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <form class="navbar-form" role="search" id="searchform" action="{$CONFIG_SITE_URL}/search.php" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control-header" placeholder="{lang}Search{/lang}" name="search" id="inputString" onkeyup="lookup(this.value);" autocomplete="off">
                            <div class="input-group-btn">
                                <button class="btn btn-default btn-default-header" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                        <div id="suggestions"></div>
                    </form>
                </div>     

               
                <div class="menu1 collapse navbar-collapse navbar-right">

                        
                    <ul class="nav navbar-nav">
        
                        <div class="social-network social-circle">
                                <a href="{$CONFIG_SOCIAL_FACEBOOK_URL}" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a>
                                <a href="{$CONFIG_SOCIAL_TWITTER_URL}" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a>
                                <a href="{$CONFIG_SOCIAL_GOOGLEPLUS_URL}" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a>
                                <a href="{$CONFIG_SOCIAL_YOUTUBE_URL}" class="icoYoutube" title="Youtube"><i class="fa fa-youtube"></i></a>
                                <a href="{$CONFIG_SOCIAL_PINTEREST_URL}" class="icoPinterest" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                                <a href="{$CONFIG_SOCIAL_LINKEDIN_URL}" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a>                                
                        </div>                    
                        
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->		
</header><!--/header-->


<!-- *** SECOND LINE NAVBAR *** -->
<div class="navbar-affixed-top" data-spy="affix" data-offset-top="100">

	<div class="navbar navbar-inverse yamm" role="navigation" id="navbar">

    	<div class="container">
                        
                <div class="navbar-collapse collapse" id="navigation">

                        <ul class="nav navbar-nav" style="margin-left:-20px;">                                            

                        <li class="hidden-sm"><a href="{$CONFIG_SITE_URL}" title="{$CONFIG_SITE_TITLE}">{lang}Home{/lang}</a></li>
                                                                        
                        {foreach $header_menu as $link}
                        <li><a title="{$link.title}" href="{$CONFIG_SITE_URL}/{$link.permalink}/">{$link.title}</a></li>
                        {/foreach}
                        
                        {foreach $header_menu_pages as $page}		   	           
                        <li><a title="{$page.title}" href="{$CONFIG_SITE_URL}/{$page.permalink}.html">{$page.title}</a></li>
                        {/foreach}
                    	
                        </ul>
                        
                        <ul class="nav navbar-nav pull-right">
                        {if $CONFIG_ENABLE_CONTACT_PAGE==1}
                        <li><a href="{$CONFIG_SITE_URL}/contact.html">{lang}Contact{/lang}</a></li>
                        {/if}                                                
                        </ul>


				</div><!--/.nav-collapse -->

		</div><!-- /#container -->

	</div><!-- /#navbar -->

</div><!-- *** NAVBAR END *** -->

{if $DISABLE_ADS!=1}
<div class="container">
	<div class="col-lg-12" align="center" style="margin-top:20px; margin-bottom:20px;">
	{$ads[1]}
	</div>
</div>    
{/if}
