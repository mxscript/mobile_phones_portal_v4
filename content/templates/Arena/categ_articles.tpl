<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$META_DESCRIPTION} {if $PAGENUM > 1}- {lang}page{/lang} {$PAGENUM}{/if}">
    <meta name="keywords" content="{$META_KEYWORDS}">
    <title>{$META_TITLE} {if $PAGENUM > 1}- {lang}page{/lang} {$PAGENUM}{/if}</title>
{include file='global_head_code.tpl'}     
</head>

<body>

    {include file='header.tpl'}     

    <section class="container">

            <div class="row">

                <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs">
					{include file='sidebar.tpl'}     				
    			</aside>  

            
                 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                 	<div class="article_header">
		            <h2>{$META_TITLE}</h2>
                    </div>
        		    <p class="lead">{$META_DESCRIPTION}</p>

                 	{foreach $content_this_page as $item}	                        	                    		
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 articles_list">
                            <a title="{$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html"><img class="img-responsive img-blog" src="{$CONFIG_SITE_MEDIA}/large/{$item.image}" alt="{$item.title}" title="{$item.title}" /></a>
							<a title="{$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html">{$item.title}</a>
                    		</div>                                                
                    {/foreach}
					
                    <div class="clear"></div>
                    {include file='pagination.tpl'} 
                                                                                        
                </div><!--/.col-md-8-->

                <aside class="hidden-lg hidden-md hidden-sm col-xs-12">
					{include file='sidebar.tpl'}     				
    			</aside>  
                                
            </div><!--/.row-->

    </section>

{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 
</body>
</html>