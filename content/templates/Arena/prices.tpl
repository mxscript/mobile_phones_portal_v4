<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$META_DESCRIPTION}">
    <meta name="keywords" content="{$META_KEYWORDS}">
    <title>{$META_TITLE} {if $PAGENUM > 1}- {lang}page{/lang} {$PAGENUM} {/if}</title>
{include file='global_head_code.tpl'}     
</head>

<body>

    {include file='header.tpl'}     

    <section class="container">

            <div class="row">

                <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs">
					{include file='sidebar.tpl'}     				
    			</aside>  
            
                 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">

                 	{foreach $prices_page as $item}	                        	                    		
                    <div class="device_header">
                    <h1>{$item.categ_title} {$item.title} {lang}prices{/lang}
                    <span style="float:right; font-weight:100;"><a class="btn btn-info" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}-prices.html">{lang}View all prices{/lang}</a></span>
					</h1>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">        
                    <a title="{$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html"><img class="img-responsive img-blog" src="{$CONFIG_SITE_MEDIA}/large/{$item.image}" alt="{$item.title}" title="{$item.title}" /></a>
                    <div class="clear"></div>
                    <center>
                    <a class="btn btn-default" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html">{lang}View specs{/lang}</a>
                    <div class="clear"></div>
                    </center>
                    </div>
                    
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    
                            <div class="table-responsive">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th width="120">{lang}Seller{/lang}</th>
                                  <th align="center">{lang}Details{/lang}</th>
                                  <th align="center" width="110">{lang}Price{/lang}</th>
                                  <th align="center" width="90">{lang}Shop{/lang}</th>
                                </tr>
                              </thead>
                              <tbody>
                                {foreach $item.prices as $price}
                                <tr>
                                  <th><span class="prices_logo"><img class="img-responsive "src="{$CONFIG_SITE_MEDIA}/thumbs/{$price.seller_logo}" /></span></th>
                                  <td align="center"><span class="prices_details">{$price.details}</span></td>
                                  <td align="center"><span class="prices_price">{$price.price}</span></td>
                                  <td align="center"><a class="btn btn-danger" target="_blank" href="{$price.url}">{lang}SHOP{/lang}</a></td>
                                </tr>
                                {/foreach} 
                              </tbody>
                            </table>	
                            </div>
                    
                    </div>
                    		
                    <div class="clear"></div>
                    {/foreach}
					
                    <div class="clear"></div>

                <aside class="hidden-lg hidden-md hidden-sm col-xs-12">
					{include file='sidebar.tpl'}     				
    			</aside>  
                                                                                        
                </div><!--/.col-md-8-->
                
            </div><!--/.row-->

    </section>

{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 
</body>
</html>