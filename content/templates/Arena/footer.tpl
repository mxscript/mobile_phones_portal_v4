    <footer id="footer_top">
        <div class="container">
            <div class="row">                
                <div class="col-sm-12">
                	<ul class="pull-left">
                    <a href="{$CONFIG_SITE_URL}" title="{$CONFIG_SITE_TITLE}"><img src="{$CONFIG_SITE_MEDIA}/img/{$CONFIG_SITE_LOGO_FOOTER}" alt="{$CONFIG_SITE_TITLE}" class="hidden-xs hidden-sm hidden-md"></a>
                    </ul>
                    
                    <ul class="pull-right">
                        <li><a href="{$CONFIG_SITE_URL}" title="{$CONFIG_SITE_TITLE}">{lang}Home{/lang}</a></li>
                        {foreach $footer_categories as $link}	                        
                        <li><a title="{$link.title}" href="{$CONFIG_SITE_URL}/{$link.permalink}/">{$link.title}</a></li>
                        {/foreach}
                        {foreach $footer_pages as $page}		   	           
                        <li><a title="{$page.title}" href="{$CONFIG_SITE_URL}/{$page.permalink}.html">{$page.title}</a></li>
                        {/foreach}                    	
                        {if $CONFIG_ENABLE_CONTACT_PAGE==1}
                        <li><a href="{$CONFIG_SITE_URL}/contact.html">{lang}Contact{/lang}</a></li>                        
                        {/if}

                    </ul>
                </div>
                <div class="col-sm-12 clear">
                    {$CONFIG_FOOTER_CODE}                    
                </div>
            </div>
        </div>
    </footer><!--/#footer-->


    <footer id="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    {$CONFIG_COPYRIGHT_CODE}
                    {$FOOTER_ANALYTICS_CODE|unescape:'html'}
                    <ul class="pull-right">	
                    <a href="{$CONFIG_SOCIAL_FACEBOOK_URL}" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i> Facebook</a>
                    <a href="{$CONFIG_SOCIAL_TWITTER_URL}" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i> Twitter</a>
                    <a href="{$CONFIG_SOCIAL_GOOGLEPLUS_URL}" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i> Google+</a>
                    <a href="{$CONFIG_SOCIAL_YOUTUBE_URL}" class="icoYoutube" title="Youtube"><i class="fa fa-youtube"></i> Youtube</a>
                    <a href="{$CONFIG_SOCIAL_PINTEREST_URL}" class="icoPinterest" title="Pinterest"><i class="fa fa-pinterest"></i> Pinterest</a>
                    <a href="{$CONFIG_SOCIAL_LINKEDIN_URL}" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i> Linkedin</a>
					</ul>
                </div>
            </div>
        </div>        
    </footer><!--/#footer-->
