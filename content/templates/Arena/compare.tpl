<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{if $NUMBER_OF_ITEMS_TO_COMPARE==0}{lang}Compare products{/lang}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==1}{lang}Compare{/lang} {$CONTENT_TITLE_1}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==2}{$CONTENT_TITLE_1} {lang}vs{/lang} {$CONTENT_TITLE_2}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==3}{$CONTENT_TITLE_1} {lang}vs{/lang} {$CONTENT_TITLE_2} {lang}vs{/lang} {$CONTENT_TITLE_3}{/if}">
    <meta name="keywords" content="{if $NUMBER_OF_ITEMS_TO_COMPARE==0}{lang}Compare{/lang}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==1}{lang}compare{/lang}, {$CONTENT_TITLE_1}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==2}{lang}compare{/lang}, {$CONTENT_TITLE_1}, {$CONTENT_TITLE_2}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==3}{lang}compare{/lang}, {$CONTENT_TITLE_1}, {$CONTENT_TITLE_2}, {$CONTENT_TITLE_3}{/if}">
    <title>{if $NUMBER_OF_ITEMS_TO_COMPARE==0}{lang}Compare products{/lang}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==1}{lang}Compare{/lang} {$CONTENT_TITLE_1}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==2}{$CONTENT_TITLE_1} {lang}vs{/lang} {$CONTENT_TITLE_2}{/if}{if $NUMBER_OF_ITEMS_TO_COMPARE==3}{$CONTENT_TITLE_1} {lang}vs{/lang} {$CONTENT_TITLE_2} {lang}vs{/lang} {$CONTENT_TITLE_3}{/if}</title>    
{include file='global_head_code.tpl'} 
</head>

<body>
	
    {include file='header.tpl'}     


    <section id="about-us">
        <div class="container">
			<h2>{lang}Compare products{/lang}</h2>

			{include file='compare_results.tpl'} 

		</div><!--/.container-->
    </section><!--/about-us-->
	
{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 

</body>
</html>