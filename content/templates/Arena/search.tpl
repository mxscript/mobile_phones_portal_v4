<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$SEARCh} - {lang}Search results{/lang}">
    <meta name="keywords" content="{$SEARCH}">
    <title>{$SEARCH} - {lang}Search results{/lang}</title>
{include file='global_head_code.tpl'}     
</head>

<body>

    {include file='header.tpl'}     

    <section class="container">

            <div class="row">

                <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs">
					{include file='sidebar.tpl'}     				
    			</aside>  
            
                 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                 	<div class="article_header">
		            <h2>{$SEARCH} - {lang}Search results{/lang} ({lang}devices{/lang})</h2>
                    </div>
					
                 	{foreach $search_results_devices as $item}	                        	                    		
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 devices_list">
                            <a title="{$item.categ_title} {$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html"><img class="img-responsive img-blog" src="{$CONFIG_SITE_MEDIA}/thumbs/{$item.image}" alt="{$item.title}" title="{$item.categ_title} {$item.title}" /></a>
							<a title="{$item.categ_title} {$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html">{$item.categ_title} {$item.title}</a>
                    		</div>                                                
                    {/foreach}
					
                    <div class="clear"></div>
					<div class="article_header">
		            <h2>{$SEARCH} - {lang}Search results{/lang} ({lang}articles{/lang})</h2>
                    </div>
                 	{foreach $search_results_articles as $item}	                        	                    		
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 articles_list">
                            <a title="{$item.categ_title} {$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html"><img class="img-responsive img-blog" src="{$CONFIG_SITE_MEDIA}/thumbs/{$item.image}" alt="{$item.title}" title="{$item.categ_title} {$item.title}" /></a>
							<a title="{$item.categ_title} {$item.title}" href="{$CONFIG_SITE_URL}/{$item.categ_permalink}/{$item.permalink}-{$item.id}.html">{$item.categ_title} {$item.title}</a>
                    		</div>                                                
                    {/foreach}
                                                                                        
                </div><!--/.col-md-8-->

                <aside class="hidden-lg hidden-md hidden-sm col-xs-12">
					{include file='sidebar.tpl'}     				
    			</aside> 
                                
            </div><!--/.row-->

    </section>

{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 
</body>
</html>