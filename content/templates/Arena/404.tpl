<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$CONFIG_SITE_DESCRIPTION}">
    <meta name="keywords" content="{$CONFIG_SITE_KEYWORDS}">
    <title>{$CONFIG_SITE_TITLE}</title>    
    {include file='global_head_code.tpl'}   
</head><!--/head-->

<body class="homepage">

	{include file='header.tpl'} 
    
	<section id="feature">        
    	<div class="container">           
        	<div class="center wow fadeInDown">                
            	<h1>Error! Content not found</h1>
                <p class="lead">Content page deleted or invalid content page.</p>
                
            </div>
            
        </div><!--/.container-->    
    </section><!--/#feature-->

	{include file='footer.tpl'} 
    
	{include file='global_footer_code.tpl'} 
</body>
</html>