<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$CONFIG_SITE_DESCRIPTION}">
    <meta name="keywords" content="{$CONFIG_SITE_KEYWORDS}">
    <title>{$CONFIG_SITE_TITLE}</title>    
    {include file='global_head_code.tpl'}   
</head><!--/head-->

<body class="homepage">

	{include file='header.tpl'} 
    

    <section class="container">

            <div class="row">

                <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs">
					{include file='sidebar.tpl'}     				
    			</aside>  
            
                 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                 	
                    <div class="article_header">
                    <h2>{lang}Popular devices{/lang} <span class="hidden-xs more"><i class="fa fa-mobile grey"></i> <a href="{$CONFIG_SITE_URL}/mobiles/">{lang}All mobiles{/lang}</a></span></h2>
                    </div>
                    <!-- ITEM-->							                            
                    {foreach from=$popular_mobiles item=device name=foo}
                            {if $smarty.foreach.foo.index < 12}
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 devices_list">  
                                <div class="device_box_v2">                        
                                    {if $device.image}
                                    <a title="{$device.categ_title} {$device.title}" href="{$CONFIG_SITE_URL}/{$device.categ_permalink}/{$device.permalink}-{$device.id}.html"><img class="img-responsive" alt="{$device.categ_title} {$device.title}" title="{$device.title}" src="{$CONFIG_SITE_MEDIA}/thumbs/{$device.image}" /></a>
                                    {/if}
                                    <a title="{$device.categ_title} {$device.title}" href="{$CONFIG_SITE_URL}/{$device.categ_permalink}/{$device.permalink}-{$device.id}.html">{$device.categ_title} {$device.title}</a>
                                </div>              
                            </div>      
                            {/if}                        
                    {/foreach}
					
                    <div class="clear"></div>

                    <div class="article_header">
                    <h2>{lang}Latest News{/lang} <span class="more hidden-xs"><i class="fa fa-retweet grey"></i> <a href="{$CONFIG_SITE_URL}/news/">{lang}All news{/lang}</a></span></h2>
                    </div>
                    <!-- ITEM-->							                            
                    {foreach from=$latest_news item=article name=foo}
                    {if $smarty.foreach.foo.index < 6}
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">	
                        <div class="article_index"> 
                            {if $article.image}
                            <div class="article_index_image"> 
                            <a title="{$article.title}" href="{$CONFIG_SITE_URL}/{$article.categ_permalink}/{$article.permalink}-{$article.id}.html"><img alt="{$article.title}" title="{$article.title}" src="{$CONFIG_SITE_MEDIA}/small/{$article.image}" class="img-responsive darker" /></a>
                            </div>
                            {/if}            
                            <a title="{$article.title}" href="{$CONFIG_SITE_URL}/{$article.categ_permalink}/{$article.permalink}-{$article.id}.html">{$article.title}</a>
                        </div> 
                        </div>
                    {/if}             
                    {/foreach}                    

					
                    <div class="clear"></div>

                    <div class="article_header">
                    <h2>{lang}Latest Reviews{/lang} <span class="more hidden-xs"><i class="fa fa-retweet grey"></i> <a href="{$CONFIG_SITE_URL}/reviews/">{lang}All reviews{/lang}</a></span></h2>
                    </div>
                    <!-- ITEM-->							                            
                    {foreach from=$latest_reviews item=article name=foo}
                    {if $smarty.foreach.foo.index < 6}
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">	
                        <div class="article_index"> 
                            {if $article.image}
                            <div class="article_index_image"> 
                            <a title="{$article.title}" href="{$CONFIG_SITE_URL}/{$article.categ_permalink}/{$article.permalink}-{$article.id}.html"><img alt="{$article.title}" title="{$article.title}" src="{$CONFIG_SITE_MEDIA}/small/{$article.image}" class="img-responsive darker" /></a>
                            </div>
                            {/if}            
                            <a title="{$article.title}" href="{$CONFIG_SITE_URL}/{$article.categ_permalink}/{$article.permalink}-{$article.id}.html">{$article.title}</a>
                        </div> 
                        </div>
                    {/if}             
                    {/foreach}                    

					<div class="clear"></div>
                                        
                    <div class="article_header">
                    <h2>{lang}Top rated devices{/lang} <span class="more hidden-xs"><i class="fa fa-mobile grey"></i> <a href="{$CONFIG_SITE_URL}/mobiles/">{lang}All mobiles{/lang}</a></span></h2>
                    </div>
                    <!-- ITEM-->							                            
                    {foreach from=$top_rated_mobiles item=device name=foo}
                            {if $smarty.foreach.foo.index < 12}
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 devices_list">  
                                <div class="device_box_v2">                        
                                    {if $device.image}
                                    <a title="{$device.categ_title} {$device.title}" href="{$CONFIG_SITE_URL}/{$device.categ_permalink}/{$device.permalink}-{$device.id}.html"><img class="img-responsive" alt="{$device.categ_title} {$device.title}" title="{$device.title}" src="{$CONFIG_SITE_MEDIA}/thumbs/{$device.image}" /></a>
                                    {/if}
                                    <a title="{$device.categ_title} {$device.title}" href="{$CONFIG_SITE_URL}/{$device.categ_permalink}/{$device.permalink}-{$device.id}.html">{$device.categ_title} {$device.title}</a>
                                </div>              
                            </div>      
                            {/if}                        
                    {/foreach}
					
                    <div class="clear"></div>
                                                                                        
                </div><!--/.col-md-8-->

                <aside class="hidden-lg hidden-md hidden-sm col-xs-12">
					{include file='sidebar.tpl'}     				
    			</aside> 
                                
            </div><!--/.row-->

    </section>

	{include file='footer.tpl'} 
    
	{include file='global_footer_code.tpl'} 
</body>
</html>