<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$META_DESCRIPTION} - {lang}Opinions{/lang}">
    <meta name="keywords" content="{$META_KEYWORDS}, {lang}Opinions{/lang}">
    <title>{$META_TITLE} - {lang}Opinions{/lang}</title>
{include file='global_head_code.tpl'}    

    <meta property="og:title" content="{$META_TITLE}" />
    <meta property="og:image" content="{$CONFIG_SITE_URL}/content/media/large/{$CONTENT_IMAGE}" />
    <meta property="og:image:width" content="160" />
    <meta property="og:image:height" content="212" />        
    <link rel="image_src" href="{$CONFIG_SITE_URL}/content/media/large/{$CONTENT_IMAGE}" />
    <meta property="og:site_name" content="{$CONFIG_SITE_TITLE}" />
    <meta property="og:description" content="{if !$META_DESCRIPTION}{$META_TITLE} - {lang}Best prices, specifications and reviews{/lang}{else} {$META_DESCRIPTION}{/if}" />
    <meta property="fb:app_id" content="{$CONFIG_FB_APP_ID}" />
    <meta property="og:type" content="article" />
 
</head>

<body>

    {include file='header.tpl'}     

    <section class="container">

            <div class="row">

                <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs">
					{include file='sidebar.tpl'}     				
    			</aside>  
            
                 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                 	
                    <div class="device_header">

                    <span style="float:right" class="header_google_button">
                    <a target="_blank" href="https://plusone.google.com/_/+1/confirm?hl=en&url={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-opinions.html"  title="Google plus"><i class="fa fa-google-plus"></i></a>
                    </span> 
		            <span style="float:right" class="header_twitter_button">
                    <a target="_blank" href="https://twitter.com/intent/tweet?text={$CONTENT_TITLE}&url={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-opinions.html" title="Twitter"><i class="fa fa-twitter"></i></a>
                    </span>                    
		            <span style="float:right" class="header_facebook_button">
                    <a target="_blank" href="http://www.facebook.com/sharer.php?u={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-opinions.html" title="Facebook"><i class="fa fa-facebook"></i></a>
                    </span>
                    
		            <h1>{$CONTENT_TITLE} {lang}Opinions{/lang}</h1>   
                    </div>
                    
                    	
	                    <img style="float:left;" class="img-responsive device_image" src="{$CONFIG_SITE_MEDIA}/large/{$CONTENT_IMAGE}" alt="{$CONTENT_TITLE}" title="{$CONTENT_TITLE}" />
                                            
                    	<div class="device_summary">
                            <i class="fa fa-calendar-o"></i> {$SUMMARY_STATUS} 
                            <br>
                            <i class="fa fa-laptop"></i> {$SUMMARY_DISPLAY} 
                            <br>
                            <i class="fa fa-android"></i> {$SUMMARY_OS} 
                            <br>
                            <i class="fa fa-cog"></i> {$SUMMARY_CPU} 
                            <br>
                            <i class="fa fa-database"></i> {$SUMMARY_MEMORY} 
                            <br>
                            <i class="fa fa-camera"></i> {$SUMMARY_CAMERA} 
                            <br>
                            <i class="fa fa-battery-2"></i> {$SUMMARY_BATTERY} 
						</div>

                    <div class="clear"></div>
					
                    {if $DISABLE_RATINGS==0}
                    <span style="float:left;"><input id="star-rating" type="text" class="rating-loading" data-size="xs" value="{$STAR_RATING_VALUE}" ></span>                    <span style="font-size: 20px; margin-left:10px; bottom:0; line-height:30px;">{if $STAR_RATING_NUMB_RATINGS==1}({lang}One vote{/lang}){/if} {if $STAR_RATING_NUMB_RATINGS>1}({$STAR_RATING_NUMB_RATINGS} {lang}votes{/lang}){/if}</span>                                         
                    <div class="rating_click_result" style="font-weight:bold"></div>
                    <div class="clear"></div>
                    {/if}                                        
  					
                    <ul class="device_menu">
                    	<li><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}.html"><i class="fa fa-bars fa-fw"></i> {lang}Specs{/lang}</a></li>
                        {if $EXIST_PRICES==1}
                        <li><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-prices.html"><i class="fa fa-money fa-fw"></i> {lang}Prices{/lang}</a></li>
                        {/if}
                        <li class="active"><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-opinions.html"><i class="fa fa-comments fa-fw"></i> {lang}Opinions{/lang}</a></li>
                        {if $EXIST_MEDIA==1}
                        <li><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-media.html"><i class="fa fa-image fa-fw"></i> {lang}Media{/lang}</a></li>
                        {/if}
                        <li><a href="{$CONFIG_SITE_URL}/compare.html?id_product1={$CONTENT_ID}"><i class="fa fa-check-square-o fa-fw"></i> {lang}Compare{/lang}</a></li>
                    </ul>
                               

					{if $DISABLE_COMMENTS == 1}
                    {lang}Comments disabled for this content{/lang}
                    {/if}

                    <a name="comments"></a>
                    {if $CONFIG_ENABLE_SITE_COMMENTS == 1 AND $DISABLE_COMMENTS == 0}
                    <h3>{lang}Opinions{/lang}: <span style="float:right"><a class="btn btn-primary btn-sm" href="#post_comment">{lang}Write a Comment{/lang}</a></span></h3>        
                    
                        {foreach from=$post_comments item=comment}
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding_l comment">  
                        
                            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-2 nopadding_l"> 
                            {if $comment.comment_user_id==0}
                            <img class="img-responsive" title="{$comment.comment_name}" src="{$CONFIG_SITE_MEDIA}/avatars/no_avatar.png" />
                            {else}
                            <img class="img-responsive" title="{$comment.comment_name}" src="{$CONFIG_SITE_MEDIA}/avatars/{$comment.comment_avatar}" />
                            {/if}    
                            </div>
                              
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-10 nopadding_l">          
                            <span class="comment_name">{$comment.comment_name}</span><span class="comment_date">{$comment.comment_datetime}</span>
                            <div class="comment_content">{$comment.comment}</div>
                            </div>      	
                                    
                        </div>
                        {/foreach}
                    {/if}    
                    
                    <a name="post_comment"></a>
                    
                    {if $CONFIG_ENABLE_FB_COMMENTS == 1 AND $DISABLE_COMMENTS == 0}
                    <div class="fb-comments" data-href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-opinions.html" data-numposts="10" data-colorscheme="light" data-order-by="reverse_time" data-width="100%"></div>
                    {/if}      
                    
                    {if $MSG == 'error_name'}
                        <div class="clear"></div><p class='bg-danger'>{lang}Error{/lang}! {lang}Please input your name{/lang}</p>
                    {/if}      
                    
                    {if $MSG == 'error_comment'}
                        <div class="clear"></div><p class='bg-danger'>{lang}Error{/lang}! {lang}Please input comment{/lang}</p>
                    {/if}      
                    
                    {if $MSG == 'error_demo_mode'}
                        <div class="clear"></div><p class='bg-danger'>{lang}Error! This action is disabled in demo mode{/lang}</p>
                    {/if}      
                    
                    {if $MSG == 'error_captcha'}
                        <div class="clear"></div><p class='bg-danger'>{lang}Error{/lang}! {lang}Please input captcha code{/lang}</p>
                    {/if}  
                    
                    {if $MSG == 'comment_ok'}
                        <div class="clear"></div><p class='bg-info'>{lang}Comment added{/lang}</p>
                    {/if}  
                    
                    
                    {if $CONFIG_ENABLE_SITE_COMMENTS == 1 AND $DISABLE_COMMENTS == 0}
                    
                    
                    <div id="comments_form">    
                        <h3>{lang}Write a Comment{/lang}</h3>        
                        <form action="{$CONFIG_SITE_URL}/core/CommentSubmit.php" method="post" novalidate>
                                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding control-group">        
                            <div class="form-group controls">
                            <label>{lang}Your name{/lang} <small>({lang}required{/lang})</small></label>
                            <input type="text" class="form-control" name="comment_name" required data-validation-required-message="{lang}Input your name{/lang}" />
                            </div>
                        </div>
                                                            
						<div class="clear"></div>
                                            
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding control-group">                
                            <div class="form-group controls">
                            <label>{lang}Comment{/lang} <small>({lang}required{/lang})</small></label>
                            <textarea class="form-control" rows="4" name="comment_content" required data-validation-required-message="{lang}Input your comment{/lang}"></textarea>
                            </div>
                        </div>
                        
                        {if $CONFIG_ENABLE_CAPTCHA_COMMENTS == 1}
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding control-group">
                            <div class="form-group controls">
                            <label>{lang}Antispam code{/lang}</label>
                            <input autocomplete="off" name="captcha_box" type="text" class="form-control" required data-validation-required-message="{lang}Input antispam code{/lang}"/>        
                            <img src="{$CONFIG_SITE_URL}/core/plugins/captchaimage.php?{$CAPTCHA_RANDOM_CODE}" alt="antispam" class="img_antispam" />        
                            </div>
                        </div>
                        
                        {/if}
                                    
                        <input type="hidden" name="content_id" value="{$CONTENT_ID}" />
                        <input type="hidden" name="content_permalink" value="{$CONTENT_PERMALINK}" />                        
                        <input type="hidden" name="categ_permalink" value="{$CATEG_PERMALINK}" />                                                
                        <input type="hidden" name="redirect" value="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-opinions.html" />
                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                        <button type="submit" name="action_publish" class="btn btn-success">{lang}Post comment{/lang}</button>
                        </div>
                        </form>
                    </div>
                    {/if}
                            
                    <div class="clear"></div>
                                                                                        
                </div><!--/.col-md-8-->

                <aside class="hidden-lg hidden-md hidden-sm col-xs-12">
					{include file='sidebar.tpl'}     				
    			</aside>  
                
            </div><!--/.row-->

    </section>

{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 
</body>
</html>