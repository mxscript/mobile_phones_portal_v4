    <script src="{$CONFIG_TEMPLATE_URL}/assets/js/jquery.js"></script>
    <script src="{$CONFIG_TEMPLATE_URL}/assets/js/bootstrap.min.js"></script>
    <script src="{$CONFIG_TEMPLATE_URL}/assets/js/jquery.isotope.min.js"></script>
    <script src="{$CONFIG_TEMPLATE_URL}/assets/js/main.js"></script>
    <script src="{$CONFIG_TEMPLATE_URL}/assets/js/wow.min.js"></script>
    <script src="{$CONFIG_SITE_URL}/core/assets/lightbox/lightbox.min.js"></script>
    <script src="{$CONFIG_SITE_URL}/core/assets/js/star-rating.min.js"></script>

	{literal}
	<script type="text/javascript">
	$("#star-rating").rating({
			size: 'xs',
			step: 1,
			starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'},
			filledStar: '<i class="fa fa-star"></i>',
	        emptyStar: '<i class="fa fa-star-o"></i>',
			starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'},			
		}).on("rating.change", function(event, value, caption) {
			
				$.ajax({
					type: 'POST',
					data: {content_id: '{/literal}{$CONTENT_ID}{literal}', rating: + value},
					url: '{/literal}{$CONFIG_SITE_URL}{literal}/core/StarRating.php',
					success: function(data) {
						//alert(data);
						$(".rating_click_result").text(data);
	
					}
			
		})		
        //alert("You rated: " + value + " (" + $(caption).text() + ")");
    });	

	</script>
    
	<script type="text/javascript">
			$(document).ready(function ($) {
				$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
					event.preventDefault();
					return $(this).ekkoLightbox({
						always_show_close: true
					});
				});

			});
	</script>
    
    <script type="text/javascript">


		$('body').click(function(e) {
			if (!$(e.target).closest('#suggestions').length){
				$("#suggestions").fadeOut();
			}
		});

		google.load("jquery", "1.3.1");
		google.setOnLoadCallback(function()
		{
			// Safely inject CSS3 and give the search results a shadow
			var cssObj = { 'box-shadow' : '#888 5px 10px 10px', // Added when CSS3 is standard
				'-webkit-box-shadow' : '#888 5px 10px 10px', // Safari
				'-moz-box-shadow' : '#888 5px 10px 10px'}; // Firefox 3.5+
			$("#suggestions").css(cssObj);
			
			// Fade out the suggestions box when not active
			 $("input").blur(function(){
				$('#suggestions').fadeOut();
			 });
			 
		});
		
		function lookup(inputString) {
			if(inputString.length == 0) {
				$('#suggestions').fadeOut(); // Hide the suggestions box
			} else {
				$.post("{/literal}{$CONFIG_SITE_URL}{literal}/search_autocomplete.php", {queryString: ""+inputString+""}, function(data) { // Do an AJAX call
					$('#suggestions').fadeIn(); // Show the suggestions box
					$('#suggestions').html(data); // Fill the suggestions box
				});
			}
		}   

	</script>
	{/literal}    