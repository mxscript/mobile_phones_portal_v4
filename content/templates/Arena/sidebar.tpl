<div class="table_brands light l-box clearfix">
<h3>{lang}Phones Brands{/lang}</h3>
<ul class="list-unstyled three_cols">
	{foreach from=$important_brands item=brand}
    <li><a title="{$brand.title}" href="{$CONFIG_SITE_URL}/{$brand.permalink}/">{$brand.title}</a></li>
	{/foreach}
</ul>
<h4><a title="{lang}Phones Brands{/lang}" href="{$CONFIG_SITE_URL}/mobile-phone-brands.html"><span class="fa-big"><i class="fa fa-mobile"></i></span> {lang}See All Brands{/lang}</a></h4>
</div>     

{if $DISABLE_ADS!=1}
<div align="center" style="margin-top:20px; margin-bottom:20px;">
	{$ads[2]}
</div>
{/if}

<div class="latest_articles_box">
<h3>{lang}Latest News{/lang}</h3>
<ul class="list-unstyled">
	{foreach from=$latest_news item=article}
    <li><i class="fa fa-retweet grey"></i> <a title="{$article.title}" href="{$CONFIG_SITE_URL}/{$article.categ_permalink}/{$article.permalink}-{$article.id}.html">{$article.title}</a></li>
	{/foreach}
</ul>
</div>           

<div class="latest_articles_box">
<h3>{lang}Latest reviews{/lang}</h3>
</div>
{foreach from=$latest_reviews item=article name=foo}
	{if $smarty.foreach.foo.index < 12}
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    	<div class="sidebar_article_image">
		<a title="{$article.title}" href="{$CONFIG_SITE_URL}/{$article.categ_permalink}/{$article.permalink}-{$article.id}.html"><img class="img-responsive" src="{$CONFIG_SITE_MEDIA}/small/{$article.image}" /></a>
      	<a title="{$article.title}" href="{$CONFIG_SITE_URL}/{$article.categ_permalink}/{$article.permalink}-{$article.id}.html"><p>{$article.title}</p></a>
		</div>                    
	</div>
    {/if}
{/foreach}


<div class="clear"></div>

{if $DISABLE_ADS!=1}
<div align="center" style="margin-top:20px; margin-bottom:20px;">
{$ads[2]}
</div>
{/if}

<div class="latest_articles_box custom_list">
<h3>{lang}Latest Compares{/lang}</h3>
<ul class="list-unstyled custom_list">
        {foreach from=$latest_compares item=product name=foo}
            {if $smarty.foreach.foo.index < 10} <!--  show 10 compares -->           
            <li> <a title="Compare {$product.product1_title} {lang}vs{/lang} {$product.product2_title}" href="{$CONFIG_SITE_URL}/compare.html?id_product1={$product.product1_id}&id_product2={$product.product2_id}">{$product.product1_title} {lang}vs{/lang}  {$product.product2_title}</a></li>
            {/if}
        {/foreach}
</ul>        
</div>

<div class="clear"></div>

<div class="latest_articles_box">
<h3>{lang}Top rated mobiles{/lang}</h3>
{foreach from=$top_rated_mobiles item=product name=foo}        
    	{if $smarty.foreach.foo.index < 6}
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 devices_list_sidebar">
        <a title="{$product.title}" href="{$CONFIG_SITE_URL}/{$product.categ_permalink}/{$product.permalink}-{$product.id}.html"><img class="img-responsive" src="{$CONFIG_SITE_MEDIA}/thumbs/{$product.image}" /></a>
		<a title="{$product.categ_title} {$product.title}" href="{$CONFIG_SITE_URL}/{$product.categ_permalink}/{$product.permalink}-{$product.id}.html">{$product.categ_title} {$product.title}</a>
        </div>
        {/if}
{/foreach}
</div>

<div class="clear"></div>
