    <meta name="author" content="{$CONFIG_META_AUTHOR}">
    <link rel="shortcut icon" href="{$CONFIG_SITE_URL}/content/media/img/favicon.gif">
	<!-- core CSS -->
    <link href="{$CONFIG_TEMPLATE_URL}/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$CONFIG_TEMPLATE_URL}/assets/css/animate.min.css" rel="stylesheet">
    <link href="{$CONFIG_TEMPLATE_URL}/assets/css/main.css" rel="stylesheet">
    <link href="{$CONFIG_TEMPLATE_URL}/assets/css/social.css" rel="stylesheet">    
    <link href="{$CONFIG_TEMPLATE_URL}/assets/css/search.css" rel="stylesheet">        
    <link href="{$CONFIG_TEMPLATE_URL}/assets/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css" rel="stylesheet">	
    <link href="{$CONFIG_SITE_URL}/core/assets/lightbox/lightbox.css" rel="stylesheet">
    <link href="{$CONFIG_SITE_URL}/core/assets/css/star-rating.css" rel="stylesheet">    
    
	<div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&appId={$CONFIG_FB_APP_ID}&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>    