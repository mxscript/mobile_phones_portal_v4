<!DOCTYPE html>
<html lang="{$CONFIG_META_LANG}" dir="{$CONFIG_META_DIR}">
<head>
    <meta charset="{$CONFIG_META_CHARSET}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$META_DESCRIPTION} images and videos">
    <meta name="keywords" content="{$META_KEYWORDS}, prices">
    <title>{$META_TITLE} - {lang}images and videos{/lang}</title>
{include file='global_head_code.tpl'}     

    <meta property="og:title" content="{$META_TITLE}" />
    <meta property="og:image" content="{$CONFIG_SITE_URL}/content/media/large/{$CONTENT_IMAGE}" />
    <meta property="og:image:width" content="160" />
    <meta property="og:image:height" content="212" />        
    <link rel="image_src" href="{$CONFIG_SITE_URL}/content/media/large/{$CONTENT_IMAGE}" />
    <meta property="og:site_name" content="{$CONFIG_SITE_TITLE}" />
    <meta property="og:description" content="{if !$META_DESCRIPTION}{$META_TITLE} - {lang}Best prices, specifications and reviews{/lang}{else} {$META_DESCRIPTION}{/if}" />
    <meta property="fb:app_id" content="{$CONFIG_FB_APP_ID}" />
    <meta property="og:type" content="article" />

</head>

<body>

    {include file='header.tpl'}     

    <section class="container">

            <div class="row">

                <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 hidden-xs">
					{include file='sidebar.tpl'}     				
    			</aside>  
            
                 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                 	
                    <div class="device_header">

                    <span style="float:right" class="header_google_button">
                    <a target="_blank" href="https://plusone.google.com/_/+1/confirm?hl=en&url={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-media.html"  title="Google plus"><i class="fa fa-google-plus"></i></a>
                    </span> 
		            <span style="float:right" class="header_twitter_button">
                    <a target="_blank" href="https://twitter.com/intent/tweet?text={$CONTENT_TITLE}&url={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-media.html" title="Twitter"><i class="fa fa-twitter"></i></a>
                    </span>                    
		            <span style="float:right" class="header_facebook_button">
                    <a target="_blank" href="http://www.facebook.com/sharer.php?u={$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-media.html" title="Facebook"><i class="fa fa-facebook"></i></a>
                    </span>
                    
		            <h1>{$CONTENT_TITLE} {lang}images and videos{/lang}</h1>   
                    </div>
                    
                    	
	                    <img style="float:left;" class="img-responsive device_image" src="{$CONFIG_SITE_MEDIA}/large/{$CONTENT_IMAGE}" alt="{$CONTENT_TITLE}" title="{$CONTENT_TITLE}" />
                                            
                    	<div class="device_summary">
                            <i class="fa fa-calendar-o"></i> {$SUMMARY_STATUS} 
                            <br>
                            <i class="fa fa-laptop"></i> {$SUMMARY_DISPLAY} 
                            <br>
                            <i class="fa fa-android"></i> {$SUMMARY_OS} 
                            <br>
                            <i class="fa fa-cog"></i> {$SUMMARY_CPU} 
                            <br>
                            <i class="fa fa-database"></i> {$SUMMARY_MEMORY} 
                            <br>
                            <i class="fa fa-camera"></i> {$SUMMARY_CAMERA} 
                            <br>
                            <i class="fa fa-battery-2"></i> {$SUMMARY_BATTERY} 
						</div>

                    <div class="clear"></div>
					
                    {if $DISABLE_RATINGS==0}
                    <span style="float:left;"><input id="star-rating" type="text" class="rating-loading" data-size="xs" value="{$STAR_RATING_VALUE}" ></span>                    <span style="font-size: 20px; margin-left:10px; bottom:0; line-height:30px;">{if $STAR_RATING_NUMB_RATINGS==1}({lang}One vote{/lang}){/if} {if $STAR_RATING_NUMB_RATINGS>1}({$STAR_RATING_NUMB_RATINGS} {lang}votes{/lang}){/if}</span>                                         
                    <div class="rating_click_result" style="font-weight:bold"></div>
                    <div class="clear"></div>
                    {/if}                                        
                      					
                    <ul class="device_menu">
                    	<li><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}.html"><i class="fa fa-bars fa-fw"></i> {lang}Specs{/lang}</a></li>
                        {if $EXIST_PRICES==1}
                        <li><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-prices.html"><i class="fa fa-money fa-fw"></i> {lang}Prices{/lang}</a></li>
                        {/if}
                        <li><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-opinions.html"><i class="fa fa-comments fa-fw"></i> {lang}Opinions{/lang}</a></li>
                        {if $EXIST_MEDIA==1}
                        <li class="active"><a href="{$CONFIG_SITE_URL}/{$CATEG_PERMALINK}/{$CONTENT_PERMALINK}-{$CONTENT_ID}-media.html"><i class="fa fa-image fa-fw"></i> {lang}Media{/lang}</a></li>
                        {/if}
                        <li><a href="{$CONFIG_SITE_URL}/compare.html?id_product1={$CONTENT_ID}"><i class="fa fa-check-square-o fa-fw"></i> {lang}Compare{/lang}</a></li>
                    </ul>

                               
                    {foreach $content_media_images as $image}
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <a href="{$CONFIG_SITE_MEDIA}/large/{$image.file}" data-toggle="lightbox" data-gallery="multiimages" data-title="{$image.title}" data-footer="{$image.description}"><img class="device_image img-responsive" src="{$CONFIG_SITE_MEDIA}/small/{$image.file}" alt="{$image.title}"></a>
                    </div>
					{/foreach}
		        
                    <div class="clear"></div>

					<div class="col-md-12">
                    {foreach $content_media_videos as $video}                    
	                    {$video.embed_code|unescape}
                        <div class="clear"></div>
					{/foreach}
					</div>
                    
                    <div class="clear"></div>
                                                                                        
                </div><!--/.col-md-8-->

                <aside class="hidden-lg hidden-md hidden-sm col-xs-12">
					{include file='sidebar.tpl'}     				
    			</aside>  
                                
            </div><!--/.row-->

    </section>

{include file='footer.tpl'} 
{include file='global_footer_code.tpl'} 
</body>
</html>