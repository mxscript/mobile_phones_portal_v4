<?php
session_start();
if(isset($_SESSION['user_token']) or isset($_COOKIE['user_token']) )
	{
	// user is logged
	header("location: ../login/account.php");
	exit;
	}

require ("../config.php");
require ("dbConnect.php");
require ("functions.php");
require ("getGlobalSettings.php");


$return_page = "../login/register.php";

if($config_registration_disabled==1)
	{
	header("Location: $return_page?msg=error_registration_disabled");
	exit();
	}

$name = $_POST['name'];
$name = Secure($name);
$permalink = RewriteUrl($name);

$email = $_POST['email'];
$email = Secure($email);

$passw = $_POST['passw'];
$passw = stripslashes($passw);

$passw2 = $_POST['passw2'];
$passw2 = stripslashes($passw2);

if (strlen($name)==0)
	{
	header("Location: $return_page?msg=error_name&name=$name&email=$email");
	exit();
	}

if (!ValidEmail($email))
	{
	header("Location: $return_page?msg=error_email&name=$name&email=$email");
	exit();
	}

if (strlen($passw)<5)
	{
	header("Location: $return_page?msg=error_short_passw&name=$name&email=$email");
	exit();
	}

if ($passw!=$passw2)
	{
	header("Location: $return_page?msg=error_passw&name=$name&email=$email");
	exit();
	}	

if($config_captcha_registration==1)
	{
	$captcha_box = trim($_POST['captcha_box']);
	$captcha_box = Secure($captcha_box);

	// captcha verification
	if($_COOKIE['mx_captcha'] != md5($captcha_box))
		{
			header("Location: $return_page?msg=captcha_error&name=$name&email=$email");
			exit();
		}
	}


if (checkDuplicateEmail($email)==1)
	{
	header("Location: $return_page?msg=error_duplicate_email&name=$name&email=$email");
	exit();
	}


$now = date("Y-m-d H:i:s");
$ip_reg = $_SERVER['REMOTE_ADDR'];
$salt = generateSaltPassword (); //salt
$password_db = generateHashPassword ($password, $salt);
$activation_code = md5(random_code());	
$active = 0; // need email activation
$role_id = getUserRoleId (); // user role ID

$query = "INSERT INTO ".$database_table_prefix."users (id, email, name, permalink, password, salt, role_id, active, email_verified, activation_code, register_time, register_ip) VALUES (NULL, '$email', '$name', '$permalink', '$password_db', '$salt', '$role_id', '$active', '0', '$activation_code', '$now', '$ip_reg')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}


// *****************************************************************************************************************************
// SEND MAIL
// *****************************************************************************************************************************
if($config_email_send_option=="smtp")
	{
		// SMTP MAILER	
		//----------------------------------------------------------------------------------------------------------		
		require ("../includes/smtp/class.phpmailer.php");
		$mail = new PHPMailer;
		
		$mail->IsSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $config_email_smtp_server;                 // Specify main and backup server
		$mail->Port = $config_email_smtp_port;                                    // Set the SMTP port
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $config_email_smtp_user;                // SMTP username
		$mail->Password = $config_email_smtp_password;                  // SMTP password
		$mail->SMTPSecure = $config_email_smtp_encryption;                            // Enable encryption, 'ssl' also accepted
		
		$mail->From = $config_site_email;
		$mail->FromName = $config_site_title;
		$mail->AddAddress($email);  
		
		$mail->IsHTML(true);                                  // Set email format to HTML
		
		$mail->Subject = 'Your registration and activation on '.$config_site_url;
		$mail->Body    = '
		<html>
		<head>
		  <title>New user registration</title>
		</head>
		<body>
		  <div style="font-size:12px;font-family:arial;">
		  <p>Hello!</p>
		  You have registered on '.$config_site_url.'<br><br>
		  <strong>Your Email is: '.$email.'<br>
		  Your password is: '.$passw.'</strong><br><br>
		  <strong>YOU MUST ACTIVATE ACCOUNT. </strong><br />
		  Click here to activate your account: <a href="'.$config_site_url.'/login/activate_user.php?activation_code='.$activation_code.'">ACTIVATE ACCOUNT</a><br><br>
		  Thank you!<br><br>
		  </div>
		</body>
		</html>
		';
		$mail->AltBody = 'Your Email is: '.$email.'\n  Your password is: '.$passw.'\n\n YOU MUST ACTIVATE ACCOUNT HERE: '.$config_site_url.'/login/activate_user.php?activation_code='.$activation_code.'';
		
		if(!$mail->Send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   exit;
		}
		

	}

else
	{
		// PHP MAILER	
		//----------------------------------------------------------------------------------------------------------
		$to      = '$email';
		$subject = 'Your registration and activation on '.$config_site_url;
		$message = '
		<html>
		<head>
		  <title>New user registration</title>
		</head>
		<body>
		  <div style="font-size:12px;font-family:arial;">
		  <p>Hello!</p>
		  You have registered on '.$config_site_url.'<br><br>
		  <strong>Your Email is: '.$email.'<br>
		  Your password is: '.$passw.'</strong><br><br>
		  <strong>YOU MUST ACTIVATE ACCOUNT. </strong><br />
		  Click here to activate your account: <a href="'.$config_site_url.'/login/activate_user.php?activation_code='.$activation_code.'">ACTIVATE ACCOUNT</a><br><br>
		  Thank you!<br><br>
		  </div>
		</body>
		</html>
		';

		// HTML mail
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		$headers .= 'From: '.$config_site_email."\r\n" .
			'Reply-To: '.$config_site_email."\r\n" .
			'X-Mailer: PHP/' . phpversion();
		mail($email, $subject, $message, $headers);
		
		//-------------------------------------------------------------------------------------------------------------
	}

// delete cookie if captcha is active
setcookie("mx_captcha", "", time()-60*60*2, "/"); // 2 hours ago

header("Location: index.php?msg=register_ok");
exit;
?>