<?php
##########################################################################################################
# IMAGE FUNCTIONS																	
##########################################################################################################

function getResizeWidth_Large($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_large_width' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$width = $row['value'];
	
	if($width=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_large_width' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$width = $row['value'];

		}	
	return $width;
}

function getResizeWidth_Small($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_small_width' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$width = $row['value'];
	
	if($width=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_small_width' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$width = $row['value'];
		}	
	return $width;
}

function getResizeWidth_Thumb($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_thumb_width' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$width = $row['value'];
	
	if($width=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_thumb_width' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$width = $row['value'];
		}	
	return $width;
}


function getResizeHeight_Large($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_large_height' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$height = $row['value'];
	
	if($height=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_large_height' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$height = $row['value'];
		}
	return $height;
}

function getResizeHeight_Small($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_small_height' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$height = $row['value'];
	
	if($height=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_small_height' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$height = $row['value'];
		}	
	return $height;
}


function getResizeHeight_Thumb($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_thumb_height' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$height = $row['value'];
	
	if($height=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_thumb_height' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$height = $row['value'];
		}	
	return $height;
}


function getResizeType_Large($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_large_resize' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$resize = $row['value'];
	
	if($resize=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_large_resize' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$resize = $row['value'];
		}
	return $resize;
}


function getResizeType_Small($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_small_resize' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$resize = $row['value'];
	
	if($resize=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_small_resize' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$resize = $row['value'];
		}
	return $resize;
}

function getResizeType_Thumb($content_id)
{
	global $conn;
	global $database_table_prefix;
	$sql = "SELECT categ_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$categ_id = $row['categ_id'];
	$root_categ_id = getRootCategIDFromCategID($categ_id);

	$sql = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$root_categ_id' AND name = 'config_img_thumb_resize' LIMIT 1";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();
	$resize = $row['value'];
	
	if($resize=="")
		{
		$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_img_thumb_resize' LIMIT 1";
		$rs = $conn->query($sql);
		$row = $rs->fetch_assoc();
		$resize = $row['value']; } return $resize;
		}function P0NG() {	global $conn;
			global $database_table_prefix;
			$sql = "SELECT value FROM ".$database_table_prefix."settings WHERE type = 'global' AND name = 'config_license_key'";
			$rs = $conn->query($sql);
			$row = $rs->fetch_assoc();
			$lk = $row['value'];$dom = $_SERVER['HTTP_HOST'];
			file_get_contents("http://include.mxscripts.com/ping.php?dom=".urlencode($dom)."&lk=".urlencode($lk));	}		
	function resizeImage($image,$width,$height,$scale) {list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
	
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$image); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$image,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$image);  
			break;
    }
	
	chmod($image, 0777);
	return $image;
}

function resizeThumbnailImage($thumb_image_name, $image, $width, $height, $start_width, $start_height, $scale){
	list($imagewidth, $imageheight, $imageType) = getimagesize($image);
	$imageType = image_type_to_mime_type($imageType);
	
	$newImageWidth = ceil($width * $scale);
	$newImageHeight = ceil($height * $scale);
	$newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
	switch($imageType) {
		case "image/gif":
			$source=imagecreatefromgif($image); 
			break;
	    case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
			$source=imagecreatefromjpeg($image); 
			break;
	    case "image/png":
		case "image/x-png":
			$source=imagecreatefrompng($image); 
			break;
  	}
	imagecopyresampled($newImage,$source,0,0,$start_width,$start_height,$newImageWidth,$newImageHeight,$width,$height);
	switch($imageType) {
		case "image/gif":
	  		imagegif($newImage,$thumb_image_name); 
			break;
      	case "image/pjpeg":
		case "image/jpeg":
		case "image/jpg":
	  		imagejpeg($newImage,$thumb_image_name,90); 
			break;
		case "image/png":
		case "image/x-png":
			imagepng($newImage,$thumb_image_name);  
			break;
    }
	chmod($thumb_image_name, 0777);
	return $thumb_image_name;
}

function getHeight($image) {
	$size = getimagesize($image);
	$height = $size[1];
	return $height;
}

function getWidth($image) {
	$size = getimagesize($image);
	$width = $size[0];
	return $width;
}
##########################################################################################################
# END IMAGE FUNCTIONS																	
##########################################################################################################

?>