<?php
##########################################################################################################
# EMAIL FUNCTIONS																	
##########################################################################################################


function SendEmail_NewMessage ($unique_code)
{
	global $conn;
	global $database_table_prefix;
	require ("includes/getGlobalSettings.php");

	$sql_db = "SELECT to_user_id, time FROM ".$database_table_prefix."messages WHERE unique_code = '$unique_code' LIMIT 1";
	$rs_db = $conn->query($sql_db);
	$row = $rs_db->fetch_assoc();
	$to_user_id = $row['to_user_id'];
	$time = $row['time'];

	$sql_db = "SELECT email, first_name, last_name FROM ".$database_table_prefix."users WHERE id = '$to_user_id' LIMIT 1";
	$rs_db = $conn->query($sql_db);
	$row = $rs_db->fetch_assoc();
	$mailer_email = $row['email'];
	$mailer_first_name = stripslashes($row['first_name']);
	$mailer_last_name = stripslashes($row['last_name']);
	$mailer_full_name = $mailer_last_name." ".$mailer_first_name;
	
		
	if($config_email_send_option=="smtp")
	{
		// SMTP MAILER	
		//----------------------------------------------------------------------------------------------------------		
		require_once ("includes/smtp/class.phpmailer.php");
		$mail = new PHPMailer;
		
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $config_email_smtp_server;                 // Specify main and backup server
		$mail->Port = $config_email_smtp_port;                                    // Set the SMTP port
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $config_email_smtp_user;                // SMTP username
		$mail->Password = $config_email_smtp_password;                  // SMTP password
		$mail->SMTPSecure = $config_email_smtp_encryption;                            // Enable encryption, 'ssl' also accepted
		
		$mail->From = $config_site_email;
		$mail->FromName = $config_school_name;
		$mail->AddAddress($mailer_email);  
		
		$mail->IsHTML(true);                                  // Set email format to HTML
		
		$mail->Subject = $lang["email_new_message_from"].' '.$config_school_name;
		$mail->Body    = '
		<html>
		<head>
		  <title>'.$lang["email_new_message_from"].' '.$config_school_name.'</title>
		</head>
		<body>
		  <div style="font-size:12px;font-family:arial;">
		  <p>'.$lang["email_hello"].', '.$mailer_full_name.'</p>
		  '.$lang["email_new_message_from"].' '.$config_school_name.'<br><br>		  		  
		  '.$lang["email_login_to_read_message"].'.<br>
		  '.$lang["email_login_here_view_messages"].': <a href="'.$config_site_url.'">'.$lang["Login"].'</a><br><br>
		  '.$lang["email_thank_you"].'!<br><br>
		  </div>
		</body>
		</html>
		';
		$mail->AltBody = $lang["email_new_message_from"].' '.$config_school_name;
		
		if(!$mail->Send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   exit;
		}
		
	}
	else
	{
		// PHP MAILER	
		//----------------------------------------------------------------------------------------------------------		
		$to      = '$email';
		$subject = $lang["email_new_message_from"].' '.$config_school_name;
		//$message = '';
		$message = '
		<html>
		<head>
		  <title>'.$lang["email_new_message_from"].' '.$config_school_name.'</title>
		</head>
		<body>
		  <div style="font-size:12px;font-family:arial;">
		  <p>'.$lang["email_hello"].', '.$mailer_full_name.'</p>
		  '.$lang["email_new_message_from"].' '.$config_school_name.'<br><br>		  		  
		  '.$lang["email_login_to_read_message"].'.<br>
		  '.$lang["email_login_here_view_messages"].': <a href="'.$config_site_url.'">'.$lang["Login"].'</a><br><br>
		  '.$lang["email_thank_you"].'!<br><br>
		  </div>
		</body>
		</html>
		';
		
		// HTML mail
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		
		$headers .= 'From: '.$config_site_email."\r\n" .
			'Reply-To: '.$config_site_email."\r\n" .
			'X-Mailer: PHP/' . phpversion();
		mail($mailer_email, $subject, $message, $headers);
		
		//-------------------------------------------------------------------------------------------------------------
	}	
}
?>