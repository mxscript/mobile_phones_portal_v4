<?php 
include '../init.php';

$content_id = $_POST["content_id"];
$content_id = (int)Secure($content_id);

$rating = $_POST["rating"];
$rating = (int)Secure($rating);


if($rating!=0)
{
	// check cookie (if already rated)
	if(!isset($_COOKIE['mx_star_rating_'.$content_id]))
	{
	$now = date("Y-m-d H:i:s");
	$query = "INSERT INTO ".$database_table_prefix."content_extra (id, content_id, name, value, extra) VALUES (NULL, '$content_id', 'star_rating', '$rating', '$now')"; 
	if($conn->query($query) === false) {} 
	else
		{ 
	  	$last_inserted_id = $conn->insert_id;
		$affected_rows = $conn->affected_rows;
		
		updateContentRating($content_id);
		setcookie("mx_star_rating_".$content_id, $rating, time()+60*60*24*30, "/"); // 30 days
		
		echo 'Thank you for rating! Your rating: '.$rating.' stars';					
		}
	}
	else
		echo 'You already rated this content!';		
}

