<?php
date_default_timezone_set('Europe/London');

// Include all functions 
foreach (glob("includes/functions/*.php") as $filename)
{
    include $filename;
}

?>