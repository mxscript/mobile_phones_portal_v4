<?php
if (!function_exists('mysqli_connect'))
{
	exit('<strong>FATAL ERROR! This software requires mysqli extension.</strong> Please contact your hosting!<br><br>Note: The mysqli extension, or as it is sometimes known, the MySQL improved extension, was developed to take advantage of new features found in MySQL systems versions 4.1.3 and newer. <br>The mysqli extension is included with PHP versions 5 and later. Read more about <a href="http://php.net/manual/en/book.mysqli.php" target="_blank">mysqli</a>');
}

// check for PHP version
if (version_compare(phpversion(), '5.3', '<')) {
	// php version isn't high enough
	$php_version = phpversion();		
	exit('Error! Your version of PHP ('.$php_version.') is very old. You need at least PHP 5.3.1 to be installed on your server!');
}

else {
	// Connect to server and select databse.
	$conn = @new mysqli($database_host, $database_user, $database_password, $database_name);
	
	// check connection
	if ($conn->connect_error)
		{
		$db_connect_ok = false; // connect error
		exit ('Database connect error. Please check database configuration file');
		}
	else	
		{
		$db_connect_ok = true; // connect OK
		global $conn;
		global $database_table_prefix;
		}
}	
?>