<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once '../init.php';

$search = $_POST["search"];
$search = Secure($search);

$results = $_POST["results"];
$results = Secure($results);

$now = date("Y-m-d H:i:s");
$ip = $_SERVER['REMOTE_ADDR'];
	
$query = "INSERT INTO ".$database_table_prefix."search_log (id, search, date, ip, results) VALUES (NULL, '$search', '$now', '$ip', '$results')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

