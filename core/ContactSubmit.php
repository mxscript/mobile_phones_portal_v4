<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once '../init.php';

$email = isset($_POST['email']) ? $_POST['email'] : '';
$email = Secure($email);

$name = isset($_POST['name']) ? $_POST['name'] : '';
$name = Secure($name);

$message = isset($_POST['message']) ? $_POST['message'] : '';
$message = Secure($message);
$message2 = urlencode($message);

$captcha_box = isset($_POST['captcha_box']) ? $_POST['captcha_box'] : '';
$captcha_box = Secure($captcha_box);

if ($name=="")
	{
	header("Location: ../contact.php?msg=error_name&name=$name&message=$message2#form");
	exit();
	}

if ($email=="")
	{
	header("Location: ../contact.php?msg=error_email&name=$name&message=$message2#form");
	exit();
	}

// captcha verification
if($_COOKIE['mx_captcha'] != md5($captcha_box))
	{
		header("Location: ../contact.php?msg=error_captcha&email=$email&name=$name&message=$message2#form");
		exit();
	}


$now = date("Y-m-d H:i:s");
$unique_code = random_code();	
$ip = $_SERVER['REMOTE_ADDR'];
$time_read = '0000-00-00 00:00:00';	

$query = "INSERT INTO ".$database_table_prefix."contact_messages (id, unique_code, name, email, message, time, ip, is_read, time_read) VALUES (NULL, '$unique_code', '$name', '$email', '$message', '$now', '$ip', '0', '$time_read')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}


// delete cookie if captcha is active
setcookie("mx_captcha", "", time()-60*60*24*30, "/"); // 30 days ago

header("Location: ../contact.php?msg=ok#form");
exit;