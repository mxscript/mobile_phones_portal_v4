function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Wrong email address")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Wrong email address")
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Wrong email address")
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Wrong email address")
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Wrong email address")
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Wrong email address")
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Wrong email address")
		    return false
		 }

 		 return true					
}


function checkNewAccount(){
	var name=document.formAdaugaCont.name	
	if ((nume.value==null)||(nume.value=="")){
		alert("Input your name")
		name.focus()
		return false
	}

	var email=document.formAdaugaCont.email	
	if ((email.value==null)||(email.value=="")){
		alert("Wrong email address")
		email.focus()
		return false
	}
	if (echeck(email.value)==false){
		email.value=""
		email.focus() 
		return false 	
	}	

	var passw1=document.formAdaugaCont.passw1	
	if ((passw1.value==null)||(passw1.value=="")){
		alert("Input password")
		passw1.focus()
		return false
	}

	
	return true
}



function checkContactForm(){

	var email=document.formContact.email	
	if ((email.value==null)||(email.value=="")){
		alert("Wrong email address")
		email.focus()
		return false
	}
	if (echeck(email.value)==false){
		email.value=""
		email.focus() 
		return false 	
	}	

	var name=document.formContact.name	
	if ((name.value==null)||(name.value=="")){
		alert("Input name")
		name.focus()
		return false
	}

	var message=document.formContact.message	
	if ((message.value==null)||(message.value=="")){
		alert("Input message")
		message.focus()
		return false
	}

	var captcha_box=document.formContact.captcha_box	
	if ((captcha_box.value==null)||(captcha_box.value=="")){
		alert("Input antispam code")
		captcha_box.focus()
		return false
	}
	
	return true
}
