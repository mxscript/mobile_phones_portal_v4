<?php
require ("../../config.php");
require ("../functions/functions.php");
require ("../DatabaseConnect.php");

// check if software is already installed
$sql = "SELECT id FROM ".$database_table_prefix."settings WHERE name ='config_is_installed' AND value = '1' LIMIT 1";
$rs = $conn->query($sql);
$is_installed = $rs->num_rows;


$title = isset($_REQUEST['title']) ? $_REQUEST['title'] : '';
$title = Secure($title);

$license_key = isset($_REQUEST['license_key']) ? $_REQUEST['license_key'] : '';
$license_key = Secure($license_key);

$client_id = isset($_REQUEST['client_id']) ? $_REQUEST['client_id'] : '';
$client_id = Secure($client_id);

$msg = isset($_REQUEST['msg']) ? $_REQUEST['msg'] : '';
$msg = Secure($msg);

$license_type = isset($_REQUEST['license_type']) ? $_REQUEST['license_type'] : '';
$license_type = Secure($license_type);

$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
$name = Secure($name);

$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
$email = Secure($email);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Software install">
    <meta name="author" content="mxscripts.com">

    <title>Software install</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../assets/css/sticky-footer.css" rel="stylesheet">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Begin page content -->
    <div class="container">
      
      <?php
	  if($is_installed!=0 and $msg!="install_ok") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Software is already installed. You can start a fresh new install from admin area.</p>';
			echo '<p><a href="'.ScriptDomain().'/login/"><button type="button" class="btn btn-default">Go to admin area</button></a> ';
			echo ' <a href="'.ScriptDomain().'"><button type="button" class="btn btn-default">Go to website</button></a></p>';	
			echo '</div>';
	  }
	  
	  if($msg=="error_title") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Please input your website title</p>';
			echo '</div>';
	  }

	  if($msg=="error_key") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Please input your license key. You can genereate your license key in your user accoun on www.mxscripts.com</p>';
			echo '</div>';
	  }

	  if($msg=="error_email") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Please input your valid admin email. You will login to your admin area using this email address.</p>';
			echo '</div>';
	  }

	  if($msg=="error_password") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Please input your admin password. Password must be the same in both fields. <br />Minimum 5 characters are required for password.</p>';
			echo '</div>';
	  }

	  if($msg=="install_ok") {
		  	echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Well done!</h4>';
			echo '<p>Sowtware is now installed. You can go to your admin area or visit your website.</p>';
			echo '<p><a href="'.ScriptDomain().'/login/"><button type="button" class="btn btn-default">Go to admin area</button></a> ';
			echo ' <a href="'.ScriptDomain().'"><button type="button" class="btn btn-default">Go to website</button></a></p>';			
			echo '</div>';
	  }
	  
	  if($msg=="error_php_version") {
		  	$php_version = phpversion();		
			echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';	
			echo 'Error! Your version of PHP ('.$php_version.') is very old. You need at least PHP 5.3.1 to be installed on your server!';
			echo '</div>';
	  }

	  else if($msg=="error_db_connect") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Database connection failed. Please edit config.php file and try again</p>';
			echo '<p><a href="install.php"><button type="button" class="btn btn-danger">Try again</button></a></p>';
			echo '</div>';
	  }
	  
	  else if($msg!="install_ok" and $is_installed==0)
	  {
		  ?>
	      <form action="install-submit.php" method="post">
       	  <h3>Software install:</h3>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">  
          <div class="form-group">
            <label>Website title:</label>
            <input name="title" type="text" class="form-control" placeholder="Input your website title" value="<?php echo $title;?>">
          </div>
          </div>
          
		  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label>Administrator Full name:</label>
            <input name="name" type="text" class="form-control" placeholder="Enter admin full name" value="<?php echo $name;?>">
          </div>
          </div>
                    
		  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label>Administrator email (used for login):</label>
            <input name="email" type="email" class="form-control" placeholder="Enter admin email" value="<?php echo $email;?>">
          </div>
          </div>
          
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label>Administrator password</label>
            <input name="password" type="password" class="form-control" placeholder="Password">
          </div>
          </div>
          
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="form-group">
            <label>Password again</label>
            <input name="password2" type="password" class="form-control" placeholder="Input password again">
          </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <input type="hidden" name="license_key" value="<?php echo $license_key; ?>" />
    	  <input type="hidden" name="license_type" value="<?php echo $license_type; ?>" />
    	  <input type="hidden" name="client_id" value="<?php echo $client_id; ?>" />          
          <button type="submit" class="btn btn-default">Install software</button>
          </div>
        </form>
          
          <?php
	  }
	  ?>
    </div><!-- End page content -->

    <footer class="footer">
      <div class="container">
        <p class="text-muted">&copy; 2007 - <?php echo date("Y");?> <a target="_blank" href="http://www.mxscripts.com">MXscripts.com</a></p>
      </div>
    </footer>


	<script src="../assets/js/jquery.min_1.11.1.js"></script>
    <script src="../assets/js/alert.js"></script>
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
