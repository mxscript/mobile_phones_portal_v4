<?php
require ("../../config.php");
require ("../functions/functions.php");
require ("../DatabaseConnect.php");

// check if software is already installed
$sql = "SELECT id FROM ".$database_table_prefix."settings WHERE name ='config_is_installed' AND value = '1' LIMIT 1";
$rs = $conn->query($sql);
$is_installed = $rs->num_rows;

$msg = isset($_REQUEST['msg']) ? $_REQUEST['msg'] : '';
$msg = Secure($msg);

$title = isset($_REQUEST['title']) ? $_REQUEST['title'] : '';
$title = Secure($title);

$license_key = isset($_REQUEST['msg']) ? $_REQUEST['license_key'] : '';
$license_key = Secure($license_key);

$client_id = isset($_REQUEST['client_id']) ? $_REQUEST['client_id'] : '';
$client_id = Secure($client_id);

$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
$name = Secure($name);

$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
$email = Secure($email);

$forced = isset($_REQUEST['forced']) ? $_REQUEST['forced'] : '0';
$forced = Secure($forced);
$forced = (int)$forced;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Software install">
    <meta name="author" content="mxscripts.com">

    <title>Software install</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../assets/css/sticky-footer.css" rel="stylesheet">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Begin page content -->
    <div class="container">
    	<h3>Software install:</h3>
      
      <?php
	  if($is_installed!=0 and $msg!="install_ok") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Software is already installed.</p>';
			echo '<p><a href="'.ScriptDomain().'/login/"><button type="button" class="btn btn-default">Go to admin area</button></a> ';
			echo ' <a href="'.ScriptDomain().'"><button type="button" class="btn btn-default">Go to website</button></a>';	
			echo ' <a href="install.php?forced=1"><button type="button" class="btn btn-default">Force reinstall</button></a></p>';	
			echo '</div>';
	  }
	  

	  if($msg=="error_key") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Please input your license key. You can genereate your license key in your user accoun on www.mxscripts.com</p>';
			echo '</div>';
	  }


	  if($msg=="install_ok") {
		  	echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Well done!</h4>';
			echo '<p>Sowtware is now installed. You can go to your admin area or visit your website.</p>';
			echo '<p><a href="'.ScriptDomain().'/login/"><button type="button" class="btn btn-default">Go to admin area</button></a> ';
			echo ' <a href="'.ScriptDomain().'"><button type="button" class="btn btn-default">Go to website</button></a></p>';			
			echo '</div>';
	  }
	  
	  if($msg=="error_php_version") {
		  	$php_version = phpversion();		
			echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';	
			echo 'Error! Your version of PHP ('.$php_version.') is very old. You need at least PHP 5.3.1 to be installed on your server!';
			echo '</div>';
	  }

	  else if($msg=="error_db_connect") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Database connection failed. Please edit config.php file and try again</p>';
			echo '<p><a href="install.php"><button type="button" class="btn btn-danger">Try again</button></a></p>';
			echo '</div>';
	  }

	  else if($msg=="error_license_key") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Invalid license key</p>';
			echo '<p><a href="install.php"><button type="button" class="btn btn-danger">Try again</button></a></p>';
			echo '</div>';
	  }

	  else if($msg=="error_check") {
		  	echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4>Error!</h4>';
			echo '<p>Invalid license key or invalid Client ID</p>';
			echo '<p><a href="install.php"><button type="button" class="btn btn-danger">Try again</button></a></p>';
			echo '</div>';
	  }

	  
	  else if(($msg!="install_ok" and $is_installed==0 and $forced==0))
	  {
		  header("location: {$MT_SITE_URL}/key-bypass.php");
	  }
	  
	  else if($forced==1)
	  {
		  ?>
	      <form action="install-forced-submit.php" method="post">       	  
                    
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>License key</label>
            <input name="license_key" type="text" class="form-control" placeholder="Input your license key" value="<?php echo $license_key;?>">
          </div>          
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>Client ID</label>
            <input name="client_id" type="text" class="form-control" placeholder="Input your client ID" value="<?php echo $client_id;?>">
          </div>          
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <input type="hidden" name="dom" value="<?php  echo scriptDomain();?>" />
          <input type="hidden" name="item_code" value="8pr5vf3m" />
          <input type="hidden" name="forced" value="1" />
          <button type="submit" class="btn btn-default">Install</button>
          </div>
        </form>
		<?php
	}	  
	?>
    
    </div><!-- End page content -->

    <footer class="footer">
      <div class="container">
        <p class="text-muted">&copy; 2007 - <?php echo date("Y");?> <a target="_blank" href="http://www.mxscripts.com">MXscripts.com</a></p>
      </div>
    </footer>


	<script src="../assets/js/jquery.min_1.11.1.js"></script>
    <script src="../assets/js/alert.js"></script>
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
