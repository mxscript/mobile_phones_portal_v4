<?php
/*
=========================================================================================
Copyright www.mxscripts.com
=========================================================================================
*/
require ("../../config.php");
require ("../functions/functions.php");
require ("../DatabaseConnect.php");

$license_key = $_POST['license_key'];
$license_key = Secure($license_key);

$client_id = $_POST['client_id'];
$client_id = Secure($client_id);

// check
$query = "SELECT id FROM ".$database_table_prefix."settings WHERE name='config_license_key' AND value = '$license_key' LIMIT 1";
$rs = $conn->query($query);
$check_ok1 = $rs->num_rows;

$query = "SELECT id FROM ".$database_table_prefix."settings WHERE name='config_client_id' AND value = '$client_id' LIMIT 1";
$rs = $conn->query($query);
$check_ok2 = $rs->num_rows;

if($check_ok1==0 or $check_ok2==0)
	{
	header("Location: install.php?msg=error_data");
	exit();
	}

$sql = "UPDATE ".$database_table_prefix."settings SET value = '' WHERE name = 'config_site_url'";
if($conn->query($sql)===false){trigger_error('Error: '.$conn->error, E_USER_ERROR);}

$sql = "UPDATE ".$database_table_prefix."settings SET value = '0' WHERE name = 'config_is_installed' ";
if($conn->query($sql)===false){trigger_error('Error: '.$conn->error, E_USER_ERROR);}

header("Location: install2.php?license_key=$license_key&client_id=$client_id");
exit;
