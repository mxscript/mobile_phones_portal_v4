<?php
/*
=========================================================================================
Copyright www.mxscripts.com
=========================================================================================
*/
require ("../../config.php");
require ("../functions/functions.php");
require ("../DatabaseConnect.php");

// check if software is installed
$query = "SELECT id FROM ".$database_table_prefix."settings WHERE name='is_installed' AND value = '1' LIMIT 1";
$rs = $conn->query($query);
$is_installed = $rs->num_rows;
if($is_installed==1)
	{
	header("Location: install.php");
	exit();
	}

$title = isset($_POST['title']) ? $_POST['title'] : '';
$title = Secure($title);

$license_key = isset($_POST['license_key']) ? $_POST['license_key'] : '';
$license_key = Secure($license_key);

$client_id = isset($_POST['client_id']) ? $_POST['client_id'] : '';
$client_id = Secure($client_id);

$license_type = isset($_POST['license_type']) ? $_POST['license_type'] : '';
$license_type = Secure($license_type);

$email = isset($_POST['email']) ? $_POST['email'] : '';
$email = Secure($email);

$name = isset($_POST['name']) ? $_POST['name'] : '';
$name = Secure($name);
$permalink = RewriteUrl($name);

$password = isset($_POST['password']) ? $_POST['password'] : '';
$password = Secure($password);

$password2 = isset($_POST['password2']) ? $_POST['password2'] : '';
$password2 = Secure($password2);

if (strlen($title)==0)
	{
	header("Location: install2.php?msg=error_title&license_key=$license_key&email=$email&name=$name&license_type=$license_type&client_id=$client_id");
	exit();
	}

if (strlen($license_key)==0)
	{
	header("Location: install2.php?msg=error_key&title=$title&email=$email&name=$name&license_type=$license_type&client_id=$client_id");
	exit();
	}

if (strlen($email)==0)
	{
	header("Location: install2.php?msg=error_email&title=$title&email=$email&license_key=$license_key&name=$name&license_type=$license_type&client_id=$client_id");
	exit();
	}

if (strlen($password)<5 or $password!=$password2)
	{
	header("Location: install2.php?msg=error_password&title=$title&email=$email&license_key=$license_key&name=$name&license_type=$license_type&client_id=$client_id");
	exit();
	}

if (!empty($database_charset))
	$charset_collate = "DEFAULT CHARACTER SET $database_charset";
if (!empty($database_collate))
	$charset_collate .= " COLLATE $database_collate";


$now = date("Y-m-d H:i:s");
$ip_reg = $_SERVER['REMOTE_ADDR'];
$host_reg = gethostbyaddr($ip_reg);

$salt = generateSaltPassword (); //salt
$password_db = generateHashPassword ($password, $salt);
$activation_code = md5(random_code());	

addSettings ('config_site_url', ScriptDomain(), "global", 1);	
addSettings ('config_site_media', ScriptDomain()."/content/media", "global", 1);			
addSettings ('config_template', "Arena", "global", 1);			
addSettings ('config_site_title', $title, "global", 1);			
addSettings ('config_license_key', $license_key, "global", 1);
addSettings ('config_is_installed', 1, "global", 1);
addSettings ('config_install_datetime', $now, "global", 1);
addSettings ('config_site_email', $email, "global", 1);			
addSettings ('config_client_id', $client_id, "global", 1);			
		
addSettings ('config_img_large_width', "900", "global", 1);
addSettings ('config_img_large_height', "400", "global", 1);
addSettings ('config_img_large_resize', "auto", "global", 1);
addSettings ('config_img_small_width', "480", "global", 1);
addSettings ('config_img_small_height', "350", "global", 1);
addSettings ('config_img_small_resize', "crop", "global", 1);
addSettings ('config_img_thumb_width', "160", "global", 1);
addSettings ('config_img_thumb_height', "160", "global", 1);
addSettings ('config_img_thumb_resize', "crop", "global", 1);
addSettings ('config_img_avatar_width', "180", "global", 1);
addSettings ('config_img_avatar_height', "180", "global", 1);
addSettings ('config_img_avatar_resize', "crop", "global", 1);

addSettings ('config_date_format', "F d, Y", "global", 1);	
addSettings ('config_meta_dir', "ltr", "global", 1);	
addSettings ('config_meta_charset', "UTF-8", "global", 1);	
addSettings ('config_meta_lang', "en", "global", 1);	


$sql = "INSERT INTO ".$database_table_prefix."users(id, name, permalink, email, avatar, bio, password, salt, token, role_id, active, email_verified, activation_code, register_time, register_ip, register_host, protected, last_activity) VALUES (NULL, '$name', '$permalink', '$email', 'no_avatar.png', '', '$password_db', '$salt', '', '1', '1', '1', '$activation_code', '$now', '$ip_reg', '$host_reg',  '1', '0000-00-00 00:00:00');";
if($conn->query($sql)===false){trigger_error('Error: '.$conn->error, E_USER_ERROR);}


// update user_id of all content with your admin user_id
$query_id = "SELECT id FROM ".$database_table_prefix."users ORDER BY id DESC LIMIT 1";
$rs_id = $conn->query($query_id);
$row = $rs_id->fetch_assoc();
$user_id = $row['id'];
$sql = "UPDATE ".$database_table_prefix."content SET user_id = '$user_id'";
if($conn->query($sql)===false){trigger_error('Error: '.$conn->error, E_USER_ERROR);}

header("Location: install.php?msg=install_ok");
exit;
