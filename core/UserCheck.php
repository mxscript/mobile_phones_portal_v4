<?php 
session_start();
$user_logged = 0;	
$logged_user_id = isset($logged_user_id) ? $logged_user_id : '';

$_SESSION['user_token'] = isset($_SESSION['user_token']) ? $_SESSION['user_token'] : '';
$_SESSION['user_logged'] = isset($_SESSION['user_logged']) ? $_SESSION['user_logged'] : '';
$_COOKIE['user_token'] = isset($_COOKIE['user_token']) ? $_COOKIE['user_token'] : '';

if((isset($_SESSION['user_token']) and $_SESSION['user_logged']==1) or (isset($_COOKIE['user_token']) and $_SESSION['user_logged']==1))
	{
		// User is logged
	    if (isset($_SESSION['user_token']))
			$user_token = $_SESSION['user_token'];
		else	
			$user_token = $_COOKIE['user_token'];
	
		$user_token = strip_tags(htmlentities($user_token));
		
		$query = "SELECT id, name, email, avatar, website, role_id, active FROM ".$database_table_prefix."users WHERE ACTIVE ='1' AND SHA1(token) = '$user_token' LIMIT 1";
		$rs = $conn->query($query);
		$valid_user = $rs->num_rows;
		if($valid_user==0) 
			{
				session_destroy();
				setcookie("user_token", "", time()-60*60*24*120, "/");  // 120 days ago
				header("location:user.php?msg=error");
				exit;
			}
		$row = $rs->fetch_assoc();
		$logged_user_id = $row['id']; 
		$logged_user_name = stripslashes($row['name']); 
		$logged_user_email = $row['email'];
		$logged_user_avatar = $row['avatar'];	
		$logged_user_website = $row['website'];
		$logged_user_role_id = $row['role_id'];
		if($logged_user_avatar=="") $logged_user_avatar = "no_image.png";						
		$user_logged = 1;	
		
		$smarty->assign("USER_LOGGED", 1);
		$smarty->assign("LOGGED_USER_ID", $logged_user_id);
		$smarty->assign("LOGGED_USER_NAME", $logged_user_name);
		$smarty->assign("LOGGED_USER_EMAIL", $logged_user_email);
		$smarty->assign("LOGGED_USER_ROLE_ID", $logged_user_role_id);	
		$smarty->assign("LOGGED_USER_AVATAR", $logged_user_avatar);
		
		$now = date("Y-m-d H:i:s");
		$query = "UPDATE users SET last_activity = '$now' WHERE id = '$logged_user_id' LIMIT 1";
		$rs = $conn->query($query);

	}
else
	{
		$user_logged = 0;	
		$smarty->assign("USER_LOGGED", 0);
		// user not logged		
	}
	
//echo $user_logged;	
//echo "<br>".$_SESSION['user_token'];
?>
