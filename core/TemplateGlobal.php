<?php
$activeContentStatusID = getActiveContentStatusID();

// ****************************************************************************************************
// HEADER MENUS TEMPLATE 
// ****************************************************************************************************
$menu =  array();

$query = "SELECT id, title, permalink FROM ".$database_table_prefix."categories WHERE show_in_menu = 1 AND parent_id = 0 AND active = 1 AND (show_only_in_template = '$config_template' OR show_only_in_template = '') ORDER BY position ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
{
	$categ_id = $row['id'];	
	$categ_title = stripslashes($row['title']);
    $categ_permalink = $row['permalink'];
	
	// subcategories
	$categ_subcategories = array();
	$query_subcateg = "SELECT id, title, permalink FROM ".$database_table_prefix."categories WHERE show_in_menu = 1 AND parent_id = '$categ_id ' AND (show_only_in_template = '$config_template' OR show_only_in_template = '') ORDER BY position ASC";
	$rs_subcateg = $conn->query($query_subcateg);
	while($row = $rs_subcateg->fetch_assoc())
		{
		$subcateg_id = $row['id'];	
		$subcateg_title = stripslashes($row['title']);
		$subcateg_permalink = $row['permalink'];
	
		$categ_subcategories[] = array("id" => $subcateg_id, "title" => $subcateg_title, "permalink" => $subcateg_permalink);
		}

	$menu[] = array("id" => $categ_id, "title" => $categ_title, "permalink" => $categ_permalink, "subcategories" => $categ_subcategories); 
}
$smarty->assign('header_menu', $menu);


// ****************************************************************************************************
// FOOTER CATEGORIES
// ****************************************************************************************************
$footer_categories =  array();
$query = "SELECT id, title, permalink FROM ".$database_table_prefix."categories WHERE show_in_menu = 1 AND parent_id = 0 AND active = 1 AND (show_only_in_template = '$config_template' OR show_only_in_template = '') ORDER BY position ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
{
	$categ_id = $row['id'];	
	$categ_title = stripslashes($row['title']);
    $categ_permalink = $row['permalink'];
	

	$footer_categories[] = array("id" => $categ_id, "title" => $categ_title, "permalink" => $categ_permalink); 
}
$smarty->assign('footer_categories', $footer_categories);


// ****************************************************************************************************
// LATEST CONTENT ITEMS
// ****************************************************************************************************
$latest_content = array();
$query = "SELECT id, unique_code, title, permalink, user_id, categ_id, image, content_short, datetime_added, hits FROM ".$database_table_prefix."content WHERE status_id = '$activeContentStatusID' ORDER BY sticky DESC, id DESC LIMIT 24";
$rs = $conn->query($query);
$exist = $rs->num_rows;
while($row = $rs->fetch_assoc())
	{
	$content_id = $row['id'];
	$content_unique_code = $row['unique_code'];	
	$content_title = stripslashes($row['title']);
	$content_permalink = $row['permalink'];
	$content_user_id = $row['user_id'];
	$content_categ_id = $row['categ_id'];
	$content_image = $row['image'];
	$content_short = stripslashes($row['content_short']);
	$content_date = $row['datetime_added'];
	$content_hits = $row['hits'];

	$UserDetailsArray = getUserDetailsArray($content_user_id);
    $content_user_name = stripslashes($UserDetailsArray['name']);
   	$content_user_avatar = $UserDetailsArray['avatar'];

	$CategDetailsArray = getCategDetailsArray($content_categ_id);
    $content_categ_title = stripslashes($CategDetailsArray['title']);
	$content_categ_permalink = stripslashes($CategDetailsArray['permalink']);

	$latest_content[] = array("id" => $content_id, "unique_code" => $content_unique_code, "title" => $content_title, "content_short" => $content_short, "permalink" => $content_permalink, "image" => $content_image, "date" => DateFormat($content_date), "hits" => $content_hits, "user_id" => $content_user_id, "user_name" => $content_user_name, "user_avatar" => $content_user_avatar, "categ_id" => $content_categ_id, "categ_title" => $content_categ_title, "categ_permalink" => $content_categ_permalink);
	}	
$smarty->assign('latest_content', $latest_content);


// ****************************************************************************************************
// ADS
// ****************************************************************************************************
$ads = array();
$query = "SELECT id, code FROM ".$database_table_prefix."ads_manager WHERE active = 1 ORDER BY id ASC";
$rs = $conn->query($query);
$exist = $rs->num_rows;
while($row = $rs->fetch_assoc())
	{
	$ad_id = $row['id'];	
	$ad_code = stripslashes($row['code']);

	$ads[$ad_id] = $ad_code;
	}
$smarty->assign('ads', $ads);


// ****************************************************************************************************
// PAGES
// ****************************************************************************************************
$header_menu_pages =  array();
$query = "SELECT title, permalink FROM ".$database_table_prefix."pages WHERE show_in_header = 1 AND active = 1 AND (show_only_in_template = '$config_template' OR show_only_in_template = '') ORDER BY position ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
	$page_title = stripslashes($row['title']);
    $page_permalink = $row['permalink'];
	$header_menu_pages[] =  array("title" => $page_title, "permalink" => $page_permalink);
	}
$smarty->assign('header_menu_pages', $header_menu_pages);

$footer_pages =  array();
$query = "SELECT title, permalink FROM ".$database_table_prefix."pages WHERE show_in_footer = 1 AND active = 1 AND (show_only_in_template = '$config_template' OR show_only_in_template = '') ORDER BY position ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
	$page_title = stripslashes($row['title']);
    $page_permalink = $row['permalink'];
	$footer_pages[] =  array("title" => $page_title, "permalink" => $page_permalink);
	}
$smarty->assign('footer_pages', $footer_pages);


// ****************************************************************************************************
// HOMEPAGE SLIDER
// ****************************************************************************************************
$main_slider =  array();
$query = "SELECT id, name, content_source, content_defined_ids, content_bg, content_latest_numb_items, content_latest_categ_id FROM ".$database_table_prefix."sliders WHERE active = 1 AND homepage_of_template = '$config_template' ORDER BY id DESC LIMIT 1";
$rs = $conn->query($query);

$slider_enabled = $rs->num_rows;
$smarty->assign('SLIDER_ENABLED', $slider_enabled);

if($slider_enabled==1)
	{
	$row = $rs->fetch_assoc();
	$slider_id = $row['id'];
	$slide_name = stripslashes($row['name']);
	$content_source = $row['content_source'];
	$content_defined_ids = $row['content_defined_ids'];
	$content_bg = $row['content_bg'];
	$content_latest_numb_items = $row['content_latest_numb_items'];
	$content_latest_categ_id = $row['content_latest_categ_id'];
	
	if($content_source=="manual")
		{
		$query = "SELECT id, title, content, url, target, image FROM ".$database_table_prefix."sliders_content WHERE active = 1 AND slider_id = '$slider_id' ORDER BY position ASC";
		$rs = $conn->query($query);
		$numb_slides = $rs->num_rows;
		while($row = $rs->fetch_assoc())
			{
			$slide_id = $row['id'];
			$slide_title = stripslashes($row['title']);
			$slide_content = stripslashes($row['content']);	
			$slide_url = $row['url'];
			$slide_target = $row['target'];
			$slide_image = $row['image'];		
			$main_slider[] =  array("id" => $slide_id, "title" => $slide_title, "content" => $slide_content, "url" => $slide_url, "target" => $slide_target, "image" => $slide_image);
			}
		}
	$smarty->assign('main_slider', $main_slider);
	$smarty->assign('numb_slides', $numb_slides);	
	}



// FOOTER CODE
$footer_analytics_code = stripslashes(footer());
$smarty->assign("FOOTER_ANALYTICS_CODE", $footer_analytics_code);
?>