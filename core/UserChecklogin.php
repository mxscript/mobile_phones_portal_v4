<?php
session_start();
if((isset($_SESSION['user_logged']) and $_SESSION['user_logged']==1) and isset($_SESSION['user_token']))
	{
		// User is logged (session)
		$user_session_token = Secure($_SESSION['user_token']);
		$sql_checklogin = "SELECT id, name, email, role_id FROM ".$database_table_prefix."users WHERE sha1(token) = '$user_session_token' AND active = 1 LIMIT 1";
	}

else if (isset($_COOKIE['user_token']))
	{
		// User is logged (cookie)
	    $user_cookie_token = Secure($_COOKIE['user_token']);
		$sql_checklogin = "SELECT id, name, email, role_id FROM ".$database_table_prefix."users WHERE sha1(token) = '$user_cookie_token' AND active = 1 LIMIT 1";
	}

else
	{
		// User not logged
		header("location: index.php?msg=not_logged");
		exit;
	}

		// User logged	
		$rs = $conn->query($sql_checklogin);
		$is_valid_user = $rs->num_rows;

		if ($is_valid_user==0)
			{
			$_SESSION = array();
			session_destroy();
			setcookie("user_token", "", time()-60*60*24*120, "/");  // 120 days ago
			header("location:index.php?msg=invalid_user");
			exit;
			}
		while($row = $rs->fetch_assoc())
			{
			$logged_user_id = $row['id'];
			$logged_user_name = stripslashes($row['name']);
			$logged_user_email = stripslashes($row['email']);
			$logged_user_role_id = $row['role_id'];
			}
?>
