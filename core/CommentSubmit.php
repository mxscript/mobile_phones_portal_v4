<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once '../init.php';

$comment_name = $_POST["comment_name"];
$comment_name = Secure($comment_name);

$captcha_box = $_POST["captcha_box"];
$captcha_box = Secure($captcha_box);

$comment_content = $_POST["comment_content"];
$comment_content = strip_tags($comment_content);
$comment_content = addslashes($comment_content);

$content_id = $_POST['content_id'];
$content_id = Secure($content_id);

$content_permalink = $_POST['content_permalink'];
$content_permalink = Secure($content_permalink);

$categ_permalink = $_POST['categ_permalink'];
$categ_permalink = Secure($categ_permalink);

$redirect = $_POST['redirect'];
$redirect = Secure($redirect);
$redirect2 = $config_site_url."/post-opinions.php?permalink=".$content_permalink."&id=".$content_id."&categ_permalink=".$categ_permalink;

if($comment_name=="")
	{
	header("Location:$redirect2&msg=error_name#post_comment");
	exit();
	}

if($comment_content=="")
	{
	header("Location:$redirect2&msg=error_comment#post_comment");
	exit();
	}

if($site_demo_mode!=0)
	{
	header("Location:$redirect2&msg=error_demo_mode#post_comment");
	exit();
	}

if($config_enable_site_comments==0)
	{
	header("Location:$redirect2&msg=error_site_comments_disabled#post_comment");
	exit();
	}


if($config_enable_captcha_comments==1)
	{
	// captcha verification
	if($_COOKIE['mx_captcha'] != md5($captcha_box))
		{
		header("Location: $redirect2&msg=error_captcha#post_comment");
		exit();
		}
	}


$time = date("Y-m-d H:i:s");
$ip = $_SERVER['REMOTE_ADDR'];

$approved = 0;
if($config_comments_manually_approved==0) $approved = 1;


$query = "INSERT INTO ".$database_table_prefix."comments (id, content_id, user_id, name, website, ip, content, datetime, approved) VALUES (NULL, '$content_id', '$user_id', '$comment_name', '$comment_website', '$ip', '$comment_content', '$time', '$approved')"; 
$rs = $conn->query($query);
$affected_rows = $conn->affected_rows;	


// DEVICE REVIEWS
$sql = "SELECT id FROM ".$database_table_prefix."comments ORDER BY id DESC LIMIT 1";
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$comment_id = $row['id'];

// delete cookie if captcha is active
setcookie("mx_captcha", "", time()-60*60*24, "/"); // 1 day ago

// form OK:
header("Location: $redirect2&msg=comment_ok#comments");	
exit;