<?php
/*
=========================================================================================
Copyright www.mxscripts.com
=========================================================================================
*/

// YOU CAN CHANGE SETTINGS BELOW:

// 1. DATABASE CONNECT (REQUIRED)
$database_host = 'localhost'; // mysql server (usually "localhost")
$database_user = 'root'; // Mysql username
$database_password = 'mysql'; // Mysql password
$database_name = 'mxscripts'; // Mysql Database name

// 2. OTHER SETTINGS (YOU DON'T NEED TO CHANGE THIS IF YOU ARE NOT SURE)
$database_table_prefix = "mx_"; // Tables prefix.
$database_charset = "utf8"; // Mysql charset
$database_collate = "utf8_general_ci"; // Don't change if you are not sure
$site_demo_mode = "0"; // This must be 0 for real websites

define('ABSPATH', dirname(__FILE__)); 

// MORE SETTINGS
$mobiles_categ_id = 52; // ID of category for all mobile phones (brands)
$mobiles_cf_group_id = 5; // ID of custom fields froup for mobiles
$news_categ_id = 9; // ID of news category
$reviews_categ_id = 50; // ID of reviews category

if(!function_exists('siteURL')){
    function siteURL()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'].'/';
        return $protocol.$domainName;
    }
}
$MT_SITE_URL = siteURL();

?>