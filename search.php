<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once 'init.php';

$search = $_GET["search"];
$search = Secure($search);

$search_results_devices = array();
$search_results_articles = array();

$status_id_active = getActiveContentStatusID();

if(strlen($search)>3)	
	{
	$query_devices = "SELECT id, categ_id, title, permalink, image, MATCH (search_terms) AGAINST ('".$search."' IN BOOLEAN MODE) AS relevance FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND root_categ_id = '$mobiles_categ_id' AND MATCH (search_terms) AGAINST ('".$search."' IN BOOLEAN MODE) ORDER BY relevance DESC, sticky DESC, mobile_release_date DESC LIMIT 40";
		
	$query_articles = "SELECT id, categ_id, title, permalink, image, MATCH (title) AGAINST ('".$search."' IN BOOLEAN MODE) AS relevance FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND (root_categ_id = '$news_categ_id' OR root_categ_id = '$reviews_categ_id') AND MATCH (title) AGAINST ('".$search."' IN BOOLEAN MODE) ORDER BY relevance DESC, sticky DESC, id DESC LIMIT 20";
	}
else
	{
	$query_devices = "SELECT id, categ_id, title, permalink, image FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND root_categ_id = '$mobiles_categ_id' AND search_terms LIKE '%$search%' ORDER BY sticky DESC, mobile_release_date DESC LIMIT 40";
	
	$query_articles = "SELECT id, categ_id, title, permalink, image FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' AND (root_categ_id = '$news_categ_id' OR root_categ_id = '$reviews_categ_id') AND title LIKE '%$search%' ORDER BY sticky DESC, id DESC LIMIT 20";				
	}
	
	$rs_devices = $conn->query($query_devices);
	$rows_devices = $rs_devices->num_rows;

	$rs_articles = $conn->query($query_articles);
	$rows_articles = $rs_articles->num_rows;
	
while ($row = $rs_devices->fetch_assoc())
	{
	$content_id = $row['id'];	
	$content_categ_id = $row['categ_id'];
	$content_title = stripslashes($row['title']);
	$content_permalink = $row['permalink'];	
	$content_image = $row['image'];	

	$CategDetailsArray = getCategDetailsArray($content_categ_id);
	$content_categ_title = stripslashes($CategDetailsArray['title']);
	$content_categ_permalink = stripslashes($CategDetailsArray['permalink']);

	$search_results_devices[] = array("id" => $content_id, "title" => $content_title, "permalink" => $content_permalink, "image" => $content_image, "categ_title" => $content_categ_title, "categ_permalink" => $content_categ_permalink);
	}
	
while ($row = $rs_articles->fetch_assoc())
	{
	$content_id = $row['id'];	
	$content_categ_id = $row['categ_id'];
	$content_title = stripslashes($row['title']);
	$content_permalink = $row['permalink'];	
	$content_image = $row['image'];	

	$CategDetailsArray = getCategDetailsArray($content_categ_id);
	$content_categ_title = stripslashes($CategDetailsArray['title']);
	$content_categ_permalink = stripslashes($CategDetailsArray['permalink']);
	
	$search_results_articles[] = array("id" => $content_id, "title" => $content_title, "permalink" => $content_permalink, "image" => $content_image, "categ_title" => $content_categ_title, "categ_permalink" => $content_categ_permalink);
	}


$numb_results = $rows_articles + $rows_devices;	
$now = date("Y-m-d H:i:s");
	
// SEARCH LOG
if($search!="")
	{
	$query = "INSERT INTO ".$database_table_prefix."search_log (id, search, date, ip, results) VALUES (NULL, '$search', '$now', '$ip_source', '$numb_results')"; 
if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
	else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }
	}

	
$smarty->assign('search_results_devices', $search_results_devices);
$smarty->assign('search_results_articles', $search_results_articles);
$smarty->assign('SEARCH',$search);

$smarty->display('search.tpl');
