<?php
$activeContentStatusID = getActiveContentStatusID();

// mobile category details
$query_root = "SELECT permalink FROM ".$database_table_prefix."categories WHERE id = '$mobiles_categ_id' LIMIT 1";
$rs_root = $conn->query($query_root);
$row = $rs_root->fetch_assoc();
$mobile_section_permalink = $row['permalink'];	


// ****************************************************************************************************
// POST DETAILS
// ****************************************************************************************************
function postDetailsArray($content_id)
{
	global $conn;
	global $database_table_prefix;
	global $mobiles_categ_id;
	
	$postDetailsArray = array();

	$query = "SELECT title, permalink, image, categ_id, root_categ_id, cf_group_id, user_id, status_id, content_short, content, meta_title, meta_description, meta_keywords, datetime_added, datetime_modified, hits, aditional_info, disable_comments, disable_ads, disable_ratings FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($query); 
	$exist = $rs->num_rows; 
	$row = $rs->fetch_assoc();
	
	$title = stripslashes($row['title']);
	$permalink = stripslashes($row['permalink']);	
	$image = $row['image'];	
	$categ_id = $row['categ_id'];
	$root_categ_id = $row['root_categ_id'];
	$cf_group_id = $row['cf_group_id'];
	$user_id = $row['user_id'];
	$status_id = $row['status_id'];
	$content_short = stripslashes($row['content_short']);
	$content = stripslashes(html_entity_decode($row['content']));
	$meta_title = stripslashes($row['meta_title']);
	$meta_description = stripslashes($row['meta_description']);
	$meta_keywords = stripslashes($row['meta_keywords']);
	$datetime_added = $row['datetime_added'];
	$datetime_modified = $row['datetime_modified'];
	$hits = $row['hits'];
	$aditional_info = stripslashes($row['aditional_info']);
	$disable_comments = $row['disable_comments'];	
	$disable_ads = $row['disable_ads'];
	$disable_ratings = $row['disable_ratings'];

	$query_user = "SELECT name, email, permalink, avatar FROM ".$database_table_prefix."users WHERE id = '$user_id' LIMIT 1";
	$rs_user = $conn->query($query_user);
	$row = $rs_user->fetch_assoc();
	$user_name = stripslashes($row['name']);	
	$user_email = $row['email'];	
	$user_permalink = stripslashes($row['permalink']);
	$user_avatar = $row['avatar'];		
	if($user_avatar=="") $user_avatar = "no_avatar.png";
	
	$query_categ = "SELECT title, permalink, parent_id FROM ".$database_table_prefix."categories WHERE id = '$categ_id' LIMIT 1";
	$rs_categ = $conn->query($query_categ);
	$row = $rs_categ->fetch_assoc();
	$categ_title = stripslashes($row['title']);	
	$categ_parent_id = $row['parent_id'];	
	$categ_permalink = stripslashes($row['permalink']);
				
	if($categ_parent_id == $mobiles_categ_id)
		{
		$title = $categ_title." ".$title;
		}

	$title2 = strip_tags(html_entity_decode($title, ENT_QUOTES));
	$title2 = substr($title2, 0, 250);
		
	if($meta_title!="") $meta_title = $meta_title; else $meta_title = $title;
	if($meta_description!="") $meta_description = $meta_description; else $meta_description = $title2;
	//if($datetime_modified>$datetime_added) $datetime = $datetime_modified; else $datetime = $datetime_added;
	
	$postDetailsArray = array("exist" => $exist, "title" => $title, "permalink" => $permalink, "image" => $image, "categ_id" => $categ_id, "root_categ_id" => $root_categ_id, "cf_group_id" => $cf_group_id, "user_id" => $user_id, "status_id" => $status_id, "content_short" => $content_short, "content" => $content, "meta_title" => $meta_title, "meta_description" => $meta_description, "meta_keywords" => $meta_keywords, "datetime_added" => $datetime_added, "datetime_modified" => $datetime_modified, "hits" => $hits, "aditional_info" => $aditional_info, "disable_comments" => $disable_comments, "disable_ads" => $disable_ads, "disable_ratings" => $disable_ratings, "categ_title" => $categ_title, "categ_permalink" => $categ_permalink, "user_id" => $user_id, "user_name" => $user_name, "user_email" => $user_email, "user_permalink" => $user_permalink, "user_avatar" => $user_avatar);
	
	return $postDetailsArray;		
}


// ****************************************************************************************************
// ALL BRANDS 
// ****************************************************************************************************
function allBrands()
{
	global $conn;
	global $database_table_prefix;
	global $mobiles_categ_id;
	global $config_site_media;
	global $activeContentStatusID;
	
	$mobile_brands =  array();

	$query = "SELECT id, title, permalink, image_type, image FROM ".$database_table_prefix."categories WHERE parent_id = '$mobiles_categ_id' AND active = 1 ORDER BY title ASC";
	$rs = $conn->query($query);
	while($row = $rs->fetch_assoc())
	{
		$categ_id = $row['id'];	
		$categ_title = stripslashes($row['title']);
		$categ_permalink = $row['permalink'];
		$categ_image_type = $row['image_type'];
		$categ_image = $row['image'];
		
		// number of items
		$query_items = "SELECT id FROM ".$database_table_prefix."content WHERE status_id = '$activeContentStatusID' AND categ_id = '$categ_id'";
		$rs_items = $conn->query($query_items);
		$number_items = $rs_items->num_rows;
		
		// image
		if($categ_image_type=="fa") // font awesome
			$image = $categ_image;
		if($categ_image_type=="upload")
			$image = '<img src="'.$config_site_media."/img/".$categ_image.'" />';
		if($categ_image_type=="")
			$image = "";	
	
		$mobile_brands[] = array("id" => $categ_id, "title" => $categ_title, "permalink" => $categ_permalink, "image_type" => $categ_image_type, "image" => $image, "number_items" => $number_items); 
	}

return $mobile_brands;
}


// ****************************************************************************************************
// IMPORTANT BRANDS
// ****************************************************************************************************
function importantBrands()
{
	global $conn;
	global $database_table_prefix;
	global $mobiles_categ_id;

	$important_brands =  array();

	$limit = 27; // number of brands (3 cols meanst must be multiple of 3)
	$query = "SELECT id, title, permalink FROM ".$database_table_prefix."categories WHERE parent_id = '$mobiles_categ_id' AND active = 1 ORDER BY position ASC LIMIT $limit";
	$rs = $conn->query($query);
	while($row = $rs->fetch_assoc())
	{
		$categ_id = $row['id'];	
		$categ_title = stripslashes($row['title']);
		$categ_permalink = $row['permalink'];
			
		$important_brands[] = array("id" => $categ_id, "title" => $categ_title, "permalink" => $categ_permalink); 
	}

return $important_brands;
}


// ****************************************************************************************************
// LATEST MOBILES
// ****************************************************************************************************
function latestMobiles()
{
	global $conn;
	global $database_table_prefix;
	global $mobiles_categ_id;
	global $activeContentStatusID;

	$latest_mobiles = array();
	$query = "SELECT id, unique_code, title, permalink, user_id, categ_id, image, content_short, datetime_added, hits FROM ".$database_table_prefix."content WHERE status_id = '$activeContentStatusID' AND root_categ_id = '$mobiles_categ_id' ORDER BY sticky DESC, id DESC, mobile_release_date DESC LIMIT 24";
	$rs = $conn->query($query);
	$exist = $rs->num_rows;
	while($row = $rs->fetch_assoc())
		{
		$content_id = $row['id'];
		$content_unique_code = $row['unique_code'];	
		$content_title = stripslashes($row['title']);
		$content_permalink = $row['permalink'];
		$content_user_id = $row['user_id'];
		$content_categ_id = $row['categ_id'];
		$content_image = $row['image'];
		$content_short = stripslashes($row['content_short']);
		$content_date = $row['datetime_added'];
		$content_hits = $row['hits'];
	
		$UserDetailsArray = getUserDetailsArray($content_user_id);
		$content_user_name = stripslashes($UserDetailsArray['name']);
		$content_user_avatar = $UserDetailsArray['avatar'];
	
		$CategDetailsArray = getCategDetailsArray($content_categ_id);
		$content_categ_title = stripslashes($CategDetailsArray['title']);
		$content_categ_permalink = stripslashes($CategDetailsArray['permalink']);
	
		$latest_mobiles[] = array("id" => $content_id, "unique_code" => $content_unique_code, "title" => $content_title, "content_short" => $content_short, "permalink" => $content_permalink, "image" => $content_image, "date" => DateFormat($content_date), "hits" => $content_hits, "user_id" => $content_user_id, "user_name" => $content_user_name, "user_avatar" => $content_user_avatar, "categ_id" => $content_categ_id, "categ_title" => $content_categ_title, "categ_permalink" => $content_categ_permalink);
		}	

return $latest_mobiles;		
}



// ****************************************************************************************************
// POPULAR MOBILES
// ****************************************************************************************************
function popularMobiles()
{
	global $conn;
	global $database_table_prefix;
	global $mobiles_categ_id;
	global $activeContentStatusID;

	$popular_mobiles = array();
	$query = "SELECT id, title, permalink, categ_id, image FROM ".$database_table_prefix."content WHERE status_id = '$activeContentStatusID' AND root_categ_id = '$mobiles_categ_id' ORDER BY mobile_release_date DESC, RAND() LIMIT 24";
	$rs = $conn->query($query);	
	$exist = $rs->num_rows;
	while($row = $rs->fetch_assoc())
		{
		$content_id = $row['id'];
		$content_title = stripslashes($row['title']);
		$content_permalink = $row['permalink'];
		$content_categ_id = $row['categ_id'];
		$content_image = $row['image'];
	
		$CategDetailsArray = getCategDetailsArray($content_categ_id);
		$content_categ_title = stripslashes($CategDetailsArray['title']);
		$content_categ_permalink = stripslashes($CategDetailsArray['permalink']);
	
		$popular_mobiles[] = array("id" => $content_id, "title" => $content_title, "permalink" => $content_permalink, "image" => $content_image, "categ_id" => $content_categ_id, "categ_title" => $content_categ_title, "categ_permalink" => $content_categ_permalink);
		}	

return $popular_mobiles;		
}


// ****************************************************************************************************
// TOP RATED MOBILES
// ****************************************************************************************************
function topRatedMobiles()
{
	global $conn;
	global $database_table_prefix;
	global $mobiles_categ_id;
	global $activeContentStatusID;

	$top_rated_mobiles = array();
	$query = "SELECT id, title, permalink, categ_id, image FROM ".$database_table_prefix."content WHERE status_id = '$activeContentStatusID' AND root_categ_id = '$mobiles_categ_id' ORDER BY rating_value DESC, mobile_release_date DESC LIMIT 12";
	$rs = $conn->query($query);	
	$exist = $rs->num_rows;
	while($row = $rs->fetch_assoc())
		{
		$content_id = $row['id'];
		$content_title = stripslashes($row['title']);
		$content_permalink = $row['permalink'];
		$content_categ_id = $row['categ_id'];
		$content_image = $row['image'];
	
		$CategDetailsArray = getCategDetailsArray($content_categ_id);
		$content_categ_title = stripslashes($CategDetailsArray['title']);
		$content_categ_permalink = stripslashes($CategDetailsArray['permalink']);
	
		$top_rated_mobiles[] = array("id" => $content_id, "title" => $content_title, "permalink" => $content_permalink, "image" => $content_image, "categ_id" => $content_categ_id, "categ_title" => $content_categ_title, "categ_permalink" => $content_categ_permalink);
		}	

return $top_rated_mobiles;		
}


// LATEST NEWS
function latestNews()
{
	global $conn;
	global $database_table_prefix;
	global $news_categ_id;
	global $config_site_media;
	global $activeContentStatusID;

	$latest_news = array();
	$query = "SELECT id, categ_id, title, permalink, image, content_short, datetime_added FROM ".$database_table_prefix."content WHERE categ_id = '$news_categ_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 48";
	$rs = $conn->query($query); $exist = $rs->num_rows; 
	while ($row = $rs->fetch_assoc())
				{
				$content_id = $row['id'];	
				$content_categ_id = $row['categ_id'];
				$content_title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
				$content_permalink = $row['permalink'];
				$content_image = $row['image'];
				$content_short = stripslashes(htmlentities($row['content_short'], ENT_QUOTES));
				$content_date = $row['datetime_added'];
	
				$query_categ = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id' LIMIT 1";
				$rs_categ = $conn->query($query_categ);
				$row = $rs_categ->fetch_assoc();
				$content_categ_title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
				$content_categ_permalink = $row['permalink'];
				
				$latest_news[] = array("id" => $content_id, "title" => $content_title, "content_short" => $content_short, "permalink" => $content_permalink, "image" => $content_image, "date" => DateFormat($content_date), "categ_permalink" => $content_categ_permalink, "categ_title" => $content_categ_title);
				}

return $latest_news;	
}


// LATEST REVIEWS
function latestReviews()
{
	global $conn;
	global $database_table_prefix;
	global $reviews_categ_id;
	global $config_site_media;
	global $activeContentStatusID;

	$latest_reviews = array();
	$query = "SELECT id, categ_id, title, permalink, image, content_short, datetime_added FROM ".$database_table_prefix."content WHERE categ_id = '$reviews_categ_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 48";
	$rs = $conn->query($query); $exist = $rs->num_rows; 
	while ($row = $rs->fetch_assoc())
				{
				$content_id = $row['id'];	
				$content_categ_id = $row['categ_id'];
				$content_title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
				$content_permalink = $row['permalink'];
				$content_image = $row['image'];
				$content_short = stripslashes(htmlentities($row['content_short'], ENT_QUOTES));
				$content_date = $row['datetime_added'];
	
				$query_categ = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id' LIMIT 1";
				$rs_categ = $conn->query($query_categ);
				$row = $rs_categ->fetch_assoc();
				$content_categ_title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
				$content_categ_permalink = $row['permalink'];
				
				$latest_reviews[] = array("id" => $content_id, "title" => $content_title, "content_short" => $content_short, "permalink" => $content_permalink, "image" => $content_image, "date" => DateFormat($content_date), "categ_permalink" => $content_categ_permalink, "categ_title" => $content_categ_title);
				}
				
return $latest_reviews;	
}


// ****************************************************************************************************
// LATEST COMPARES (2 ITEMS)
// ****************************************************************************************************
$latest_compares = array();

$query = "SELECT product1_id, product2_id FROM ".$database_table_prefix."compares WHERE items_compared = 2 AND product1_id != product2_id ORDER BY id DESC LIMIT 10";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
	$product1_id = $row['product1_id'];	
	$product2_id = $row['product2_id'];	
	
	$query_product1 = "SELECT title, permalink, image FROM ".$database_table_prefix."content WHERE id = '$product1_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 1";
	$rs1 = $conn->query($query_product1);
	$row = $rs1->fetch_assoc();
	$product1_title = stripslashes($row['title']);	
	$product1_permalink = stripslashes($row['permalink']);
	$product1_image = $row['image'];

	$query_product2 = "SELECT title, permalink, image FROM ".$database_table_prefix."content WHERE id = '$product2_id' AND status_id = '$activeContentStatusID' ORDER BY id DESC LIMIT 1";
	$rs2 = $conn->query($query_product2);
	$row = $rs2->fetch_assoc();
	$product2_title = stripslashes($row['title']);	
	$product2_permalink = stripslashes($row['permalink']);
	$product2_image = $row['image'];

	$latest_compares[] = array("product1_id" => $product1_id, "product2_id" => $product2_id, "product1_title" => $product1_title, "product2_title" => $product2_title, "product1_permalink" => $product1_permalink, "product2_permalink" => $product2_permalink, "product1_image" => $product1_image, "product2_image" => $product2_image);
	}

$smarty->assign('latest_compares', $latest_compares);


//**************************************************************************************************
// check if exist prices
function checkExistPrices($content_id)
{
	global $conn;
	global $database_table_prefix;

	$query = "SELECT id FROM ".$database_table_prefix."prices WHERE active = 1 AND content_id = '$content_id' LIMIT 1";
	$rs = $conn->query($query); 
	$exist = $rs->num_rows; 
	return $exist;
}



//**************************************************************************************************
// check if exist media
function checkExistMedia($content_id)
{
	global $conn;
	global $database_table_prefix;

	$query = "SELECT id FROM ".$database_table_prefix."media WHERE source = 'content' AND content_id = '$content_id' LIMIT 1";
	$rs = $conn->query($query); 
	$exist = $rs->num_rows; 
	return $exist;
}


//**************************************************************************************************
// number of images for content item
function numberImages($content_id)
{
	global $conn;
	global $database_table_prefix;

	$query = "SELECT id FROM ".$database_table_prefix."media WHERE source = 'content' AND content_id = '$content_id' AND type = 'image'";
	$rs = $conn->query($query); 
	$number = $rs->num_rows; 
	return $number;
}



//**************************************************************************************************
// number of videos for content item
function numberVideos($content_id)
{
	global $conn;
	global $database_table_prefix;

	$query = "SELECT id FROM ".$database_table_prefix."media WHERE source = 'content' AND content_id = '$content_id' AND type = 'video'";
	$rs = $conn->query($query); 
	$number = $rs->num_rows; 
	return $number;
}


// ****************************************
// SPECS ARRAY 
function specsArray($content_id)
{
	global $conn;
	global $database_table_prefix;

	$query = "SELECT cf_array, cf_group_id FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$cf_array = $row['cf_array'];
	$cf_group_id = $row['cf_group_id'];

	if($cf_array!="")
		{
		$specs_array = unserialize($cf_array);
		}
	else
		{	
			$specs_array = array();
			
			$query = "SELECT id, title, description FROM ".$database_table_prefix."cf_sections WHERE group_id = '$cf_group_id' AND active = 1 ORDER BY position ASC";
			$rs = $conn->query($query);
			while ($row = $rs->fetch_assoc())
					{
					$section_id = $row['id'];	
					$section_title = stripslashes($row['title']);
					$edited_description = stripslashes($row['description']);
							
					$query_cf = "SELECT id, title, type, show_in_specs FROM ".$database_table_prefix."cf WHERE group_id = '$cf_group_id' AND section_id = '$section_id' AND active = 1 ORDER BY position ASC";		
					$rs_cf = $conn->query($query_cf);
					while ($row = $rs_cf->fetch_assoc())
						{
						$cf_id = $row['id'];
						$cf_title = stripslashes($row['title']);
						$cf_type = $row['type'];
						$cf_show_in_specs = $row['show_in_specs'];
						
						$query_cf_values = "SELECT cf_id, value, extra FROM ".$database_table_prefix."cf_values WHERE content_id = '$content_id' AND cf_id = '$cf_id' AND group_id = '$cf_group_id' LIMIT 1";
						$rs_cf_values = $conn->query($query_cf_values);
						$row = $rs_cf_values->fetch_assoc();
			
						$edited_cf_id = $row['cf_id'];	
						$edited_cf_value = stripslashes($row['value']);
						$edited_cf_extra = stripslashes($row['extra']);
									
						if($cf_type == "defined")
							{
							$query_cf_defined = "SELECT title FROM ".$database_table_prefix."cf_defined WHERE id = '$cf_value' LIMIT 1";
							$rs_cf_defined = $conn->query($query_cf_defined);
							$row_defined = $rs_cf_defined->fetch_assoc();
							$edited_cf_value = $row_defined['title'];	
							}
							
						if($cf_show_in_specs==0) $cf_value = ""; else $cf_value = $edited_cf_value." ";			
						if($cf_extra) $cf_text = $edited_cf_value.$cf_extra; else $cf_text = $edited_cf_value;
						$cf_text = trim($cf_text);			
						if($cf_text!="")			
							$specs_array[$section_title][$cf_title] = $cf_text;	
						}
					}
		} // end if
	
return $specs_array;
}



// ****************************************
// ITEM PRICES
function Prices($content_id)
{
	global $conn;
	global $database_table_prefix;

	$prices = array();

	$query = "SELECT id, price, details, seller_logo, url FROM ".$database_table_prefix."prices WHERE active = 1 AND content_id = '$content_id' ORDER BY price ASC";
	$rs = $conn->query($query); $exist = $rs->num_rows; 
	while ($row = $rs->fetch_assoc())
			{
			$price_id = $row['id'];	
			$price = $row['price'];
			$seller_logo = $row['seller_logo'];
			$redirect_url = $row['url'];
			$price_details = nl2br(stripslashes($row['details']));
			
			$price = returnPrice($price);
	
			$prices[] = array("seller_logo" => $seller_logo, "price" => $price, "url" => $redirect_url, "details" => $price_details);
			}
return $prices;
}


// ****************************************
// ITEM COMMENTS
function Comments($content_id)
{
	global $conn;
	global $database_table_prefix;

	$post_comments = array();
	
	$query = "SELECT id, user_id, name, website, content, datetime FROM ".$database_table_prefix."comments WHERE approved = 1 AND content_id = '$content_id' ORDER BY id DESC";
	$rs = $conn->query($query); $exist = $rs->num_rows; 
	while ($row = $rs->fetch_assoc())
			{
			$comment_id = $row['id'];	
			$comment_user_id = $row['user_id'];
			$comment_name = stripslashes($row['name']);
			$comment_website = $row['website'];
			$comment = stripslashes(html_entity_decode($row['content']));
			$comment_datetime = $row['datetime'];
			$comment_avatar = "";
			
			if($comment_user_id!=0)
				{
				$query_user = "SELECT name, avatar FROM ".$database_table_prefix."users WHERE id = '$comment_user_id' LIMIT 1";
				$rs_user = $conn->query($query_user); 
				$row = $rs_user->fetch_assoc();
				$comment_name = stripslashes($row['name']);
				$comment_avatar = stripslashes($row['avatar']);
				}
						
			$post_comments[] = array("comment_id" => $comment_id, "comment_name" => $comment_name, "comment_website" => $comment_website, "comment" => $comment, "comment_datetime" => $comment_datetime, "comment_avatar" => $comment_avatar, "comment_user_id" => $comment_user_id);
			}

return $post_comments;
}



// ****************************************
// ITEM MEDIA IMAGES
function ContentMediaImages($content_id)
{
	global $conn;
	global $database_table_prefix;

	$content_media_images = array();
	$query = "SELECT id, title, description, file FROM ".$database_table_prefix."media WHERE content_id = '$content_id' AND type = 'image' AND source = 'content' ORDER BY id DESC";
	$rs = $conn->query($query); 
	$content_number_of_media_images = $rs->num_rows; 
	while ($row = $rs->fetch_assoc())
		{
		$media_id = $row['id'];	
		$media_file = $row['file'];
		$media_title = stripslashes($row['title']);
		$media_description = stripslashes($row['description']);
				
		$content_media_images[] = array("id" => $media_id, "file" => $media_file, "title" => $media_title, "description" => $media_description);
		}

return $content_media_images;
}


// ****************************************
// ITEM MEDIA VIDEOS
function ContentMediaVideos($content_id)
{
	global $conn;
	global $database_table_prefix;

	$content_media_videos = array();
	$query = "SELECT id, type, title, embed_code FROM ".$database_table_prefix."media WHERE content_id = '$content_id' AND type = 'video' AND source = 'content' ORDER BY id DESC";
	$rs = $conn->query($query); 
	$content_number_of_media_videos = $rs->num_rows; 
	while ($row = $rs->fetch_assoc())
		{
		$media_id = $row['id'];	
		$media_type = $row['type'];
		$media_embed_code = $row['embed_code'];
		$media_title = stripslashes($row['title']);
				
		$content_media_videos[] = array("id" => $media_id, "type" => $media_type, "embed_code" => $media_embed_code, "title" => $media_title);
		}
		
return $content_media_videos;
}


