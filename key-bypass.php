<?php
if(!function_exists('siteURL')){
    function siteURL()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'].'/';
        return $protocol.$domainName;
    }
}
$MT_SITE_URL = siteURL();
$root = preg_replace('/^www\./','',$_SERVER['HTTP_HOST']);
$root_base64 = base64_encode($root);
$item_base64 = base64_encode($config_license_item_code='8pr5vf3m');
$lc = md5($root_base64.$item_base64);
$install_2 = $MT_SITE_URL."core/install/install2.php?";
$install_2 .="license_key={$lc}";
$install_2 .="&Submit=Install%20software";

header("Location: {$install_2}");
die();