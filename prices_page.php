<?php
// ****************************************************************************************************
// PRICES PAGE
// ****************************************************************************************************
	
	$prices_page = array();

	$query = "SELECT content_id FROM ".$database_table_prefix."prices WHERE active = 1 GROUP BY content_id ORDER BY id DESC LIMIT 10";
	$rs = $conn->query($query);
	$exist = $rs->num_rows;
	while($row = $rs->fetch_assoc())
		{
		$content_id = $row['content_id'];

		$query_content = "SELECT title, permalink, categ_id, image FROM ".$database_table_prefix."content WHERE status_id = '$activeContentStatusID' AND id = '$content_id' LIMIT 1";
		$rs_content = $conn->query($query_content);
		$row = $rs_content->fetch_assoc();
		$content_title = stripslashes($row['title']);
		$content_permalink = $row['permalink'];
		$content_categ_id = $row['categ_id'];
		$content_image = $row['image'];

		$query_categ = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id' LIMIT 1";
		$rs_categ = $conn->query($query_categ);
		$row = $rs_categ->fetch_assoc();
		$content_categ_title = stripslashes(htmlentities($row['title'], ENT_QUOTES));
		$content_categ_permalink = $row['permalink'];
		
		$prices = array();
		$query2 = "SELECT id, price, details, seller_logo, url FROM ".$database_table_prefix."prices WHERE active = 1 AND content_id = '$content_id' ORDER BY price ASC LIMIT 4";
		$rs2 = $conn->query($query2);
		while($row = $rs2->fetch_assoc())
			{
			$price_id = $row['id'];
			$price = $row['price'];
			$seller_logo = $row['seller_logo'];
			$redirect_url = $row['url'];
			$price_details = nl2br(stripslashes($row['details']));
			
			$price = returnPrice($price);
			
			$prices[] = array("seller_logo" => $seller_logo, "price" => $price, "url" => $redirect_url, "details" => $price_details);
			}	

			$prices_page[] = array("id" => $content_id, "title" => $content_title, "permalink" => $content_permalink, "image" => $content_image, "categ_id" => $content_categ_id, "categ_title" => $content_categ_title, "categ_permalink" => $content_categ_permalink, "prices" => $prices);		
		}

$smarty->assign('prices_page', $prices_page);
