<?php 
session_start();
require "core/core.php";

require_once(__DIR__.'/core/plugins/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->setCompileDir(__DIR__.'/temp/templates_c/');
$smarty->setConfigDir(__DIR__ .'/core/plugins/smarty/configs/');
$smarty->setCacheDir(__DIR__.'/temp/cache/');
$smarty->plugins_dir = array('plugins', __DIR__.'/core/plugins/smarty/plugins/');
					   
// define global site constants
$query = "SELECT name, value FROM ".$database_table_prefix."settings WHERE type='global'";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
   	{
	$define_name = strtoupper($row['name']);
	$define_value = $row['value'];
	$define_value = html_entity_decode($define_value, ENT_QUOTES);
	$smarty->assign($define_name,$define_value);
	}

// Template folder
$config_template_url = $config_site_url.'/content/templates/'.$config_template;
$smarty->setTemplateDir(__DIR__ .'/content/templates/'.$config_template.'/');
$smarty->assign("CONFIG_TEMPLATE_URL",$config_template_url);

// site offline?
if($config_site_offline==1)
	{
	$smarty->display($config_site_offline_tpl_file);
	exit;
	}
	
// function translate
function do_translation ($params, $content, &$smarty, &$repeat)
{
	if (isset($content)) 
  		{			
		global $conn;
		global $database_table_prefix;
		
		// default language
		$sql_lang = "SELECT value FROM ".$database_table_prefix."settings WHERE name = 'config_lang_id' LIMIT 1";
		$rs_lang = $conn->query($sql_lang);
		$row = $rs_lang->fetch_assoc();
		$config_lang_id = $row['value'];

		// load language translation
		$sql_translate = "SELECT lang_key, translate FROM ".$database_table_prefix."lang_translates WHERE lang_id = '$config_lang_id' AND lang_key = '$content' LIMIT 1";
		$rs_translate = $conn->query($sql_translate);
		$row = $rs_translate->fetch_assoc();
		$lang_key = $row['lang_key'];
		$translate = stripslashes($row['translate']);
				
		if(isset($translate) and $translate!='')
			$translation = $translate;
		else
			$translation = $content;	
		
		return html_entity_decode($translation);	
	  	}
}
$smarty->registerPlugin("block", "lang", "do_translation");


//*******************************************************************
// Analytics
//*******************************************************************
if($config_mx_analytics_enabled==1)
	{
		$time = date("Y-m-d H:i:s");
		$ip_source = $_SERVER['REMOTE_ADDR'];
		$actual_url  = 'http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'."{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		
		$query = "INSERT INTO ".$database_table_prefix."visitors_log (id, url, date, ip) VALUES (NULL, '$actual_url','$time', '$ip_source')"; 
		if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
		else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows;}	
	}


$smarty->assign('DISABLE_ADS', 0);

require 'core/TemplateGlobal.php';
require 'global.php';
require 'sidebar.php';
?>
