<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."cf_groups";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Custom Fields Groups</h1> 
          <div class="clear"></div>  
          <a class="btn btn-primary" data-toggle="modal" href="#" data-target=".modal_add_cf_group"><i class="fa fa-plus"></i> Add Custom Fields Group</a></h1>      
            <?php include ("static/modal_add_cf_group.php");?>       
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
	if ($msg =='error_name')
		echo '<p class="bg-danger">Input name</p>';	
	if ($msg =='error_delete_protected')
		echo '<p class="bg-danger">Error! This item can not be deleted</p>';	
	if ($msg =='error_duplicate')
		echo '<p class="bg-danger">There is another item with this title</p>';	
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "No custom fields group";
	}

else
	{
		$page_rows = 50;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, title, permalink, active FROM ".$database_table_prefix."cf_groups ORDER BY title ASC $max";
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th>Group Details</th>
        <th width="260">Custom Fields</th>
        <th width="160">Content</th>
		<th width="120">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $cf_group_id = $row['id'];
			$cf_group_title = stripslashes($row['title']);
			$cf_group_permalink = stripslashes($row['permalink']);
			$cf_group_active = $row['active'];				
        ?>	
        <tr <?php if($cf_group_active==0) echo 'bgcolor="#FEEAD6"';?>>            
            <td>            
            <h4><?php echo $cf_group_title;?></h4> 
            <small>
            Active: <?php if($cf_group_active==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?>
            </small>
            </td>
			
            <td>
            <?php 
            $data_cf = "SELECT id FROM ".$database_table_prefix."cf WHERE group_id = '$cf_group_id'";
			if($conn->query($data_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); }
            $rs_cf = $conn->query($data_cf);
            $number_cf = $rs_cf->num_rows;
            ?>
            <a href="account.php?page=custom_fields_cf&group_id=<?php echo $cf_group_id;?>"><?php echo $number_cf;?> custom fields</a>
            </td>
                    
            <td>
            
            </td>
           
            <td>
            <a data-toggle="modal" href="#" data-target=".modal_edit_cf_group_<?php echo $cf_group_id;?>"><i class="fa fa-pencil fa-fw"></i> Edit group</a>
	        <?php include ("static/modal_edit_cf_group.php");?> 
    	  	<div class="clear"></div>
            <a href="javascript:deleteRecord_<?php echo $cf_group_id;?>('<?php echo $cf_group_id;?>');"><i class="fa fa-trash-o fa-fw"></i> Delete group</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $cf_group_id;?>(RecordId)
			{
				if (confirm('Warning. Group will be deleted. Also, custom fields from content asigned to this group will be removed!')) {
					window.location.href = 'custom_fields_group_delete.php?id=<?php echo $cf_group_id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php 
        }
        ?>
        </tbody>
		</table>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=custom_fields&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=custom_fields&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=custom_fields&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=custom_fields&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->