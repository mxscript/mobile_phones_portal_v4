function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Wrong email address")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Wrong email address")
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Wrong email address")
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Wrong email address")
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Wrong email address")
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Wrong email address")
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Wrong email address")
		    return false
		 }

 		 return true					
}


function ValidateAddCateg(){
	  
	var title=document.AddCateg.title	
	if ((title.value==null)||(title.value=="")){
		alert("Please input category title")
		title.focus()
		return false
	}
	
	var parent_id=document.AddCateg.parent_id	
	if ((parent_id.value==null)||(parent_id.value=="")){
		alert("Please select parent category")
		parent_id.focus()
		return false
	}
		
	return true
}



function ValidateEditCateg(){
	  
	var title=document.EditCateg.title	
	if ((title.value==null)||(title.value=="")){
		alert("Please input category title")
		title.focus()
		return false
	}
	
	var parent_id=document.EditCateg.parent_id	
	if ((parent_id.value==null)||(parent_id.value=="")){
		alert("Please select parent category")
		parent_id.focus()
		return false
	}
		
	return true
}


function ValidateAddPage(){
	  
	var title=document.AddPage.title	
	if ((title.value==null)||(title.value=="")){
		alert("Please input page title")
		title.focus()
		return false
	}
			
	return true
}

function ValidateEditPage(){
	  
	var title=document.EditPage.title	
	if ((title.value==null)||(title.value=="")){
		alert("Please input page title")
		title.focus()
		return false
	}
			
	return true
}


function ValidateAddContent(){
	  
	var title=document.AddContent.title	
	if ((title.value==null)||(title.value=="")){
		alert("Please input title")
		title.focus()
		return false
	}

	var destination_categ_id=document.AddContent.destination_categ_id	
	if ((destination_categ_id.value==null)||(destination_categ_id.value=="")){
		alert("Please select category")
		destination_categ_id.focus()
		return false
	}

	var status_id=document.AddContent.status_id	
	if ((status_id.value==null)||(status_id.value=="")){
		alert("Please select status")
		status_id.focus()
		return false
	}
			
	return true
}
