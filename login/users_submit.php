<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$name = isset($_POST['name']) ? $_POST['name'] : '';
$name = Secure($name);

$permalink = RewriteUrl($name);

$email = isset($_POST['email']) ? $_POST['email'] : '';
$email = Secure($email);

$password = isset($_POST['password']) ? $_POST['password'] : '';
$password = Secure($password);

$role_id = isset($_POST['role_id']) ? $_POST['role_id'] : '';
$role_id = Secure($role_id);

$active = isset($_POST['active']) ? $_POST['active'] : '';
$active = Secure($active);


// check for inputs
if($name=="")
	{
	header("Location:account.php?page=users&msg=error_name");
	exit();
	}
if($email=="")
	{
	header("Location:account.php?page=users&msg=error_email");
	exit();
	}
if($password=="")
	{
	header("Location:account.php?page=users&msg=error_password");
	exit();
	}


// check for duplicate email
$sql = "SELECT id FROM ".$database_table_prefix."users WHERE email LIKE '$email' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=users&msg=error_duplicate_email");
	exit();
	}

$now = date("Y-m-d H:i:s");
$ip_reg = $_SERVER['REMOTE_ADDR'];
$host_reg = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$salt = generateSaltPassword (); //salt
$password_db = generateHashPassword ($password, $salt);
$activation_code = md5(random_code());	

$query = "INSERT INTO ".$database_table_prefix."users (id, email, name, permalink, avatar, bio, password, salt, token, role_id, active, email_verified, activation_code, register_time, register_ip, register_host, protected) VALUES (NULL, '$email', '$name', '$permalink', '', '', '$password_db', '$salt', '', '$role_id', '$active', '1', '$activation_code', '$now', '$ip_reg', '$host_reg', 0)"; 
if($conn->query($query) === false) {
	echo $conn->error;
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}


// get user ID
$query_id = "SELECT id FROM ".$database_table_prefix."users ORDER BY id DESC LIMIT 1";
$rs_id = $conn->query($query_id);
$row = $rs_id->fetch_assoc();
$user_id = $row['id'];
		
// AVATAR
if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/temp/".$image);

		// create avatar image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage($config_img_avatar_width, $config_img_avatar_height, $config_img_avatar_resize); // (options: exact, portrait, landscape, auto, crop) 
		$resizeObj -> saveImage("../content/media/avatars/".$image);
		
		@unlink ("../content/media/temp/".$image);
		$query = "UPDATE ".$database_table_prefix."users SET avatar = '$image' WHERE id = '$user_id' LIMIT 1"; 
		if($conn->query($query) === false) {} else { $affected_rows = $conn->affected_rows;	}

		}
	}

// form OK:
header("Location: account.php?page=users&msg=add_ok");	
exit;