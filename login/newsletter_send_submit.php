<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");
require ("../core/plugins/smtp/PHPMailerAutoload.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

if($config_email_send_option=="php")
	{
	header("Location:account.php?page=newsletter_send&msg=error_smtp&code=$code");
	exit();
	}
	
$code = $_POST["code"];
$code = Secure($code);

$receivers = $_POST["receivers"];
$receivers = Secure($receivers);

if($code=="" or $receivers=="")
	{
	header("Location:account.php?page=newsletter_send&msg=error&code=$code");
	exit();
	}


$query = "SELECT id, subject, message FROM ".$database_table_prefix."nl_newsletters WHERE code = '$code' LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$newsletter_id = $row['id'];
$newsletter_subject = stripslashes($row['subject']);
$newsletter_message = stripslashes($row['message']);
$newsletter_message = html_entity_decode($newsletter_message, ENT_QUOTES);

$now = date("Y-m-d H:i:s");

if($receivers=="defined")
	{
		$subscriber_ids = $_POST["subscriber_ids"];		
		$number_recipients = count($subscriber_ids);		
		$recipients_array = array();
		
		for ($i=0; $i<$number_recipients; $i++)
			{
				$subscriber_id = $subscriber_ids[$i];
				
				$query = "SELECT email FROM ".$database_table_prefix."nl_subscribers WHERE id = '$subscriber_id' LIMIT 1";
				$rs = $conn->query($query);
				$row = $rs->fetch_assoc();
				$subscriber_email = $row['email'];
				if (!filter_var($subscriber_email, FILTER_VALIDATE_EMAIL) === false) array_push($recipients_array, $subscriber_email);

				$query = "INSERT INTO ".$database_table_prefix."nl_sent (id, newsletter_id, subscriber_id, date) VALUES (NULL, '$newsletter_id', '$subscriber_id',  '$now')"; 
				if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
				else { $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }
			} // end for
	} // end if


if($receivers=="all")
	{
	
		$recipients_array = array();
		
		$query = "SELECT id, email FROM ".$database_table_prefix."nl_subscribers WHERE is_banned = 0 ORDER BY id DESC";
		$rs = $conn->query($query);
		while ($row = $rs->fetch_assoc())
			{
			$subscriber_email = $row['email'];
			$subscriber_id = $row['id'];
				
			if (!filter_var($subscriber_email, FILTER_VALIDATE_EMAIL) === false) array_push($recipients_array, $subscriber_email);

			$query = "INSERT INTO ".$database_table_prefix."nl_sent (id, newsletter_id, subscriber_id, date) VALUES (NULL, '$newsletter_id', '$subscriber_id',  '$now')"; 
				if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
				else { $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }
			} // end while
	} // end if


	
$recipients_list = implode (", ", $recipients_array);
$count = count($recipients_array);
//echo $recipients_list;
//exit;



for($i=0; $i<$count;$i++)		
{
		// SMTP MAILER	
		//----------------------------------------------------------------------------------------------------------		
		$mail = new PHPMailer;
		
		$mail->IsSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $config_email_smtp_server;                 // Specify main and backup server
		$mail->Port = $config_email_smtp_port;                                    // Set the SMTP port
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $config_email_smtp_user;                // SMTP username
		$mail->Password = $config_email_smtp_password;                  // SMTP password
		$mail->SMTPSecure = $config_email_smtp_encryption;                            // Enable encryption, 'ssl' also accepted
		
		$mail->setFrom($config_site_email, $config_site_email_name);
		$mail->addReplyTo($config_site_email);
		$mail->AddAddress($recipients_array[$i]);  
			
		$mail->IsHTML(true);                                  // Set email format to HTML
		
		$mail->Subject = $newsletter_subject;
		$mail->Body    = '
		<html>
		<head>
		  <title>'.$newsletter_subject.'</title>
		</head>
		<body>
		  '.$newsletter_message.'
		</body>
		</html>
		';
		$mail->AltBody = $newsletter_subject;
		
		if(!$mail->send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   exit;
		}
} // end for

// form OK:
header("Location: account.php?page=newsletter&msg=send_ok&count=$count");	
exit;
?> 