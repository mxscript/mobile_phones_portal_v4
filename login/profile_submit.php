<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$name = $_POST["name"];
$name = Secure($name);

$permalink = RewriteUrl($name);

$email = $_POST["email"];
$email = Secure($email);

$password = $_POST["password"];
$password = Secure($password);

$bio = $_POST["bio"];
$bio = Secure($bio);

// check for inputs
if($name=="")
	{
	header("Location:account.php?page=profile&msg=error_name");
	exit();
	}
if($email=="")
	{
	header("Location:account.php?page=profile&msg=error_email");
	exit();
	}

// check for duplicate email
$sql = "SELECT id FROM ".$database_table_prefix."users WHERE email LIKE '$email' AND id != '$logged_user_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=profile&msg=error_duplicate_email");
	exit();
	}

if($password!="")
	{
	$salt = generateSaltPassword (); //salt
	$password_db = generateHashPassword ($password, $salt);
	$query = "UPDATE ".$database_table_prefix."users SET password = '$password_db', salt = '$salt' WHERE id = '$logged_user_id' LIMIT 1"; 
	if($conn->query($query) === false){trigger_error('Error: '.$conn->error, E_USER_ERROR);} else {$affected_rows = $conn->affected_rows;}
	}


$query = "UPDATE ".$database_table_prefix."users SET email = '$email', name = '$name', permalink = '$permalink', bio = '$bio'  WHERE id = '$logged_user_id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $affected_rows = $conn->affected_rows;
}


// AVATAR
if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = substr(strrchr($f, '.'), 1);
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/temp/".$image);

		// create avatar image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage($config_img_avatar_width, $config_img_avatar_height, $config_img_avatar_resize); // (options: exact, portrait, landscape, auto, crop) 
		$resizeObj -> saveImage("../content/media/avatars/".$image);
		
		@unlink ("../content/media/temp/".$image);
		$query = "UPDATE ".$database_table_prefix."users SET avatar = '$image' WHERE id = '$logged_user_id' LIMIT 1"; 
		if($conn->query($query) === false) {} else { $affected_rows = $conn->affected_rows;	}

		}
	}
		
// form OK:
header("Location: account.php?page=profile&msg=edit_ok");	
exit;