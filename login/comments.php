<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."comments";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<script language="JavaScript">
function toggle(source) {
  checkboxes = document.getElementsByName('select[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Discussions (<?php echo $rows;?> total)</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Comment modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Comment deleted</p>';
    if ($msg =='approved_ok')
        echo '<p class="bg-info">Comment approved</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "No comment";
	}

else
	{
		$page_rows = 30;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, content_id, user_id, name, email, website, ip, content, datetime, approved FROM ".$database_table_prefix."comments ORDER BY id DESC $max";
		$rs = $conn->query($query);
		?>
		

		<form action="comments_bulk_action.php" method="post">         
        <div class="table-responsive">
        <table class="table table-bordered" id="listings">	
		<thead> 
        <tr>
            <th align="center"><input type="checkbox" onClick="toggle(this)" /></th>
            <th>Comment details</th>
            <th width="240">Author</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $comment_id = $row['id'];
			$content_id = $row['content_id'];
			$user_id = $row['user_id'];
			$email = $row['email'];
			$name = stripslashes($row['name']);
			$website = stripslashes($row['website']);
			$content = stripslashes($row['content']);
			$ip = $row['ip'];
			$datetime = $row['datetime'];
			$approved = $row['approved'];	
			
			$content = strip_tags($content);
			$content2 = substr($content, 0, 500);
			$content2 = html_entity_decode($content2);
			
			if(strlen($website)>80) $website2 = substr($website, 0, 80); else $website2 = $website;

			$query_content = "SELECT title, permalink FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
			$rs_content = $conn->query($query_content);
			$row = $rs_content->fetch_assoc();
   	        $content_title = stripslashes($row['title']); 
			$content_permalink = stripslashes($row['permalink']); 
			
		
			if($user_id!=0)
				{
				$query_user = "SELECT name email, avatar FROM ".$database_table_prefix."users WHERE id = '$user_id' LIMIT 1";
				$rs_user= $conn->query($query_user);
				while($row = $rs_user->fetch_assoc())
					{
		   	        $name = stripslashes($row['name']); 
					$email = stripslashes($row['email']);  
					$avatar = $row['avatar'];
					}	
				}
			
        ?>	
        <tr <?php if($approved==0) echo 'bgcolor="#FFEAE1"';?>>            
			<td align="left" valign="top">                    
			<input name="select[]" type="checkbox" value="<?php echo $comment_id;?>" />
            </td>        
            
            <td>
            <h4><a target="_blank" href="<?php echo $config_site_url."/".$content_id."/".$content_permalink.".html"; ?>"><?php echo $content_title;?></a></h4>
            <small>
            <?php echo DateTimeFormat($datetime);?> | IP: <?php echo $ip;?>
            </small>
            <?php if($website) echo '<br>Website: <a target="_blank" href="'.$website.'">'.$website2.'</a>';?>
            
            <div class="clear"></div>
            <?php echo $content2;?>
            <div class="clear"></div>
            <?php	
            
            if($approved==0) { ?>
            <i class="fa fa-thumbs-o-up fa-fw"></i> <a href="comments_approve.php?id=<?php echo $comment_id;?>&pagenum=<?php echo $pagenum;?>">Approve</a> <?php } ?>
            <i class="fa fa-edit fa-fw"></i> <a href="account.php?page=comments_edit&id=<?php echo $comment_id;?>&pagenum=<?php echo $pagenum;?>">Edit comment</a>
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $comment_id;?>('<?php echo $comment_id;?>');">Delete comment</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $comment_id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'comments_delete.php?id=<?php echo $comment_id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>
			
            <td>
            <small>
            <?php 
			if($user_id==0) 
				{
					echo '<strong>Anonimous</strong><div class="clear"></div>';
					echo $name.'<br>'.$email;
				}
			else
				{
				if($avatar)
					{
					?>
            	    <span style="float: left; margin-right:5px;"><img style="max-width:40px; height:auto;" src="<?php echo $config_site_media;?>/avatars/<?php echo $avatar;?>" /></span>
                	<?php					
					}
				echo '<a href="account.php?page=users_edit&id='.$user_id.'">'.$name.'</a>';
				?>
                <div class="clear"></div>
                <?php echo $email;           
				}
				?> 
			</small>                
            </td>

        </tr>
        <?php
        }
        ?>
        </tbody>
		</table>
        </div>
        
        <div class="clear"></div>
        With selected items:
            <select name="action" class="select">
                <option value="approve">Approve</option>
                <option value="delete">Delete</option>
                <option value="waiting">Waiting</option>    
            </select>    
            
            <input type="hidden" name="page" value="comments" />
            <input type="hidden" name="pagenum" value="<?php echo $pagenum;?>" />
            <input id="submit" name="input" type="submit" value="Apply" />
        </form>        


    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=comments&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=comments&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=comments&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=comments&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->