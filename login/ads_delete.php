<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$id = $_GET["id"];
$id = Secure($id);
$id = (int)$id;

$sql = "DELETE FROM ".$database_table_prefix."ads_manager WHERE id = '$id' AND protected = 0 LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location:account.php?page=ads&mes=delete_ok");
exit();
?>