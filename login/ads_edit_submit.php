<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$id = $_POST['id'];
$id = stripslashes($id);

$code = $_POST['code'];
$code = addslashes($code);

$code = str_replace('##scr_start##', 'script', $code);
$code = str_replace('##scr_end##', '/script', $code);


$name = $_POST['name'];
$name = Secure($name);

$active = $_POST['active'];
$active = Secure($active);

if (!$name)
	{
	header("Location:account.php?page=ads&msg=error_name");
	exit();
	}

if (!$code)
	{
	header("Location:account.php?page=ads&msg=error_code");
	exit();
	}

$query = "UPDATE ".$database_table_prefix."ads_manager SET name = '$name', code = '$code', active = '$active' WHERE id = '$id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

header("Location:account.php?page=ads&msg=edit_ok");
exit;