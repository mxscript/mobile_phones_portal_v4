<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<form action="settings_submit.php" method="post">
<div class="content-wrapper">

        <section class="content-header">
          <h1>Discussion Settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>
	
              
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

                                
                <div class="col-md-4">
	                <div class="form-group">
					<label>Enable site comments</label>
					<select name="enable_site_comments" class="form-control">
                    	<option value="" selected="selected"> - select - </option>
						<option <?php if ($config_enable_site_comments=='0') echo "selected=\"selected\"";?> value="0">Disabled</option>
						<option <?php if ($config_enable_site_comments=='1') echo "selected=\"selected\"";?> value="1">Enabled</option>
                    </select>
                    </div>
				</div>
				
                <div class="col-md-4">
	                <div class="form-group">
					<label>Enable Facebook comments</label>
					<select name="enable_fb_comments" class="form-control">
                    	<option value="" selected="selected"> - select - </option>
						<option <?php if ($config_enable_fb_comments=='0') echo "selected=\"selected\"";?> value="0">Disabled</option>
						<option <?php if ($config_enable_fb_comments=='1') echo "selected=\"selected\"";?> value="1">Enabled</option>
                    </select>
                    </div>
				</div>
                    
				
                <div class="col-md-4">
	                <div class="form-group">
					<label>Facebook APPLICATION ID</label>
					<input type="text" name="fb_app_id" class="form-control" value="<?php echo $config_fb_app_id;?>">
                    </div>
                </div>    


                <div class="col-md-4">
	                <div class="form-group">
					<label>Captcha verification for comments</label>
					<select name="enable_captcha_comments" class="form-control">
                    	<option value="" selected="selected"> - select - </option>
						<option <?php if ($config_enable_captcha_comments=='0') echo "selected=\"selected\"";?> value="0">Captcha disabled</option>
						<option <?php if ($config_enable_captcha_comments=='1') echo "selected=\"selected\"";?> value="1">Captcha enabled</option>
                    </select>
                    </div>
				</div>

                <div class="col-md-4">
	                <div class="form-group">
					<label>Number of comments on page</label>
					<input name="comments_per_page" class="form-control" type="text" value="<?php echo $config_comments_per_page;?>">                    	
                    </div>
				</div>

                <div class="col-md-4">
	                <div class="form-group">
					<label>Comments approvement</label>
					<select name="comments_manually_approved" class="form-control">
                    	<option <?php if ($config_comments_manually_approved=='0') echo "selected=\"selected\"";?> value="0" selected="selected">Approve all comments automatic</option>
                        <option <?php if ($config_comments_manually_approved=='1') echo "selected=\"selected\"";?> value="1">All comment must be manually approved </option>
                        <?php /*?><option <?php if ($config_comments_approvement=='approve_all_logged') echo "selected=\"selected\"";?> value="approve_all_logged">Approve automatic only for logged users</option><?php */?>
                    </select>
                    </div>
				</div>
                
				<?php /*?><div class="col-lg-12">
                <div class="form-group">
                <label>Discussion options:</label>
                                	
                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_login" type="checkbox" name="comments_require_login" <?php if ($config_comments_require_login=='1') echo "checked";?>>
                    <label for="checkbox_login"> Users must be registered and logged in to comment </label>
                    </div>
					                                      
                </div>
                </div><?php */?>                                                                     
                                				
                <div class="col-lg-12">    
                    <div class="form-group">    	
                      	<input type="hidden" name="settings_section" value="comments" />
		            	<input type="hidden" name="return_page" value="settings_comments" />                 		                    
                        <button type="submit" class="btn btn-primary">Change settings</button>		    
                    </div>
                </div>
                    

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->




<?php /*?>        <section class="content-header">
          <h1>Ratings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">
	
              
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">
                                                                  
                <div class="col-md-4">
	                <div class="form-group">
					<label>Enable content ratings</label>
					<select name="enable_site_ratings" class="form-control">
						<option <?php if ($config_enable_site_ratings=='0') echo "selected=\"selected\"";?> value="0">Disabled</option>
						<option <?php if ($config_enable_site_ratings=='1') echo "selected=\"selected\"";?> value="1">Enabled</option>
                    </select>
                    </div>
				</div>

                <div class="col-md-4">
	                <div class="form-group">
					<label>Who can rate</label>
					<select name="site_ratings_persmissions" class="form-control">
						<option <?php if ($config_site_ratings_persmissions=='all') echo "selected=\"selected\"";?> value="all">Anybody can rate content</option>
						<option <?php if ($config_site_ratings_persmissions=='registered') echo "selected=\"selected\"";?> value="registered">Only registered users can rate content</option>
                    </select>
                    </div>
				</div>
                                                                				
                <div class="col-lg-12">    
                    <div class="form-group">    
                      	<input type="hidden" name="settings_section" value="comments" />
		            	<input type="hidden" name="return_page" value="settings_comments" />                 		
                        <button type="submit" class="btn btn-primary">Change settings</button>		    
                    </div>
                </div>

			                        
				<div class="clear"></div>            

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->
<?php */?>

</div><!-- /.content-wrapper -->
</form>   