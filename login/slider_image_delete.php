<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$slide_id = $_GET['slide_id'];
$slide_id = Secure($slide_id);

$slider_id = $_GET['slider_id'];
$slider_id = Secure($slider_id);

// delete images
$query_img = "SELECT image FROM ".$database_table_prefix."sliders_content WHERE id = '$slide_id' LIMIT 1";
$rs_img = $conn->query($query_img);
$row = $rs_img->fetch_assoc();
$image = $row['image'];

@unlink ("../content/media/large/".$image);
@unlink ("../content/media/small/".$image);		

$sql = "DELETE FROM ".$database_table_prefix."sliders_content WHERE id = '$slide_id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=slider_content&msg=delete_ok&slider_id=$slider_id");
exit;
?>