<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");
require ("../core/plugins/simple_html_dom.php");

require ("checklogin.php");
require ("check_permision.php");

$number = 1;

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$subcategories_list = createSubcategories_string(52); // subcategories of mobiles
$search_cond_categ = "AND categ_id IN ($subcategories_list)";

$query = "SELECT id FROM ".$database_table_prefix."content WHERE search_terms = '' AND root_categ_id  = $mobiles_categ_id ORDER BY id ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
		
	ob_start();
	ob_implicit_flush(true);
	set_time_limit(0);

	$content_id = $row['id'];
	
	updateSearchTerms($content_id);
	
	echo '<a name="scrolldown'.$number.'"></a>';
	echo "Content #<strong>".$content_id."</strong> - done<hr>";

	ob_flush();
	flush();
	sleep(0.1);
	ob_end_flush();
	?>
	 <script language="javascript">
	   self.location="#scrolldown<?php echo $number;?>";
	 </script>
	<?php 
	$number = $number+1;
	
	}
	
exit;
?>
