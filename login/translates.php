<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."users";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Translates</h1> 
          <div class="clear"></div>  
          <a class="btn btn-primary btn-flat" data-toggle="modal" href="#" data-target=".modal_add_language"><i class="fa fa-plus"></i> Add new language</a>      
          <?php include ("static/modal_add_language.php");?>       
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Language added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Language modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Language deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
	if ($msg =='error_lang')
		echo '<p class="bg-danger">Input language name</p>';	
	if ($msg =='error_duplicate_lang')
		echo '<p class="bg-danger">There is another language with this name</p>';	
	if ($msg =='error_delete_protected')
		echo '<p class="bg-danger">Error! Default language can not be deleted</p>';	
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "No language";
	}

else
	{
		$page_rows = 50;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, name, is_default FROM ".$database_table_prefix."lang ORDER BY is_default DESC, name ASC $max";
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th>Language details</th>
        <th width="200">Content</th>
		<th width="160">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $id = $row['id'];
			$name = stripslashes($row['name']);
			$is_default = $row['is_default'];			
        ?>	
        <tr>            
            <td>       
            <?php if($config_lang_id == $id) echo '<span style="float:right"><i class="fa fa-flag-o fa-fw"></i> Active language</span>';?>
            <h4><?php echo $name;?></h4> 
            <?php if($is_default==1) echo "<font color='#0099CC'><small>Default language</small></font>"; ?>            
            </td>			
                    
            <td>
            <a class="btn btn-danger btn-flat" href="account.php?page=translates_lang&lang_id=<?php echo $id;?>">Manage translates</a>
            <div class="clear"></div>
            <?php
			$data2 = "SELECT id FROM ".$database_table_prefix."lang_translates WHERE lang_id = '$id'";
			if($conn->query($data2) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); }

			$rs2 = $conn->query($data2);
			$number_content = $rs2->num_rows;
            echo $number_content;?> translates
            </td>
           
            <td>
            <i class="fa fa-pencil fa-fw"></i> <a data-toggle="modal" href="#" data-target=".modal_edit_language_<?php echo $id;?>"> Edit language</a>
        	<?php include ("static/modal_edit_language.php");?> 
            <div class="clear"></div>
            <?php if($is_default!=1) { ?>
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $id;?>('<?php echo $id;?>');">Delete language</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'translates_delete.php?id=<?php echo $id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            <?php } ?>
            </td>
        </tr>
        <?php 
        }
        ?>
        </tbody>
		</table>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=translates&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=translates&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=translates&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=translates&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->