<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$email = $_POST["email"];
$email = Secure($email);
$email_lower = strtolower($email);

$is_banned = $_POST["is_banned"];
$is_banned = Secure($is_banned);

$id = $_POST["id"];
$id = Secure($id);

if($email=="")
	{
	header("Location:account.php?page=newsletter_subscribers&msg=error_email");
	exit();
	}

// check for duplicate email
$sql = "SELECT id FROM ".$database_table_prefix."nl_subscribers WHERE LOWER(email) LIKE '$email_lower' AND id != '$id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=newsletter_subscribers&msg=error_duplicate_email");
	exit();
	}

$now = date("Y-m-d H:i:s");
$ip_reg = $_SERVER['REMOTE_ADDR'];

$query = "UPDATE ".$database_table_prefix."nl_subscribers SET email = '$email', is_banned = '$is_banned' WHERE id = '$id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=newsletter_subscribers&msg=edit_ok");	
exit;
?> 