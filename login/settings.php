<?php
require ("checklogin.php");
require ("check_permision.php");
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Website settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">
		
        <form action="settings_submit.php" method="post">         
        <?php
				if ($msg =='edit_ok')
					echo '<p class="bg-info">Settings changed</p>';		
	            if($msg =='demo_mode')
					echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
		?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

	       
    <div class="row">
        
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> General Settings
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

				<div class="col-md-4">
	                <div class="form-group">
					<label>Site ONLINE / OFFLINE
                    <br />
					<small>Setup if website is available or not</small></label> 
                    <select name="site_offline" class="form-control" onchange="showDiv(this)">						
                    	<option <?php if($config_site_offline==0) echo 'selected="selected"';?> value="0">Site ONLINE</option>
                    	<option <?php if($config_site_offline==1) echo 'selected="selected"';?> value="1">Site OFFLINE</option>
					</select>
                    </div>
                </div>  
                
                <script>
				function showDiv(elem){
			   	if(elem.value == 1)
			      document.getElementById('hidden_div').style.display = "block";				
				if(elem.value == 0)
			      document.getElementById('hidden_div').style.display = "none";
				}
				</script>

				<div id="hidden_div" class="col-md-4" style="display:<?php if($config_site_offline==1) echo 'block'; else echo 'none';?>">
	                <div class="form-group">
					<label>Template file to display when OFFLINE 
                    <br />
					<small>Must be located in active template folder</small></label>
                    <input type="text" name="site_offline_tpl_file" class="form-control" value="<?php echo $config_site_offline_tpl_file;?>">	
                    </div>
                </div>
                                
                <div class="clear"></div>

				<div class="col-md-4">
	                <div class="form-group">
					<label>Show main price<br /><small>(you can setup currency <a href="account.php?page=settings_locale">here</a>)</small></label> 
                    <select name="show_main_prices" class="form-control">
					<option <?php if ($config_show_main_prices==1) echo "selected=\"selected\"";?> value="1">Show devices main price</option>
					<option <?php if ($config_show_main_prices==0) echo "selected=\"selected\"";?> value="0">Don't show devices main price</option>					
                    </select>
                    </div>
                </div>
                                
				<div class="col-md-4">
	                <div class="form-group">
					<label>Active Language<br /><small>You can edit translates in "Appearance -> Translates" menu</small></label> 
                    <select name="lang_id" class="form-control">
						<?php
                        $query_lang = "SELECT id, name FROM ".$database_table_prefix."lang ORDER BY name ASC";
                        $rs_lang = $conn->query($query_lang);
                        while ($row = $rs_lang->fetch_assoc())
                            {
                            $lang_id_selected = $row['id'];
                            $lang_name_selected = stripslashes($row['name']);
                            ?>
                            <option <?php if($config_lang_id==$lang_id_selected) echo 'selected="selected"';?> value="<?php echo $lang_id_selected;?>"><?php echo $lang_name_selected;?></option>
                            <?php
                            }
                        ?>
					</select>
                    </div>
                </div>    


				<div class="col-md-4">
	                <div class="form-group">
					<label>Items on page<br /><small>Used in pagination</small></label> 
                    <input type="text" name="items_on_page" class="form-control" value="<?php echo $config_items_on_page;?>">					
                    </div>
                </div>    

				<div class="clear"></div>
                
				<div class="col-md-4">
	                <div class="form-group">
					<label>Visitors statistics <br /><small>(IP, visits, visit date...)</small></label> 
                    <select name="mx_analytics_enabled" class="form-control">
					<option <?php if ($config_mx_analytics_enabled==1) echo "selected=\"selected\"";?> value="1">Statistics enabled</option>
					<option <?php if ($config_mx_analytics_enabled==0) echo "selected=\"selected\"";?> value="0">Statistics disabled</option>					</select>
                    </div>
                </div>

				<div class="col-md-4">
	                <div class="form-group">
					<label>Search log <br /><small>(statistics about site searches)</small></label> 
                    <select name="search_log_enabled" class="form-control">
					<option <?php if ($config_search_log_enabled==1) echo "selected=\"selected\"";?> value="1">Search log enabled</option>
					<option <?php if ($config_search_log_enabled==0) echo "selected=\"selected\"";?> value="0">Search log disabled</option>					</select>
                    </div>
                </div>

				<div class="col-md-4">
	                <div class="form-group">
					<label>Newsletter system <br /><small>enable or disable newsletter system</small></label> 
                    <?php if ($config_cms_type=='free') { ?>
                    <a class="btn btn-danger btn-flat" href="http://www.mxscripts.com/update-premium-cms.php" target="_blank">Upgrade to premium for this feature</a>
					<?php } else { ?>
                    <select name="newsletter_enabled" class="form-control">
					<option <?php if ($config_newsletter_enabled==1) echo "selected=\"selected\"";?> value="1">Newsletter system enabled</option>
					<option <?php if ($config_newsletter_enabled==0) echo "selected=\"selected\"";?> value="0">Newsletter system disabled</option>					</select>
                    <?php } ?>
                    </div>
                </div>                                
                <div class="clear"></div>
                                
				<div class="col-md-6">
	                <div class="form-group">
					<label>Copyright text<br /><small>used in footer. HTML code is permited</small></label> 					
                    <?php if ($config_cms_type=='free') { ?>
                    <a class="btn btn-danger btn-flat" href="http://www.mxscripts.com/update-premium-cms.php" target="_blank">Upgrade to premium for this feature</a>
					<?php } else { ?>                    
                    <input type="text" name="copyright_code" class="form-control" value="<?php echo htmlentities($config_copyright_code);?>">					
                    <?php } ?>
                    </div>
                </div>                   

				<div class="clear"></div>    
                
				<div class="col-md-4">
	                <div class="form-group">
					<label>Charset encode<br /><small>Recommended: UTF-8</small></label> 
                    <input type="text" name="meta_charset" class="form-control" value="<?php echo $config_meta_charset;?>">					
                    </div>
                </div> 

				<div class="col-md-4">
	                <div class="form-group">
					<label>Direction<br /><small>rtl (right to left - default) or ltr (left to right)</small></label> 
                    <input type="text" name="meta_dir" class="form-control" value="<?php echo $config_meta_dir;?>">					
                    </div>
                </div> 

				<div class="col-md-4">
	                <div class="form-group">
					<label>HTML language code (default: en)<br /><small><a target="_blank" href="http://www.w3schools.com/tags/ref_language_codes.asp">Language codes list</a></small></label> 
                    <input type="text" name="meta_lang" class="form-control" value="<?php echo $config_meta_lang;?>">					
                    </div>
                </div> 
                                      
                <div class="clear"></div>                                				
                
				<div class="col-md-12">
	                <div class="form-group">				
					<label>Footer content<br /><small>Visual editor (insert text, links, images, ...)</small></label>                    
                    <textarea id="content" name="footer_code" rows="10" class="form-control"><?php echo $config_footer_code;?></textarea>
					<script>
                    $(document).ready(function() {
                        $('#content').summernote({
                            height: 200,
                            onImageUpload: function(files, editor, welEditable) {
                                sendFile(files[0], editor, welEditable);
                            }
                        });
                        function sendFile(file, editor, welEditable) {
                            data = new FormData();
                            data.append("file", file);
                            $.ajax({
                                data: data,
                                type: "POST",
                                url: "texteditor_upload.php",
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function(url) {
                                    editor.insertImage(welEditable, url);
                                }
                            });
                        }
                    });
                    </script>
					</div>
                </div>                        


				<div class="col-md-12">
	                <div class="form-group">				
					<label>Footer code</label>  <br />
					<small>You can insert html / javascript code (Google Analytics code, other javascript code...)</small>
                    <textarea name="footer_analytics_code" rows="10" class="form-control"><?php echo $config_footer_analytics_code;?></textarea>
					</div>
                </div> 
                
                                        
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-12 -->
        
        
        
        <div class="col-lg-12">    
	        <div class="form-group">   
            	<input type="hidden" name="settings_section" value="general" />
            	<input type="hidden" name="return_page" value="settings" />                 	
            	<button type="submit" class="btn btn-primary">Change settings</button>		    
            </div>
        </div>
        
        <div class="clear"></div>
                                 
    </div><!-- /.row -->            

    

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
                                
</form>   

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->