<?php
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$sql = "SET NAMES 'utf8'";
$conn->query($sql);

$sql = "SET CHARACTER 'utf8'";
$conn->query($sql);

$settings_section = $_POST["settings_section"];	
$settings_section = Secure($settings_section);

$return_page = $_POST["return_page"];	
$return_page = Secure($return_page);

// ****************************************************************************
// GENERAL SETTINGS
// ****************************************************************************
if($settings_section=="general")
{
	$site_offline = $_POST['site_offline'];
	$site_offline = Secure($site_offline);

	$site_offline_tpl_file = $_POST['site_offline_tpl_file'];
	$site_offline_tpl_file = Secure($site_offline_tpl_file);
	
	$footer_code = $_POST['footer_code'];
	$footer_code = Secure($footer_code);

	$footer_analytics_code = $_POST['footer_analytics_code'];
	$footer_analytics_code = Secure($footer_analytics_code);
	
	$lang_id = $_POST["lang_id"];	
	$lang_id = Secure($lang_id);

	$show_main_prices = $_POST["show_main_prices"];	
	$show_main_prices = Secure($show_main_prices);

	$mx_analytics_enabled = $_POST["mx_analytics_enabled"];	
	$mx_analytics_enabled = Secure($mx_analytics_enabled);

	$search_log_enabled = $_POST["search_log_enabled"];	
	$search_log_enabled = Secure($search_log_enabled);
	
	$newsletter_enabled = $_POST["newsletter_enabled"];	
	$newsletter_enabled = Secure($newsletter_enabled);
	
	$items_on_page = $_POST["items_on_page"];	
	$items_on_page = Secure($items_on_page);
	
	$copyright_code = $_POST["copyright_code"];	
	//$copyright_code = Secure($copyright_code);
	
	$meta_dir = $_POST["meta_dir"];	
	$meta_dir = Secure($meta_dir);
	$meta_dir = strtolower($meta_dir);
	if ($meta_dir == "") $meta_dir = "ltr";
	
	$meta_charset = $_POST["meta_charset"];	
	$meta_charset = Secure($meta_charset);
	
	$meta_lang = $_POST["meta_lang"];	
	$meta_lang = Secure($meta_lang);
	if ($meta_lang == "") $meta_lang = "en";
		
	addSettings ('config_site_offline', $site_offline, "global", 1);	
	addSettings ('config_site_offline_tpl_file', $site_offline_tpl_file, "global", 1);	
	addSettings ('config_footer_code', $footer_code, "global", 1);	
	addSettings ('config_show_main_prices', $show_main_prices, "global", 1);			
	addSettings ('config_footer_analytics_code', $footer_analytics_code, "global", 1);			
	addSettings ('config_mx_analytics_enabled', $mx_analytics_enabled, "global", 1);	
	addSettings ('config_search_log_enabled', $search_log_enabled, "global", 1);	
	addSettings ('config_newsletter_enabled', $newsletter_enabled, "global", 1);			
	addSettings ('config_lang_id', $lang_id, "global", 1);			
	addSettings ('config_items_on_page', $items_on_page, "global", 1);	
	addSettings ('config_copyright_code', $copyright_code, "global", 1);	
	addSettings ('config_meta_dir', $meta_dir, "global", 1);	
	addSettings ('config_meta_charset', $meta_charset, "global", 1);	
	addSettings ('config_meta_lang', $meta_lang, "global", 1);	
}


// ****************************************************************************
// SEO AND SOCIAL SETTINGS
// ****************************************************************************
if($settings_section=="seo_social")
{
	$site_title = $_POST["site_title"];	
	$site_title = Secure($site_title);
	
	$site_description = $_POST["site_description"];	
	$site_description = Secure($site_description);
	
	$site_keywords = $_POST["site_keywords"];	
	$site_keywords = Secure($site_keywords);

	$meta_author = $_POST["meta_author"];	
	$meta_author = Secure($meta_author);

	$fb_app_id = $_POST["fb_app_id"];	
	$fb_app_id = Secure($fb_app_id);
	
	$social_facebook_url = isset($_POST['social_facebook_url']) ? $_POST['social_facebook_url'] : '';
	$social_facebook_url = Secure($social_facebook_url);
	
	$social_twitter_url = isset($_POST['social_twitter_url']) ? $_POST['social_twitter_url'] : '';
	$social_twitter_url = Secure($social_twitter_url);

	$social_googleplus_url = isset($_POST['social_googleplus_url']) ? $_POST['social_googleplus_url'] : '';
	$social_googleplus_url = Secure($social_googleplus_url);

	$social_linkedin_url = isset($_POST['social_linkedin_url']) ? $_POST['social_linkedin_url'] : '';
	$social_linkedin_url = Secure($social_linkedin_url);

	$social_youtube_url = isset($_POST['social_youtube_url']) ? $_POST['social_youtube_url'] : '';
	$social_youtube_url = Secure($social_youtube_url);

	$social_pinterest_url = isset($_POST['social_pinterest_url']) ? $_POST['social_pinterest_url'] : '';
	$social_pinterest_url = Secure($social_pinterest_url);
		
	addSettings ('config_site_title', $site_title, "global", 1);			
	addSettings ('config_site_description', $site_description, "global", 1);			
	addSettings ('config_site_keywords', $site_keywords, "global", 1);			
	addSettings ('config_meta_author', $meta_author, "global", 1);	
	addSettings ('config_fb_app_id', $fb_app_id, "global", 1);	
	addSettings ('config_social_facebook_url', $social_facebook_url, "global", 1);
	addSettings ('config_social_twitter_url', $social_twitter_url, "global", 1);	
	addSettings ('config_social_googleplus_url', $social_googleplus_url, "global", 1);
	addSettings ('config_social_linkedin_url', $social_linkedin_url, "global", 1);		
	addSettings ('config_social_youtube_url', $social_youtube_url, "global", 1);		
	addSettings ('config_social_pinterest_url', $social_pinterest_url, "global", 1);				
}


// ****************************************************************************
// COMMENTS SETTINGS
// ****************************************************************************
if($settings_section=="comments")
{
	$enable_fb_comments = $_POST["enable_fb_comments"];	
	$enable_fb_comments = Secure($enable_fb_comments);
	
	$fb_app_id = $_POST["fb_app_id"];	
	$fb_app_id = Secure($fb_app_id);
	
	$enable_site_comments = $_POST["enable_site_comments"];	
	$enable_site_comments = Secure($enable_site_comments);
	
	$enable_captcha_comments = $_POST['enable_captcha_comments'];
	$enable_captcha_comments = Secure($enable_captcha_comments);
	
	$comments_per_page = $_POST["comments_per_page"];	
	$comments_per_page = Secure($comments_per_page);
	
	$comments_manually_approved = $_POST["comments_manually_approved"];	
	$comments_manually_approved = Secure($comments_manually_approved);
		
	addSettings ('config_enable_site_comments', $enable_site_comments, "global", 1);			
	addSettings ('config_enable_fb_comments', $enable_fb_comments, "global", 1);			
	addSettings ('config_fb_app_id', $fb_app_id, "global", 1);			
	addSettings ('config_enable_captcha_comments', $enable_captcha_comments, "global", 1);			
	addSettings ('config_comments_per_page', $comments_per_page, "global", 1);			
	addSettings ('config_comments_manually_approved', $comments_manually_approved, "global", 1);			
}

// ****************************************************************************
// EMAILS SETTINGS
// ****************************************************************************
if($settings_section=="emails")
{
	$email_send_option = $_POST["email_send_option"];	
	$email_send_option = Secure($email_send_option);
	
	$site_email = $_POST["site_email"];	
	$site_email = Secure($site_email);

	$site_email_name = $_POST["site_email_name"];	
	$site_email_name = Secure($site_email_name);
	
	$email_smtp_server = $_POST["email_smtp_server"];	
	$email_smtp_server = Secure($email_smtp_server);
	
	$email_smtp_user = $_POST['email_smtp_user'];
	$email_smtp_user = Secure($email_smtp_user);
	
	$email_smtp_password = $_POST["email_smtp_password"];	
	$email_smtp_password = Secure($email_smtp_password);
	
	$email_smtp_encryption = $_POST["email_smtp_encryption"];	
	$email_smtp_encryption = Secure($email_smtp_encryption);
	
	$email_smtp_port = $_POST["email_smtp_port"];	
	$email_smtp_port = Secure($email_smtp_port);
	
	addSettings ('config_email_send_option', $email_send_option, "global", 1);			
	addSettings ('config_site_email', $site_email, "global", 1);	
	addSettings ('config_site_email_name', $site_email_name, "global", 1);		
	addSettings ('config_email_smtp_server', $email_smtp_server, "global", 1);			
	addSettings ('config_email_smtp_user', $email_smtp_user, "global", 1);			
	addSettings ('config_email_smtp_password', $email_smtp_password, "global", 1);			
	addSettings ('config_email_smtp_encryption', $email_smtp_encryption, "global", 1);			
	addSettings ('config_email_smtp_port', $email_smtp_port, "global", 1);			
}


// ****************************************************************************
// LOCALE SETTINGS
// ****************************************************************************
if($settings_section=="locale")
{
	$date_format = $_POST["date_format"];	
	$date_format = Secure($date_format);
	$date_format = str_replace('"', '', $date_format);
	
	$currency_money_format = $_POST["currency_money_format"];	
	$currency_money_format = Secure($currency_money_format);
	
	$currency_name = $_POST["currency_name"];	
	$currency_name = Secure($currency_name);
	
	$currency_symbol = $_POST["currency_symbol"];	
	$currency_symbol = Secure($currency_symbol);
	
	$currency_display_style = $_POST["currency_display_style"];	
	$currency_display_style = Secure($currency_display_style);
	
	addSettings ('config_date_format', $date_format, "global", 1);	
	addSettings ('config_currency_money_format', $currency_money_format, "global", 1);			
	addSettings ('config_currency_name', $currency_name, "global", 1);			
	addSettings ('config_currency_symbol', $currency_symbol, "global", 1);			
	addSettings ('config_currency_display_style', $currency_display_style, "global", 1);				
}


// ****************************************************************************
// CONTACT PAGE SETTINGS
// ****************************************************************************
if($settings_section=="contact_page")
{
	$enable_contact_page = $_POST["enable_contact_page"];	
	$enable_contact_page = Secure($enable_contact_page);
	
	$enable_contact_form = $_POST["enable_contact_form"];	
	$enable_contact_form = Secure($enable_contact_form);
	
	$contact_form_send_notification_email = $_POST["contact_form_send_notification_email"];	
	$contact_form_send_notification_email = Secure($contact_form_send_notification_email);
	
	$contact_page_show_map = $_POST["contact_page_show_map"];	
	$contact_page_show_map = Secure($contact_page_show_map);
	
	$contact_page_map_latitude = $_POST["contact_page_map_latitude"];	
	$contact_page_map_latitude = Secure($contact_page_map_latitude);

	$contact_page_map_longitude = $_POST["contact_page_map_longitude"];	
	$contact_page_map_longitude = Secure($contact_page_map_longitude);

	$contact_page_map_zoom = $_POST["contact_page_map_zoom"];	
	$contact_page_map_zoom = Secure($contact_page_map_zoom);

	$contact_page_custom_text = $_POST["contact_page_custom_text"];	
	$contact_page_custom_text = Secure($contact_page_custom_text);
	
	addSettings ('config_enable_contact_page', $enable_contact_page, "global", 1);	
	addSettings ('config_enable_contact_form', $enable_contact_form, "global", 1);			
	addSettings ('config_contact_form_send_notification_email', $contact_form_send_notification_email, "global", 1);			
	addSettings ('config_contact_page_show_map', $contact_page_show_map, "global", 1);			
	addSettings ('config_contact_page_map_latitude', $contact_page_map_latitude, "global", 1);				
	addSettings ('config_contact_page_map_longitude', $contact_page_map_longitude, "global", 1);				
	addSettings ('config_contact_page_map_latitude', $contact_page_map_latitude, "global", 1);				
	addSettings ('config_contact_page_map_zoom', $contact_page_map_zoom, "global", 1);							
	addSettings ('config_contact_page_custom_text', $contact_page_custom_text, "global", 1);							
}


// ****************************************************************************
// HOME PAGE SETTINGS
// ****************************************************************************
if($settings_section=="home_page")
{
	$home_page_custom_text = $_POST["home_page_custom_text"];	
	$home_page_custom_text = Secure($home_page_custom_text);
	
	addSettings ('config_home_page_custom_text', $home_page_custom_text, "global", 1);							
}


// ****************************************************************************
// USERS AND REGISTER SETTINGS
// ****************************************************************************
if($settings_section=="users")
{
	$user_registration_enabled = $_POST["user_registration_enabled"];	
	$user_registration_enabled = Secure($user_registration_enabled);
	if($user_registration_enabled=="on") $user_registration_enabled = 1; else $user_registration_enabled = 0;

	$registration_need_email_validation = $_POST["registration_need_email_validation"];	
	$registration_need_email_validation = Secure($registration_need_email_validation);
	if($registration_need_email_validation=="on") $registration_need_email_validation = 1; else $registration_need_email_validation = 0;

	$registration_need_admin_validation = $_POST["registration_need_admin_validation"];	
	$registration_need_admin_validation = Secure($registration_need_admin_validation);
	if($registration_need_admin_validation=="on") $registration_need_admin_validation = 1; else $registration_need_admin_validation = 0;

	$captcha_registration = $_POST["captcha_registration"];	
	$captcha_registration = Secure($captcha_registration);
	if($captcha_registration=="on") $captcha_registration = 1; else $captcha_registration = 0;
	
	addSettings ('config_user_registration_enabled', $user_registration_enabled, "global", 1);	
	addSettings ('config_registration_need_email_validation', $registration_need_email_validation, "global", 1);	
	addSettings ('config_registration_need_admin_validation', $registration_need_admin_validation, "global", 1);	
	addSettings ('config_captcha_registration', $captcha_registration, "global", 1);		
}

// ****************************************************************************
// MEDIA SETTINGS
// ****************************************************************************
if($settings_section=="media")
{
$categ_id = $_POST["categ_id"];	
$categ_id = Secure($categ_id);

$config_img_large_width = $_POST["config_img_large_width"];	
$config_img_large_width = Secure($config_img_large_width);

$config_img_large_height = $_POST["config_img_large_height"];	
$config_img_large_height = Secure($config_img_large_height);

$config_img_large_resize = $_POST["config_img_large_resize"];	
$config_img_large_resize = Secure($config_img_large_resize);

$config_img_small_width = $_POST["config_img_small_width"];	
$config_img_small_width = Secure($config_img_small_width);

$config_img_small_height = $_POST["config_img_small_height"];	
$config_img_small_height = Secure($config_img_small_height);

$config_img_small_resize = $_POST["config_img_small_resize"];	
$config_img_small_resize = Secure($config_img_small_resize);

$config_img_thumb_width = $_POST["config_img_thumb_width"];	
$config_img_thumb_width = Secure($config_img_thumb_width);

$config_img_thumb_height = $_POST["config_img_thumb_height"];	
$config_img_thumb_height = Secure($config_img_thumb_height);

$config_img_thumb_resize = $_POST["config_img_thumb_resize"];	
$config_img_thumb_resize = Secure($config_img_thumb_resize);

if($categ_id==0) // Global settings
	{
	addSettings ('config_img_large_width', $config_img_large_width, "global", 1);			
	addSettings ('config_img_large_height', $config_img_large_height, "global", 1);			
	addSettings ('config_img_large_resize', $config_img_large_resize, "global", 1);			
	addSettings ('config_img_small_width', $config_img_small_width, "global", 1);			
	addSettings ('config_img_small_height', $config_img_small_height, "global", 1);			
	addSettings ('config_img_small_resize', $config_img_small_resize, "global", 1);			
	addSettings ('config_img_thumb_width', $config_img_thumb_width, "global", 1);			
	addSettings ('config_img_thumb_height', $config_img_thumb_height, "global", 1);			
	addSettings ('config_img_thumb_resize', $config_img_thumb_resize, "global", 1);			
	}
else // Settings for specific category
	{
	addCategExtraUnique ($categ_id, 'config_img_large_width', $config_img_large_width);			
	addCategExtraUnique ($categ_id, 'config_img_large_height', $config_img_large_height);			
	addCategExtraUnique ($categ_id, 'config_img_large_resize', $config_img_large_resize);			
	addCategExtraUnique ($categ_id, 'config_img_small_width', $config_img_small_width);			
	addCategExtraUnique ($categ_id, 'config_img_small_height', $config_img_small_height);			
	addCategExtraUnique ($categ_id, 'config_img_small_resize', $config_img_small_resize);			
	addCategExtraUnique ($categ_id, 'config_img_thumb_width', $config_img_thumb_width);			
	addCategExtraUnique ($categ_id, 'config_img_thumb_height', $config_img_thumb_height);			
	addCategExtraUnique ($categ_id, 'config_img_thumb_resize', $config_img_thumb_resize);			
	}
}


header("Location: account.php?page=$return_page&msg=edit_ok");
exit;
?>
