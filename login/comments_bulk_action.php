<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");
	
$values = $_POST['select'];
$action = $_POST['action'];
$action = Secure($action);

$page = $_POST['page'];
$page = Secure($page);

$pagenum = $_REQUEST['pagenum'];
$pagenum = Secure($pagenum);

if(empty($values)) 
{
	header("Location: account.php?page=$page");
exit;
} 

else
{
$nr = count($values); 
	
for($i=0; $i < $nr; $i++)
	{
	//echo $action."<br>";				
    //echo($values[$i]."<br>");
	$comment_id = $values[$i];

	// get content ID
	$sql = "SELECT id FROM ".$database_table_prefix."comments WHERE id = '$comment_id' LIMIT 1";
	$rs = $conn->query($sql);
	if($conn->query($sql) === false) {trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
	$row = $rs->fetch_assoc();
	$content_id = $row['id'];


	// DELETE *************************************************************************************************
	if($action=="delete")
		{	
			$query = "DELETE FROM ".$database_table_prefix."comments WHERE id = '$comment_id' LIMIT 1"; 
			if($conn->query($query) === false) {
			  trigger_error('Error: '.$conn->error, E_USER_ERROR);
			} else {
			  $affected_rows = $conn->affected_rows;
			}
					
		} // end if($action=="delete")
	// *************************************************************************************************



	// APPROVE *************************************************************************************************
	if($action=="approve")
		{	
		$query = "UPDATE ".$database_table_prefix."comments SET approved = 1 WHERE id = '$comment_id' LIMIT 1"; 
		if($conn->query($query) === false) {
			  trigger_error('Error: '.$conn->error, E_USER_ERROR);
			} else {
			  $affected_rows = $conn->affected_rows;
			}
		} // end if($action=="approve")
	// *************************************************************************************************


	// WAITING APPROVEMENT *************************************************************************************************
	if($action=="waiting")
		{	
		$query = "UPDATE ".$database_table_prefix."comments SET approved = 0 WHERE id = '$comment_id' LIMIT 1"; 
		if($conn->query($query) === false) {
			  trigger_error('Error: '.$conn->error, E_USER_ERROR);
			} else {
			  $affected_rows = $conn->affected_rows;
			}
		} // end if($action=="waiting")
	// *************************************************************************************************



    } // end for
header("Location: account.php?page=$page&status=$status&pagenum=$pagenum");
exit;
}
?>