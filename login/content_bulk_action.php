<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

$status_id_active = getActiveContentStatusID();
$status_id_draft = getDraftContentStatusID();
$status_id_pending = getPendingContentStatusID();
	
$values = $_POST['select'];
$action = $_POST['action'];
$action = Secure($action);

$page = $_POST['page'];
$page = Secure($page);

$pagenum = $_REQUEST['pagenum'];
$pagenum = Secure($pagenum);

// **************************************************
// FILTER BY USER ROLE
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************


if(empty($values)) 
{
	header("Location: account.php?page=$page");
exit;
} 

else
{
$nr = count($values); 
	
for($i=0; $i < $nr; $i++)
	{
	//echo $action."<br>";				
    //echo($values[$i]."<br>");
	$content_id = $values[$i];


	// DELETE *************************************************************************************************
	if($action=="delete")
		{	
			if($logged_user_role=="author" and authorPermissionOK($content_id, $logged_user_id)==0)
			exit;				
			
			$query = "SELECT image FROM ".$database_table_prefix."content WHERE id = '$content_id' $filter_user_role LIMIT 1";
			$rs = $conn->query($query);
			$row = $rs->fetch_assoc();
			$image = $row['image'];
			@unlink ("../content/media/large/".$image);
			@unlink ("../content/media/small/".$image);
			@unlink ("../content/media/thumbs/".$image);

			$sql = "DELETE FROM ".$database_table_prefix."content WHERE id = '$content_id' $filter_user_role LIMIT 1"; 	
			if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
			else { $affected_rows = $conn->affected_rows; }												
		} // end if($action=="delete")
	// *************************************************************************************************



	// APPROVE *************************************************************************************************
	if($action=="approve")
		{			
			$query = "UPDATE ".$database_table_prefix."content SET status_id = '$status_id_active' WHERE id = '$content_id' $filter_user_role LIMIT 1"; 
			$conn->query($query);
			$last_inserted_id = $conn->insert_id;
			$affected_rows = $conn->affected_rows;
		} // end if($action=="approve")
	// *************************************************************************************************


	// PENDING *************************************************************************************************
	if($action=="pending")
		{	
		if($logged_user_role=="author" and authorPermissionOK($content_id, $logged_user_id)==0)
			{}
		else
			{		
			$query = "UPDATE ".$database_table_prefix."content SET status_id = '$status_id_pending' WHERE id = '$content_id' $filter_user_role LIMIT 1"; 
			$conn->query($query);
			$last_inserted_id = $conn->insert_id;
			$affected_rows = $conn->affected_rows;
			}
		} // end if($action=="waiting")
	// *************************************************************************************************

	// DRAFT *************************************************************************************************
	if($action=="draft")
		{	
		if($logged_user_role=="author" and authorPermissionOK($content_id, $logged_user_id)==0)
			{}
		else
			{			
			$query = "UPDATE ".$database_table_prefix."content SET status_id = '$status_id_draft' WHERE id = '$content_id' $filter_user_role LIMIT 1"; 
			$conn->query($query);
			$last_inserted_id = $conn->insert_id;
			$affected_rows = $conn->affected_rows;
			}
		} // end if($action=="waiting")
	// *************************************************************************************************


    } // end for
header("Location: account.php?page=$page&status=$status&pagenum=$pagenum");
exit;
}
?>