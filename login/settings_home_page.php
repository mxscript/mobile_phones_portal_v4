<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<form action="settings_submit.php" method="post">  
<div class="content-wrapper">

        <section class="content-header">
          <h1>Home Page Settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>	
                
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">    
        
             


            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Custom text
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                <div class="col-md-12">
	                <div class="form-group">
					<label>Visual editor</label>
					<textarea id="custom_text" name="home_page_custom_text" rows="5" class="form-control"><?php echo $config_home_page_custom_text;?></textarea>
  					<script>
                    $(document).ready(function() {
                        $('#custom_text').summernote({
                            height: 200,
                            onImageUpload: function(files, editor, welEditable) {
                                sendFile(files[0], editor, welEditable);
                            }
                        });
                        function sendFile(file, editor, welEditable) {
                            data = new FormData();
                            data.append("file", file);
                            $.ajax({
                                data: data,
                                type: "POST",
                                url: "texteditor_upload.php",
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function(url) {
                                    editor.insertImage(welEditable, url);
                                }
                            });
                        }
                    });
                    </script>
                    </div>
				</div>

                                				                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->


		        <div class="form-group">    
            	<input type="hidden" name="settings_section" value="home_page" />
            	<input type="hidden" name="return_page" value="settings_home_page" />                 	            	                	
            	<button type="submit" class="btn btn-primary">Change settings</button>		    
    	        </div>

				<div class="clear"></div>            

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->

</div><!-- /.content-wrapper -->

</form>
