<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."nl_newsletters";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function()
{
	 var dontSort = [];
                $('#listings thead th').each( function () {
                    if ( $(this).hasClass( 'no_sort' )) {
                        dontSort.push( { "bSortable": false } );
                    } else {
                        dontSort.push( null );
                    }
                } );
				
    $('#listings').dataTable({"aoColumns": dontSort, "order": [[ 0, "desc" ]]});	

});
</script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Newsletters (<?php echo $rows;?> messages)
          <span style="float:right"><a class="btn btn-danger btn-flat" href="account.php?page=newsletter_new"><i class="fa fa-edit"></i> Create a newsletter</a> </span></h1> 
          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
	if ($msg =='error_subject')
		echo '<p class="bg-danger">Input subject</p>';	
	if($msg=="send_ok")
			{
			$count = $_GET["count"];
			$count = Secure($count);
			echo '<p class="bg-info">Newsletter sent to '.$count.' recipients</p>';		
			}
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
if ($rows==0)
	{
		echo "No newsletter";
	}

else
	{
		$query = "SELECT id, code, subject, date FROM ".$database_table_prefix."nl_newsletters ORDER BY id DESC";
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th width="40">ID</th>
        <th>Subject</th>
        <th width="140">Sent counter</th>
		<th class="no_sort" width="160">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $id = $row['id'];
			$code = $row['code'];
			$subject = stripslashes($row['subject']);
			$date = $row['date'];
        ?>	
        <tr> 
        	<td>
            <?php echo $id;?>
            </td>
                       
            <td>           
            <h4 style="margin-top:0"><?php echo $subject;?></h4> 
            <small>
            Created: <?php echo DateTimeFormat($date);?>
            </small>
            </td>
			
            <td>
            <?php
			$data2 = "SELECT id FROM ".$database_table_prefix."nl_sent WHERE newsletter_id = '$id'";
			if($conn->query($data2) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); }

			$rs2 = $conn->query($data2);
			$number_content = $rs2->num_rows;
			
            echo $number_content;?>
            <a href="account.php?page=newsletter_sent_log&id=<?php echo $id;?>">View history log</a>
            </td>
           	
            <td>
            <a class="btn btn-info btn-flat" href="account.php?page=newsletter_send&code=<?php echo $code;?>"><i class="fa fa-paper-plane-o"></i> Send newsletter</a>
            <div class="clear"></div>
            <i class="fa fa-pencil fa-fw"></i> <a href="account.php?page=newsletter_edit&id=<?php echo $id;?>">Edit</a>
        	<?php include ("static/modal_edit_subscriber.php");?> 
            
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $id;?>('<?php echo $id;?>');">Delete</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'newsletter_delete.php?id=<?php echo $id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php 
        }
        ?>
        </tbody>
		</table>

		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->