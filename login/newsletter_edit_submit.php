<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$id = $_POST["id"];
$id = Secure($id);

$subject = $_POST["subject"];
$subject = Secure($subject);

$message = $_POST["message"];
$message = addslashes($message);

if($subject=="")
	{
	header("Location:account.php?page=newsletter_edit&msg=error_subject&id=$id");
	exit();
	}

$query = "UPDATE ".$database_table_prefix."nl_newsletters SET subject = '$subject', message = '$message' WHERE id = '$id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=newsletter&msg=edit_ok");	
exit;
?> 