<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

$group_id = isset($_POST['group_id']) ? $_POST['group_id'] : '';
$group_id = Secure($group_id);

$cf_id = isset($_POST['cf_id']) ? $_POST['cf_id'] : '';
$cf_id = Secure($cf_id);

$defined_cf_id = isset($_POST['defined_cf_id']) ? $_POST['defined_cf_id'] : '';
$defined_cf_id = Secure($defined_cf_id);

$section_id = isset($_POST['section_id']) ? $_POST['section_id'] : '';
$section_id = Secure($section_id);

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=custom_fields&msg=error_demo_mode&group_id=$group_id");
	exit();
	}

$name = isset($_POST['name']) ? $_POST['name'] : '';
$name = Secure($name);

$active = isset($_POST['active']) ? $_POST['active'] : '';
$active = Secure($active);
$active = (int)$active;

$position = isset($_POST['position']) ? $_POST['position'] : '';
$position = Secure($position);
$position = (int)$position;

$permalink = RewriteUrl($name);

if($name=="")
	{
	header("Location:account.php?page=custom_fields&msg=error_defined_cf_name&group_id=$group_id");
	exit();
	}

// check for duplicate
$sql = "SELECT id FROM ".$database_table_prefix."cf_defined WHERE permalink LIKE '$permalink' AND group_id = '$group_id' AND section_id =  '$section_id' AND cf_id = '$cf_id' AND id!= '$defined_cf_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=custom_fields&msg=error_duplicate_defined_cf&group_id=$group_id");
	exit();
	}


$query = "UPDATE ".$database_table_prefix."cf_defined SET title = '$name', permalink = '$permalink', active = '$active', position = '$position' WHERE id = '$defined_cf_id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $affected_rows = $conn->affected_rows;
}

//allSpecsTable();
//allSpecsTableRecountData();

// form OK:
header("Location: account.php?page=custom_fields_cf&msg=defined_cf_edit_ok&group_id=$group_id");	
exit;