<?php 
require ("../core/core.php");
require ("../core/functions/functions.images.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$sql = "SET NAMES 'utf8'";
$conn->query($sql);

$sql = "SET CHARACTER 'utf8'";
$conn->query($sql);

$pagenum = isset($_POST['pagenum']) ? $_POST['pagenum'] : '';
$pagenum = Secure($pagenum);

$content_id = isset($_POST['content_id']) ? $_POST['content_id'] : '';
$content_id = Secure($content_id);

$cf_group_id = isset($_POST['cf_group_id']) ? $_POST['cf_group_id'] : '';
$cf_group_id = Secure($cf_group_id);


// **************************************************
// AUTHORS FILTERS
if (authorPermissionOK($content_id, $logged_user_id)==0)
	{
	header("Location: account.php?page=content&msg=no_permission&pagenum=$pagenum");
	exit;
	}
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************


// CUSTOM FIELDS
$query = "DELETE FROM ".$database_table_prefix."cf_values WHERE content_id = '$content_id'"; 
if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows;	}

if($cf_group_id!=0)
	{
	$sql_cf = "SELECT id, title, type FROM ".$database_table_prefix."cf WHERE group_id = '$cf_group_id' AND active = 1";
	$rs_cf = $conn->query($sql_cf);
	while ($row = $rs_cf->fetch_assoc())
			{
           		$cf_id = $row['id'];	
				$cf_title = stripslashes($row['title']);	
				$cf_type = $row['type'];
						
				//$value = $_POST[$cf_id];
				$value = isset($_POST[$cf_id]) ? $_POST[$cf_id] : '';
				$value = addslashes ($value);
				//echo "VALUE: ".stripslashes($value)."<BR>";
				//$value = Secure($value);
				
				$value_extra = isset($_POST[$cf_id."_extra"]) ? $_POST[$cf_id."_extra"] : '';
				//$value_extra = $_POST[$cf_id."_extra"];
				$value_extra = addslashes($value_extra);
				
				$query = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, group_id, cf_id, value, extra) VALUES (NULL, '$content_id', '$cf_group_id', '$cf_id', '$value', '$value_extra')"; 
				//echo $query;
				if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
				else { $last_inserted_id = $conn->insert_id; $affected_rows = $conn->affected_rows;	}

						
			} // end while
	} // end if


updateReleaseDate($content_id);
updateSpecsArray($content_id);

// form OK:
header("Location: account.php?page=content&msg=edit_ok&pagenum=$pagenum");	
exit;
