<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Templates</h1> 
        </section>

        <!-- Main content -->
        <section class="content">

			<?php
            if ($msg =='edit_ok')
                echo '<p class="bg-info">Template modified</p>';
            ?>
        
            <div class="row">
                <div class="col-lg-12">				
        
                    <div class="box box-info">                        
                        <div class="box-body">
        
                            <?php
                            if ($handle = opendir('../content/templates/'))
                                {
                                while (false !== ($entry = readdir($handle)))
                                    {
                                    if ($entry != "." && $entry != "..")
                                        {
                                            ?>
                           					<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
							                <div class="form-group">
											<img src="<?php echo $config_site_url;?>/content/templates/<?php echo $entry;?>/screenshot.jpg" class="img-responsive" style="border: #666 1px solid;" />
                                            <div class="clear"></div>
                                            <center><strong><?php echo $entry;?></strong></center>
                                            <div class="clear"></div>
                                            <?php if ($config_template==$entry) echo '<span style="float:left" class="btn btn-default btn-flat">ACTIVE TEMPLATE</span>'; else echo '<span style="float:left"><a href="templates_activate.php?theme='.$entry.'"><span class="btn btn-danger btn-flat">ACTIVATE</span></a></span>'?>
                                            <span style="float:right"><a target="_blank" class="btn btn-info btn-flat" href="<?php echo $config_site_url;?>/index.php?theme=<?php echo $entry;?>">PREVIEW TEMPLATE</a></span>
						                    </div>
						           			</div>    
                                            
                                            <?php
                                        }
                                    }
                                closedir($handle);
                                }
                            ?>					
        
        
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->