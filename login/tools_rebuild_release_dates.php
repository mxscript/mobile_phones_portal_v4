<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");
require ("../core/plugins/simple_html_dom.php");

require ("checklogin.php");
require ("check_permision.php");

$number = 1;

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$query = "SELECT id, title FROM ".$database_table_prefix."content WHERE mobile_release_date = '0000-00-00' AND root_categ_id = '$mobiles_categ_id' ORDER BY id ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
		
	ob_start();
	ob_implicit_flush(true);
	set_time_limit(0);

    $title = stripslashes($row['title']);
	$content_id = $row['id'];
	
	updateReleaseDate($content_id);

	echo '<a name="scrolldown'.$number.'"></a>';
	echo "<strong>".$title."<br>".$announced."</strong><br>Year: ".$year.", month: ".$month."<hr>";

	ob_flush();
	flush();
	sleep(0.1);
	ob_end_flush();
	?>
	 <script language="javascript">
	   self.location="#scrolldown<?php echo $number;?>";
	 </script>
	<?php 
	$number = $number+1;
	
	}
	
exit;
?>
