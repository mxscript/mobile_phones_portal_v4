<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT email, name, bio, avatar, role_id FROM ".$database_table_prefix."users WHERE id = '$logged_user_id' LIMIT 1";
$rs = $conn->query($query);
$exist = $rs->num_rows;

while($row = $rs->fetch_assoc())
	{
	$email = $row['email'];
	$name = stripslashes($row['name']);
	$bio = stripslashes($row['bio']);
	$avatar = $row['avatar'];
	$role_id = $row['role_id'];	
	}
?>


<div class="content-wrapper">

        <section class="content-header">
          <h1>My Profile</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
	if ($msg =='error_name')
		echo '<p class="bg-danger">Input full name</p>';	
	if ($msg =='error_email')
		echo '<p class="bg-danger">Input valid email</p>';	
	if ($msg =='error_password')
		echo '<p class="bg-danger">Input password</p>';	
	if ($msg =='error_duplicate_email')
		echo '<p class="bg-danger">There is another user with this email address</p>';	
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Profile updated</p>';	
	?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
            	        
				<div class="box-body">

			<?php
			if($exist == 0) echo "Invalid user";
			else
			{
			?>
            <form name="EditUser" action="profile_submit.php" method="post" onsubmit="return ValidateEditUser()" enctype="multipart/form-data">

                        
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Full name</label>
            <input class="form-control" name="name" type="text" value="<?php echo $name;?>" />
            </div>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Email</label>
            <input class="form-control" name="email" type="text" value="<?php echo $email;?>" />
            </div>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Change password (leave empty to not change)</label>
            <input class="form-control" name="password" type="text" />
            </div>
            </div>
			
            <div class="clear"></div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>User bio</label>
            <textarea name="bio" rows="3" class="form-control"><?php echo $bio;?></textarea>
            </div>
            </div>



            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Change avatar</label>
            <input type="file" name="image">
            </div>
            </div>

                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input class="btn btn-primary" name="input" type="submit" value="Edit profile" />
            </div>  
            </div>                      
            </form>
			<?php
            } // END if 
            ?>

			<div class="clear"></div>            

                <?php
               	if($avatar)
					{
					?>
                    <div id="avatar_image">
                            <span style="float: left; margin-right:10px;"><a target="_blank" href="<?php echo $config_site_media;?>/avatars/<?php echo $avatar;?>"><img style="max-width:100px; height:auto;" src="<?php echo $config_site_media;?>/avatars/<?php echo $avatar;?>" /></a></span>
                            <i class="fa fa-trash-o fa-fw"></i> <a class="delete_image" href="profile_remove_avatar.php">Remove avatar</a>
                            <script type="text/javascript">
                            $(function(){
                                $('.delete_image').click(function(){
                                    var id = $(this).attr('id');
                                    
                                    $.ajax({
                                        type: "POST",
                                        url: "profile_remove_avatar.php",                                        
        
                                        success: function() {
												$('#avatar_image').hide();
												$("#image_deleted_text").html("Avatar removed").css('color','red');
                                            }
                                    });
                                    return false;
                                });
                            });
                            </script>  
                    </div>  
                    <div id="image_deleted_text"></div>                      
                	<?php					
					}
					?> 


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->

</div><!-- /.content-wrapper -->