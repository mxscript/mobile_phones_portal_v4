<?php 
require ("checklogin.php");
require ("check_permision.php");

$content_id = isset($_GET['content_id']) ? $_GET['content_id'] : '';
$content_id = Secure($content_id);

$pagenum = isset($_GET['pagenum']) ? $_GET['pagenum'] : '';
$pagenum = Secure($pagenum);

$query = "SELECT title, permalink, categ_id, cf_group_id, image FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
$rs = $conn->query($query);
$exist = $rs->num_rows;
$row = $rs->fetch_assoc();
$title = stripslashes($row['title']);
$permalink = $row['permalink'];
$categ_id = $row['categ_id'];
$cf_group_id = $row['cf_group_id'];
$image = $row['image'];
	
// **************************************************
// AUTHORS FILTERS
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Edit custom fields for "<?php echo $title;?>"</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
	if ($msg =='error_title')
		echo '<p class="bg-danger">Error. Input title</p>';
	if ($msg =='error_categ')
		echo '<p class="bg-danger">Error. Select category</p>';	
	if (authorPermissionOK($content_id, $logged_user_id)==0)
		{
		echo '<p class="bg-danger">ERROR! You don\'t have permission to do that.</p>';
		exit;
		}
		
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
            	                
				<div class="box-body">
                             
            <form action="content_edit_cf_submit.php" method="post" id="cf">            
                                    
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding_l">
	            <?php include ("content_custom_fields.php");?>            
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input type="hidden" name="content_id" value="<?php echo $content_id;?>" />
            <input type="hidden" name="cf_group_id" value="<?php echo $cf_group_id;?>" />
            <input type="hidden" name="pagenum" value="<?php echo $pagenum;?>" />
            <input class="btn btn-primary" name="input" type="submit" value="Edit custom fields" />
                
                <div class="clear"></div>
                
                <script>         
                $('#cf').submit(function() {
                $('#loading').show();
                return true;
                });
                </script>
                
                <div id="loading" style="display:none">
                <small><strong>Note:</strong> It can takes about 15-20 seconds to update custom fields database, so be patient. Page will be redirected automatic after update</small>
                <div class="clear"></div>
                <img src="../content/media/img/loading_big.gif"  />
                </div>
            
            
            </div>  
            </div>
                                              
            </form>
            <div class="clear"></div>

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->