<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$sql = "SET NAMES 'utf8'";
$conn->query($sql);

$sql = "SET CHARACTER 'utf8'";
$conn->query($sql);

$name = isset($_POST['name']) ? $_POST['name'] : '';
$name = Secure($name);

$fetch_lang_id = isset($_POST['fetch_lang_id']) ? $_POST['fetch_lang_id'] : '';
$fetch_lang_id = Secure($fetch_lang_id);
$fetch_lang_id = (int)$fetch_lang_id;

// check for inputs
if($name=="")
	{
	header("Location:account.php?page=translates&msg=error_lang");
	exit();
	}

// check for duplicate
$sql = "SELECT id FROM ".$database_table_prefix."lang WHERE name LIKE '$name' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=translates&msg=error_duplicate_lang");
	exit();
	}

$query = "INSERT INTO ".$database_table_prefix."lang (id, name) VALUES (NULL, '$name')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// fetch data
if($fetch_lang_id!=0)
{
	// get lang ID
	$query_id = "SELECT id FROM ".$database_table_prefix."lang ORDER BY id DESC LIMIT 1";
	$rs_id = $conn->query($query_id);
	$row = $rs_id->fetch_assoc();
	$lang_id = $row['id'];		

	$query = "SELECT id, lang_key, translate FROM ".$database_table_prefix."lang_translates WHERE lang_id = '$fetch_lang_id' ORDER BY id ASC ";
	$rs = $conn->query($query);
	while ($row = $rs->fetch_assoc())
		{
        $id = $row['id'];
        $lang_key = $row['lang_key'];
        $translate = stripslashes($row['translate']);	
		
		$query_insert = "INSERT INTO ".$database_table_prefix."lang_translates (id, lang_id, lang_key, translate) VALUES (NULL, '$lang_id', '$lang_key', '$translate')"; 
		if($conn->query($query_insert) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
		else { $last_inserted_id = $conn->insert_id; $affected_rows = $conn->affected_rows;	}
		}
		
}

// form OK:
header("Location: account.php?page=translates&msg=add_ok");	
exit;
?> 