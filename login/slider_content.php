<?php 
require ("checklogin.php");
require ("check_permision.php");

$slider_id = $_GET["slider_id"];
$slider_id = Secure($slider_id);

$query = "SELECT name, homepage_of_template, content_source, content_defined_ids, content_bg, content_latest_numb_items, content_latest_categ_id FROM ".$database_table_prefix."sliders WHERE id = '$slider_id' LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$name = stripslashes($row['name']);			
$homepage_of_template = $row['homepage_of_template'];
$content_source = $row['content_source'];	
$content_defined_ids = $row['content_defined_ids'];
$content_bg = $row['content_bg'];
$content_latest_numb_items = $row['content_latest_numb_items'];
$content_latest_categ_id = $row['content_latest_categ_id'];			
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Slider Content for <?php echo $name;?></h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Modified</p>';		
	if ($msg =='add_ok')
		echo '<p class="bg-info">Added</p>';		
	if ($msg =='delete_ok')
		echo '<p class="bg-info">Deleted</p>';		
	if ($msg =='error_upload_image')
		echo '<p class="bg-danger">ERROR! Please upload image file</p>';		
	?>
	
              
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

				<?php if($content_source=="manual") { ?>
                <div class="col-md-12">    
                <a class="btn btn-danger" data-toggle="modal" href="#" data-target=".modal_add_slider_image"><i class="fa fa-plus"></i> Add new slider image</a>      
                </div>
                
	          	<?php include ("static/modal_add_slider_image.php");    
                
				$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
				$query = "SELECT id, position, title, content, url, target, image, active FROM ".$database_table_prefix."sliders_content WHERE slider_id = '$slider_id' ORDER BY position ASC";
				$rs = $conn->query($query);
				?>
				<div class="clear"></div>
                
                <div class="col-md-12">   
                <table class="table table-bordered" id="listings">
                <thead> 
                <tr>
                <th width="80">Position</th>
                <th width="200">Image</th>
                <th>Slider details</th>
                <th width="100">Actions</th>
                </tr>
                </thead>
                
                <tbody>
                <?php
                while($row = $rs->fetch_assoc())
                    {
                    $slide_id = $row['id'];
                    $position = $row['position'];
                    $title = stripslashes($row['title']);
                    $content = stripslashes($row['content']);
                    $url = stripslashes($row['url']);
                    $target = $row['target'];
                    $image = $row['image']; 
					$active = $row['active'];                   
                ?>	
                <tr <?php if($active==0) echo 'bgcolor="#FEEAD6"';?>>            
                	<td>
                    <?php echo $position;?>
                    </td>
                    <td>
                    <?php
                    if($image)
                        {
                        ?>
                        <a target="_blank" href="<?php echo $config_site_media;?>/large/<?php echo $image;?>"><img style="max-width:180px; height:auto;" src="<?php echo $config_site_media;?>/small/<?php echo $image;?>" /></a>
                        <?php					
                        }
                    ?>
                    </td>
                    
                    <td>
                    <h4><?php echo $title;?></h4> 
                    <?php if($content) echo $content.'<div class="clear"></div>';?>
                    <small>
                    URL: <a target="_blank" href="<?php echo $url;?>"><?php echo $url;?></a><br />
                    Active: <?php if($active==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?>                    
                    </small>
                    </td>                                                                   
                   
                    <td>
                    <a data-toggle="modal" href="#" data-target=".modal_edit_slider_image_<?php echo $slide_id;?>"><i class="fa fa-edit"></i> Edit</a>
                    <?php include ("static/modal_edit_slider_image.php"); ?>
                    <div class="clear"></div>
                    <a href="javascript:deleteRecord_<?php echo $slide_id;?>('<?php echo $slide_id;?>');"><i class="fa fa-trash-o fa-fw"></i> Delete</a>
                    <script language="javascript" type="text/javascript">
                    function deleteRecord_<?php echo $slide_id;?>(RecordId)
                    {
                        if (confirm('Confirm delete')) {
                            window.location.href = 'slider_image_delete.php?slide_id=<?php echo $slide_id;?>&slider_id=<?php echo $slider_id;?>';
                        }
                    }
                    </script>  
                    </td>
                </tr>
                <?php 
                }
                ?>
                </tbody>
                </table>                  
                </div>
				<?php } ?>
				

				<?php if($content_source=="latest_content") { ?>   
                <form action="slider_content_submit.php" method="post">
                <div class="col-md-4">
	                <div class="form-group">
					<label>Background image</label>
                    <br /><small>located in "/content/media/sliders/" folder</small>
                    <select name="content_bg" class="form-control">
                            <?php
                            if ($handle = opendir('../content/media/sliders/'))
                                {
                                while (false !== ($entry = readdir($handle)))
                                    {
                                    if ($entry != "." && $entry != "..")
                                        {
                                            ?>
                                            <option <?php if ($content_bg==$entry) echo "selected=\"selected\"";?> value="<?php echo $entry;?>"><?php echo $entry;?></option>
                                            <?php
                                        }
                                    }
                                closedir($handle);
                                }
                            ?>					
                    </select>
                    </div>
                </div> 
                                                                             
                <div class="col-md-4">
	                <div class="form-group">
					<label>Number of items</label>
                    <br /><small>Number of slides</small>
					<input type="text" name="content_latest_numb_items" class="form-control" value="<?php echo $content_latest_numb_items;?>">
                    </div>
                </div> 

                <div class="col-md-4">
	                <div class="form-group">
					<label>Content category</label>
                    <br /><small>Select category</small>
					<select name="content_latest_categ_id" class="form-control">
						<option <?php if ($content_latest_categ_id=='0') echo "selected=\"selected\"";?> value="0">All categories</option>
                    </select>
                    </div>
				</div>
                
                <div class="col-lg-12">    
                    <div class="form-group">    	
                      	<input type="hidden" name="slider_id" value="<?php echo $slider_id;?>" />
                        <button type="submit" class="btn btn-primary">Change settings</button>		    
                    </div>
                </div>    
                </form>              
              	<?php } ?>
                                                
				<?php if($content_source=="defined_content") { ?>    
                <form action="slider_content_submit.php" method="post">              
                <div class="col-md-6">
	                <div class="form-group">
					<label>Defined content items (enter content IDs)</label><br />
					<small>Example: 15, 25, 7</small>                    
					<input type="text" name="content_defined_ids" class="form-control" value="<?php echo $content_defined_ids;?>">
                    </div>
                </div>  

                <div class="col-md-6">
	                <div class="form-group">
					<label>Background image</label>
                    <br /><small>located in "/content/media/sliders/" folder</small>
                    <select name="content_bg" class="form-control">
                            <?php
							$dirname = '../content/media/sliders/';
							$dir = opendir($dirname);							
							while(false != ($file = readdir($dir)))
							{
							if($file != "." && $file != ".." && $file != "index.php")
							{
							  $list[] = $file;
							}
							}							
							sort($list);
							
                            foreach($list as $item)
                                        {
                                            ?>
                                            <option <?php if ($content_bg==$item) echo "selected=\"selected\"";?> value="<?php echo $item;?>"><?php echo $item;?></option>
                                            <?php
                                        }

                            ?>					
                    </select>
                    </div>
                </div> 

                                  
                <div class="col-lg-12">    
                    <div class="form-group">    	
                      	<input type="hidden" name="slider_id" value="<?php echo $slider_id;?>" />              		                    
                        <button type="submit" class="btn btn-primary">Change settings</button>		    
                    </div>
                </div> 
                </form>                 
				<?php } ?>
                              
                
                                				
 

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->

</div><!-- /.content-wrapper -->
