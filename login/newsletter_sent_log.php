<?php 
require ("checklogin.php");
require ("check_permision.php");

$id = $_GET["id"];
$id = Secure($id);

$query = "SELECT id FROM ".$database_table_prefix."nl_sent WHERE newsletter_id = '$id'";
$rs = $conn->query($query);
$rows = $rs->num_rows;

$query = "SELECT subject FROM ".$database_table_prefix."nl_newsletters WHERE id = '$id' LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$subject = stripslashes($row['subject']);
?>

<div class="content-wrapper">

        <section class="content-header">
        <span style="float:right"><a class="btn btn-danger btn-flat" href="account.php?page=newsletter"><i class="fa fa-undo"></i> Back to newsletters</a> </span>
          <h1>Sent log for this newsletter (<?php echo $rows;?> total)</h1>          
          Newsletter subject: <strong><?php echo $subject;?></strong>
        </section>

        <!-- Main content -->
        <section class="content">


    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "Log empty";
	}

else
	{
		$page_rows = 30;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, subscriber_id, date FROM ".$database_table_prefix."nl_sent WHERE newsletter_id = '$id' ORDER BY id DESC $max";
		$rs = $conn->query($query);
		?>
		

        <div class="table-responsive">
        <table class="table table-bordered" id="listings">	
		<thead> 
        <tr>
            <th>Subscriber</th>
            <th width="240">Date</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $log_id = $row['id'];
			$subscriber_id = $row['subscriber_id'];
			$date = $row['date'];
			
			$query_sub = "SELECT email FROM ".$database_table_prefix."nl_subscribers WHERE id = '$subscriber_id' LIMIT 1";
			$rs_sub= $conn->query($query_sub);
			$row = $rs_sub->fetch_assoc();
			$subscriber_email = stripslashes($row['email']);  			
        ?>	
        <tr>            			       
            
            <td>
            <h4><a href="account.php?page=newsletter_subscriber_log&id=<?php echo $subscriber_id; ?>"><?php echo $subscriber_email;?></a></h4>
            </td>
            
            <td>
            <?php echo DateTimeFormat($date);?>
            </td>
			
        </tr>
        <?php
        }
        ?>
        </tbody>
		</table>
        </div>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=newsletter_sent_log&pagenum=1&id=$id'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=newsletter_sent_log&pagenum=$previous&id=$id'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=newsletter_sent_log&pagenum=$next&id=$id'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=newsletter_sent_log&pagenum=$last&id=$id'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->