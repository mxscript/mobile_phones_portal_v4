<?php 
require ("../core/core.php");
require ("checklogin.php");
require ("check_permision.php");

$number = 1;

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}


$query_all = "SELECT id, title FROM ".$database_table_prefix."content WHERE root_categ_id = '$mobiles_categ_id' ORDER BY id DESC";
$rs_all = $conn->query($query_all);
while($row = $rs_all->fetch_assoc())
	{
		
	ob_start();
	ob_implicit_flush(true);
	set_time_limit(0);

	$content_id = $row['id'];
	$title= $row['title'];	

	$specs_array = array();
			
	$query = "SELECT id, title FROM ".$database_table_prefix."cf_sections WHERE group_id = '$mobiles_cf_group_id' AND active = 1 ORDER BY position ASC";
	$rs = $conn->query($query);
	while ($row = $rs->fetch_assoc())
				{
				$section_id = $row['id'];	
				$section_title = stripslashes($row['title']);
						
				$query_cf = "SELECT id, title, type FROM ".$database_table_prefix."cf WHERE group_id = '$mobiles_cf_group_id' AND section_id = '$section_id' AND show_in_specs = 1 AND active = 1 ORDER BY position ASC";		
				$rs_cf = $conn->query($query_cf);
				while ($row = $rs_cf->fetch_assoc())
					{
					$cf_id = $row['id'];
					$cf_title = stripslashes($row['title']);
					$cf_type = $row['type'];					
					
					$query_cf_values = "SELECT value, extra FROM ".$database_table_prefix."cf_values WHERE content_id = '$content_id' AND cf_id = '$cf_id' LIMIT 1";
					$rs_cf_values = $conn->query($query_cf_values);
					$row = $rs_cf_values->fetch_assoc();		

					$edited_cf_value = addslashes($row['value']);
					$edited_cf_extra = addslashes($row['extra']);
					
					$cf_value = '';
					if($cf_type == "defined")
						{
						$query_cf_defined = "SELECT title FROM ".$database_table_prefix."cf_defined WHERE id = '$edited_cf_value' LIMIT 1";
						$rs_cf_defined = $conn->query($query_cf_defined);
						$row_defined = $rs_cf_defined->fetch_assoc();
						$edited_cf_value = $row_defined['title'];	
						}
						
					$cf_value = $edited_cf_value." ";			
					if($edited_cf_extra) $cf_text = $edited_cf_value.$edited_cf_extra; else $cf_text = $edited_cf_value;
					$cf_text = trim($cf_text);			
					if($cf_text!="")			
						$specs_array[$section_title][$cf_title] = $cf_text;	
					}
				}
		
	$cf_array = serialize($specs_array);
	
	$query_update = "UPDATE ".$database_table_prefix."content SET cf_array = '$cf_array' WHERE id = '$content_id' LIMIT 1"; 
	if($conn->query($query_update) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
	else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }
	
	
	echo '<a name="scrolldown'.$number.'"></a>';
	echo "<strong>".$title."</strong><br><hr>";	
	
	ob_flush();
	flush();
	sleep(0.3);
	ob_end_flush();
	?>
	 <script language="javascript">
	   self.location="#scrolldown<?php echo $number;?>";
	 </script>
	<?php 
	$number = $number+1;
	
	}
	
exit;
?>
