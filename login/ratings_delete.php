<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

$id = $_REQUEST["id"];
$id = Secure($id);

$pagenum = $_REQUEST["pagenum"];
$pagenum = Secure($pagenum);


if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$query = "DELETE FROM ".$database_table_prefix."content_extra WHERE id = '$id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $affected_rows = $conn->affected_rows;
}

updateContentRating($content_id);

header("Location: account.php?page=ratings&msg=delete_ok&pagenum=$pagenum");	
exit;
?> 