<?php 
require ("checklogin.php");
require ("check_permision.php");

$lang_id = $_GET["lang_id"];
$lang_id = Secure($lang_id);

$query = "SELECT name FROM ".$database_table_prefix."lang WHERE id = '$lang_id' LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$lang_name = stripslashes($row['name']);

$sql = "SET NAMES 'utf8'";
$conn->query($sql);

$sql = "SET CHARACTER 'utf8'";
$conn->query($sql);
?>

<script src="assets/js/jquery.jeditable.js"></script>

<script language="javascript">
$(function() {
  $(".edit_lang").editable("translates_lang_live_edit.php", { 
      submitdata: { _method: "put" },
      select : true,
      submit : 'Modify',
      cssclass : "editable",
      width : "98%",
	  height: "200px",
	  type : "textarea",
      loadtext  : 'Loading ...'
  });  
    
});

var state = 'none';
function showhide(layer_ref) {
if (state == 'block') {
state = 'none';
}
else {
state = 'block';
}
if (document.all) { //IS IE 4 or 5 (or 6 beta)
eval( "document.all." + layer_ref + ".style.display = state");
}
if (document.layers) { //IS NETSCAPE 4 or below
document.layers[layer_ref].display = state;
}
if (document.getElementById &&!document.all) {
hza = document.getElementById(layer_ref);
hza.style.display = state;
}
}

</script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Translates for <?php echo $lang_name;?></h1> 
        </section>

        <!-- Main content -->
        <section class="content">

			<?php
            if ($msg =='add_ok')
                echo '<p class="bg-info">Added</p>';
            if ($msg =='delete_ok')
                echo '<p class="bg-info">Deleted</p>';
            if ($msg =='edit_ok')
                echo '<p class="bg-info">Modified</p>';
            if ($msg =='error_inputs')
                echo '<p class="bg-danger">Error! Input key and translates</p>';
            if ($msg =='error_duplicate_lang_key')
                echo '<p class="bg-danger">Error! This language key already exist</p>';
            ?>

        
            <div class="row">
                <div class="col-lg-12">				
        
                    <div class="box box-info">                        
                        <div class="box-body">
        
                        <a class="btn btn-danger btn-flat" onclick="showhide('form_add_lang_key');"><i class="fa fa-plus"></i> CREATE NEW KEY</a>
                        <div class="clear"></div>

                        <div id="form_add_lang_key" style="display: none;">   
                        <div class="col-lg-10"> 
                            <form action="translates_lang_key_submit.php" method="post">
                            <strong>Language Key</strong>
                            <br />
                            <small>Must be unique phrase</small><br />
                            <input class="form-control" name="lang_key" type="text" />
                            <br />                        
                        
                            <strong>Translated content</strong>
                            <br />
                            <small>You can use html code</small><br />
                            <textarea name="translate" class="form-control"></textarea>
                        
                            <br />
                            <input name="lang_id" type="hidden" value="<?php echo $lang_id;?>" />    
                            <button type="submit" class="btn btn-primary">Add language key</button>
                            </form>
                        </div>    
                        </div>
                        
                        <div class="clear"></div>


                        <table class="table table-bordered" id="listings">	
                        <tr>
                            <th align="left" valign="middle">Language Key (default translation)</th>
                            <th align="left" valign="middle">Content displayed (click to change)</th>
                        </tr>
                        <?php
						$query = "SELECT id, lang_key, translate FROM ".$database_table_prefix."lang_translates WHERE lang_id = '$lang_id' ORDER BY lang_key ASC ";
						$rs = $conn->query($query);
						while ($row = $rs->fetch_assoc())
						{
                            $id = $row['id'];
                            $lang_key = $row['lang_key'];
                            $translate = stripslashes($row['translate']);	
                    
                        ?>	                        
                        <tr id="row_<?php echo $id;?>">
                          
                            <td align="left" valign="top">
                                <?php echo $lang_key;?>
                                <br />
                                <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $id;?>('<?php echo $id;?>');">Delete key</a>
                                <script language="javascript" type="text/javascript">
                                function deleteRecord_<?php echo $id;?>(RecordId)
                                {
                                    if (confirm('Confirm delete')) {
                                        window.location.href = 'translates_delete_key.php?lang_id=<?php echo $lang_id;?>&translate_id=<?php echo $id;?>&pagenum=<?php echo $pagenum;?>';
                                    }
                                }
                                </script>  

                            </td>
                            <td align="left" valign="top">
                                <div style="cursor:pointer; border:#CCC 1px dotted; padding:5px; background-color:#FFF2D5" class="edit_lang" id="<?php echo $id;?>"><?php echo  html_entity_decode($translate);?></div>
                            </td>
                        </tr>                        
                        <?php
                        }
                        ?>
                    </table>

        
        
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->