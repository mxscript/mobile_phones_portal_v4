<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$title = $_POST["title"];
$title = Secure($title);

$content = $_POST["content"];
$content = Secure($content);

$position = $_POST["position"];
$position = Secure($position);

$url = $_POST["url"];
$url = Secure($url);

$target = $_POST["target"];
$target = Secure($target);

$active = $_POST["active"];
$active = Secure($active);

$slider_id = $_POST["slider_id"];
$slider_id = Secure($slider_id);

// IMAGE
if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/temp/".$image);

		// create big image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(1400, 500, "crop"); // (options: exact, portrait, landscape, auto, crop) 
		$resizeObj -> saveImage("../content/media/large/".$image);

		// create small image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(600, 300, "crop"); // (options: exact, portrait, landscape, auto, crop) 
		$resizeObj -> saveImage("../content/media/small/".$image);
		
		@unlink ("../content/media/temp/".$image);
		
		$query = "INSERT INTO ".$database_table_prefix."sliders_content (id, slider_id, position, title, content, url, target, image, active) VALUES (NULL, '$slider_id', '$position', '$title', '$content', '$url', '$target', '$image', '$active')"; 
		if($conn->query($query) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
		else { $last_inserted_id = $conn->insert_id; $affected_rows = $conn->affected_rows;}
		
		}
	}
else
	{
	header("Location:account.php?page=slider_content&msg=error_upload_image&slider_id=$slider_id");
	exit();
	}


// form OK:
header("Location: account.php?page=slider_content&msg=edit_ok&slider_id=$slider_id");	
exit;
?> 