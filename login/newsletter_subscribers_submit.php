<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$email = $_POST["email"];
$email = Secure($email);
$email_lower = strtolower($email);

$is_banned = $_POST["is_banned"];
$is_banned = Secure($is_banned);

if($email=="")
	{
	header("Location:account.php?page=newsletter_subscribers&msg=error_email");
	exit();
	}

// check for duplicate email
$sql = "SELECT id FROM ".$database_table_prefix."nl_subscribers WHERE LOWER(email) LIKE '$email_lower' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=newsletter_subscribers&msg=error_duplicate_email");
	exit();
	}

$now = date("Y-m-d H:i:s");
$ip_reg = $_SERVER['REMOTE_ADDR'];

$query = "INSERT INTO ".$database_table_prefix."nl_subscribers (id, email, register_date, register_ip, is_banned) VALUES (NULL, '$email', '$now', '$ip_reg', '$is_banned')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=newsletter_subscribers&msg=add_ok");	
exit;
?> 