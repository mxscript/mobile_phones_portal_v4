<?php
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

$cf_section_id = $_POST["cf_section_id"];
$cf_section_id = Secure($cf_section_id);

$group_id = $_POST["group_id"];
$group_id = Secure($group_id);

$title = $_POST["title"];
$title = Secure($title);

$description = $_POST["description"];
$description = Secure($description);

$active = $_POST["active"];
$active = Secure($active);

$position = $_POST["position"];
$position = Secure($position);

$permalink = RewriteUrl($title);

if($title=="")
	{
	header("Location:account.php?page=custom_fields_cf&msg=error_name&group_id=$group_id");
	exit();
	}

// check for duplicate
$sql = "SELECT id FROM ".$database_table_prefix."cf_sections WHERE permalink LIKE '$permalink' AND group_id = '$group_id' AND id != '$cf_section_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=custom_fields_cf&msg=error_duplicate&group_id=$group_id");
	exit();
	}


$query = "UPDATE ".$database_table_prefix."cf_sections SET title = '$title', permalink = '$permalink', description = '$description', active = '$active', position = '$position' WHERE id = '$cf_section_id' AND group_id = '$group_id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=custom_fields_cf&msg=section_edit_ok&group_id=$group_id");	
exit;
?> 