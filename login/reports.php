<?php
require ("checklogin.php");
require ("check_permision.php");

$search = isset($_REQUEST['search']) ? $_REQUEST['search'] : '';
$search = Secure($search);		

$search_terms_user = isset($_REQUEST['search_terms_user']) ? $_REQUEST['search_terms_user'] : '';
$search_terms_user = Secure($search_terms_user);		

$query = "SELECT id FROM ".$database_table_prefix."reports";

$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>All reports (<?php echo $rows;?> reports)</h1>          
        </section>

        <!-- Main content -->
        <section class="content">


	<?php
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Report edited</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Report deleted</p>';
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}
if ($rows==0)
	{
		echo "No reports";
	}
else
	{
		$page_rows = 50;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		
	
		$query = "SELECT id, content_id, name, details, date, resolved, ip FROM ".$database_table_prefix."reports ORDER BY id DESC";
			
		if($conn->query($query) === false) {trigger_error('Error: '.$conn->error, E_USER_ERROR);}		
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th width="130">Date</th>
        <th width="260">Content details</th>
        <th>Report details</th>
        <th width="90">Status</th>
		<th width="120">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $report_id = $row['id'];
			$content_id = $row['content_id'];
			$name = stripslashes($row['name']);
			$resolved = $row['resolved'];
			$date = $row['date'];	
			$ip = $row['ip'];	
			$details = stripslashes($row['details']);
						
			$details = strip_tags($details);
			if(strlen($details)>550)
				$details2 = substr($details, 0, 550)."...";
			else
				$details2 = $details;			
				
			$query_d = "SELECT title, permalink, user_id FROM ".$database_table_prefix."articles WHERE id = '$content_id' LIMIT 1";
			$rs_d = $conn->query($query_d);
			$row = $rs_d->fetch_assoc();
            $content_title = $row['title'];
			$content_permalink = $row['permalink'];
			$content_user_id = $row['user_id'];
		
        ?>	
        <tr>       
        	<td>
            <small><?php echo DateTimeFormat($date);?></small>
            </td>
            
            <td>                        
            <?php echo $content_title;?><br />
            <a href="account.php?page=articles_edit&article_id=<?php echo $content_id;?>"><i class="fa fa-check"></i> Content details</a>
            </td>
            
			<td>
            <?php echo "<strong>".$name."</strong>. ";?>
            </a>
            <br />
            <?php			
			echo "<small>".$details2."<br>IP: ".$ip."</small>";
			?>                       
            </td>
            
            <td>
			<?php
			if($resolved==0) 				
				echo '<font color="#FF3300">UNRESOLVED</font>';
			if($resolved==1) 	
				echo '<font color="#3399CC">RESOLVED</font>';

			?>
            </td>                    
            
            <td>    
            <small> 
            <?php
			if($resolved==0){?>
            <i class="fa fa-close fa-check"></i> <a href="reports_set_resolved.php?id=<?php echo $report_id;?>">Set as resolved</a>
            <div class="clear"></div>
            <?php } ?>
                                                               
            <i class="fa fa-close fa-trash"></i> <a href="javascript:deleteRecord_<?php echo $report_id;?>('<?php echo $report_id;?>');">Delete</a>
            </small>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $report_id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'reports_delete.php?id=<?php echo $report_id;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php
        }
        ?>
        </tbody>
		</table>
                


		<?php
        } // END if rows==0						
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->