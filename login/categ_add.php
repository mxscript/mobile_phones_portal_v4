<?php 
require ("checklogin.php");
require ("check_permision.php");

$description = isset($_GET['description']) ? $_GET['description'] : '';
$description_decoded = urldecode($description);

$title = isset($_GET['title']) ? $_GET['title'] : '';
$title_decoded = urldecode($title);

$meta_title = isset($_GET['meta_title']) ? $_GET['meta_title'] : '';
$meta_title_decoded = urldecode($meta_title);

$meta_description = isset($_GET['meta_description']) ? $_GET['meta_description'] : '';
$meta_description_decoded = urldecode($meta_description);

$meta_keywords = isset($_GET['meta_keywords']) ? $_GET['meta_keywords'] : '';
$meta_keywords_decoded = urldecode($meta_keywords);
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Add new category</h1>  
        </section>

        <!-- Main content -->
        <section class="content">

<?php
if ($msg =='error_title')
	echo '<p class="bg-danger">Error. Input title</p>';
if ($msg =='error_section')
	echo '<p class="bg-danger">Error. Select section</p>';
if ($msg =='error_is_content')
	echo '<p class="bg-danger">You can\'t delete because there is some content in this section</p>';
if ($msg =='error_duplicate_title')
	echo '<p class="bg-danger">Error. There is another category in this section with this title</p>';	
if ($msg =='error_duplicate_permalink')
	echo '<p class="bg-danger">Error. There is another category in this section with this permalink</p>';	
?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

				
            <form name="AddCateg" action="categ_submit.php" method="post" onsubmit="return ValidateAddCateg()" enctype="multipart/form-data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Title</label>
            <input class="form-control" name="title" type="text" value="<?php echo $title_decoded;?>" />
            </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Category description</label>
            <textarea name="description" rows="3" class="form-control"><?php echo $description_decoded;?></textarea>
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
	        <label>Select parent category</label>
            <select name="categ_id" class="form-control">
            	<option selected="selected" value="0">[ROOT]</option>
                <?php
                $sql_top = "SELECT id, title FROM ".$database_table_prefix."categories WHERE parent_id = 0 ORDER BY title ASC";
				$rs_top = $conn->query($sql_top);
                while ($row = $rs_top->fetch_assoc())
                	{
                    $top_categ_id = $row['id'];	
                    $top_categ_title = stripslashes($row['title']);
                    ?>
                    <option value="<?php echo $top_categ_id;?>"><?php echo $top_categ_title;?></option>
                    <?php
					get_child_categ($top_categ_id);
                    }
                ?>
            </select>
     	    </div>
			</div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Is active?</label>
            <select name="active" class="form-control">
            <option selected="selected" value="1">YES</option>
            <option value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Show in header menu</label>
            <select name="show_in_menu" class="form-control">
            <option value="1">YES</option>
            <option value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Show in footer</label>
            <select name="show_in_footer" class="form-control">
            <option value="1">YES</option>
            <option value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Position</label>
            <input class="form-control" name="position" type="text" />
            </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Custom template file</label>
            <input class="form-control" name="custom_tpl_file" type="text" />
            </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Image type</label>
            <select name="image_type" class="form-control" onchange="showDiv(this)">
            <option value="">- no image -</option>
            <option value="upload">Upload file</option>
            <option value="fa">Font awesome icon</option>
    		</select>
            </div>
            </div>

                <script>
				function showDiv(elem){
				if(elem.value == "")
					{
				      document.getElementById('hidden_div1').style.display = "none";	
					  document.getElementById('hidden_div2').style.display = "none";				
					}
			   	if(elem.value == "fa")
					{
				      document.getElementById('hidden_div1').style.display = "block";	
					  document.getElementById('hidden_div2').style.display = "none";				
					}
				if(elem.value == "upload")
					{
				      document.getElementById('hidden_div1').style.display = "none";
					  document.getElementById('hidden_div2').style.display = "block";
					}
				}
				</script>

            <div id="hidden_div1" class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display:none">
            <div class="form-group">
            <label>Font awesome code</label>
            <input class="form-control" name="image_fa" type="text" />
            </div>
            </div>

            <div id="hidden_div2" class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display:none">
            <div class="form-group">
            <label>Upload image</label>
            <input class="form-control" name="image" type="file" />
            </div>
            </div>

            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>SEO settings:</h2>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Meta title</label>
            <input class="form-control" name="meta_title" type="text" value="<?php echo $meta_title_decoded;?>" />
            </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Meta description</label>
            <input class="form-control" name="meta_description" type="text" value="<?php echo $meta_description_decoded;?>" />
            </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Meta Keywords</label>
            <input class="form-control" name="meta_keywords" type="text" value="<?php echo $meta_keywords_decoded;?>" />
            </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>URL structure (leave blank to autogenerate permalink)</label>
            <input class="form-control" name="permalink" type="text" />
            </div>
            </div>

                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input class="btn btn-primary" name="input" type="submit" value="Add category" />
            </div>  
            </div>
                      
            </form>
            
            <div class="clear2"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->