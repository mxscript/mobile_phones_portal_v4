<?php 
require ("checklogin.php");
require ("check_permision.php");

$today = date("Y-m-d");
$active_status_id = getActiveContentStatusID();
$pending_status_id = getPendingContentStatusID();
$draft_status_id = getDraftContentStatusID();


$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."content"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_content = $row['numb'];

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."comments"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_comments = $row['numb'];

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."comments WHERE approved = 0"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_pending_comments = $row['numb'];

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."users"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_users = $row['numb'];

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."visitors_log WHERE DATE(date) = '$today'"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_visits_today = $row['numb'];

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."contact_messages WHERE is_read = 0"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_new_contact_messages = $row['numb'];
?>

<script>
$(function() {
	"use strict";
	
       // LINE CHART
       var line = new Morris.Line({
        element: 'line-chart',
        resize: true,
		data: [
		<?php 
		for($i=0; $i<=30; $i++)
			{
				$period_date[$i] = date("Y-m-d", strtotime("-$i day"));	
				//echo $period_date[$i]."<br>";
	
				$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."content WHERE DATE(datetime_added) LIKE '$period_date[$i]'";
				$rs = $conn->query($sql);
				$row = $rs->fetch_assoc();
				$content_count_date[$i] = $row['numb'];
				?>{
				y: "<?php  echo $period_date[$i];?>",
				a: "<?php  echo $content_count_date[$i];?>"
				},
				<?php 
			}
			?>
			],
          xkey: 'y',
          ykeys: ['a'],
          labels: ['Content added'],
          lineColors: ['#3c8dbc'],
          hideHover: 'auto'
		});
});


$(function() {
	"use strict";
	
       // LINE CHART
       var line = new Morris.Bar({
        element: 'visitors-chart',
        resize: true,
		data: [
		<?php 
		for($i=-10; $i<=0; $i++)
			{
				$period_date[$i] = date("Y-m-d", strtotime("+$i day"));	
				//echo $period_date[$i]." - ";
				$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."visitors_log WHERE DATE(date) LIKE '$period_date[$i]'";
				$rs = $conn->query($sql);
				$row = $rs->fetch_assoc();
				$content_count_date[$i] = $row['numb'];
				?>{
				y: "<?php  echo $period_date[$i];?>",
				a: "<?php  echo $content_count_date[$i];?>"
				},
				<?php 
			}
			?>
			],
          xkey: 'y',
          ykeys: ['a'],
          labels: ['Hits'],
          lineColors: ['#3c8dbc'],
          hideHover: 'auto'
		});
});

</script>


<div class="content-wrapper">

        <section class="content-header">
          <h1>Dashboard<small>Summary</small></h1>
        </section>	

        <!-- Main content -->
        <section class="content">

		<?php
		if ($msg =='error_demo_mode' or $msg == 'demo_mode')
		echo '<p class="bg-danger"><strong style="font-size:15px;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ERROR - DEMO MODE ACTIVE!</strong> You can\'t do this operation in DEMO MODE</p><br>';	
		?>


          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $numb_content;?></h3>
                  <p>Content</p>
                </div>
                <div class="icon">
                  <i class="fa fa-th"></i>
                </div>
                <a href="account.php?page=content" class="small-box-footer">View all content <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
			<div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $numb_new_contact_messages;?></h3>
                  <p>New Messages</p>
                </div>
                <div class="icon">
                  <i class="fa fa-envelope-o"></i>
                </div>
                <a href="account.php?page=contact_messages" class="small-box-footer">View all messages <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
                        
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $numb_pending_comments;?></h3>
                  <p>Pending Discussions</p>
                </div>
                <div class="icon">
                  <i class="fa fa-comment-o"></i>
                </div>
                <a href="account.php?page=comments" class="small-box-footer">View all discussions <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
           
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $numb_visits_today;?></h3>
                  <p>Hits today</p>
                </div>
                <div class="icon">
                  <i class="fa fa-flag-o"></i>
                </div>
                <a href="account.php?page=analytics" class="small-box-footer">View all visits <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          
          
          <div class="row">
				<div class="col-lg-12">

                  <!-- LINE CHART -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">Content added in last 30 days (<a href="account.php?page=content">view all</a>)</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body chart-responsive">
                      <div class="chart" id="line-chart" style="height: 250px;"></div>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

          <div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <!-- LINE CHART -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">Visits last 10 days (<a href="account.php?page=analytics">view all</a>)</h3>                      
                    </div>
                    <div class="box-body chart-responsive">
                      <div class="chart" id="visitors-chart" style="height: 250px;"></div>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
				</div>
                
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                  <!-- MX Scripts Info -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">News and promotions from MX Scripts</h3>                      
                    </div>
                    <div class="box-body responsive">
                      <iframe frameborder="0" style="border:none;" width="100%" height="250" src="http://include.mxscripts.com/iframe_dashboard.php"></iframe>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
				</div>
                
		  </div>	


        </section><!-- /.content -->

</div><!-- /.content-wrapper -->
