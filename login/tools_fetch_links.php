<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");
require ("../core/plugins/simple_html_dom.php");

require ("checklogin.php");
require ("check_permision.php");

ini_set('user_agent', 'Googlebot/2.1 (+http://www.google.com/bot.html)');
$number = 0;

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}


$query = "SELECT url, brand_permalink FROM ".$database_table_prefix."arena_urls WHERE brand_permalink LIKE 'yezz' ORDER BY id ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
		
	ob_start();
	ob_implicit_flush(true);
	set_time_limit(0);

    $url = $row['url'];
	$brand_permalink = $row['brand_permalink'];
	
	if (!file_exists('../temp/fetch'.$brand_permalink)) 
		{
			@mkdir('../temp/fetch/'.$brand_permalink, 0777, true);
		}

	$url2 = strrchr($url, "/");
	$filename = '../temp/fetch/'.$brand_permalink.'/'.$url2;
	clearstatcache(); 

	if(!file_exists($filename)) 
		{ 
		   $fp = fopen($filename,"w");  
		   $url_content = file_get_contents($url);
		   fwrite($fp,$url_content);  
		   fclose($fp); 
		   $time = 7;
		}
	else
		$time = 0.2;	

	echo '<a name="scrolldown'.$number.'"></a>';
	echo $url."<br>".$filename."<hr>";

	ob_flush();
	flush();
	sleep($time);
	ob_end_flush();
	?>
	 <script language="javascript">
	   self.location="#scrolldown<?php echo $number;?>";
	 </script>
	<?php
	$number = $number+1;
	
	}
	
exit;
?>
