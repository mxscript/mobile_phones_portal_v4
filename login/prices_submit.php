<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}
	

$sql = "SET NAMES 'utf8'";
$conn->query($sql);

$sql = "SET CHARACTER 'utf8'";
$conn->query($sql);
	
$content_id = isset($_POST['content_id']) ? $_POST['content_id'] : '';
$content_id = Secure($content_id);

$price = isset($_POST['price']) ? $_POST['price'] : '';
$price = Secure($price);

$details = isset($_POST['details']) ? $_POST['details'] : '';
$details = Secure($details);

$url = isset($_POST['url']) ? $_POST['url'] : '';
$url = Secure($url);

$active = isset($_POST['active']) ? $_POST['active'] : '';
$active = Secure($active);

if($price=="")
	{
	header("Location:account.php?page=prices&id=$content_id&msg=error_price");
	exit();
	}

// **************************************************
// AUTHORS FILTERS
if (authorPermissionOK($content_id, $logged_user_id)==0)
	{
	header("Location: account.php?page=content&msg=no_permission&pagenum=$pagenum");
	exit;
	}
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************


$query = "INSERT INTO ".$database_table_prefix."prices (id, content_id, user_id, price, details, url, seller_logo, active) VALUES (NULL, '$content_id', '$logged_user_id', '$price', '$details', '$url', '', '$active')"; 
if($conn->query($query) === false) {
	echo $conn->error;
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}


// get ID
$query_id = "SELECT id FROM ".$database_table_prefix."prices ORDER BY id DESC LIMIT 1";
$rs_id = $conn->query($query_id);
$row = $rs_id->fetch_assoc();
$price_id = $row['id'];
		
// IMAGE
if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		header("Location:account.php?page=prices&id=$content_id&msg=error_logo");
		exit();			
		}
	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/thumbs/".$image);
		
		$query = "UPDATE ".$database_table_prefix."prices SET seller_logo = '$image' WHERE id = '$price_id' LIMIT 1"; 
		if($conn->query($query) === false) {} else { $affected_rows = $conn->affected_rows;	}

		}
	}

// form OK:
header("Location: account.php?page=prices&msg=add_ok&id=$content_id");	
exit;