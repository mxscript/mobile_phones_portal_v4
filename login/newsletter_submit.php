<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$code = $_POST["code"];
$code = Secure($code);

$subject = $_POST["subject"];
$subject = Secure($subject);

$message = $_POST["message"];
$message = addslashes($message);

if($subject=="")
	{
	header("Location:account.php?page=newsletter_new&msg=error_subject");
	exit();
	}

$now = date("Y-m-d H:i:s");

$query = "INSERT INTO ".$database_table_prefix."nl_newsletters (id, code, subject, message, date) VALUES (NULL, '$code', '$subject', '$message', '$now')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=newsletter&msg=add_ok");	
exit;
?> 