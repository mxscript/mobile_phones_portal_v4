<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$theme = $_GET["theme"];
$theme = Secure($theme);

addSettings ('config_template', $theme, "global", 1);

// form OK:
header("Location: account.php?page=templates&msg=edit_ok");	
exit;