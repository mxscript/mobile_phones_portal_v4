<?php 
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
require ("../core/core.php");
require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$title = isset($_POST['title']) ? $_POST['title'] : '';
$title = Secure($title);
$title_encoded = urlencode($title);

$description = isset($_POST['description']) ? $_POST['description'] : '';
$description = Secure($description);
$description_encoded = urlencode($description);

$active = isset($_POST['active']) ? $_POST['active'] : '';
$active = Secure($active);

$show_in_header = isset($_POST['show_in_header']) ? $_POST['show_in_header'] : '';
$show_in_header = Secure($show_in_header);

$show_in_footer = isset($_POST['show_in_footer']) ? $_POST['show_in_footer'] : '';
$show_in_footer = Secure($show_in_footer);

$position = isset($_POST['position']) ? $_POST['position'] : '';
$position = Secure($position);
$position = (int)$position;

$custom_tpl_file = isset($_POST['custom_tpl_file']) ? $_POST['custom_tpl_file'] : '';
$custom_tpl_file = Secure($custom_tpl_file);

$custom_php_file = isset($_POST['custom_php_file']) ? $_POST['custom_php_file'] : '';
$custom_php_file = Secure($custom_php_file);

$redirect_url = isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '';
$redirect_url = Secure($redirect_url);

$meta_title = isset($_POST['meta_title']) ? $_POST['meta_title'] : '';
$meta_title = Secure($meta_title);
$meta_title_encoded = urlencode($meta_title);

$meta_description = isset($_POST['meta_description']) ? $_POST['meta_description'] : '';
$meta_description = Secure($meta_description);
$meta_description_encoded = urlencode($meta_description);

$meta_keywords = isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : '';
$meta_keywords = Secure($meta_keywords);
$meta_keywords_encoded = urlencode($meta_keywords);

$permalink = $_POST["permalink"];
$permalink = Secure($permalink);
if($permalink!="")
	$permalink = RewriteUrl($permalink);
else	
	$permalink = RewriteUrl($title);


if($title=="")
	{
	header("Location:account.php?page=pages_add&msg=error_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}


// check for duplicate page
$sql = "SELECT id FROM ".$database_table_prefix."pages WHERE title LIKE '$title' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;

if($count!=0)
	{
	header("Location:account.php?page=pages_add&msg=error_duplicate_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}


// check for duplicate permalink
$sql = "SELECT id FROM ".$database_table_prefix."pages WHERE permalink LIKE '$permalink' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=pages_add&msg=error_duplicate_permalink&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}

$query = "INSERT INTO ".$database_table_prefix."pages (id, parent_id, title, permalink, description, active, position, show_in_header, show_in_footer, meta_title, meta_description, meta_keywords, custom_tpl_file, custom_php_file, slider_id, enable_comments, redirect_url, show_only_in_template) VALUES (NULL, 0, '$title', '$permalink', '$description', '$active', '$position', '$show_in_header', '$show_in_footer', '$meta_title', '$meta_description', '$meta_keywords', '$custom_tpl_file', '$custom_php_file', 0, 0, '$redirect_url', '')"; 

if($conn->query($query) === false) {
	echo "ERROR ".$conn->error;
	trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=pages&msg=add_ok");	
exit;