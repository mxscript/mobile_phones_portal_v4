<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$where = $_POST["where"];
$where = Secure($where);
	
// LOGO IMAGE
if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		header("Location: account.php?page=logo&msg=error_file");	
		exit;
		}
	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		//$image = $_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/img/".$image);
		if($where=="header") addSettings ('config_site_logo', $image, "global", 1);	
		if($where=="footer") addSettings ('config_site_logo_footer', $image, "global", 1);			
		}
	}

else
		{
		header("Location: account.php?page=logo&msg=error_file");	
		exit;
		}

// form OK:
header("Location: account.php?page=logo&msg=add_ok");	
exit;