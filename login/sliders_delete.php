<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$id = $_GET['id'];
$id = Secure($id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

// delete images
$query_img = "SELECT image FROM ".$database_table_prefix."sliders_content WHERE slider_id = '$id'";
$rs_img = $conn->query($query_img);
while($row = $rs_img->fetch_assoc())
	{
	$image = $row['image'];
	@unlink ("../content/media/large/".$image);
	@unlink ("../content/media/small/".$image);		
	}
	
$sql = "DELETE FROM ".$database_table_prefix."sliders WHERE id = '$id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$sql = "DELETE FROM ".$database_table_prefix."sliders_content WHERE slider_id = '$id'"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=sliders&msg=delete_ok&pagenum=$pagenum");
exit;
?>