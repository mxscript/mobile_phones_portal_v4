<?php
session_start();

$_SESSION['user_token'] = isset($_SESSION['user_token']) ? $_SESSION['user_token'] : '';
$_SESSION['user_logged'] = isset($_SESSION['user_logged']) ? $_SESSION['user_logged'] : '';
$_COOKIE['user_logged'] = isset($_COOKIE['user_logged']) ? $_COOKIE['user_logged'] : '';

if((isset($_SESSION['user_token']) and $_SESSION['user_logged']==1) or (isset($_COOKIE['user_token']) and $_SESSION['user_logged']==1))	{
	// user is logged
	header("location:account.php");
	exit;
	}

require ("../core/core.php");

if($config_user_registration_enabled==0)
	{
		echo "Registration disabled";
		exit;
	}
	
$msg = isset($_REQUEST['msg']) ? $_REQUEST['msg'] : '';
$msg = Secure($msg);

$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
$email = Secure($email);

$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
$name = Secure($name);
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>New account</title>

    <!-- Core CSS -->
    <link href="../core/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/login.css" rel="stylesheet">

    <!-- Checkboxes style -->
    <link href="../core/assets/css/bootstrap-checkbox.css" rel="stylesheet">

	<script language = "Javascript">
    function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Wrong email address")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Wrong email address")
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Wrong email address")
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Wrong email address")
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Wrong email address")
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Wrong email address")
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Wrong email address")
		    return false
		 }

 		 return true					
	}

	function ValidateForm(){
		var emailID=document.frmRegister.email;
		var name=document.frmRegister.name;
		var passw=document.frmRegister.passw;
		var passw2=document.frmRegister.passw2;
		var captcha_box=document.frmRegister.captcha_box;
		
		if ((name.value==null)||(name.value=="")){
			alert("Input your full name")
			name.focus()
			return false
		}	
		if ((emailID.value==null)||(emailID.value=="")){
			alert("Wrong email address")
			emailID.focus()
			return false
		}
		if (echeck(emailID.value)==false){
			emailID.value=""
			emailID.focus()
			return false
		}
		if ((passw.value==null)||(passw.value=="")){
			alert("Choose a password (min 5 characters)")
			passw.focus()
			return false
		}		
		if (passw.value!=passw2.value){
			alert("Password must be the same in both fields")
			passw2.focus()
			return false
		}		
		if ((captcha_box.value==null)||(captcha_box.value=="")){
			alert("Input antispam code")
			captcha_box.focus()
			return false
		}		
		
		return true
 	}
	</script>

</head>

<body>

     <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New account</h3>
                    </div>
                    <div class="panel-body">
                    	<?php 							
						if ($msg =='error_name') echo '<p class="text-danger">Error! Input name.</p>';							
						if ($msg =='error_email') echo '<p class="text-danger">Error! Input valid email.</p>';	
						if ($msg =='error_passw') echo '<p class="text-danger">Error! Input the same password in both fields.</p>';	
						if ($msg =='error_short_passw') echo '<p class="text-danger">Error! Password too short.</p>';	
						if ($msg =='error_duplicate_email') echo '<p class="text-danger">Error! There is another user with this email.</p>';
						if ($msg =='captcha_error') echo '<p class="text-danger">Error! Wrong antispam code.</p>';																										
						?>
                        
                        <form role="form" name="frmRegister" method="post" action="sign-up-submit.php" onSubmit="return ValidateForm()">
                            <fieldset>
                        		
                                <div class="col-md-12">
                                <label>Your full name</label>
                                <input type="text" name="name" value="<?php echo $name;?>" class="form-control" />
                                </div>
                                
                                <div class="clear"></div>
								                                
                                <div class="col-md-12">                                
                                <label>Input your valid email</label><br>
                                <small>Here we will send activation email</small><br />	
                                <input type="text" name="email" value="<?php echo $email;?>" class="form-control" />
                                </div>
                                
                                <div class="clear"></div>

                                <div class="col-md-6">
                                    <label>Password</label>
                                    <input type="password" name="passw" class="form-control" />
                                </div>
                                
                                <div class="col-md-6">    
                                    <label>Password again</label>
                                    <input type="password" name="passw2" class="form-control" />
                                </div>                                     
                                <div class="clear"></div>

                                <div class="col-md-10">
                                    <label>Input antispam code</label><br>
                                        <div class="input-group">
                                        <input name="captcha_box" type="text" class="form-control" placeholder="input antispam code" />
                                        <span class="input-group-btn">
                                        <img src="../core/plugins/captchaimage.php" alt="antispam" class="img_antispam" />
                                        </span>
                                        </div>
                                </div>
                                
                                <div class="clear"></div>
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Register new account" />
                        
                            </fieldset>
                        </form>

                        <div class="clear"></div>                        
                        <i class="fa fa-undo fa-fw"></i> Already registered? <a href="index.php">Account login</a>
                        <br>
                        <i class="fa fa-undo fa-fw"></i> <a href="reset-activation.php">Resend activation email</a>
            
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Core Scripts -->
    <script src="../core/assets/js/jquery-1.10.2.js"></script>
    <script src="../core/assets/js/bootstrap.min.js"></script>

</body>

</html>