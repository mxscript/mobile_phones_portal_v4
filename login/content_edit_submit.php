<?php 
require ("../core/core.php");
require ("../core/functions/functions.images.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$query = "SET SESSION sql_mode = ''";
$conn->query($query);

$pagenum = $_POST["pagenum"];
$pagenum = Secure($pagenum);

$content_id = $_POST["content_id"];
$content_id = Secure($content_id);

$title = $_POST["title"];
$title = Secure($title);
$title_encoded = urlencode($title);

$categ_id = $_POST["categ_id"];
$categ_id = Secure($categ_id);

$content_short = $_POST["content_short"];
$content_short = Secure($content_short);
$content_short_encoded = urlencode($content_short);

$content = $_POST["content"];
$content = Secure($content);
$content_encoded = urlencode($content);

$aditional_info = $_POST["aditional_info"];
$aditional_info = Secure($aditional_info);

$mobile_main_price = $_POST["mobile_main_price"];
$mobile_main_price = Secure($mobile_main_price);

$status_id = $_POST["status_id"];
$status_id = Secure($status_id);

$user_id = $_POST["user_id"];
$user_id = Secure($user_id);

$cf_group_id = $_POST["cf_group_id"];
$cf_group_id = Secure($cf_group_id);

$custom_datetime_added = $_POST["custom_datetime_added"];
$custom_datetime_added = Secure($custom_datetime_added);

$custom_tpl_file = $_POST["custom_tpl_file"];
$custom_tpl_file = Secure($custom_tpl_file);

$meta_title = $_POST["meta_title"];
$meta_title = Secure($meta_title);
$meta_title_encoded = urlencode($meta_title);

$meta_description = $_POST["meta_description"];
$meta_description = Secure($meta_description);
$meta_description_encoded = urlencode($meta_description);

$meta_keywords = $_POST["meta_keywords"];
$meta_keywords = Secure($meta_keywords);
$meta_keywords_encoded = urlencode($meta_keywords);

$sticky = $_POST["sticky"];
$sticky = Secure($sticky);
if($sticky=="on") $sticky = 1; else $sticky = 0;

$disable_comments = $_POST["disable_comments"];
$disable_comments = Secure($disable_comments);
if($disable_comments=="on") $disable_comments = 1; else $disable_comments = 0;

$disable_ratings = $_POST["disable_ratings"];
$disable_ratings = Secure($disable_ratings);
if($disable_ratings=="on") $disable_ratings = 1; else $disable_ratings = 0;

$disable_ads = $_POST["disable_ads"];
$disable_ads = Secure($disable_ads);
if($disable_ads=="on") $disable_ads = 1; else $disable_ads = 0;

$permalink = $_POST["permalink"];
$permalink = Secure($permalink);

if($permalink!="")
	$permalink = RewriteUrl($permalink);
else	
	$permalink = RewriteUrl($title);


// **************************************************
// AUTHORS FILTERS
if (authorPermissionOK($content_id, $logged_user_id)==0)
	{
	header("Location: account.php?page=content&msg=no_permission&pagenum=$pagenum");
	exit;
	}
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************



if($title=="")
	{
	header("Location:account.php?page=content_edit&msg=error_title&title=$title_encoded&content=$content_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded&content_id=$content_id");
	exit();
	}

$now = date("Y-m-d H:i:s");
$root_categ_id = getRootCategIDFromCategID($categ_id);

$query = "UPDATE ".$database_table_prefix."content SET title = '$title', permalink = '$permalink', content_short = '$content_short', content = '$content', aditional_info = '$aditional_info', status_id = '$status_id', categ_id = '$categ_id', root_categ_id = '$root_categ_id', cf_group_id = '$cf_group_id', meta_title = '$meta_title', meta_description = '$meta_description', meta_keywords = '$meta_keywords', datetime_added = '$custom_datetime_added', datetime_modified = '$now', sticky = '$sticky', disable_comments = '$disable_comments', disable_ratings = '$disable_ratings', disable_ads = '$disable_ads', user_id = '$user_id', custom_tpl_file = '$custom_tpl_file', mobile_main_price = '$mobile_main_price' WHERE id = '$content_id' $filter_user_role LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		// remove existing images
        $query_img = "SELECT image FROM ".$database_table_prefix."content WHERE id = '$content_id' $filter_user_role ORDER BY id DESC LIMIT 1";
        $rs_img = $conn->query($query_img);
		$row = $rs_img->fetch_assoc();
        $existing_image = $row['image'];
		@unlink ("../content/media/large/".$existing_image);
		@unlink ("../content/media/small/".$existing_image);
		@unlink ("../content/media/thumbs/".$existing_image);
					
		$code = random_code();
		$permalink2 = substr($permalink, 0, 80);
		$image = $code."-".$permalink2.".".$ext;
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/temp/".$image);

		// create large image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(getResizeWidth_Large($content_id), getResizeHeight_Large($content_id), getResizeType_Large($content_id));
		$resizeObj -> saveImage("../content/media/large/".$image);
	
		// create small image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(getResizeWidth_Small($content_id), getResizeHeight_Small($content_id), getResizeType_Small($content_id));
		$resizeObj -> saveImage("../content/media/small/".$image);

		// create thumb image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(getResizeWidth_Thumb($content_id), getResizeHeight_Thumb($content_id), getResizeType_Thumb($content_id));
		$resizeObj -> saveImage("../content/media/thumbs/".$image);
		
		@unlink ("../content/media/temp/".$image);
	
		$query = "UPDATE ".$database_table_prefix."content SET image = '$image' WHERE id = '$content_id' $filter_user_role ORDER BY id DESC LIMIT 1"; 
		$rs = $conn->query($query); 
		$affected_rows = $conn->affected_rows;	
		}
	}


updateSearchTerms($content_id);

// form OK:
header("Location: account.php?page=content&msg=edit_ok&pagenum=$pagenum");	
exit;
