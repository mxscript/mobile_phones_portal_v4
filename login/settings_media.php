<?php 
require ("checklogin.php");
require ("check_permision.php");

// category ID
$categ_id = isset($_GET['categ_id']) ? $_GET['categ_id'] : '';
$categ_id = Secure($categ_id);
$categ_id = (int)$categ_id;

$return_page = isset($_GET['return_page']) ? $_GET['return_page'] : '';
$return_page = Secure($return_page);
if($return_page=="") $return_page = "settings_media";

$query = "SELECT title FROM ".$database_table_prefix."categories WHERE id = '$categ_id' LIMIT 1";
$rs = $conn->query($query);
$exist = $rs->num_rows;
$row = $rs->fetch_assoc();
$categ_title = stripslashes($row['title']);

if($categ_id!=0) // ID !=0 means settings for a specific categort. ID = 0 Means Global settings
{
	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_large_width' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_large_width = $row['value'];

	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_large_height' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_large_height = $row['value'];

	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_large_resize' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_large_resize = $row['value'];


	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_small_width' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_small_width = $row['value'];

	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_small_height' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_small_height = $row['value'];

	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_small_resize' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_small_resize = $row['value'];


	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_thumb_width' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_thumb_width = $row['value'];

	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_thumb_height' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_thumb_height = $row['value'];

	$query = "SELECT value FROM ".$database_table_prefix."categories_extra WHERE categ_id = '$categ_id' AND name = 'config_img_thumb_resize' LIMIT 1";
	$rs = $conn->query($query);
	$row = $rs->fetch_assoc();
	$config_img_thumb_resize = $row['value'];
	
}
?>

<form action="settings_submit.php" method="post">
<div class="content-wrapper">

        <section class="content-header">
          <h1>Media Settings <?php if ($categ_id==0) echo " - Global Settings"; else echo " for ".$categ_title." section";?></h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>
	

    <div class="row">       
        
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-file-image-o fa-fw"></i> BIG IMAGES                    
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                <div class="col-md-3">
	                <div class="form-group">
					<label>Width (in pixels)</small></label>
					<input type="text" name="config_img_large_width" class="form-control" value="<?php echo $config_img_large_width;?>">
                    </div>
                </div> 

                <div class="col-md-3">
	                <div class="form-group">
					<label>Height (in pixels)</small></label>
					<input type="text" name="config_img_large_height" class="form-control" value="<?php echo $config_img_large_height;?>">
                    </div>
                </div>    
                                             
                <div class="col-md-4">
	                <div class="form-group">
					<label>Crop / resize options:</label>
					<select name="config_img_large_resize" class="form-control">
                    <option selected="selected" value=""> - Select - </option>
                    <option <?php if ($config_img_large_resize=='crop') echo "selected=\"selected\"";?> value="crop"> Crop </option>
                    <option <?php if ($config_img_large_resize=='auto') echo "selected=\"selected\"";?> value="auto"> Auto </option>
                    <option <?php if ($config_img_large_resize=='portrait') echo "selected=\"selected\"";?> value="portrait"> Portrait </option>
                    <option <?php if ($config_img_large_resize=='landscape') echo "selected=\"selected\"";?> value="landscape"> Landscape </option>
                    <option <?php if ($config_img_large_resize=='exact') echo "selected=\"selected\"";?> value="exact"> Exact </option>
					</select>
                    </div>
				</div>
                                      
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-12 -->



        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-file-image-o fa-fw"></i> SMALL IMAGES  
					<br />
					<small>Strongly recomended resize option: <strong>crop</strong></small>                                      
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                <div class="col-md-3">
	                <div class="form-group">
					<label>Width (in pixels)</small></label>
					<input type="text" name="config_img_small_width" class="form-control" value="<?php echo $config_img_small_width;?>">
                    </div>
                </div> 

                <div class="col-md-3">
	                <div class="form-group">
					<label>Height (in pixels)</small></label>
					<input type="text" name="config_img_small_height" class="form-control" value="<?php echo $config_img_small_height;?>">
                    </div>
                </div>    
                                             
                <div class="col-md-4">
	                <div class="form-group">
					<label>Crop / resize options:</label>
					<select name="config_img_small_resize" class="form-control">
                    <option selected="selected" value=""> - Select - </option>
                    <option <?php if ($config_img_small_resize=='crop') echo "selected=\"selected\"";?> value="crop"> Crop </option>
                    <option <?php if ($config_img_small_resize=='auto') echo "selected=\"selected\"";?> value="auto"> Auto </option>
                    <option <?php if ($config_img_small_resize=='portrait') echo "selected=\"selected\"";?> value="portrait"> Portrait </option>
                    <option <?php if ($config_img_small_resize=='landscape') echo "selected=\"selected\"";?> value="landscape"> Landscape </option>
                    <option <?php if ($config_img_small_resize=='exact') echo "selected=\"selected\"";?> value="exact"> Exact </option>
					</select>
                    </div>
				</div>
                                      
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-12 -->
        

        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-file-image-o fa-fw"></i> THUMBS IMAGES   
                    <br />
					<small>Recomended: Width: <strong>120</strong>, Height: <strong>120</strong>, Resize option: <strong>crop</strong></small>
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                <div class="col-md-3">
	                <div class="form-group">
					<label>Width (in pixels)</small></label>
					<input type="text" name="config_img_thumb_width" class="form-control" value="<?php echo $config_img_thumb_width;?>">
                    </div>
                </div> 

                <div class="col-md-3">
	                <div class="form-group">
					<label>Height (in pixels)</small></label>
					<input type="text" name="config_img_thumb_height" class="form-control" value="<?php echo $config_img_thumb_height;?>">
                    </div>
                </div>    
                                             
                <div class="col-md-4">
	                <div class="form-group">
					<label>Crop / resize options:</label>
					<select name="config_img_thumb_resize" class="form-control">
                    <option selected="selected" value=""> - Select - </option>
                    <option <?php if ($config_img_thumb_resize=='crop') echo "selected=\"selected\"";?> value="crop"> Crop </option>
                    <option <?php if ($config_img_thumb_resize=='auto') echo "selected=\"selected\"";?> value="auto"> Auto </option>
                    <option <?php if ($config_img_thumb_resize=='portrait') echo "selected=\"selected\"";?> value="portrait"> Portrait </option>
                    <option <?php if ($config_img_thumb_resize=='landscape') echo "selected=\"selected\"";?> value="landscape"> Landscape </option>
                    <option <?php if ($config_img_thumb_resize=='exact') echo "selected=\"selected\"";?> value="exact"> Exact </option>
					</select>
                    </div>
				</div>
                                      
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-12 -->        
                
        <div class="col-lg-12">    
	        <div class="form-group">  
            	<input type="hidden" name="categ_id" value="<?php echo $categ_id;?>" /> 
                <input type="hidden" name="return_page" value="<?php echo $return_page;?>" />
                <input type="hidden" name="settings_section" value="media" /> 
            	<button type="submit" class="btn btn-primary">Change settings</button>		    
            </div>
        </div>
        
        <div class="clear"></div>
                                 
    </div><!-- /.row -->            

              
         
        </section><!-- /.content -->



</div><!-- /.content-wrapper -->
</form>   