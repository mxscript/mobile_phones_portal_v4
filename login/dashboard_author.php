<?php 
require ("checklogin.php");
require ("check_permision.php");

$today = date("Y-m-d");
$active_status_id = getActiveContentStatusID();
$pending_status_id = getPendingContentStatusID();
$draft_status_id = getDraftContentStatusID();


$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."content WHERE user_id = '$logged_user_id'"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_content = $row['numb'];
?>

<script>
$(function() {
	"use strict";
	
       // LINE CHART
       var line = new Morris.Line({
        element: 'line-chart',
        resize: true,
		data: [
		<?php 
		for($i=0; $i<=30; $i++)
			{
				$period_date[$i] = date("Y-m-d", strtotime("-$i day"));	
				//echo $period_date[$i]."<br>";
	
				$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."content WHERE DATE(datetime_added) LIKE '$period_date[$i]' AND user_id = '$logged_user_id'";
				$rs = $conn->query($sql);
				$row = $rs->fetch_assoc();
				$content_count_date[$i] = $row['numb'];
				?>{
				y: "<?php  echo $period_date[$i];?>",
				a: "<?php  echo $content_count_date[$i];?>"
				},
				<?php 
			}
			?>
			],
          xkey: 'y',
          ykeys: ['a'],
          labels: ['Content added'],
          lineColors: ['#3c8dbc'],
          hideHover: 'auto'
		});
});
</script>


<div class="content-wrapper">

        <section class="content-header">
          <h1>Dashboard<small>Summary</small></h1>
        </section>	

        <!-- Main content -->
        <section class="content">

		<?php
		if ($msg =='error_demo_mode' or $msg == 'demo_mode')
		echo '<p class="bg-danger"><strong style="font-size:15px;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ERROR - DEMO MODE ACTIVE!</strong> You can\'t do this operation in DEMO MODE</p><br>';	
		?>


          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $numb_content;?></h3>
                  <p>My Content</p>
                </div>
                <div class="icon">
                  <i class="fa fa-th"></i>
                </div>
                <a href="account.php?page=content" class="small-box-footer">View all content <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
          
           </div><!-- /.row -->
          <!-- Main row -->
          
          
          <div class="row">
				<div class="col-lg-12">

                  <!-- LINE CHART -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">Content added by me in last 30 days</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                    <div class="box-body chart-responsive">
                      <div class="chart" id="line-chart" style="height: 250px;"></div>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
	


        </section><!-- /.content -->

</div><!-- /.content-wrapper -->
