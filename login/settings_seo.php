<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<form action="settings_submit.php" method="post">  
<div class="content-wrapper">

        <section class="content-header">
          <h1>SEO and Social settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>
	
                
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">    

			<div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> SEO Settings
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">
                    				
				<div class="col-md-12">
					
                    <div class="form-group">
				    <label>Site META Title:</label>
				    <input type="text" name="site_title" class="form-control" placeholder="Enter website title" value="<?php echo $config_site_title;?>">
					</div>
                    
					<div class="form-group">
					<label>Site META Description:</label>
					<input type="text" name="site_description" class="form-control" placeholder="Enter website description" value="<?php echo $config_site_description;?>">
                    </div>
				
					<div class="form-group">
					<label>Site META Keywords:</label>
					<input type="text" name="site_keywords" class="form-control" placeholder="Enter website keywords" value="<?php echo $config_site_keywords;?>">
                    </div>

					<div class="form-group">
					<label>META author:</label>
					<input type="text" name="meta_author" class="form-control" placeholder="Enter meta author" value="<?php echo $config_meta_author;?>">
                    </div>
                    
                </div>    <!-- /.col-md-12 -->
				


                </div>    <!-- /.col-md-12 -->
				
                </div>
            </div>
                    
   
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Social Settings
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

				<div class="col-md-6">					
                        <div class="form-group">
                        <label>Facebook APP ID</label>
                        <input class="form-control" name="fb_app_id" type="text" value="<?php echo $config_fb_app_id;?>" />
                        </div>
            	</div>
                
                <div class="clear"></div>
                                    				
				<div class="col-md-6">					
                        <div class="form-group">
                        <label>Facebook page</label>
                        <input class="form-control" name="social_facebook_url" type="text" value="<?php echo $config_social_facebook_url;?>" />
                        </div>
            	</div>
                
				<div class="col-md-6">					
                        <div class="form-group">
                        <label>Twitter page</label>
                        <input class="form-control" name="social_twitter_url" type="text" value="<?php echo $config_social_twitter_url;?>" />
                        </div>
            	</div>

				<div class="col-md-6">					
                        <div class="form-group">
                        <label>Google+ page</label>
                        <input class="form-control" name="social_googleplus_url" type="text" value="<?php echo $config_social_googleplus_url;?>" />
                        </div>
            	</div>

				<div class="col-md-6">					
                        <div class="form-group">
                        <label>Linkedin page</label>
                        <input class="form-control" name="social_linkedin_url" type="text" value="<?php echo $config_social_linkedin_url;?>" />
                        </div>
            	</div>

				<div class="col-md-6">					
                        <div class="form-group">
                        <label>Youtube page</label>
                        <input class="form-control" name="social_youtube_url" type="text" value="<?php echo $config_social_youtube_url;?>" />
                        </div>
            	</div>

				<div class="col-md-6">					
                        <div class="form-group">
                        <label>Pinterest page</label>
                        <input class="form-control" name="social_pinterest_url" type="text" value="<?php echo $config_social_pinterest_url;?>" />
                        </div>
            	</div>                

                </div>    <!-- /.col-md-12 -->
				
                </div>
            </div>
             
				
                <div class="col-md-6">	
		        <div class="form-group">    
            	<input type="hidden" name="settings_section" value="seo_social" />
            	<input type="hidden" name="return_page" value="settings_seo" />                 	            	                	
            	<button type="submit" class="btn btn-primary">Change settings</button>		    
    	        </div>
				</div>
                

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->

</div><!-- /.content-wrapper -->

</form>