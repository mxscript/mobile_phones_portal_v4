<?php 
require ("checklogin.php");
require ("check_permision.php");

$group_id = $_GET["group_id"];
$group_id = Secure($group_id);

$query = "SELECT id, title FROM ".$database_table_prefix."cf_groups WHERE id = '$group_id' LIMIT 1";
$rs = $conn->query($query);
$valid_group = $rs->num_rows;
$row = $rs->fetch_assoc();
$group_title = stripslashes($row['title']);
					
$query = "SELECT id FROM ".$database_table_prefix."cf_sections WHERE group_id = '$group_id'";
$rs = $conn->query($query);
$number_custom_fields_sections = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Custom fields for <?php echo $group_title;?></h1> 
          <div class="clear"></div>  
          <a class="btn btn-primary" data-toggle="modal" href="#" data-target=".modal_add_cf_section"><i class="fa fa-plus"></i> Add Section for <?php echo $group_title;?></a></h1>      
            <?php include ("static/modal_add_cf_section.php");?>       
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if($valid_group==0)
            {
                echo '<p class="bg-danger">Invalid group</p>';
                exit;
            }	
    if ($msg =='add_ok')
        echo '<p class="bg-info">Added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
	if ($msg =='error_name')
		echo '<p class="bg-danger">Input title</p>';	
	if ($msg =='error_delete_protected')
		echo '<p class="bg-danger">Error! This item can not be deleted</p>';	
	if ($msg =='error_duplicate')
		echo '<p class="bg-danger">There is another item with this title</p>';	
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($number_custom_fields_sections==0)
	{
		echo "No custom fields";
	}

else
	{
		$page_rows = 50;
		$last = ceil($number_custom_fields_sections/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, title, description, active, position FROM ".$database_table_prefix."cf_sections WHERE group_id = '$group_id' ORDER BY position ASC $max";
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th width="50">Order</th>
        <th width="250">Section details</th>
        <th>Custom fields</th>
        <th width="130">Actions</th>
		</tr>
        </thead>
        
        <tbody>
                <?php
                while($row = $rs->fetch_assoc())
                    {
                    $cf_section_id = $row['id'];
                    $cf_section_title = stripslashes($row['title']);
                    $cf_section_description = stripslashes($row['description']);
                    $cf_section_active = $row['active'];
                    $cf_section_position = $row['position'];                    
                ?>	
                <tr <?php if($cf_section_active==0) echo 'bgcolor="#FFE1E1"';?>>
                    
                    <td>
                    <?php echo $cf_section_position;?>
                    </td>
                    
                    <td>                                        
                    <strong><?php echo $cf_section_title;?></strong> 
                    <div class="clear"></div>
                    <a class="btn btn-info btn-sm" href="#" data-toggle="modal" data-target=".modal_add_cf_<?php echo $cf_section_id;?>">ADD CUSTOM FIELD</a>
					<?php include ("static/modal_add_cf.php");?>
                    <div class="clear"></div>
                    <?php if($cf_section_description) echo $cf_section_description."<br />";?>                    
                    Active: <?php if($cf_section_active==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?>
                    <br />
					Section ID: <?php echo $cf_section_id;?>
                    </td>
                    
                    <td>
                    <?php 
                    $data_cf = "SELECT id FROM ".$database_table_prefix."cf WHERE group_id = '$group_id'";
					if($conn->query($data_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); }
                    $rs_cf_number = $conn->query($data_cf);
                    $number_cf = $rs_cf_number->num_rows;

                    $query_cf = "SELECT id, title, type, active, position, show_in_specs, show_in_search FROM ".$database_table_prefix."cf WHERE group_id = '$group_id' AND section_id = '$cf_section_id' ORDER BY position ASC";
					$rs_cf = $conn->query($query_cf);
					while($row = $rs_cf->fetch_assoc())
                    	{
                    	$cf_id = $row['id'];
                    	$cf_title = stripslashes($row['title']);
                    	$cf_type = stripslashes($row['type']);
						$cf_active = $row['active'];
						$cf_position = $row['position'];
						$cf_show_in_specs = $row['show_in_specs'];
						$cf_show_in_search = $row['show_in_search'];
						
						if($cf_type=="defined")
							{
							?>
    	                    <span style="float:right;">
	    	                <i class="fa fa-bars"></i> <a href="#" data-toggle="modal" data-target=".modal_add_cf_defined_<?php echo $cf_id;?>">Add defined values</a> 
    	    	            </span>
							<?php include ("static/modal_add_cf_defined.php");
							}
							?>                            
                        
                    	<strong><?php echo $cf_title;?></strong> (#<?php echo $cf_position;?>) <a href="#" data-toggle="modal" data-target=".modal_cf_edit_<?php echo $cf_id;?>">Edit</a><br /><?php include ("static/modal_edit_cf.php");?>
                        
						<small>
                        Type: <?php echo "<strong>".$cf_type."</strong> | Active: "; if($cf_active==1) echo "YES"; else echo "NO"; echo " | Position: ".$cf_position." | Show in specs: "; if($cf_show_in_specs==1) echo "YES"; else echo "NO"; echo " | Show in search: "; if($cf_show_in_search==1) echo "YES"; else echo "NO"; ?>
                        </small>
                        <?php
                        if($cf_type=="defined")
							{
							echo "<br /><small>Values: ";	
							$query_defined_cf = "SELECT id, title, active, position FROM ".$database_table_prefix."cf_defined WHERE group_id = '$group_id' AND section_id = '$cf_section_id' AND cf_id = '$cf_id' ORDER BY position ASC";
							$rs_defined_cf = $conn->query($query_defined_cf);
							while($row = $rs_defined_cf->fetch_assoc())
                    			{
			                    $defined_cf_id = $row['id'];
			                    $defined_cf_title = stripslashes($row['title']);
								$defined_cf_active = $row['active'];
								$defined_cf_position = $row['position'];
								?>
                                #<?php echo $defined_cf_position;?> <a href="#" data-toggle="modal" data-target=".modal_defined_cf_edit_<?php echo $defined_cf_id;?>"><?php echo $defined_cf_title;?></a> <?php if($defined_cf_active==0) echo '<font color="#CC3300">(inactive)</font>';?> | 
                                <?php
								include ("static/modal_edit_cf_defined.php");
								}
							echo "</small>";	
							}
						?>   
						<div class="line"></div>
                        <?php
						}
					?>
                    </td>
                                        
                    <td>
                    <i class="fa fa-edit fa-fw"></i> <a href="#" data-toggle="modal" data-target=".modal_cf_section_edit_<?php echo $cf_section_id;?>">Edit section</a>
                    <?php include ("static/modal_edit_cf_section.php");?>
                    <div class="clear"></div>
                    <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $cf_section_id;?>('<?php echo $cf_section_id;?>');">Delete section</a>
                    <script language="javascript" type="text/javascript">
                    function deleteRecord_<?php echo $cf_section_id;?>(RecordId)
                    {
                        if (confirm('Section will be deleted. Also, all custom fields from this section will be DELETED!!')) {
                            window.location.href = 'custom_fields_section_delete.php?id=<?php echo $cf_section_id;?>&group_id=<?php echo $group_id;?>';
                        }
                    }
                    </script>  
                    </td>
                </tr>
                <?php 
                }
                ?>
        </tbody>
		</table>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=custom_fields_cf&pagenum=1&group_id=$group_id'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=custom_fields_cf&pagenum=$previous&group_id=$group_id'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=custom_fields_cf&pagenum=$next&group_id=$group_id'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=custom_fields_cf&pagenum=$last&group_id=$group_id'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->