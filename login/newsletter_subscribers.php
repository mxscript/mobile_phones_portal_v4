<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."nl_subscribers";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>
<script type="text/javascript" charset="utf-8">
$(document).ready(function()
{
	 var dontSort = [];
                $('#listings thead th').each( function () {
                    if ( $(this).hasClass( 'no_sort' )) {
                        dontSort.push( { "bSortable": false } );
                    } else {
                        dontSort.push( null );
                    }
                } );
				
    $('#listings').dataTable({"aoColumns": dontSort, "order": [[ 0, "desc" ]]});	

});
</script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Subscribers (<?php echo $rows;?> users)</h1> 
          <div class="clear"></div>  
          <a class="btn btn-primary btn-flat" data-toggle="modal" href="#" data-target=".modal_add_subscriber"><i class="fa fa-plus"></i> Add new subscriber</a>          <?php include ("static/modal_add_subscriber.php");?>       
          <span style="float:right"><a class="btn btn-danger btn-flat" href="account.php?page=newsletter_new"><i class="fa fa-paper-plane-o"></i> Create a newsletter</a> </span>
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
	if ($msg =='error_email')
		echo '<p class="bg-danger">Input valid email</p>';	
	if ($msg =='error_duplicate_email')
		echo '<p class="bg-danger">There is another subscriber with this email address</p>';	
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
if ($rows==0)
	{
		echo "No subscriber";
	}

else
	{
		$query = "SELECT id, email, register_date, register_ip, is_banned FROM ".$database_table_prefix."nl_subscribers ORDER BY id DESC";
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th width="40">ID</th>
        <th>Subscriber</th>
        <th width="160">Sent counter</th>
        <th width="120">Is banned</th>
		<th class="no_sort" width="100">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $id = $row['id'];
			$email = $row['email'];
			$register_date = $row['register_date'];
			$register_ip = $row['register_ip'];
			$is_banned = $row['is_banned'];
        ?>	
        <tr> 
        	<td>
            <?php echo $id;?>
            </td>
                       
            <td>           
            <h4 style="margin-top:0"><?php echo $email;?></h4> 
            <small>
            Register time: <?php echo DateTimeFormat($register_date);?> | 
			Register IP: <?php echo $register_ip;?>
            </small>
            </td>
			
            <td>
            <?php
			$data2 = "SELECT id FROM ".$database_table_prefix."nl_sent WHERE subscriber_id = '$id'";
			if($conn->query($data2) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); }

			$rs2 = $conn->query($data2);
			$number_content = $rs2->num_rows;
			?>
            <?php echo $number_content;?><br />
            <a href="account.php?page=newsletter_subscriber_log&id=<?php echo $id;?>">View history log</a>
            </td>
           	
            <td>
            <?php if($is_banned==0) echo "NO"; else echo "<font color='#CC0000'>YES</font>"; ?>
            </td>
            
            <td>
            <i class="fa fa-pencil fa-fw"></i> <a data-toggle="modal" href="#" data-target=".modal_edit_subscriber_<?php echo $id;?>">Edit</a>
        	<?php include ("static/modal_edit_subscriber.php");?> 
            <div class="clear"></div>
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $id;?>('<?php echo $id;?>');">Delete</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'newsletter_subscribers_delete.php?id=<?php echo $id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php 
        }
        ?>
        </tbody>
		</table>

		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->