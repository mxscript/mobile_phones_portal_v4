<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

$group_id = isset($_REQUEST['group_id']) ? $_REQUEST['group_id'] : '';
$group_id = Secure($group_id);

$defined_cf_id = isset($_REQUEST['defined_cf_id']) ? $_REQUEST['defined_cf_id'] : '';
$defined_cf_id = Secure($defined_cf_id);

$query = "DELETE FROM ".$database_table_prefix."cf_defined WHERE id = '$defined_cf_id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $affected_rows = $conn->affected_rows;
}

//allSpecsTable();
//allSpecsTableRecountData();

// form OK:
header("Location: account.php?page=custom_fields_cf&msg=defined_cf_delete_ok&group_id=$group_id");	
exit;