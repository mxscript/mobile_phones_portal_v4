<?php
session_start();

$_SESSION['user_token'] = isset($_SESSION['user_token']) ? $_SESSION['user_token'] : '';
$_SESSION['user_logged'] = isset($_SESSION['user_logged']) ? $_SESSION['user_logged'] : '';
$_COOKIE['user_logged'] = isset($_COOKIE['user_logged']) ? $_COOKIE['user_logged'] : '';

if((isset($_SESSION['user_token']) and $_SESSION['user_logged']==1) or (isset($_COOKIE['user_token']) and $_SESSION['user_logged']==1))	{
	// user is logged
	header("location:account.php");
	exit;
	}

require ("../core/core.php");
require ("../core/functions/functions.php");

$activation_code = $_GET['activation_code'];
$activation_code = Secure($activation_code);

$sql = "SELECT id, active FROM ".$database_table_prefix."users WHERE activation_code = '$activation_code' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;

if($count==0)
	{
	echo "Invalid code";
	exit();
	}

while ($row = $rs->fetch_assoc())
	{
	$user_id = $row["id"]; 
	$active = $row["active"]; 	
	}

if($active==1)
	{
	header("Location:index.php?msg=user_already_active");
	exit();
	}

$query = "UPDATE ".$database_table_prefix."users SET active = '1', email_verified = '1' WHERE id = '$user_id' LIMIT 1"; 
$rs = $conn->query($query);
$affected_rows = $conn->affected_rows;	

header("Location: index.php?msg=user_active");
exit;
?>