<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$media_id = $_POST["media_id"];
$media_id = Secure($media_id);

$content_id = $_POST["content_id"];
$content_id = Secure($content_id);

$source = $_POST["source"];
$source = Secure($source);
if($source=="") $source = "content";

$title = $_POST["title"];
$title = Secure($title);

$description = $_POST["description"];
$description = Secure($description);

$type = $_POST["type"];
$type = Secure($type);

$embed_code = $_POST["embed_code"];
$embed_code = Secure($embed_code);

$url_redirect = $_POST["url_redirect"];
$url_redirect = Secure($url_redirect);


// **************************************************
// AUTHORS FILTERS
if (authorPermissionOK($content_id, $logged_user_id)==0)
	{
	header("Location: account.php?page=content&msg=no_permission&pagenum=$pagenum");
	exit;
	}
// **************************************************


$query = "UPDATE ".$database_table_prefix."media SET type = '$type', title = '$title', description = '$description', embed_code = '$embed_code', url_redirect = '$url_redirect' WHERE id = '$media_id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// FILE
if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}
	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../media/temp/".$image);

		// create large image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage($config_img_large_width, $config_img_large_height, $config_img_large_resize); 
		$resizeObj -> saveImage("../content/media/large/".$image);

		// create small image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage($config_img_small_width, $config_img_small_height, $config_img_small_resize); 
		$resizeObj -> saveImage("../content/media/small/".$image);

		// create thumb image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage($config_img_thumb_width, $config_img_thumb_height, $config_img_thumb_resize); 
		$resizeObj -> saveImage("../content/media/thumbs/".$image);
		
		@unlink ("../content/media/temp/".$image);
		$query = "UPDATE ".$database_table_prefix."media SET file = '$image' WHERE id = '$media_id' LIMIT 1"; 
		if($conn->query($query) === false) {} else { $affected_rows = $conn->affected_rows;	}

		}
	}

		
// form OK:
header("Location: account.php?page=content_media&msg=edit_ok&id=$content_id&source=$source");	
exit;
?> 