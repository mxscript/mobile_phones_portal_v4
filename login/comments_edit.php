<?php 
require ("checklogin.php");
require ("check_permision.php");

$comment_id = $_REQUEST["id"];
$comment_id = Secure($comment_id);

$pagenum = $_REQUEST["pagenum"];
$pagenum = Secure($pagenum);

$query = "SELECT content_id, user_id, name, email, website, content, datetime FROM ".$database_table_prefix."comments WHERE id = '$comment_id' ORDER BY id DESC LIMIT 1";
$rs = $conn->query($query);
$exist = $rs->num_rows;
while($row = $rs->fetch_assoc())
    {
	$content_id = $row['content_id'];
	$user_id = $row['user_id'];
	$email = $row['email'];
	$name = stripslashes($row['name']);
	$website = stripslashes($row['website']);
	$content = stripslashes($row['content']);
	$datetime = $row['datetime'];
	}
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Edit comment</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
	if($exist==0)
		{
			echo "Invalid comment";
			exit;
		}
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">
        
   
                <form action="comments_edit_submit.php" method="post">

				<div class="form-group">
                  <label>Name:</label>	
                  <input name="name" type="text" class="form-control" value="<?php echo $name;?>" />
        	    </div>
                
				<div class="form-group">
                  <label>Website:</label>	
                  <input name="website" type="text" class="form-control" value="<?php echo $website;?>" />
        	    </div>
                                
                <div class="form-group">
                <textarea id="comment_<?php echo $comment_id;?>" name="comment" rows="10" class="form-control"><?php echo $content;?></textarea>
        	    </div>

				<script>
                $(document).ready(function() {
                    $('#comment_<?php echo $comment_id;?>').summernote({
                        height: 250,
                        onImageUpload: function(files, editor, welEditable) {
                            sendFile(files[0], editor, welEditable);
                        }
                    });
                    function sendFile(file, editor, welEditable) {
                        data = new FormData();
                        data.append("file", file);
                        $.ajax({
                            data: data,
                            type: "POST",
                            url: "texteditor_upload.php",
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(url) {
                                editor.insertImage(welEditable, url);
                            }
                        });
                    }
                });
                </script>
                
                <div class="form-group">
                <input type="hidden" name="comment_id" value="<?php echo $comment_id;?>" />
                <input type="hidden" name="content_id" value="<?php echo $content_id;?>" />
                <input type="hidden" name="pagenum" value="<?php echo $pagenum;?>" />
                <button type="submit" class="btn btn-primary">Edit comment</button>
                </div>
                </form>			

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->