<?php
ob_start(); 
session_start();
require ("../config.php");
require ("../core/DatabaseConnect.php");
require ("../core/functions/functions.php");

$login_email = isset($_POST['login_email']) ? $_POST['login_email'] : '';
$login_password = isset($_POST['login_password']) ? $_POST['login_password'] : '';

$redirect = isset($_POST['redirect']) ? $_POST['redirect'] : '';
$redirect = Secure($redirect);

$login_email = Secure($login_email);
$login_password = Secure($login_password);
$login_password_sha1 = sha1($login_password);

$_SESSION['user_logged'] = "0";	

if (checkPassword ($login_email, $login_password)==true)
	{
		// paswword OK
		$sql = "SELECT id, role_id, email, active FROM ".$database_table_prefix."users WHERE email LIKE '$login_email' LIMIT 1";
		$rs = $conn->query($sql);
		$exist = $rs->num_rows;
		if($exist==0)
			{
			header("Location: index.php?msg=error&redirect=$redirect");
			exit;
			}

		$row = $rs->fetch_assoc();
		$user_id = $row['id'];
		$user_active = $row['active'];
		if($user_active==0)
			{
			header("Location: index.php?msg=error_inactive&redirect=$redirect");
			exit;
			}
			
		$token = sha1(random_code());	
		// insert user token in database
		$query = "UPDATE ".$database_table_prefix."users SET token = '$token' WHERE id = '$user_id' LIMIT 1"; 				
		if($conn->query($query)){ $affected_rows = $conn->affected_rows;}
		
		session_regenerate_id (); 
		$_SESSION['user_logged'] = "1";	
		$_SESSION['user_token'] =  sha1($token);
	
	   	if(isset($_POST['remember']))
   			{			
      		setcookie("user_token", sha1($token), time()+60*60*24*90, "/"); // 90 days
	   		}
	if($redirect!="")
		header("location: $redirect");
	else
		header("location: account.php");	
	ob_end_flush(); 
	exit;	

	}
else
	{
	header("Location: index.php?msg=error&redirect=$redirect");
	ob_end_flush(); 
	exit;
	}
	
exit;
?>
