<?php 
require ("checklogin.php");
require ("check_permision.php");

// **************************************************
// FILTER BY USER ROLE
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************

	
$search_terms = isset($_GET['search_terms']) ? $_GET['search_terms'] : '';
$search_terms = Secure($search_terms);
$search_terms_encoded = urlencode($search_terms);
		
$search_categ_id = isset($_GET['search_categ_id']) ? $_GET['search_categ_id'] : '';
$search_categ_id = Secure($search_categ_id);
$search_categ_id = (int)$search_categ_id;

$search_status_id = isset($_GET['search_status_id']) ? $_GET['search_status_id'] : '';
$search_status_id = Secure($search_status_id);
$search_status_id = (int)$search_status_id;

$search_user_id = isset($_GET['search_user_id']) ? $_GET['search_user_id'] : '';
$search_user_id = Secure($search_user_id);
$search_user_id = (int)$search_user_id;

if($search_terms!="")	
	$search_cond_terms = "AND title LIKE '%$search_terms%'";
else	
	$search_cond_terms = "";
	
if($search_status_id!=0) 
	$search_cond_status = "AND status_id = '$search_status_id'";
else
	$search_cond_status = "";	

if($search_user_id!=0) 
	$search_cond_user = "AND user_id = '$search_user_id'";
else	
	$search_cond_user = "";

if($search_categ_id!=0)	
	{
		$subcategories_list = createSubcategories_string($search_categ_id);
		$search_cond_categ = "AND categ_id IN ($subcategories_list)";			
	}
else
	$search_cond_categ = "";
	
$query = "SELECT id FROM ".$database_table_prefix."content WHERE 1 $filter_user_role $search_cond_terms $search_cond_categ $search_cond_status $search_cond_user";
$rs = $conn->query($query);
$rows = $rs->num_rows;

$status_id_active = getActiveContentStatusID();
$status_id_draft = getDraftContentStatusID();
$status_id_pending = getPendingContentStatusID();

$query_active = "SELECT id FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active' $filter_user_role";
$rs = $conn->query($query_active);
$rows_active = $rs->num_rows;

$query_draft = "SELECT id FROM ".$database_table_prefix."content WHERE status_id = '$status_id_draft' $filter_user_role";
$rs = $conn->query($query_draft);
$rows_draft = $rs->num_rows;

$query_pending = "SELECT id FROM ".$database_table_prefix."content WHERE status_id = '$status_id_pending' $filter_user_role";
$rs = $conn->query($query_pending);
$rows_pending = $rs->num_rows;
?>

<script language="JavaScript">
function toggle(source) {
  checkboxes = document.getElementsByName('select[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>CONTENT ITEMS (<strong><?php echo $rows_active;?></strong> <a href="account.php?page=content&search_status_id=<?php echo $status_id_active;?>">active</a> and <strong><?php echo $rows_pending;?></strong> <a href="account.php?page=content&search_status_id=<?php echo $status_id_pending;?>">pending</a> and <strong><?php echo $rows_draft;?></strong> <a href="account.php?page=content&search_status_id=<?php echo $status_id_draft;?>">draft</a>)</h1>
          <div class="clear"></div>
          <a class="btn btn-info btn-flat" href="account.php?page=content_add"><i class="fa fa-plus"></i> Add content</a>
          <span style="float:right"><a class="btn btn-danger btn-flat" href="content_activate_all_pending.php"><i class="fa fa-check"></i> Activate all pending content</a> </span>
        </section>
		
        
        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
    if ($msg =='no_permission')
        echo '<p class="bg-danger">ERROR! You don\'t have permission to do that.</p>';
	if($logged_user_role=="author")		
	    echo '<p class="bg-danger">Note: You can not edit / delete / add prices / add media for active content (if it is already published)</p>';

    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
            	                
				<div class="box-body">                

                <section>
                <form action="account.php" method="get" class="form-inline">
                	<label>Filter results: </label>
                    <input type="text" name="search_terms" placeholder="Search in title" class="form-control" value="<?php echo $search_terms;?>" />
                    <select name="search_categ_id" class="form-control">
                        <option value="0">ALL CATEGORIES</option>
                        <?php
                        $sql_top = "SELECT id, title FROM ".$database_table_prefix."categories WHERE parent_id = 0 ORDER BY title ASC";
                        $rs_top = $conn->query($sql_top);
                        while ($row = $rs_top->fetch_assoc())
                            {
                            $top_categ_id = $row['id'];	
                            $top_categ_title = stripslashes($row['title']);
                            ?>
                            <option <?php if ($search_categ_id==$top_categ_id) echo "selected=\"selected\"";?> value="<?php echo $top_categ_id;?>"><?php echo $top_categ_title;?></option>
                            <?php
                            get_child_categ_edit($top_categ_id, $search_categ_id);
                            }
                        ?>
                    </select>
                	                 
                    <input type="hidden" name="page" value="content" />                    
                    <button class="btn btn-default btn-flat" type="submit"/>Apply</button>
                </form>
                </section>
                
                <div class="clear"></div>
            
		<?php
        // ------------------------------------------------------------------------------------------------------
        if (!(isset($pagenum)))
            {
            $pagenum = 1;
            }
        
        if ($rows==0)
            {
                echo "No content";
            }
        
        else
            {
				echo $rows." content items<br>";	
                $page_rows = 10;
                $last = ceil($rows/$page_rows);
        
                if ($pagenum < 1)
                {
                $pagenum = 1;
                }
                elseif ($pagenum > $last)
                {
                $pagenum = $last;
                }
        
                $max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
                $query = "SELECT id, categ_id, root_categ_id, cf_group_id, title, permalink, content_short, status_id, sticky, image, hits, user_id, disable_comments, disable_ads, disable_ratings, datetime_added, datetime_modified, mobile_release_date, mobile_main_price FROM ".$database_table_prefix."content WHERE 1 $filter_user_role $search_cond_terms $search_cond_categ $search_cond_status $search_cond_user ORDER BY id DESC $max";
                $rs = $conn->query($query);
				if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
                ?>
                
                        
                <form action="content_bulk_action.php" method="post" class="form-inline"> 
                <table class="table table-bordered" id="listings">
                <thead> 
                <tr>
                <th width="30" align="center"><input type="checkbox" onClick="toggle(this)" /></th>
                <th width="80">Image</th>
                <th>Details</th>
                <th width="210">Info</th>
                <th width="180">Extra</th>
                </tr>
                </thead>
                
                <tbody>
                <?php
                while($row = $rs->fetch_assoc())
                    {
                    $content_id = $row['id'];
					$categ_id = $row['categ_id'];
					$root_categ_id = $row['root_categ_id'];
					$cf_group_id = $row['cf_group_id'];
                    $title = stripslashes($row['title']);
                    $permalink = $row['permalink'];
					$content_short = strip_tags(html_entity_decode(stripslashes($row['content_short'])));
                    $status_id = $row['status_id'];
					$sticky = $row['sticky'];
					$image = $row['image'];
					$hits = $row['hits'];
					$user_id = $row['user_id'];
					$disable_comments = $row['disable_comments'];
					$disable_ads = $row['disable_ads'];
					$disable_ratings = $row['disable_ratings'];
					$mobile_release_date = $row['mobile_release_date'];
					$mobile_main_price = $row['mobile_main_price'];
					
					$content_short2 = substr($content_short, 0, 350);					
					$time_release = strtotime($mobile_release_date);
					
	                $query3 = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$categ_id' LIMIT 1";
                	$rs3 = $conn->query($query3);
					$row = $rs3->fetch_assoc();
                    $categ_title = stripslashes($row['title']);
					$categ_permalink = stripslashes($row['permalink']);
					
					$query4 = "SELECT title FROM ".$database_table_prefix."cf_groups WHERE id = '$cf_group_id' LIMIT 1";
                	$rs4 = $conn->query($query4);
					$row = $rs4->fetch_assoc();
                    $cf_group_title = stripslashes($row['title']);
					
					$numb_comments = getContentComments($content_id, 1);
					
					$UserDetailsArray = getUserDetailsArray($user_id);
   	                $user_name = stripslashes($UserDetailsArray['name']);
                   	$user_avatar = $UserDetailsArray['avatar'];
					$user_role_id = $UserDetailsArray['role_id'];
					
                ?>	
                <tr>                   
                    <td align="left" valign="top">                    
                    <input name="select[]" type="checkbox" value="<?php echo $content_id;?>" />
                    </td>     

                    <td align="left" valign="top">
                    <?php if($image!="")
                        {
                        ?>
                      <a target="_blank" href="<?php echo $config_site_media;?>/large/<?php echo $image;?>"><img style="padding:0; max-width:80px; height:auto;" src="<?php echo $config_site_media;?>/thumbs/<?php echo $image;?>" /></a>
                        <?php
                        }
                    ?>
                    </td>
                    
                    <td>
                    <span style="float: right"><?php display_status_box($status_id);?></span>
                    <h4 style="margin-top:0; font-weight:600;"><a target="_blank" href="<?php echo $config_site_url.'/'.$categ_permalink.'/'.$permalink.'-'.$content_id.'.html';?>"><?php echo $title;?></a></h4>                     
                    <?php
                    echo createPath_plain($categ_id);
					?>
                    <div class="clear"></div>
                    <?php if($mobile_release_date!="0000-00-00") echo 'Release date: '.date("F Y",$time_release).'</br>'; ?>
                    <?php if($cf_group_title) echo 'Custom fields group: '.$cf_group_title.'</br>'; ?>
                    <?php if($content_short) echo $content_short2.'<div class="clear"></div>'; ?>
                    <small>
                    <?php if($sticky==1) echo '<span class="sticky_box"><i class="fa fa-thumb-tack"></i> sticky</span>';?>
                    <?php if($disable_comments==1) echo '&nbsp;&nbsp;<span class="sticky_box"><i class="fa fa-comments-o"></i> comments disabled</span>';?>
                    <?php if($disable_ads==1) echo '&nbsp;&nbsp;<span class="sticky_box"><i class="fa fa-flag-o"></i> ads disabled</span>';?>
					<?php if($disable_ratings==1) echo '&nbsp;&nbsp;<span class="sticky_box"><i class="fa fa-star-o"></i> ratings disabled</span>';?>                    
                    </small>
                    
                    <div class="clear"></div>
                    
                    <?php if($logged_user_role=="author" and $status_id==$status_id_active) {} else { ?>
                    <i class="fa fa-edit fa-fw"></i> <a href="account.php?page=content_edit&content_id=<?php echo $content_id;?>&pagenum=<?php echo $pagenum;?>">Edit</a>
                    <?php /*?><i class="fa fa-clone fa-fw"></i> <a data-toggle="modal" href="#" data-target=".modal_clone_content_<?php echo $content_id;?>">Clone</a>
                    <?php include ("static/modal_clone_content.php");?><?php */?>
                    <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $content_id;?>('<?php echo $content_id;?>');">Delete</a>
                    <script language="javascript" type="text/javascript">
                    function deleteRecord_<?php echo $content_id;?>(RecordId)
                    {
                        if (confirm('Confirm delete')) {
                            window.location.href = 'content_delete.php?id=<?php echo $content_id;?>&pagenum=<?php echo $pagenum;?>';
                        }
                    }
                    </script>
                    <?php } ?>
                    </td>

					<td>
                    <img style="border:#999 1px solid; padding:0; max-width:20px; height:auto; float:left; margin-right:5px;" src="<?php echo $config_site_media;?>/avatars/<?php echo $user_avatar;?>" />
                    <a href="account.php?page=users_edit&id=<?php echo $user_id;?>"><?php echo $user_name;?></a>
                    <div class="clear"></div>
                    <?php if($mobile_main_price!=0) echo 'Price: '.$mobile_main_price.'<br>';?>
                    ID: <?php echo $content_id;?><br />
                    Hits: <?php echo $hits;?><br />
                    <?php
					if($numb_comments==0) echo 'No comment';
					if($numb_comments==1) echo '<a href="account.php?page=comments&content_id="One comment';
					if($numb_comments>1) echo $numb_comments.' comments';
					?>			
                    </td>                    

                    <td>                    
                    <?php
                    $query_media = "SELECT id FROM ".$database_table_prefix."media WHERE source = 'content' AND content_id = '$content_id'";
   					$rs_media = $conn->query($query_media);
					$numb_media = $rs_media->num_rows;
					?>
                    <small>
					<i class="fa fa-image fa-fw"></i> <a href="account.php?page=content_media&id=<?php echo $content_id;?>&pagenum=<?php echo $pagenum;?>&source=content"> <?php if ($numb_media==0) echo 'No media content'; else echo $numb_media." media content";?></a>
                    </small>
					<div class="clear"></div>
                    
                    <?php
					if($root_categ_id==$mobiles_categ_id and $cf_group_id!=0) { ?>
                    <i class="fa fa-bars fa-fw"></i> <a href="account.php?page=content_edit_cf&content_id=<?php echo $content_id;?>&pagenum=<?php echo $pagenum;?>">Custom fields</a>
                    <div class="clear"></div>
                    <?php } 
					if($root_categ_id==$mobiles_categ_id) 
					{ ?>					
                    <i class="fa fa-money fa-fw"></i> <a href="account.php?page=prices&id=<?php echo $content_id;?>">Manage prices</a>                    
                    <?php } ?>
                    </td>
                    
                </tr>
                <?php
                }
                ?>
                </tbody>
                </table>

	        <div class="clear"></div>
    	    With selected items:
            <select name="action" class="form-control">
                <option value="approve">Approve</option>
                <option value="pending">Pending</option>
                <option value="draft">Draft</option>                
                <option value="delete">Delete</option>    
            </select>    
            
            <input type="hidden" name="page" value="content" class="form-control" />
            <input type="hidden" name="pagenum" value="<?php echo $pagenum;?>" />
            <button id="submit" type="submit" class="btn btn-default btn-flat">Apply</button>
			</form>
        
            <div class="clear"></div>
            <ul class="pagination">
            <?php
            echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
                if ($pagenum == 1)
                {
                }
                else
                    {
                    echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=content&pagenum=1&search_categ_id=$search_categ_id&search_status_id=$search_status_id&search_terms=$search_terms&search_user_id=$search_user_id'><strong>First page</strong></a></li>";
                    echo " ";
                    $previous = $pagenum-1;
                    echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=content&pagenum=$previous&search_categ_id=$search_categ_id&search_status_id=$search_status_id&search_terms=$search_terms&search_user_id=$search_user_id'>".$previous."</a></li>";
                    }
        
                    echo "";
        
        
                if ($pagenum == $last)
                    {			
                    }
                else {
                    $next = $pagenum+1;
                    echo "<li><a href='{$_SERVER['PHP_SELF']}?page=content&pagenum=$next&search_categ_id=$search_categ_id&search_status_id=$search_status_id&search_terms=$search_terms&search_user_id=$search_user_id'> ".$next."</a></li>";
                    echo " ";
                    echo "<li><a href='{$_SERVER['PHP_SELF']}?page=content&pagenum=$last&search_categ_id=$search_categ_id&search_status_id=$search_status_id&search_terms=$search_terms&search_user_id=$search_user_id'><strong>Last page</strong></a></li>";
                } 
                ?>	
                </ul>
        
        
                <?php
                } // END if rows==0
                ?>


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->