<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$slider_id = $_POST["slider_id"];
$slider_id = Secure($slider_id);

$query = "SELECT content_source FROM ".$database_table_prefix."sliders WHERE id = '$slider_id' LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$content_source = $row['content_source'];				

if($content_source=="latest_content")
{
	$content_bg = $_POST["content_bg"];
	$content_bg = Secure($content_bg);
	
	$content_latest_numb_items = $_POST["content_latest_numb_items"];
	$content_latest_numb_items = Secure($content_latest_numb_items);

	$content_latest_categ_id = $_POST["content_latest_categ_id"];
	$content_latest_categ_id = Secure($content_latest_categ_id);
	
	$query = "UPDATE ".$database_table_prefix."sliders SET content_bg = '$content_bg', content_latest_numb_items = '$content_latest_numb_items', content_latest_categ_id = '$content_latest_categ_id' WHERE id = '$slider_id'"; 
	if($conn->query($query) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
	else { $last_inserted_id = $conn->insert_id; $affected_rows = $conn->affected_rows;}

}

if($content_source=="defined_content")
{
	$content_bg = $_POST["content_bg"];
	$content_bg = Secure($content_bg);
	
	$content_defined_ids = $_POST["content_defined_ids"];
	$content_defined_ids = Secure($content_defined_ids);
	
	$query = "UPDATE ".$database_table_prefix."sliders SET content_bg = '$content_bg', content_defined_ids = '$content_defined_ids' WHERE id = '$slider_id'"; 
	if($conn->query($query) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
	else { $last_inserted_id = $conn->insert_id; $affected_rows = $conn->affected_rows;}

}


// form OK:
header("Location: account.php?page=slider_content&msg=edit_ok&slider_id=$slider_id");	
exit;
?> 