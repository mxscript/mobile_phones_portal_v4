<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<!-- DateTimePicker -->
<script type="text/javascript" src="assets/js/moment.js"></script>    
<script type="text/javascript" src="assets/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="assets/plugins/datetimepicker/bootstrap-datetimepicker.min.css">    

<div class="content-wrapper">

        <section class="content-header">
          <h1>Add content</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
	if ($msg =='error_title')
		echo '<p class="bg-danger">Error. Input title</p>';
	if ($msg =='error_categ')
		echo '<p class="bg-danger">Error. Select category</p>';	
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
            	                
				<div class="box-body">
                             
            <form name="AddContent" action="content_submit.php" method="post" enctype="multipart/form-data" onsubmit="return ValidateAddContent()">
            
            <div class="col-lg-12">
            <div class="form-group">
            <label>Content title</label>
            <input class="form-control" name="title" type="text" />
            </div>
            </div>


            <div class="col-lg-3">
            <div class="form-group">
	        <label>Select category</label>
            <select name="categ_id" class="form-control">
            	<option selected="selected" value="">- please select -</option>
            	<option value="0">[ROOT]</option>
                <?php
                $sql_top = "SELECT id, title FROM ".$database_table_prefix."categories WHERE parent_id = 0 ORDER BY title ASC";
				$rs_top = $conn->query($sql_top);
                while ($row = $rs_top->fetch_assoc())
                	{
                    $top_categ_id = $row['id'];	
                    $top_categ_title = stripslashes($row['title']);
                    ?>
                    <option value="<?php echo $top_categ_id;?>"><?php echo $top_categ_title;?></option>
                    <?php
					get_child_categ($top_categ_id, 1);
                    }
                ?>
            </select>
     	    </div>
			</div>


            <div class="col-lg-3">
                <div class="form-group">
	            <label>Content status</label>
    	        <select name="status_id" class="form-control">
                <option value="">- please select -</option>
                <?php
                $query_user_role = "SELECT id, button_value FROM ".$database_table_prefix."content_status ORDER BY id ASC";
				$rs_user_role = $conn->query($query_user_role);
				while ($row = $rs_user_role->fetch_assoc())
					{
					$status_id_selected = $row['id'];
	   	            $status_title_selected = stripslashes($row['button_value']);
					?>
		            <option value="<?php echo $status_id_selected;?>"><?php echo $status_title_selected;?></option>
                    <?php
					}
				?>
    			</select>
        	    </div>
			</div>


			<div class="col-lg-3">

			</div>
            

            <div class="col-lg-12">
            <div class="form-group">
            <label>Summary</label>
            <textarea name="content_short" rows="3" class="form-control"></textarea>
            </div>
            </div>

                           
            <div class="col-lg-12">
            <div class="form-group">
            <label>Content</label>
            <textarea id="textarea" name="content" rows="3" class="form-control"></textarea>
            <script>
			$(document).ready(function() {
				$('#textarea').summernote({
					height: 200,
					onImageUpload: function(files, editor, welEditable) {
						sendFile(files[0], editor, welEditable);
					}
				});
				function sendFile(file, editor, welEditable) {
					data = new FormData();
					data.append("file", file);
					$.ajax({
						data: data,
						type: "POST",
						url: "texteditor_upload.php",
						cache: false,
						contentType: false,
						processData: false,
						success: function(url) {
							editor.insertImage(welEditable, url);
						}
					});
				}
			});
			</script>
            
            </div>
            </div>

                        
            
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 nopadding_l">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>SEO settings:</h2>
                </div>
            
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <label>Meta title</label>
                <input class="form-control" name="meta_title" type="text" />
                </div>
                </div>
    
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <label>Meta description</label>
                <input class="form-control" name="meta_description" type="text" />
                </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <label>Meta Keywords</label>
                <input class="form-control" name="meta_keywords" type="text" />
                </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <label>URL structure (leave blank to autogenerate permalink)</label>
                <input class="form-control" name="permalink" type="text" />
                </div>
                </div>
            
            </div>


            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 nopadding_l">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>More settings:</h2>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <label>Aditional info</label>
                <textarea name="aditional_info" rows="3" class="form-control"></textarea>
                </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                    <label>Author</label>
                    <select name="user_id" class="form-control">
                    <option value="">- please select -</option>
                    <?php
                    $query_user_role = "SELECT id, name, email FROM ".$database_table_prefix."users WHERE active = 1 ORDER BY name ASC";
                    $rs_user_role = $conn->query($query_user_role);
                    while ($row = $rs_user_role->fetch_assoc())
                        {
                        $user_id_selected = $row['id'];
                        $user_name_selected = stripslashes($row['name']);
						$user_email_selected = stripslashes($row['email']);
                        ?>
                        <option <?php if($logged_user_id==$user_id_selected) echo 'selected="selected"';?> value="<?php echo $user_id_selected;?>"><?php echo $user_name_selected." (".$user_email_selected.")";?></option>
                        <?php
                        }
                    ?>
                    </select>
                    </div>
                </div>
            
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                <label>Custom template file</label>
                <input class="form-control" name="custom_tpl_file" type="text" />
                </div>
                </div>

                <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                    <div class="form-group">
                    <label>Custom date</label>
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" name="custom_datetime_added" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker1').datetimepicker({
                    		format: 'YYYY-MM-DD HH:mm'
                			});
                    });
                </script>
                    
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_important" type="checkbox" name="sticky">
                    <label for="checkbox_important"> Sticky</label>
                    </div>
    				             
                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_disable_comments" type="checkbox" name="disable_comments">
                    <label for="checkbox_disable_comments"> Disable comments</label>
                    </div>

                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_disable_ratings" type="checkbox" name="disable_ratings">
                    <label for="checkbox_disable_ratings"> Disable ratings</label>
                    </div>
                                        
                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_disable_ads" type="checkbox" name="disable_ads">
                    <label for="checkbox_disable_ads"> Disable ads</label>
                    </div>                 
                </div>     

			</div>
                        
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Upload Image</label>
            <input type="file" name="image" />
            </div>
            </div> 
                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input class="btn btn-primary" name="input" type="submit" value="Add content" />
            </div>  
            </div>
                      
            <div class="clear"></div>
            
            <div class="col-lg-12 nopadding_l">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>Custom Fields</h2>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                    <label>Custom fields group</label>
                    <select name="cf_group_id" class="form-control" onchange="showDiv(this)">
                    <option value="0">- no custom fields -</option>
                    <?php
                    $query_user_role = "SELECT id, title FROM ".$database_table_prefix."cf_groups WHERE active = 1 ORDER BY title ASC";
                    $rs_user_role = $conn->query($query_user_role);
                    while ($row = $rs_user_role->fetch_assoc())
                        {
                        $cf_group_id_select = $row['id'];
                        $cf_group_title_select = stripslashes($row['title']);
                        ?>
                        <option <?php if($cf_group_id==$cf_group_id_select) echo 'selected="selected"';?> value="<?php echo $cf_group_id_select;?>"><?php echo $cf_group_title_select;?></option>
                        <?php
                        }
                    ?>
                    </select>
                    </div>
                </div>
				
                <script>
				function showDiv(elem){
			   	if(elem.value != 0)
			      document.getElementById('hidden_div').style.display = "block";				
				if(elem.value == 0)
			      document.getElementById('hidden_div').style.display = "none";
				}
				</script>

                <div class="clear"></div>

				<div id="hidden_div" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 nopadding_l" style="display:<?php if($cf_group_id!=0) echo 'block'; else echo 'none';?>">
	            <?php include ("content_custom_fields.php");?>
                </div>
				                            
            </div>
          
                      
                      
            </form>
            <div class="clear"></div>

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->