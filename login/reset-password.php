<?php
session_start();

$_SESSION['user_token'] = isset($_SESSION['user_token']) ? $_SESSION['user_token'] : '';
$_SESSION['user_logged'] = isset($_SESSION['user_logged']) ? $_SESSION['user_logged'] : '';
$_COOKIE['user_logged'] = isset($_COOKIE['user_logged']) ? $_COOKIE['user_logged'] : '';

if((isset($_SESSION['user_token']) and $_SESSION['user_logged']==1) or (isset($_COOKIE['user_token']) and $_SESSION['user_logged']==1))	{
	// user is logged
	header("location:account.php");
	exit;
	}

require ("../core/core.php");

$msg = isset($_GET['msg']) ? $_GET['msg'] : '';
$msg = Secure($msg);
?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Password reset</title>

    <!-- Core CSS -->
    <link href="../core/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/login.css" rel="stylesheet">

    <!-- Checkboxes style -->
    <link href="../core/assets/css/bootstrap-checkbox.css" rel="stylesheet">

</head>

<body>

     <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Password reset</h3>
                    </div>
                    <div class="panel-body">
                    	<?php								
						if ($msg =='error')
							echo '<p class="text-danger">Please input your email address</p>';
						if ($msg =='error_demo')
							echo '<p class="text-danger">You can\'t change password for user DEMO</p>';
						if ($msg =='wrong_email')
							echo '<p class="text-danger">No user in database with this email address</p>';			
						if ($msg =='reset_ok')
							echo '<p class="text-info">Your new password was sent in your email address!</strong></p>';														
						?>
                        <form role="form" method="post" action="reset-password-submit.php">
                            <fieldset>
                                <div class="input-group">
	                                <span class="input-group-addon">@</span>
                                    <input class="form-control" placeholder="Email" name="email" type="email" autofocus>
                                </div>
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Reset password" />
                            </fieldset>
                        </form>

                        <div class="clear"></div>
                        <?php
						if($config_user_registration_enabled==1) { ?>
                        <i class="fa fa-user fa-fw"></i> No account yet? <a href="sign-up.php">Register new account</a><br />
                        <?php } ?>                        
                        <i class="fa fa-undo fa-fw"></i> Already registered? <a href="index.php">Account login</a>                                  
                        
                        </div>   
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Core Scripts -->
    <script src="../core/assets/js/jquery-1.10.2.js"></script>
    <script src="../core/assets/js/bootstrap.min.js"></script>

</body>

</html>
