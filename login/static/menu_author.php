      <aside class="main-sidebar">

        <section class="sidebar">

          <ul class="sidebar-menu">
                        
            <li><a href="account.php?page=dashboard"><i class="fa fa-bars fa-fw"></i> <span>Dashboard</span></a></li>
            <li><a href="account.php?page=profile"><i class="fa fa-user fa-fw"></i> <span>My Profile</span></a></li>            
            			
            <li><a href="account.php?page=content"><i class="fa fa-folder-o fa-fw"></i> <span>My Content</span></a></li>
            
          </ul>
        </section>

      </aside>

