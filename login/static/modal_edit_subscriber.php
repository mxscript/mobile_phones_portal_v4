<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_edit_subscriber_<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit subscriber</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="newsletter_subscribers_edit_submit.php" method="post">
                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Input valid Email (required)</label>
    	        <input class="form-control" name="email" type="text" value="<?php echo $email;?>" />
        	    </div>
				</div>  

                <div class="col-lg-6">
                <div class="form-group">
	            <label>Is banned?</label>
    	        <select name="is_banned" class="form-control">
                <option <?php if ($is_banned==0) echo "selected=\"selected\"";?> value="0">NO</option>
    	        <option <?php if ($is_banned==1) echo "selected=\"selected\"";?> value="1">YES</option>
    			</select>
        	    </div>
                </div>

                                
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $id;?>" />
                <button type="submit" class="btn btn-primary">Edit subscriber</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>