<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_cf_<?php echo $cf_section_id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Add custom field for <?php echo $cf_section_title;?></h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="custom_fields_submit.php" method="post">
                
                <div class="col-lg-12">	
                <div class="form-group">
	            <label>Name</label>
    	        <input class="form-control" name="name" type="text" />
        	    </div>
				</div>
                
                <div class="col-lg-12">	
                <div class="form-group">
	            <label>Type</label>
    	        <select name="type" class="form-control">
	            <option selected="selected" value="text">Text input</option>
    	        <option value="yes_no">YES / NO type</option>
                <option value="defined">Select from defined values / intervals</option>
    			</select>
        	    </div>
				</div>
                               
                <div class="col-lg-6">	
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option value="1">YES</option>
                <option value="0">NO</option>
    			</select>
        	    </div>
				</div>

                <div class="col-lg-6">	
                <div class="form-group">
	            <label>Position</label>
    	        <input class="form-control" name="position" type="text" />
        	    </div>
                </div>				
                               
                <div class="col-lg-6">	
                <div class="form-group">
	            <label>Show in specs page</label>
    	        <select name="show_in_specs" class="form-control">
    	        <option value="1">YES</option>
                <option value="0">NO</option>
    			</select>
        	    </div>
				</div>

                <div class="col-lg-6">	
                <div class="form-group">
	            <label>Show in searh filters</label>
    	        <select name="show_in_search" class="form-control">
    	        <option value="1">YES</option>
                <option value="0">NO</option>
    			</select>
        	    </div>
				</div>

				
                <div class="col-lg-12">	
                <div class="form-group">
                <input type="hidden" name="group_id" value="<?php echo $group_id;?>" />
                <input type="hidden" name="section_id" value="<?php echo $cf_section_id;?>" />
                <button type="submit" class="btn btn-primary">Add custom field</button>
                </div>
                </div>
                </form>			
			</div>
                          
        </div>
	</div>
</div>