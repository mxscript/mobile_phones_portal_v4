<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_user" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Add new user</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="users_submit.php" method="post" enctype="multipart/form-data">
                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Full name (required)</label>
    	        <input class="form-control" name="name" type="text" />
        	    </div>
				</div>

                <div class="clear"></div>

                <div class="col-lg-6">
                <div class="form-group">
	            <label>Valid Email (required)</label>
    	        <input class="form-control" name="email" type="text" />
        	    </div>
				</div>  
                                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Password (required)</label>
    	        <input class="form-control" name="password" type="text" />
        	    </div>
                </div>              			
                
                <div class="clear"></div>
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Role</label>
    	        <select name="role_id" class="form-control">
                <option value="">- select -</option>
                <optgroup label="Staff member">
                <?php
                $query_user_role = "SELECT id, title FROM ".$database_table_prefix."users_roles WHERE active = 1 AND is_staff = 1 ORDER BY id ASC";
				$rs_user_role = $conn->query($query_user_role);
				while ($row = $rs_user_role->fetch_assoc())
					{
					$role_id_selected = $row['id'];
	   	            $role_title_selected = stripslashes($row['title']);
					?>
		            <option value="<?php echo $role_id_selected;?>"><?php echo $role_title_selected;?></option>
                    <?php
					}
				?>
                </optgroup>
                <?php /*
                <optgroup label="Registered member">
                <?php
                $query_user_role = "SELECT id, title FROM ".$database_table_prefix."users_roles WHERE active = 1 AND is_staff = 0 ORDER BY id ASC";
				$rs_user_role = $conn->query($query_user_role);
				while ($row = $rs_user_role->fetch_assoc())
					{
					$role_id_selected = $row['id'];
	   	            $role_title_selected = stripslashes($row['title']);
					?>
		            <option value="<?php echo $role_id_selected;?>"><?php echo $role_title_selected;?></option>
                    <?php
					}
				?>
                </optgroup>
				 */ ?>
                
    			</select>
        	    </div>
				</div>
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option value="1">YES</option>
                <option value="0">NO</option>
    			</select>
        	    </div>
                </div>
				
                <div class="clear"></div>
                
				<div class="col-lg-12">
                <div class="form-group">
                <label>Avatar image (optional):</label> 
                <input type="file" name="image">
                </div>
                </div>
                
                <div class="col-lg-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary">Add user</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>