<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_edit_slider_<?php echo $slider_id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit slider</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="sliders_edit_submit.php" method="post">

                <div class="col-lg-6">
                <div class="form-group">
	            <label>Slider name (required)</label>
                <input name="name" type="text" class="form-control" value="<?php echo $name;?>" />
        	    </div>
				</div>  
                                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option <?php if ($active==1) echo "selected=\"selected\"";?> value="1">YES</option>
                <option <?php if ($active==0) echo "selected=\"selected\"";?> value="0">NO</option>
    			</select>
        	    </div>
                </div>

                <div class="col-lg-6">
	                <div class="form-group">
					<label>Slider content source</label>
					<select name="content_source" class="form-control">
						<option <?php if ($content_source=='latest_content') echo "selected=\"selected\"";?> value="latest_content">Latest content items</option>
						<option <?php if ($content_source=='defined_content') echo "selected=\"selected\"";?> value="defined_content">Defined content items</option>
						<option <?php if ($content_source=='manual') echo "selected=\"selected\"";?> value="manual">Manual content</option>                        
                    </select>
                    </div>
				</div>

                <div class="col-lg-6">
                        <div class="form-group">
                        <label>Show in homepage of template</label> 
                        <select name="homepage_of_template" class="form-control">
                        <option <?php if ($homepage_of_template=='') echo "selected=\"selected\"";?> value="">- no template -</option>
                        <?php
                        if ($handle = opendir('../content/templates/'))
                            {
                            while (false !== ($entry = readdir($handle)))
                                {
                                if ($entry != "." && $entry != "..")
                                    {
                                        $entry2 = str_replace(".php", "", $entry);
                                        ?>
                                        <option <?php if ($homepage_of_template==$entry2) echo "selected=\"selected\"";?> value="<?php echo $entry2;?>"><?php echo $entry2;?></option>
                                        <?php
                                    }
                                }
                            closedir($handle);
                            }
                        ?>					
                        </select>
                        </div>
                </div>    

                                                         
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="slider_id" value="<?php echo $slider_id;?>" />
                <button type="submit" class="btn btn-primary">Edit slider</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>