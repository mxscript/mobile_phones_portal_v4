<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_cf_edit_<?php echo $cf_id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit custom field</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="custom_fields_edit_submit.php" method="post">
                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Name</label>
    	        <input class="form-control" name="name" type="text" value="<?php echo $cf_title;?>" />
        	    </div>
                </div>
				
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Type</label>
    	        <select name="type" class="form-control">
	            <option <?php if($cf_type=="text") echo 'selected="selected"';?> value="text">Text input</option>
    	        <option <?php if($cf_type=="yes_no") echo 'selected="selected"';?> value="yes_no">YES / NO type</option>
                <option <?php if($cf_type=="defined") echo 'selected="selected"';?> value="defined">Select from defined values / intervals</option>
    			</select>
        	    </div>
                </div>

				<div class="col-lg-6">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option <?php if($cf_active==1) echo 'selected="selected"';?> value="1">YES</option>
                <option <?php if($cf_active==0) echo 'selected="selected"';?> value="0">NO</option>
    			</select>
        	    </div>
				</div>	
				
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Position</label>
    	        <input class="form-control" name="position" type="text" value="<?php echo $cf_position;?>" />
        	    </div>
                </div>

                <div class="col-lg-6">	
                <div class="form-group">
	            <label>Show in specs page</label>
    	        <select name="show_in_specs" class="form-control">
    	        <option <?php if($cf_show_in_specs==1) echo 'selected="selected"';?> value="1">YES</option>
                <option <?php if($cf_show_in_specs==0) echo 'selected="selected"';?> value="0">NO</option>
    			</select>
        	    </div>
				</div>

                <div class="col-lg-6">	
                <div class="form-group">
	            <label>Show in searh filters</label>
    	        <select name="show_in_search" class="form-control">
    	        <option <?php if($cf_show_in_search==1) echo 'selected="selected"';?> value="1">YES</option>
                <option <?php if($cf_show_in_search==0) echo 'selected="selected"';?> value="0">NO</option>
    			</select>
        	    </div>
				</div>

				
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="group_id" value="<?php echo $group_id;?>" />
                <input type="hidden" name="cf_id" value="<?php echo $cf_id;?>" /> 
                <input type="hidden" name="section_id" value="<?php echo $cf_section_id;?>" />                              
                <button type="submit" class="btn btn-primary">Edit</button>
                
                <span style="float:right"><a class="btn btn-danger" href="javascript:deleteRecord_<?php echo $cf_id;?>('<?php echo $cf_id;?>');">Delete</a></span>
                <script language="javascript" type="text/javascript">
				function deleteRecord_<?php echo $cf_id;?>(RecordId)
				{
					if (confirm('Confirm delete')) {
						window.location.href = 'custom_fields_delete.php?cf_id=<?php echo $cf_id;?>&group_id=<?php echo $group_id;?>';
					}
				}
				</script> 
                
                </div>
                </div>
                
                </form>			
			</div>
                          
        </div>
	</div>
</div>