<?php 
include ("menu_admin.php");

		switch ($page)
        {
            case "ads":
            include ("ads.php");
            break;

            case "analytics":
            include ("analytics.php");
            break;

            case "content":
            include ("content.php");
            break;

            case "content_add":
            include ("content_add.php");
            break;

            case "content_edit":
            include ("content_edit.php");
            break;

            case "content_edit_cf":
            include ("content_edit_cf.php");
            break;

            case "content_media":
            include ("content_media.php");
            break;

            case "cart_settings":
            include ("cart_settings.php");
            break;

            case "cart_discounts":
            include ("cart_discounts.php");
            break;
						
            case "categ":
            include ("categ.php");
            break;

            case "categ_add":
            include ("categ_add.php");
            break;

            case "categ_edit":
            include ("categ_edit.php");
            break;

            case "comments":
            include ("comments.php");
            break;

            case "comments_edit":
            include ("comments_edit.php");
            break;

            case "contact_messages":
            include ("contact_messages.php");
            break;

            case "contact_messages_details":
            include ("contact_messages_details.php");
            break;

            case "custom_fields":
            include ("custom_fields.php");
            break;

            case "custom_fields_cf":
            include ("custom_fields_cf.php");
            break;

            case "dashboard":
                include ("dashboard.php");
            break;

            case "db_tools":
                include ("db_tools.php");
            break;

            case "logo":
            include ("logo.php");
            break;

            case "messages":
            include ("messages.php");
            break;

            case "messages_details":
            include ("messages_details.php");
            break;

            case "messages_create":
            include ("messages_create.php");
            break;

            case "newsletter":
            include ("newsletter.php");
            break;

            case "newsletter_subscribers":
            include ("newsletter_subscribers.php");
            break;

            case "newsletter_new":
            include ("newsletter_new.php");
            break;

            case "newsletter_edit":
            include ("newsletter_edit.php");
            break;

            case "newsletter_send":
            include ("newsletter_send.php");
            break;

            case "newsletter_sent_log":
            include ("newsletter_sent_log.php");
            break;

            case "newsletter_subscriber_log":
            include ("newsletter_subscriber_log.php");
            break;

            case "pages":
            include ("pages.php");
            break;

            case "profile":
            include ("profile.php");
            break;

            case "pages_add":
            include ("pages_add.php");
            break;

            case "pages_edit":
            include ("pages_edit.php");
            break;

            case "reports":
            include ("reports.php");
            break;

            case "ratings":
            include ("ratings.php");
            break;

            case "search_log":
            include ("search_log.php");
            break;

            case "settings":
            include ("settings.php");
            break;

            case "settings_contact_page":
            include ("settings_contact_page.php");
            break;

            case "settings_home_page":
            include ("settings_home_page.php");
            break;

            case "settings_comments":
            include ("settings_comments.php");
            break;

            case "settings_locale":
            include ("settings_locale.php");
            break;

            case "settings_media":
            include ("settings_media.php");
            break;

            case "settings_emails":
            include ("settings_emails.php");
            break;

            case "settings_users":
            include ("settings_users.php");
            break;

            case "settings_seo":
            include ("settings_seo.php");
            break;

            case "sliders":
            include ("sliders.php");
            break;

            case "slider_content":
            include ("slider_content.php");
            break;

            case "tools":
            include ("tools.php");
            break;

            case "templates":
            include ("templates.php");
            break;

            case "translates":
            include ("translates.php");
            break;

            case "translates_lang":
            include ("translates_lang.php");
            break;
			
            case "users":
            include ("users.php");
            break;

            case "users_edit":
            include ("users_edit.php");
            break;

            case "prices":
            include ("prices.php");
            break;

            case "help":
            include ("help.php");
            break;
						
            default:
            include ("dashboard.php");
	        break;						
        }	
?>
