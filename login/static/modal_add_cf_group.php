<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_cf_group" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Add new Custom Fields Group</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="custom_fields_group_submit.php" method="post">
                
                <div class="col-lg-8">
                <div class="form-group">
	            <label>Group name (required)</label>
    	        <input class="form-control" name="name" type="text" />
        	    </div>
				</div>

                <div class="col-lg-4">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option value="1">YES</option>
                <option value="0">NO</option>
    			</select>
        	    </div>
                </div>

                <div class="col-lg-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary">Add group</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>