<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_edit_price_<?php echo $price_id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit price</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="prices_edit_submit.php" method="post" enctype="multipart/form-data">
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Price (required)</label>
    	        <input class="form-control" name="price" type="text" value="<?php echo $price;?>" />
        	    </div>
                </div>              			

                                                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option <?php if ($active==1) echo "selected=\"selected\"";?> value="1">YES</option>
                <option <?php if ($active==0) echo "selected=\"selected\"";?> value="0">NO</option>
    			</select>
        	    </div>
                </div>

				<div class="clear"></div>
                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Details (optional)</label>
                <textarea name="details" rows="2" class="form-control"><?php echo $details;?></textarea>
        	    </div>
				</div>  
                                                
                <div class="clear"></div>

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Redirect URL</label>
    	        <input class="form-control" name="url" type="text" value="<?php echo $url;?>" />
        	    </div>
				</div>                                                 
				
                <div class="clear"></div>
                
				<div class="col-lg-12">
                <div class="form-group">
                <label>Change Seller Logo <small>(leave empty to keep existing logo)</small></label> 
                <input type="file" name="image">
                </div>
                </div>
                
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="content_id" value="<?php echo $content_id;?>" />
                <input type="hidden" name="price_id" value="<?php echo $price_id;?>" />                
                <button type="submit" class="btn btn-primary">Edit price</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>