<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_slider" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Add new slider</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="sliders_submit.php" method="post">
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Slider name (required)</label>
    	        <input class="form-control" name="name" type="text" />
        	    </div>
				</div>

                <div class="col-lg-6">
	                <div class="form-group">
					<label>Enable / disable slider</label>
					<select name="active" class="form-control">
						<option <?php if ($active=='1') echo "selected=\"selected\"";?> value="1">Slider enabled</option>
						<option <?php if ($active=='0') echo "selected=\"selected\"";?> value="0">Slider disabled</option>
                    </select>
                    </div>
				</div>
				
                <div class="col-lg-6">
	                <div class="form-group">
					<label>Slider content source</label>
					<select name="content_source" class="form-control">
						<option <?php if ($content_source=='latest_content') echo "selected=\"selected\"";?> value="latest_content">Latest content items</option>
						<option <?php if ($content_source=='defined_content') echo "selected=\"selected\"";?> value="defined_content">Defined content items</option>
						<option <?php if ($content_source=='manual') echo "selected=\"selected\"";?> value="manual">Manual content</option>                        
                    </select>
                    </div>
				</div>

                <div class="col-lg-6">
                        <div class="form-group">
                        <label>Show in homepage of template</label> 
                        <select name="homepage_of_template" class="form-control">
                        <option <?php if ($homepage_of_template=='') echo "selected=\"selected\"";?> value="">- no template -</option>
                        <?php
                        if ($handle = opendir('../content/templates/'))
                            {
                            while (false !== ($entry = readdir($handle)))
                                {
                                if ($entry != "." && $entry != "..")
                                    {
                                        $entry2 = str_replace(".php", "", $entry);
                                        ?>
                                        <option <?php if ($homepage_of_template==$entry2) echo "selected=\"selected\"";?> value="<?php echo $entry2;?>"><?php echo $entry2;?></option>
                                        <?php
                                    }
                                }
                            closedir($handle);
                            }
                        ?>					
                        </select>
                        </div>
                </div>    
				                
                <div class="col-lg-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary">Add slider</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>