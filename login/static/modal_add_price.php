<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_price" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Add new price</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="prices_submit.php" method="post" enctype="multipart/form-data">
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Price (required)</label>
    	        <input class="form-control" name="price" type="text" />
        	    </div>
                </div>              			

                <div class="col-lg-6">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option value="1">YES</option>
                <option value="0">NO</option>
    			</select>
        	    </div>
                </div>

				<div class="clear"></div>
                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Details (optional)</label>
                <textarea name="details" rows="2" class="form-control"></textarea>
        	    </div>
				</div>  
                                                
                <div class="clear"></div>

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Redirect URL</label>
    	        <input class="form-control" name="url" type="text" />
        	    </div>
				</div>  
                                                
                <div class="clear"></div>
                                                                
				<div class="col-lg-12">
                <div class="form-group">
                <label>Seller Logo (required):</label> 
                <input type="file" name="image">
                </div>
                </div>
                
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="content_id" value="<?php echo $content_id;?>" />
                <button type="submit" class="btn btn-primary">Add price</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>