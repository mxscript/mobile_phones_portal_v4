<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_edit_slider_image_<?php echo $slide_id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit slider image</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="slider_edit_image.php" method="post" enctype="multipart/form-data">

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Content title</label>
                <input name="title" type="text" class="form-control" value="<?php echo $title;?>" />
        	    </div>
				</div>  
                                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Content text</label>
                <textarea name="content" rows="3" class="form-control"><?php echo $content;?></textarea>
        	    </div>
				</div>  

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Destination URL (optional)</label>
                <input name="url" type="text" class="form-control" value="<?php echo $url;?>" />
        	    </div>
				</div> 
                                                
                <div class="col-lg-4">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option <?php if($active==1) echo 'selected="selected"';?> value="1">YES</option>
                <option <?php if($active==0) echo 'selected="selected"';?> value="0">NO</option>
    			</select>
        	    </div>
                </div>

                <div class="col-lg-4">
                <div class="form-group">
	            <label>URL target (optional)</label>
    	        <select name="target" class="form-control">
                <option <?php if($target=="_self") echo 'selected="selected"';?> value="_self">Same window</option>
    	        <option <?php if($target=="_blank") echo 'selected="selected"';?> value="_blank">New window</option>
    			</select>
        	    </div>
                </div>
                
                <div class="col-lg-4">
                <div class="form-group">
	            <label>Position</label>
    	        <input name="position" type="text" class="form-control" value="<?php echo $position;?>" />
        	    </div>
                </div>
                				
                <div class="clear"></div>
                
				<div class="col-lg-12">
                <div class="form-group">
                <label>Change image (optional)</label> 
                <input type="file" name="image">
                </div>
                </div>                         
                                                         
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="slide_id" value="<?php echo $slide_id;?>" />
                <input type="hidden" name="slider_id" value="<?php echo $slider_id;?>" />
                <button type="submit" class="btn btn-primary">Edit slider image</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>