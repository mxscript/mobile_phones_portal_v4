<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_edit_ad_<?php echo $ad_id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit ad</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="ads_edit_submit.php" method="post" enctype="multipart/form-data" onsubmit="MakeSafe_<?php echo $ad_id;?>()">
                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Ad name (required)</label>
    	        <input class="form-control" name="name" type="text" value="<?php echo nl2br($ad_name);?>" />
        	    </div>
				</div>
                
                <div class="clear"></div>

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Code</label>
                <textarea name="code" rows="8" class="form-control" id="ad_<?php echo $ad_id;?>"><?php echo $ad_code;?></textarea>
        	    </div>
				</div>  
                                                
	             <div class="clear"></div>
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option <?php if ($ad_active==1) echo "selected=\"selected\"";?> value="1">YES</option>
                <option <?php if ($ad_active==0) echo "selected=\"selected\"";?> value="0">NO</option>
    			</select>
        	    </div>
                </div>
				
                <div class="clear"></div>
                                
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $ad_id;?>" />
                <input type="submit" class="btn btn-primary" value="Edit ad" />
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>

<script language="javascript">
function MakeSafe_<?php echo $ad_id;?>(){
	var element = document.getElementById('ad_<?php echo $ad_id;?>');
	if(element.value != '' && element.value != null){
		var string = element.value;
		string = string.replace( /script/g, '##scr_start##' );
		string = string.replace( /\script/g, '##scr_end##' );

		//alert(string);
		element = document.getElementById('ad_<?php echo $ad_id;?>');
		element.value = string;
	}
}
</script>
