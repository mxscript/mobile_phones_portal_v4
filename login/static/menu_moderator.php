      <aside class="main-sidebar">

        <section class="sidebar">

          <ul class="sidebar-menu">
                        
            <li><a href="account.php?page=dashboard"><i class="fa fa-bars fa-fw"></i> <span>Dashboard</span></a></li>
            <li><a href="account.php?page=profile"><i class="fa fa-user fa-fw"></i> <span>My Profile</span></a></li>            
            			
            <li><a href="account.php?page=content"><i class="fa fa-folder-o fa-fw"></i> <span>Content</span></a></li>
            
			<li class="treeview">
              <a href="#"><i class="fa fa-comment fa-fw"></i> <span>Website Interactions</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=comments">Discussions</a></li>
                <li><a href="account.php?page=ratings">Ratings</a></li>                
                <li><a href="account.php?page=contact_messages">Contact messages</a></li>                
              </ul>
            </li> 
            
			<li class="treeview">
              <a href="#"><i class="fa fa-bar-chart fa-fw"></i> <span>Analytics</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=analytics">Analytics</a></li>
                <li><a href="account.php?page=search_log">Searches</a></li>
              </ul>
            </li>   

          </ul>
        </section>

      </aside>

