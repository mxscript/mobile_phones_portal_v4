<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_edit_cf_group_<?php echo $cf_group_id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit Custom Fields Group</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="custom_fields_group_edit_submit.php" method="post">
                
                <div class="col-lg-8">
                <div class="form-group">
	            <label>Group name (required)</label>
    	        <input class="form-control" name="name" type="text" value="<?php echo $cf_group_title;?>" />
        	    </div>
				</div>

                <div class="col-lg-4">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option <?php if ($cf_group_active==1) echo "selected=\"selected\"";?> value="1">YES</option>
                <option <?php if ($cf_group_active==0) echo "selected=\"selected\"";?> value="0">NO</option>
    			</select>
        	    </div>
                </div>


                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $cf_group_id;?>" />
                <button type="submit" class="btn btn-primary">Edit group</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>