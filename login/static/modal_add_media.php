<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_media" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">New media</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="content_media_submit.php" method="post" enctype="multipart/form-data">
                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Title</label>
    	        <input class="form-control" name="title" type="text" />
        	    </div>
				</div>                

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Description (optional)</label>
                <textarea name="description" rows="2" class="form-control"></textarea>
        	    </div>
				</div>  
                                                                
	            <div class="clear"></div>
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Type</label>
    	        <select name="type" class="form-control">
    	        <option value="image">Image</option>
                <option value="video">Video</option>
    			</select>
        	    </div>
                </div>
                

                <div class="col-lg-6">
                <div class="form-group">
	            <label>Image upload (for <strong>image</strong> media type)</label>
                <input type="file" name="image">
        	    </div>
				</div>  

                <div class="clear"></div>

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Embed code (for <strong>video</strong> media type)</label>
                <textarea name="embed_code" rows="4" class="form-control"></textarea>
        	    </div>
				</div>  

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Go to url (optional)</label><br />
				<small>Use this option when you want to go to a separate page, instead opening media. Useful to create multiple galleries</small>
    	        <input class="form-control" name="url_redirect" type="text" placeholder="http:// ......" />
        	    </div>
				</div>                
                                				
                <div class="clear"></div>
                                
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="content_id" value="<?php echo $content_id;?>" />
                <input type="hidden" name="source" value="<?php echo $source;?>" />
                <button type="submit" class="btn btn-primary">Add new media</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>