<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_slider_image" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Add new slider image</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="slider_add_image.php" method="post" enctype="multipart/form-data">

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Content title</label>
                <input name="title" type="text" class="form-control" />
        	    </div>
				</div>  
                                
                <div class="col-lg-12">
                <div class="form-group">
	            <label>Content text</label>
                <textarea name="content" rows="3" class="form-control"></textarea>
        	    </div>
				</div>  

                <div class="col-lg-12">
                <div class="form-group">
	            <label>Destination URL (optional)</label>
                <input name="url" type="text" class="form-control" />
        	    </div>
				</div> 
                                                
                <div class="col-lg-4">
                <div class="form-group">
	            <label>Active</label>
    	        <select name="active" class="form-control">
    	        <option value="1">YES</option>
                <option value="0">NO</option>
    			</select>
        	    </div>
                </div>

                <div class="col-lg-4">
                <div class="form-group">
	            <label>URL target (optional)</label>
    	        <select name="target" class="form-control">
                <option value="_self">Same window</option>
    	        <option value="_blank">New window</option>
    			</select>
        	    </div>
                </div>
                
                <div class="col-lg-4">
                <div class="form-group">
	            <label>Position</label>
    	        <input name="position" type="text" class="form-control" />
        	    </div>
                </div>
                				
                <div class="clear"></div>
                
				<div class="col-lg-12">
                <div class="form-group">
                <label>Upload image</label> 
                <input type="file" name="image">
                </div>
                </div>
                         
                                                         
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="slider_id" value="<?php echo $slider_id;?>" />
                <button type="submit" class="btn btn-primary">Add slider image</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>