      <aside class="main-sidebar">

        <section class="sidebar">

          <ul class="sidebar-menu">
                        
            <li><a href="account.php?page=dashboard"><i class="fa fa-bars fa-fw"></i> <span>Dashboard</span></a></li>

            <li><a href="account.php?page=profile"><i class="fa fa-user fa-fw"></i> <span>Profile</span></a></li>            
            
            <li><a href="account.php?page=comments"><i class="fa fa-comment fa-fw"></i> <span>Discussions</span></a></li>

            <li><a href="account.php?page=user_addressbook"><i class="fa fa-th-list fa-fw"></i> <span>Address Book</span></a></li>
            
          </ul>
        </section>

      </aside>

