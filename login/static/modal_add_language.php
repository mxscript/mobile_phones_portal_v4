<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_language" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Add new language</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="translates_submit.php" method="post">
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>language name (required)</label>
    	        <input class="form-control" name="name" type="text" />
        	    </div>
				</div>
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Language to fetch data</label>
    	        <select name="fetch_lang_id" class="form-control">
                <option value="0">- Empty language -</option>
                <?php
                $query_lang = "SELECT id, name, is_default FROM ".$database_table_prefix."lang ORDER BY is_default DESC, name ASC";
				$rs_lang = $conn->query($query_lang);
				

				while ($row = $rs_lang->fetch_assoc())
					{
					$fetch_lang_id = $row['id'];
	   	            $name = stripslashes($row['name']);
					$is_default = $row['is_default'];
					?>
		            <option value="<?php echo $fetch_lang_id;?>"><?php echo $name; if ($is_default==1) echo ' (default)';?></option>
                    <?php
					}
				?>
    			</select>
        	    </div>
				</div>
                                
                <div class="col-lg-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary">Add language</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>