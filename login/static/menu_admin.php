      <aside class="main-sidebar">

        <section class="sidebar">

          <ul class="sidebar-menu">
                        
            <li><a href="account.php?page=dashboard"><i class="fa fa-bars fa-fw"></i> <span>Dashboard</span></a></li>
            
            <li class="treeview">
              <a href="#"><i class="fa fa-wrench fa-fw"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=settings">General Settings</a></li>
                <li><a href="account.php?page=settings_seo">SEO and Social</a></li>
                <li><a href="account.php?page=settings_comments">Discussions and Ratings</a></li>
                <?php /*?><li><a href="account.php?page=settings_users">Users and Registration</a></li><?php */?>                
                <li><a href="account.php?page=settings_emails">Email Settings</a></li>
                <li><a href="account.php?page=settings_locale">Locale Settings</a></li>
                <li><a href="account.php?page=settings_media">Media Settings</a></li> 
                <li><a href="account.php?page=tools">Tools</a></li>              
              </ul>
            </li>

			<li class="treeview">
              <a href="#"><i class="fa fa-sitemap fa-fw"></i> <span>Website</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=categ">Structure</a></li>
                <li><a href="account.php?page=custom_fields">Custom Fields</a></li>                
                <li><a href="account.php?page=pages">Pages</a></li>
                <li><a href="account.php?page=settings_contact_page">Contact Page</a></li>                                
              </ul>
            </li>            
			
            <li><a href="account.php?page=content"><i class="fa fa-folder-o fa-fw"></i> <span>Content</span></a></li>
            
			<li class="treeview">
              <a href="#"><i class="fa fa-comment fa-fw"></i> <span>Website Interactions</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=comments">Discussions</a></li>
                <li><a href="account.php?page=contact_messages">Contact Messages</a></li>
                <li><a href="account.php?page=ratings">Ratings</a></li>                
              </ul>
            </li> 
            
            <li><a href="account.php?page=users"><i class="fa fa-user fa-fw"></i> <span>Users</span></a></li>
                        
            <li><a href="account.php?page=ads"><i class="fa fa-link fa-fw"></i> <span>Ads Management</span></a></li>            

			<li class="treeview">
              <a href="#"><i class="fa fa-bar-chart fa-fw"></i> <span>Analytics</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=analytics">Analytics</a></li>
                <li><a href="account.php?page=search_log">Searches</a></li>
              </ul>
            </li>   

			<li class="treeview">
              <a href="#"><i class="fa fa-magic fa-fw"></i> <span>Appearance</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=logo">Logo</a></li>                              
                <li><a href="account.php?page=templates">Templates</a></li>
                <li><a href="account.php?page=translates">Translates</a></li>                
              </ul>
            </li>

			<li class="treeview">
              <a href="#"><i class="fa fa-paper-plane-o fa-fw"></i> <span>Newsletters</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="account.php?page=newsletter_new">Create a newsletter</a></li>
                <li><a href="account.php?page=newsletter">Newsletters</a></li>
                <li><a href="account.php?page=newsletter_subscribers">Subscribers</a></li>                
              </ul>
            </li>  
                                                
            <li><a href="account.php?page=help"><i class="fa fa-question-circle fa-fw"></i> <span>Help and Support</span></a></li>
          </ul>
        </section>

      </aside>

