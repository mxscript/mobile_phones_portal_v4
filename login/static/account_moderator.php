<?php 
include ("menu_moderator.php");

		switch ($page)
        {
            case "analytics":
            include ("analytics.php");
            break;

            case "content":
            include ("content.php");
            break;

            case "content_add":
            include ("content_add.php");
            break;

            case "content_edit":
            include ("content_edit.php");
            break;

            case "content_edit_cf":
            include ("content_edit_cf.php");
            break;

            case "content_media":
            include ("content_media.php");
            break;

            case "prices":
            include ("prices.php");
            break;

            case "comments":
            include ("comments.php");
            break;

            case "comments_edit":
            include ("comments_edit.php");
            break;

            case "contact_messages":
            include ("contact_messages.php");
            break;

            case "contact_messages_details":
            include ("contact_messages_details.php");
            break;

            case "ratings":
            include ("ratings.php");
            break;

            case "dashboard":
                include ("dashboard.php");
            break;

            case "profile":
            include ("profile.php");
            break;

            case "reports":
            include ("reports.php");
            break;

            case "search_log":
            include ("search_log.php");
            break;
						
            default:
            include ("dashboard.php");
	        break;						
        }	
?>
