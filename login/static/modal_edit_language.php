<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_edit_language_<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">Edit language</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="translates_edit_submit.php" method="post">
                
                <div class="col-lg-6">
                <div class="form-group">
	            <label>Language name (required)</label>
    	        <input class="form-control" name="name" type="text" value="<?php echo $name;?>" />
        	    </div>
				</div>
                                                
                <div class="col-lg-12">
                <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $id;?>" />
                <button type="submit" class="btn btn-primary">Edit language</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>