<?php
debug_backtrace() || die ("Direct access not permitted"); 
require ("check_permision.php");
?>
<div class="modal fade modal_add_tag" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    	<div class="modal-content">
        	
            <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          	<h4 class="modal-title" id="mySmallModalLabel">New tag</h4>
			</div>
				
			<div class="modal-body">                
   
                <form action="tags_submit.php" method="post" enctype="multipart/form-data">
                
                <div class="col-lg-8">
                <div class="form-group">
	            <label>Tag</label>
    	        <input class="form-control" name="tag" type="text" />
        	    </div>
				</div>
                
               
                <div class="col-lg-4">
                <div class="form-group">
	            <label>Type</label>
    	        <select name="type" class="form-control">
    	        <option value="categ_tag">Category tag</option>                
    	        <option value="brand">Brand</option>

    			</select>
        	    </div>
                </div>
				
                <div class="clear"></div>
                                
                <div class="col-lg-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary">Add new tag</button>
                </div>
                </div>
                </form>			
                <div class="clear"></div>
			</div>
                          
        </div>
	</div>
</div>