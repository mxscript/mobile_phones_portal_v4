<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."categories";
$rs = $conn->query($query);
$rows = $rs->num_rows;

$query = "SELECT id FROM ".$database_table_prefix."categories WHERE parent_id = 0";
$rs = $conn->query($query);
$rows_root = $rs->num_rows;

$query = "SELECT id FROM ".$database_table_prefix."categories WHERE parent_id != 0";
$rs = $conn->query($query);
$rows_subcateg = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Website Structure (<?php echo $rows_root;?> main sections and <?php echo $rows_subcateg;?> subcategories)</h1> 
          <div class="clear"></div>  
          <a class="btn btn-primary" href="account.php?page=categ_add"><i class="fa fa-plus"></i> Add new category</a></h1>      
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
	if ($msg =='error_exist_content')
		echo '<p class="bg-danger">You can\'t delete because there is content in this category</p>';	
	if ($msg =='error_exist_subcategory')
		echo '<p class="bg-danger">You can\'t delete because this category have one ore more subcategories</p>';	

    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "No category";
	} 

else
	{
		$page_rows = 50;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		
		$query = "SELECT id, title, permalink, description, position, active, meta_title, meta_description, meta_keywords, show_in_menu, show_in_footer, image_type, image FROM ".$database_table_prefix."categories WHERE parent_id = 0 ORDER BY position ASC, title ASC $max";		
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
		<th width="60">Order</th>
        <th>Details</th>
        <th width="120">Content</th>
		<th width="180">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $categ_id = $row['id'];
			$title = stripslashes($row['title']);
			$permalink = $row['permalink'];
			$description = stripslashes($row['description']);
			$position = $row['position'];	
			$active = $row['active'];
			$meta_title = stripslashes($row['meta_title']);
			$meta_description = stripslashes($row['meta_description']);
			$meta_keywords = stripslashes($row['meta_keywords']);
			$show_in_menu = $row['show_in_menu'];
			$show_in_footer = $row['show_in_footer'];
			$image_type = $row['image_type'];
			$image = $row['image'];			
        ?>	
        <tr>
        	<td><?php echo $position;?></td>
            
            <td>
            <h4>
			<?php if($active==0) echo "<font color='#CC0000'>".$title."</font>"; else echo $title;?> <small>(ID: <?php echo $categ_id;?>)</small>
            <?php if ($image_type=="upload") { ?> <img style="max-height:20px; max-width:50px; height:auto; width:auto; margin-left:10px;" src="../content/media/img/<?php echo $image;?>" /> <?php } else echo html_entity_decode($image);?>
            </h4> 
            <div class="clear"></div>
            <?php	            
			get_child_categ_admin_details($categ_id);			            
			?> 
            </td>

                                               
            <td>
            <?php
			$subcategories_list = createSubcategories_string($categ_id);
			$data3 = "SELECT id FROM ".$database_table_prefix."content WHERE categ_id IN ($subcategories_list)";
			if($conn->query($data3) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); }
			$rs3 = $conn->query($data3);
			$number_items = $rs3->num_rows;
			?>
			<a href="account.php?page=content&search_categ_id=<?php echo $categ_id;?>"><?php echo $number_items;?> items</a>
            </td>
            
            <td>
            <i class="fa fa-edit fa-fw"></i> <a href="account.php?page=categ_edit&id=<?php echo $categ_id;?>">Edit Section</a>
            <div class="clear"></div>
            <i class="fa fa-image fa-fw"></i> <a href="account.php?page=settings_media&categ_id=<?php echo $categ_id;?>&return_page=categ">Custom Media Settings</a>
            <div class="clear"></div>
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $categ_id;?>('<?php echo $categ_id;?>');">Delete Section</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $categ_id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'categ_delete.php?id=<?php echo $categ_id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php } ?>
        </tbody>
		</table>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=categ&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=categ&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=categ&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=categ&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->