<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$name = $_POST["name"];
$name = Secure($name);

$id = $_POST["id"];
$id = Secure($id);

// check for inputs
if($name=="")
	{
	header("Location:account.php?page=translates&msg=error_lang");
	exit();
	}

// check for duplicate
$sql = "SELECT id FROM ".$database_table_prefix."lang WHERE name LIKE '$name' AND id != '$id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=translates&msg=error_duplicate_lang");
	exit();
	}

$query = "UPDATE ".$database_table_prefix."lang SET name = '$name' WHERE id = '$id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=translates&msg=edit_ok");	
exit;
?> 