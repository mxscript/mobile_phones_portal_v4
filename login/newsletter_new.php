<?php 
require ("checklogin.php");
require ("check_permision.php");
$code = random_code();	
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Create a new newsletter</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
	if ($msg =='error_subject')
		echo '<p class="bg-danger">Error. Input subject</p>';
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">
                             
           <form action="newsletter_submit.php" method="post">
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Newsletter subject</label>
            <input class="form-control" name="subject" type="text" />
            </div>
            </div>

   
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Message</label>
            <textarea id="textarea" name="message" class="form-control"></textarea>
            <script>
			$(document).ready(function() {
				$('#textarea').summernote({
					height: 200,
					onImageUpload: function(files, editor, welEditable) {
						sendFile(files[0], editor, welEditable);
					}
				});
				function sendFile(file, editor, welEditable) {
					data = new FormData();
					data.append("file", file);
					$.ajax({
						data: data,
						type: "POST",
						url: "texteditor_upload.php",
						cache: false,
						contentType: false,
						processData: false,
						success: function(url) {
							editor.insertImage(welEditable, url);
						}
					});
				}
			});
			</script>
            
            </div>
            </div>

                                    
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <a class="btn btn-danger btn-flat" href="account.php?page=newsletter_send&code=<?php echo $code;?>">Send now</a>
            <input type="hidden" name="code" value="<?php echo $code;?>" />
            <input class="btn btn-primary btn-flat" name="input" type="submit" value="Save draft" />            
            </div>  
            </div>
                      
            </form>
            <div class="clear"></div>


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->