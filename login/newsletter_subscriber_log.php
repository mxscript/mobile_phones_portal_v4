<?php 
require ("checklogin.php");
require ("check_permision.php");

$id = $_GET["id"];
$id = Secure($id);

$query = "SELECT id FROM ".$database_table_prefix."nl_sent WHERE subscriber_id = '$id'";
$rs = $conn->query($query);
$rows = $rs->num_rows;

$query = "SELECT email FROM ".$database_table_prefix."nl_subscribers WHERE id = '$id' LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$email = stripslashes($row['email']);
?>

<div class="content-wrapper">

        <section class="content-header">
        <span style="float:right"><a class="btn btn-danger btn-flat" href="account.php?page=newsletter"><i class="fa fa-undo"></i> Back to newsletters</a> </span>
          <h1>Messages log for subscriber "<?php echo $email;?>" (<?php echo $rows;?> total)</h1>                    
        </section>

        <!-- Main content -->
        <section class="content">


    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "Log empty";
	}

else
	{
		$page_rows = 30;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, newsletter_id, date FROM ".$database_table_prefix."nl_sent WHERE subscriber_id = '$id' ORDER BY id DESC $max";
		$rs = $conn->query($query);
		?>
		

        <div class="table-responsive">
        <table class="table table-bordered" id="listings">	
		<thead> 
        <tr>
            <th>Newsletter</th>
            <th width="240">Sent date</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $log_id = $row['id'];
			$newsletter_id = $row['newsletter_id'];
			$date = $row['date'];
			
			$query_sub = "SELECT subject FROM ".$database_table_prefix."nl_newsletters WHERE id = '$newsletter_id' LIMIT 1";
			$rs_sub= $conn->query($query_sub);
			$row = $rs_sub->fetch_assoc();
			$subject = stripslashes($row['subject']);  			
        ?>	
        <tr>            			       
            
            <td>
            <h4><?php echo $subject;?></h4>
            <i class="fa fa-edit"></i> <a href="account.php?page=newsletter_edit&id=<?php echo $newsletter_id;?>">View newsletter</a>
            <i class="fa fa-area-chart"></i> <a href="account.php?page=newsletter_sent_log&id=<?php echo $newsletter_id;?>">View log for this newsletter</a>            
            </td>
            
            <td>
            <?php echo DateTimeFormat($date);?>
            </td>
			
        </tr>
        <?php
        }
        ?>
        </tbody>
		</table>
        </div>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=newsletter_subscriber_log&pagenum=1&id=$id'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=newsletter_subscriber_log&pagenum=$previous&id=$id'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=newsletter_subscriber_log&pagenum=$next&id=$id'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=newsletter_subscriber_log&pagenum=$last&id=$id'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->