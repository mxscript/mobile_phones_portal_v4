<?php
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$id = $_GET['id'];
$id = Secure($id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

$content_id = $_GET['content_id'];
$content_id = Secure($content_id);

$source = $_GET['source'];
$source = Secure($source);

// **************************************************
// AUTHORS FILTERS
if (authorPermissionOK($content_id, $logged_user_id)==0)
	{
	header("Location: account.php?page=content&msg=no_permission&pagenum=$pagenum");
	exit;
	}
// **************************************************


// DELETE IMAGES
$sql = "SELECT file FROM ".$database_table_prefix."media WHERE id = '$id' LIMIT 1";
$rs = $conn->query($sql);
while ($row = $rs->fetch_assoc())
		{
		$img = $row['file'];
		if($img)
			{
			@unlink('../media/thumbs/'.$img); 
			@unlink('../media/small/'.$img);
			@unlink('../media/large/'.$img);
			}
		}

$sql = "DELETE FROM ".$database_table_prefix."media WHERE id = '$id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=content_media&msg=delete_ok&pagenum=$pagenum&id=$content_id&source=$source");
exit;
?>