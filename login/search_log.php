<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."search_log";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<script language="JavaScript">
function toggle(source) {
  checkboxes = document.getElementsByName('select[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Search log (<?php echo $rows;?> searches)</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "Log is empty";
	}

else
	{
		$page_rows = 30;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, search, date, ip, results FROM ".$database_table_prefix."search_log ORDER BY id DESC $max";
		$rs = $conn->query($query);
		?>
		

		<form action="search_log_bulk_action.php" method="post">         
        <div class="table-responsive">
        <table class="table table-bordered" id="listings">	
		<thead> 
        <tr>
            <th width="50" align="center"><input type="checkbox" onClick="toggle(this)" /></th>
            <th>Search term</th>
            <th width="160">Time</th>
            <th width="220">IP</th>
            <th width="120">Actions</th>            
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $id = $row['id'];
			$counter = $row['counter'];
			$ip = $row['ip'];
			$date = $row['date'];
			$search = stripslashes($row['search']);
			$results = $row['results'];							
        ?>	
        <tr>            
			<td align="left" valign="top">                    
			<input name="select[]" type="checkbox" value="<?php echo $id;?>" />
            </td>        

            <td>
			<strong><?php echo $search;?></strong>
            </td>
            
            <td>
            <?php echo DateTimeFormat($date);?>
            </td>
			            
            <td>
            <?php echo $ip;?>
            </td>
                        
            <td>
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $id;?>('<?php echo $id;?>');">Delete</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'search_log_delete.php?id=<?php echo $id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>

        </tr>
        <?php
        }
        ?>
        </tbody>
		</table>
        </div>
        
        <div class="clear"></div>
        With selected items:
            <select name="action" class="select">
                <option value="delete">Delete</option>
            </select>    
            
            <input type="hidden" name="page" value="search_log" />
            <input type="hidden" name="pagenum" value="<?php echo $pagenum;?>" />
            <input id="submit" name="input" type="submit" value="Apply" />
        </form>        


    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=search_log&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=search_log&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=search_log&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=search_log&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->