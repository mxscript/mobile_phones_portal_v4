<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$name = $_POST["name"];
$name = Secure($name);

$permalink = RewriteUrl($name);

$active = $_POST["active"];
$active = Secure($active);

// check for inputs
if($name=="")
	{
	header("Location:account.php?page=custom_fields&msg=error_name");
	exit();
	}

// check for duplicate
$sql = "SELECT id FROM ".$database_table_prefix."cf_groups WHERE permalink LIKE '$permalink' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=custom_fields&msg=error_duplicate");
	exit();
	}

$query = "INSERT INTO ".$database_table_prefix."cf_groups (id, title, permalink, active) VALUES (NULL, '$name', '$permalink', '$active')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=custom_fields&msg=add_ok");	
exit;
?> 