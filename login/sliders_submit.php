<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$name = $_POST["name"];
$name = Secure($name);

$homepage_of_template = $_POST["homepage_of_template"];
$homepage_of_template = Secure($homepage_of_template);

$content_source = $_POST["content_source"];
$content_source = Secure($content_source);

$active = $_POST["active"];
$active = Secure($active);

// check for inputs
if($name=="")
	{
	header("Location:account.php?page=sliders&msg=error_name");
	exit();
	}
$name_lower = strtolower($name);

// check for duplicate name
$sql = "SELECT id FROM ".$database_table_prefix."sliders WHERE LOWER(name) LIKE '$name_lower' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=sliders&msg=error_duplicate_name");
	exit();
	}

// a template can have only one slider active in same page
$sql = "UPDATE ".$database_table_prefix."sliders SET homepage_of_template = '' WHERE homepage_of_template = '$homepage_of_template'"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$query = "INSERT INTO ".$database_table_prefix."sliders (id, name, homepage_of_template, active, content_source) VALUES (NULL, '$name', '$homepage_of_template', '$active', '$content_source')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=sliders&msg=add_ok");	
exit;
?> 