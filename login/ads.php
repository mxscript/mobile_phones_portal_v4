<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Ads management</h1>
          <div class="clear"></div>
          <a class="btn btn-primary" data-toggle="modal" href="#" data-target=".modal_new_ad"><i class="fa fa-plus"></i> Create new ad</a>  
          <?php include ("static/modal_new_ad.php");?>      
        </section>

        <!-- Main content -->
        <section class="content">


		<?php         
        if ($msg =='error_code')
            echo '<p class="bg-danger">ERROR! Input code!</p>';	
        if ($msg =='error_name')
            echo '<p class="bg-danger">ERROR! Input name!</p>';	
        if ($msg =='delete_ok')
            echo '<p class="bg-info">Ad Deleted!</p>';	
        if ($msg =='add_ok')
            echo '<p class="bg-info">Ad Added!</p>';	
        if ($msg =='edit_ok')
            echo '<p class="bg-info">Ad Edited!</p>';	
		if ($msg =='error_demo_mode')
    		echo '<p class="bg-danger">Action disabled in demo mode!</p>';					
        ?>



    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

                To add ads, input follow code in your template file (.tpl file) in the place you want to show the ads (replace ID withh ad ID).
                <br /><br />

				<pre>
                    {$ads[ID]}
                </pre>
 
    
			<div class="clear"></div>


	<table class="table table-bordered" id="listings">	
    <tr>
      <th width="50" align="left" valign="middle">ID</th>
      	<th align="left" valign="middle">Ad code</th>
        <th width="100" align="left" valign="middle">Status</th>
		<th width="160" align="left" valign="middle">Actions</th>
	</tr>
	<?php
	$query = "SELECT id, name, code, active, protected FROM ".$database_table_prefix."ads_manager ORDER BY id ASC";
	$rs = $conn->query($query);

	while($row = $rs->fetch_assoc())
	{
		$ad_id = $row['id'];
		$ad_name = stripslashes($row['name']);
		$ad_code = $row['code'];
		$ad_active = $row['active'];
		$ad_protected = $row['protected'];
	?>	
    
	<tr>
	  <td align="left" valign="top">
      <strong><?php  echo $ad_id;?></strong>
      </td>
      
	  <td align="left" valign="top">
      <?php
	  if($ad_protected==1)
	  {
	  ?>
	  <span style="float:right"><i class="fa fa-lock fa-fw"></i> protected</span>	      
      <?php
	  }

	  if($ad_name!="") echo "<strong>".$ad_name."</strong><br />"; 	
	  echo "<small>".nl2br(htmlentities($ad_code))."</small>";
	  ?>
      </td>

	  <td align="left" valign="top">
      <?php 
	  if($ad_active == "1") echo "<font color='#339966'>ACTIVE</font>";
	  else echo "<font color='#CC3300'>INACTIVE</font>";
	  ?>
      </td>
      
	  <td align="left" valign="top">
      	<a data-toggle="modal" href="#" data-target=".modal_edit_ad_<?php echo $ad_id;?>"><i class="fa fa-pencil fa-fw"></i> Edit ad</a>
        <?php include ("static/modal_edit_ad.php");?> 
      	<div class="clear"></div>
        <?php
		if($ad_protected==0)
		{
		?>
		<a href="javascript:deleteRecord_<?php echo $ad_id;?>('<?php echo $ad_id;?>');"><i class="fa fa-trash-o fa-fw"></i> Delete ad</a>
        <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $ad_id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'ads_delete.php?id=<?php echo $ad_id;?>';
				}
			}
		</script>
        <?php
		}
		?>
      </td>
	</tr>
    
	<?php 
	}
	?>
</table>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->