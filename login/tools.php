<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Tools</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='sitemap_ok')
		echo '<p class="bg-info">Sitemap file updated</p>';		
	if ($msg =='rebuild_ok')
		echo '<p class="bg-info">Rebuild successfull</p>';		
	if ($msg =='delete_ok')
		echo '<p class="bg-info">Deleted</p>';		

	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>

	<div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">
                

        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Database tools
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                Rebuild xml sitemap
                <br />
                <a href="tools_rebuild_sitemap.php" class="btn btn-primary btn-flat">Rebuild sitemap file</a> 
                
				<div class="clear2"></div>

                Delete visitors log older than 30 days (recomended to save database space)
                <br />
                <a href="tools_clean_old_visits.php?return_page=tools" class="btn btn-primary btn-flat">Delete visitors log older than 30 days</a> 
                <div class="clear"></div>
                
                Rebuild content ratings
                <br />
                <a target="_blank" href="tools_rebuild_ratings.php" class="btn btn-primary btn-flat">Rebuild content ratings</a> 
                
				<div class="clear2"></div>

                Rebuild specs arrays <br />
				<small>This operation require time and resources. Use this only when you make any changes to custom fields (add, delete or edit custom fields)
                <br />
                <a target="_blank" href="tools_rebuild_specs_arrays.php" class="btn btn-primary btn-flat">Rebuild specs arrays</a> 

                
                <?php /*?>Rebuild / resize images (if you change image size settings)
                <div class="clear"></div>
                <a href="tools_rebuild_images.php" class="btn btn-primary btn-flat">Rebuild images</a> 
                
				<div class="clear2"></div><?php */?>
                               
                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        </div><!-- /.col-lg-12 -->                             

                    

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->

</div><!-- /.content-wrapper -->
