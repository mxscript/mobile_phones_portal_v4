<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$id = $_GET['id'];
$id = Secure($id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

$sql = "DELETE FROM ".$database_table_prefix."nl_subscribers WHERE id = '$id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$sql = "DELETE FROM ".$database_table_prefix."nl_sent WHERE subscriber_id = '$id'"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=newsletter_subscribers&msg=delete_ok&pagenum=$pagenum");
exit;
?>