<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

$comment_id = $_REQUEST["id"];
$comment_id = Secure($comment_id);

$pagenum = $_REQUEST["pagenum"];
$pagenum = Secure($pagenum);

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode&pagenum=$pagenum");
	exit();
	}

$query = "UPDATE ".$database_table_prefix."comments SET approved = '1' WHERE id = '$comment_id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $affected_rows = $conn->affected_rows;
}


header("Location: account.php?page=comments&msg=approve_ok&pagenum=$pagenum");	
exit;
?> 