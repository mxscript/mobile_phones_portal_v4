<?php
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	
if((isset($_SESSION['user_logged']) and $_SESSION['user_logged']==1) and isset($_SESSION['user_token']))
	{
		// User is logged (session)
		$user_session_token = Secure($_SESSION['user_token']);
		$sql_checklogin = "SELECT id, name, email, role_id, avatar FROM ".$database_table_prefix."users WHERE sha1(token) = '$user_session_token' AND active = '1' LIMIT 1";
	}

else if (isset($_COOKIE['user_token']))
	{
		// User is logged (cookie)
	    $user_cookie_token = Secure($_COOKIE['user_token']);
		$sql_checklogin = "SELECT id, name, email, role_id, avatar FROM ".$database_table_prefix."users WHERE sha1(token) = '$user_cookie_token' AND active = '1' LIMIT 1";
	}

else
	{
		// User not logged
		header("location: index.php?msg=not_logged");
		exit;
	}

		// User logged	
		$rs = $conn->query($sql_checklogin);
		$is_valid_user = $rs->num_rows;

		if ($is_valid_user==0)
			{
			$_SESSION = array();
			session_destroy();
			setcookie("user_token", "", time()-60*60*24*120, "/");  // 120 days ago
			header("location:index.php?msg=invalid_user");
			exit;
			}
	
			$row = $rs->fetch_assoc();
			$logged_user_id = $row['id'];
			$logged_user_name = stripslashes($row['name']);
			$logged_user_email = stripslashes($row['email']);
			$logged_user_role_id = $row['role_id'];
			$logged_user_avatar = $row['avatar'];
			if($logged_user_avatar=="") $logged_user_avatar = "no_avatar.png";
			
			$sql_user_role = "SELECT role FROM ".$database_table_prefix."users_roles WHERE id = '$logged_user_role_id' LIMIT 1";
			$rs_role = $conn->query($sql_user_role);
			$row_role = $rs_role->fetch_assoc();
			$logged_user_role = stripslashes($row_role['role']);

			// update last activity
			$now = date("Y-m-d H:i:s");
			$query = "UPDATE ".$database_table_prefix."users SET last_activity = '$now' WHERE id = '$logged_user_id' ORDER BY id DESC LIMIT 1"; 
			$conn->query($query);
		  	$affected_rows = $conn->affected_rows;
