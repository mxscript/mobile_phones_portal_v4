<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

$section_id = isset($_POST['section_id']) ? $_POST['section_id'] : '';
$section_id = Secure($section_id);

$group_id = isset($_POST['group_id']) ? $_POST['group_id'] : '';
$group_id = Secure($group_id);

$name = isset($_POST['name']) ? $_POST['name'] : '';
$name = Secure($name);

$type = isset($_POST['type']) ? $_POST['type'] : '';
$type = Secure($type);

$active = isset($_POST['active']) ? $_POST['active'] : '';
$active = Secure($active);
$active = (int)$active;

$position = isset($_POST['position']) ? $_POST['position'] : '';
$position = Secure($position);
$position = (int)$position;

$show_in_specs = isset($_POST['show_in_specs']) ? $_POST['show_in_specs'] : '';
$show_in_specs = Secure($show_in_specs);

$show_in_search = isset($_POST['show_in_search']) ? $_POST['show_in_search'] : '';
$show_in_search = Secure($show_in_search);

$permalink = RewriteUrl($name);

if($name=="")
	{
	header("Location:account.php?page=custom_fields_cf&msg=error_name&group_id=$group_id");
	exit();
	}

// check for duplicate
$sql = "SELECT id FROM ".$database_table_prefix."cf WHERE permalink LIKE '$permalink' AND group_id = '$group_id' AND section_id = '$section_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=custom_fields_cf&msg=error_duplicate&group_id=$group_id");
	exit();
	}


$query = "INSERT INTO ".$database_table_prefix."cf (id, group_id, section_id, type, title, permalink, active, position, show_in_specs, show_in_search) VALUES (NULL, '$group_id', '$section_id', '$type', '$name', '$permalink', '$active', '$position', '$show_in_specs', '$show_in_search')"; 
if($conn->query($query) === false) {
	echo "ERROR ".$conn->error;
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=custom_fields_cf&msg=add_ok&group_id=$group_id");	
exit;
?> 