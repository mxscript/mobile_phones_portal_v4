<?php 
require ("checklogin.php");
require ("check_permision.php");
$query = "SELECT id FROM ".$database_table_prefix."sliders";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Website Sliders</h1>
          <a class="btn btn-primary" data-toggle="modal" href="#" data-target=".modal_add_slider"><i class="fa fa-plus"></i> Add new slider</a>      
          <?php include ("static/modal_add_slider.php");?>           
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='add_ok')
		echo '<p class="bg-info">Slider added</p>';		
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Slider changed</p>';		
	if ($msg =='error_upload_image')
		echo '<p class="bg-danger">ERROR! Please upload image file</p>';		
	if ($msg =='error_name')
		echo '<p class="bg-danger">ERROR! Please input slider name</p>';		
	if ($msg =='error_duplicate_name')
		echo '<p class="bg-danger">ERROR! Duplicate name</p>';		

	?>
	
              
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "No slider";
	}

else
	{
		$page_rows = 50;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, name, homepage_of_template, active, content_source FROM ".$database_table_prefix."sliders ORDER BY id DESC $max";
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th>Slider details</th>
        <th width="260">Content source</th>
		<th width="130">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $slider_id = $row['id'];
			$name = stripslashes($row['name']);			
			$homepage_of_template = $row['homepage_of_template'];
			$active = $row['active'];
			$content_source = $row['content_source'];				
        ?>	
        <tr <?php if($active==0) echo 'bgcolor="#FEEAD6"';?>>            
            <td>
            <h4 style="margin-top:0"><?php echo $name;?></h4>
            <div class="clear"></div>
            <i>Show in homepage of template:</i> <?php echo '<strong>'.$homepage_of_template.'</strong>'; if($homepage_of_template==$config_template) echo ' (current template)';?>
            </td>
			
            <td>
            <?php echo str_replace("_", " ",$content_source);?>
            <div class="clear"></div>
            <a class="btn btn-danger btn-flat" href="account.php?page=slider_content&slider_id=<?php echo $slider_id;?>"><i class="fa fa-edit"></i> Manage slider content</a>
            </td>
           
            <td>
            <a data-toggle="modal" href="#" data-target=".modal_edit_slider_<?php echo $slider_id;?>"><i class="fa fa-pencil fa-fw"></i> Edit slider</a>
        	<?php include ("static/modal_edit_slider.php");?> 
            <div class="clear"></div>
            <a href="javascript:deleteRecord_<?php echo $slider_id;?>('<?php echo $slider_id;?>');"><i class="fa fa-trash-o fa-fw"></i> Delete slider</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $slider_id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'sliders_delete.php?id=<?php echo $slider_id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php 
        }
        ?>
        </tbody>
		</table>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=sliders&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=sliders&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=sliders&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=sliders&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->