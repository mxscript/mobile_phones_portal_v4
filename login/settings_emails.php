<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<div class="content-wrapper">
<form action="settings_submit.php" method="post">            

        <section class="content-header">
          <h1>Email settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if ($msg =='test_email_ok')
		echo '<p class="bg-info">Test email sent. Please check your email address.</p>';		

	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

	
			<div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Email settings                    
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">
                
                <div class="col-md-3">
	                <div class="form-group">
					<label>Email sending options:</label>
					<select name="email_send_option" class="form-control">
                    <option <?php if ($config_email_send_option=='php') echo "selected=\"selected\"";?> value="php"> PHP mailer </option>
                    <option <?php if ($config_email_send_option=='smtp') echo "selected=\"selected\"";?> value="smtp"> SMTP mailer (recomended)</option>
					</select>
                    </div>
				</div>
				
                <div class="col-md-4">
	                <div class="form-group">
					<label>Site email address</small></label>
					<input type="text" name="site_email" class="form-control" value="<?php echo $config_site_email;?>">
                    </div>
                </div>                    

                <div class="col-md-4">
	                <div class="form-group">
					<label>Email name (From: name)</small></label>
					<input type="text" name="site_email_name" class="form-control" value="<?php echo $config_site_email_name;?>">
                    </div>
                </div> 
                                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->




            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> SMTP settings (if enabled)                    
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                <div class="col-md-4">
	                <div class="form-group">
					<label>SMTP server</label>
					<input type="text" name="email_smtp_server" class="form-control" value="<?php echo $config_email_smtp_server;?>">
                    </div>
                </div> 

                <div class="col-md-4">
	                <div class="form-group">
					<label>SMTP user</label>
					<input type="text" name="email_smtp_user" class="form-control" value="<?php echo $config_email_smtp_user;?>">
                    </div>
                </div> 

                <div class="col-md-4">
	                <div class="form-group">
					<label>SMTP password</label>
					<input type="password" name="email_smtp_password" class="form-control" value="<?php echo $config_email_smtp_password;?>">
                    </div>
                </div> 

                                                                
                <div class="col-md-4">
	                <div class="form-group">
					<label>Encryption</label>
					<select name="email_smtp_encryption" class="form-control">
						<option <?php if ($config_email_smtp_encryption=='tls') echo "selected=\"selected\"";?> value="tls">TLS</option>
						<option <?php if ($config_email_smtp_encryption=='ssl') echo "selected=\"selected\"";?> value="ssl">SSL</option>
                    </select>
                    </div>
				</div>

                <div class="col-md-4">
	                <div class="form-group">
					<label>SMTP port</label>
					<input type="text" name="email_smtp_port" class="form-control" value="<?php echo $config_email_smtp_port;?>">
                    </div>
                </div> 
                								                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
        
	        <div class="form-group">    
            	<input type="hidden" name="settings_section" value="emails" />
            	<input type="hidden" name="return_page" value="settings_emails" />                 	            	
            	<button type="submit" class="btn btn-primary">Change email settings</button>		    
            </div>
                                             
			<div class="clear"></div>            


					</div><!-- /.box-body -->
                    
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</form>



        <section class="content-header">
          <h1>Test Email settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">


        <div class="row">
            <div class="col-lg-12">				
    
                <div class="box box-info">
                    
                    <div class="box-body">
    
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Send a test email using your settings                    
                            </div><!-- /.panel-heading -->
                            
                            <div class="panel-body">
                            
                            <form action="send_test_email.php" method="post">            
                            <div class="col-md-3">
                                <div class="form-group">
                                <input type="text" name="test_email" class="form-control" placeholder="Input email address">
                                </div>
                            </div>	
                        
                            <div class="col-md-3">    
                            <div class="form-group">    
                                <button type="submit" class="btn btn-primary">Send test email</button>		    
                            </div>                
                            </div>            			                                 
                            </form>
                                
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel -->


					</div><!-- /.box-body -->
                    
                  </div><!-- /.box -->

				</div>
		  </div>	                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->                        