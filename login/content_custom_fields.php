<?php 
require ("checklogin.php");
require ("check_permision.php");

$sql = "SET NAMES 'utf8'";
$conn->query($sql);

$sql = "SET CHARACTER 'utf8'";
$conn->query($sql);

$value = array();
$extra = array();

$cf_value = "";
$cf_extra = "";

if($cf_group_id!=0) $mysql_cond_group = "AND group_id = '$cf_group_id'";

$sql_edit = "SELECT cf_id, value, extra FROM ".$database_table_prefix."cf_values WHERE content_id = '$content_id' $mysql_cond_group";
$rs_edit = $conn->query($sql_edit);
while ($row = $rs_edit->fetch_assoc())
	{
    $edited_cf_id = $row['cf_id'];	
	$edited_cf_value = stripslashes($row['value']);
	$edited_cf_extra = stripslashes($row['extra']);
	$value[$edited_cf_id] = $edited_cf_value;
	$extra[$edited_cf_id] = $edited_cf_extra;
	//echo $edited_cf_id." - ".$value[$edited_cf_id]."<br>";
	}
		

	//if($cf_group_id==0) echo 'No custom fields for this content';

			if($cf_group_id!=0)
				{
				
				$sql_sections = "SELECT id, title FROM ".$database_table_prefix."cf_sections WHERE group_id = '$cf_group_id' AND active = 1 ORDER BY position ASC";
				$rs_sections = $conn->query($sql_sections);
        		while ($row = $rs_sections->fetch_assoc())
    	   			{
            		$cf_section_id = $row['id'];	
					$cf_section_title = stripslashes($row['title']);
					?>
                    <div class="col-lg-12">
                    <h4><?php echo $cf_section_title;?></h4>
                    </div>                    	
					<?php 				
					$sql_cf = "SELECT id, title, type FROM ".$database_table_prefix."cf WHERE group_id = '$cf_group_id' AND section_id = '$cf_section_id' AND active = 1 ORDER BY position ASC";
					$rs_cf = $conn->query($sql_cf);
        			while ($row = $rs_cf->fetch_assoc())
    	   				{
	            		$cf_id = $row['id'];	
						$cf_title = stripslashes($row['title']);	
						$cf_type = $row['type'];
						?>
                        
                        	<div class="col-lg-12">
                			<div class="form-group">
                			<label><strong><?php echo $cf_title;?></strong></label> 
                            <?php
							switch ($cf_type)
        						{
								case "yes_no":
								?>
                                <div class="clear"></div>
                                <div class="radio radio-info radio-inline">
                                <input id="inlineRadio1_<?php echo $cf_id;?>" value="yes" name="<?php echo $cf_id;?>" type="radio" <?php if ($value[$cf_id]=="yes") echo 'checked';?> />
                                <label for="inlineRadio1_<?php echo $cf_id;?>"> YES </label>
								</div>
                                
                                <div class="radio radio-info radio-inline">
                                <input id="inlineRadio2_<?php echo $cf_id;?>" value="no" name="<?php echo $cf_id;?>" type="radio" <?php if ($value[$cf_id]=="no") echo 'checked';?> />
                                <label for="inlineRadio2_<?php echo $cf_id;?>"> NO </label>
								</div>
                                
                                <div class="clear"></div>
                                <label><?php echo $cf_title;?> details (optional)</label>
                                <input class="form-control" name="<?php echo $cf_id;?>_extra" type="text" value="<?php echo $extra[$cf_id];?>" />
                                <?php
								break;
 
 
 								// ------------------------------------------------------------------
								// DEFINED VALUES / INTERVALS								
 								case "defined":
								?>
                                <br />
                                <div class="col-lg-6 nopadding_l">
                                <select name="<?php echo $cf_id;?>" class="form-control">
                                <option value=""> - please select -</option>
                                <?php
								$query_defined_cf = "SELECT id, title FROM ".$database_table_prefix."cf_defined WHERE group_id = '$cf_group_id' AND section_id = '$cf_section_id' AND active = 1 AND cf_id = '$cf_id' ORDER BY position ASC";
								$rs_defined_cf = $conn->query($query_defined_cf);
								while($row = $rs_defined_cf->fetch_assoc())
    	                			{
				                    $defined_cf_id = $row['id'];
				                    $defined_cf_title = stripslashes($row['title']);
    	                   			?>                                
	                				<option <?php if($defined_cf_id==$value[$cf_id]) echo 'selected="selected"';?> value="<?php echo $defined_cf_id;?>"><?php echo $defined_cf_title;?></option>
									<?php
									}  
									?>   
                                </select>
                                </div>
                                
                                <div class="clear"></div>
                                <label><?php echo $cf_title;?> details (optional)</label>
                                <input class="form-control" name="<?php echo $cf_id;?>_extra" type="text" value="<?php echo $extra[$cf_id];?>" />
                                <?php                             
								break;
								// ------------------------------------------------------------------
								
								
							  	default:
								?>
								<input class="form-control" name="<?php echo $cf_id;?>" value="<?php echo nl2br($value[$cf_id]);?>" type="text" />                                
                                <?php
								break;
								} // end switch
							?>

                            </div>
                            </div>
                            
                        <?php							
						} // end while
					} // end while
				}// end if
?>           