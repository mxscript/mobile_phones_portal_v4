<?php
require ("checklogin.php");
require ("check_permision.php");

$code = $_REQUEST['code'];
$code = Secure($code);

$query = "SELECT id FROM ".$database_table_prefix."contact_messages WHERE unique_code = '$code' LIMIT 1";
$rs = $conn->query($query);
$rows = $rs->num_rows;

$now = date("Y-m-d H:i:s");
$query = "UPDATE ".$database_table_prefix."contact_messages SET is_read = '1', time_read =' $now' WHERE unique_code = '$code' LIMIT 1";
$rs = $conn->query($query);
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Message details</h1> 
        </section>

        <!-- Main content -->
        <section class="content">

        <div class="row">
            <div class="col-lg-12">				
    
                <div class="box box-info">
                    
                    <div class="box-body">

			<?php
            // ------------------------------------------------------------------------------------------------------
            if ($rows==0)
                {
                    echo "Invalid message";
                }
            else
                {
				$query = "SELECT id, unique_code, name, email, time, message, is_read, time_read, ip FROM ".$database_table_prefix."contact_messages WHERE unique_code = '$code' LIMIT 1";
				$rs = $conn->query($query);
				$row = $rs->fetch_assoc();

				$message_id = $row['id'];
				$message_unique_code = $row['unique_code'];
				$name = $row['name'];
				$email = $row['email'];
				$time = $row['time'];		
				$message = stripslashes($row['message']);
				$is_read = $row['is_read'];	
				$time_read = $row['time_read'];
				$ip = $row['ip'];
				?>

	            Send by: <?php echo $name;?> (<?php echo $email;?>) <small><?php echo DateTimeFormat($time);?></small>
				
                <div class="clear"></div>
                
                <?php echo nl2br($message);
                }						
		    ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->