<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

$id = $_GET['id'];
$id = Secure($id);

$group_id = $_GET['group_id'];
$group_id = Secure($group_id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

$sql = "DELETE FROM ".$database_table_prefix."cf_sections WHERE id = '$id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$sql = "DELETE FROM ".$database_table_prefix."cf WHERE section_id = '$id'"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=custom_fields_cf&msg=delete_ok&group_id=$group_id");
exit;
?>