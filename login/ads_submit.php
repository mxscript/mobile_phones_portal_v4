<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$code = $_POST['code'];
$code = stripslashes($code);

$name = $_POST['name'];
$name = Secure($name);

$active = $_POST['active'];
$active = Secure($active);


if (!$name)
	{
	header("Location:account.php?page=ads&msg=error_name");
	exit();
	}

if (!$code)
	{
	header("Location:account.php?page=ads&msg=error_code");
	exit();
	}

$query = "INSERT INTO ".$database_table_prefix."ads_manager (id, name, code, active) VALUES (NULL, '$name', '$code', '$active')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

header("Location:account.php?page=ads&msg=add_ok");
exit;