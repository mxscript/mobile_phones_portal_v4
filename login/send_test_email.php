<?php
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");
require ("../core/plugins/smtp/PHPMailerAutoload.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$test_email = $_POST["test_email"];	
$test_email = Secure($test_email);

if($test_email=="")
	{
	header("Location: account.php?page=settings_emails");
	exit;
	}


// *****************************************************************************************************************************
// SEND MAIL
// *****************************************************************************************************************************
if($config_email_send_option=="smtp")
	{
		// SMTP MAILER	
		//----------------------------------------------------------------------------------------------------------		
		$mail = new PHPMailer;
		
		$mail->IsSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $config_email_smtp_server;                 // Specify main and backup server
		$mail->Port = $config_email_smtp_port;                                    // Set the SMTP port
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $config_email_smtp_user;                // SMTP username
		$mail->Password = $config_email_smtp_password;                  // SMTP password
		$mail->SMTPSecure = $config_email_smtp_encryption;                            // Enable encryption, 'ssl' also accepted

		$mail->setFrom($config_site_email, $config_site_email_name);
		$mail->addReplyTo($config_site_email);
		$mail->AddAddress($test_email);  
				
		$mail->IsHTML(true);                                  // Set email format to HTML
		
		$mail->Subject = 'Test email from '.$config_site_url;
		$mail->Body    = '
		<html>
		<head>
		  <title>Test email</title>
		</head>
		<body>
		  <div style="font-size:12px;font-family:arial;">
		  <p>This is a test email from '.$config_site_url.'. Congratulations! Your email settings works fine.</p>
		  </div>
		</body>
		</html>
		';
		$mail->AltBody = 'This is a test email from '.$config_site_url.'. Congratulations! Your email settings works fine.';
		
		if(!$mail->Send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   exit;
		}
		

	}

else
	{
		// PHP MAILER	
		//----------------------------------------------------------------------------------------------------------
		$to      = '$test_email';
		$subject = 'Test email from '.$config_site_url;
		$message = '
		<html>
		<head>
		  <title>Test email</title>
		</head>
		<body>
		  <div style="font-size:12px;font-family:arial;">
		  <p>This is a test email from '.$config_site_url.'. Congratulations! Your email settings works fine.</p>
		  </div>
		</body>
		</html>
		';

		// HTML mail
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		$headers .= 'From: '.$config_site_email."\r\n" .
			'Reply-To: '.$config_site_email."\r\n" .
			'X-Mailer: PHP/' . phpversion();
		mail($test_email, $subject, $message, $headers);
		
		//-------------------------------------------------------------------------------------------------------------
	}

	
header("Location: account.php?page=settings_emails&msg=test_email_ok");
exit;
?>