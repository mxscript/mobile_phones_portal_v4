<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

$id = $_GET['id'];
$id = Secure($id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

// check for subcategories
$sql = "SELECT id FROM ".$database_table_prefix."categories WHERE parent_id = '$id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=categ&msg=error_exist_subcategory");
	exit();
	}

// check for content
$sql = "SELECT id FROM ".$database_table_prefix."content WHERE categ_id = '$id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=categ&msg=error_exist_content");
	exit();
	}


// remove deleted categ ID 
$sql2 = "UPDATE ".$database_table_prefix."content SET categ_id = 0 WHERE categ_id = '$id'"; 	
if($conn->query($sql2) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }
	
$sql = "DELETE FROM ".$database_table_prefix."categories WHERE id = '$id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=categ&msg=delete_ok&pagenum=$pagenum");
exit;
?>