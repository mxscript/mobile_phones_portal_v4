<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$return_page = $_GET['return_page'];
$return_page = Secure($return_page);
if($return_page=="") $return_page = 'analytics';

$period_date = date("Y-m-d", strtotime("-30 day"));

$sql = "DELETE FROM ".$database_table_prefix."visitors_log WHERE DATE(date) <= '$period_date'";
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=$return_page&msg=delete_ok");
exit;