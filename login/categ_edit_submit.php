<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

$id = isset($_POST['id']) ? $_POST['id'] : '';
$id = Secure($id);

$parent_id = isset($_POST['parent_id']) ? $_POST['parent_id'] : '';
$parent_id = Secure($parent_id);
$parent_id = (int)$parent_id;

$title = isset($_POST['title']) ? $_POST['title'] : '';
$title = Secure($title);
$title_encoded = urlencode($title);

$position = isset($_POST['position']) ? $_POST['position'] : '';
$position = Secure($position);
$position = (int)$position;

$description = isset($_POST['description']) ? $_POST['description'] : '';
$description = Secure($description);
$description_encoded = urlencode($description);

$active = isset($_POST['active']) ? $_POST['active'] : '';
$active = Secure($active);
$active = (int)$active;

$show_in_menu = isset($_POST['show_in_menu']) ? $_POST['show_in_menu'] : '';
$show_in_menu = Secure($show_in_menu);

$show_in_footer = isset($_POST['show_in_footer']) ? $_POST['show_in_footer'] : '';
$show_in_footer = Secure($show_in_footer);

$custom_tpl_file = isset($_POST['custom_tpl_file']) ? $_POST['custom_tpl_file'] : '';
$custom_tpl_file = Secure($custom_tpl_file);

$slider_id = isset($_POST['slider_id']) ? $_POST['slider_id'] : '';
$slider_id = Secure($slider_id);
$slider_id = (int)$slider_id;

$show_only_in_template = isset($_POST['show_only_in_template']) ? $_POST['show_only_in_template'] : '';
$show_only_in_template = Secure($show_only_in_template);

$image_type = isset($_POST['image_type']) ? $_POST['image_type'] : '';
$image_type = Secure($image_type);

$image_fa = isset($_POST['image_fa']) ? $_POST['image_fa'] : '';
$image_fa = Secure($image_fa);

$meta_title = isset($_POST['meta_title']) ? $_POST['meta_title'] : '';
$meta_title = Secure($meta_title);
$meta_title_encoded = urlencode($meta_title);

$meta_description = isset($_POST['meta_description']) ? $_POST['meta_description'] : '';
$meta_description = Secure($meta_description);
$meta_description_encoded = urlencode($meta_description);

$meta_keywords = isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : '';
$meta_keywords = Secure($meta_keywords);
$meta_keywords_encoded = urlencode($meta_keywords);

$permalink = isset($_POST['permalink']) ? $_POST['permalink'] : '';
$permalink = Secure($permalink);
if($permalink!="")
	$permalink = RewriteUrl($permalink);
else	
	$permalink = RewriteUrl($title);


if($title=="")
	{
	header("Location:account.php?page=categ_edit&id=$id&msg=error_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}


// check for duplicate title (in the same section)
$sql = "SELECT id FROM ".$database_table_prefix."categories WHERE title LIKE '$title' AND id != '$id' AND parent_id = '$parent_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=categ_edit&id=$id&msg=error_duplicate_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}

// check for duplicate permalink (in the same section)
$sql = "SELECT id FROM ".$database_table_prefix."categories WHERE permalink LIKE '$permalink' AND id != '$id' AND parent_id = '$parent_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=categ_edit&id=$id&msg=error_duplicate_permalink&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}

	
$query = "UPDATE ".$database_table_prefix."categories SET title = '$title', permalink = '$permalink', parent_id = '$parent_id', description = '$description', position = '$position', active = '$active', meta_title = '$meta_title', meta_description = '$meta_description', meta_keywords = '$meta_keywords', show_in_menu = '$show_in_menu', show_in_footer = '$show_in_footer', custom_tpl_file = '$custom_tpl_file', slider_id = '$slider_id', show_only_in_template = '$show_only_in_template', image_type = '$image_type', image = '$image_fa' WHERE id = '$id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $affected_rows = $conn->affected_rows;
}

// IMAGE
if($image_type=="upload" and $_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/img/".$image);

		$query = "UPDATE ".$database_table_prefix."categories SET image = '$image' WHERE id = '$id' LIMIT 1"; 
		if($conn->query($query) === false) {} else { $affected_rows = $conn->affected_rows;	}

		}
	}


// form OK:
header("Location: account.php?page=categ&msg=edit_ok");	
exit;
?> 