<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."users";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Users (<?php echo $rows;?> users)</h1> 
          <div class="clear"></div>  
          <a class="btn btn-primary" data-toggle="modal" href="#" data-target=".modal_add_user"><i class="fa fa-plus"></i> Add new user</a>      
          <?php include ("static/modal_add_user.php");?>  
          <div class="clear"></div>
                <h4>Users roles description:</h4>
                <strong>Staff user roles</strong><br />
                <?php
				$sql_roles = "SELECT id, role, title, role_description FROM ".$database_table_prefix."users_roles WHERE active = '1' AND is_staff = 1 ORDER BY id ASC";
				$rs_roles = $conn->query($sql_roles);
				while($row = $rs_roles->fetch_assoc())
					{
					$role_id = $row['id'];
					$role = stripslashes($row['role']);
					$role_title = stripslashes($row['title']);
					$role_description = stripslashes($row['role_description']);
					?>
	                <em><?php echo $role_title." (".$role.")";?></em> - <?php echo $role_description;?><br />
                    <?php
					}
					?>
                <div class="clear"></div>    
               
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">User added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">User modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">user deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
	if ($msg =='error_name')
		echo '<p class="bg-danger">Input full name</p>';	
	if ($msg =='error_email')
		echo '<p class="bg-danger">Input valid email</p>';	
	if ($msg =='error_password')
		echo '<p class="bg-danger">Input password</p>';	
	if ($msg =='error_delete_protected')
		echo '<p class="bg-danger">Error! This user can not be deleted</p>';	

	if ($msg =='error_duplicate_email')
		echo '<p class="bg-danger">There is another user with this email address</p>';	
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "No user";
	}

else
	{
		$page_rows = 50;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, email, name, permalink, bio, avatar, role_id, active, email_verified, register_time, register_ip,register_host,  protected, last_activity FROM ".$database_table_prefix."users ORDER BY id DESC $max";
		$rs = $conn->query($query);
		?>
		
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th>User details</th>
        <th width="160">Role</th>
        <th width="160">Content</th>
		<th width="100">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $user_id = $row['id'];
			$email = $row['email'];
			$name = stripslashes($row['name']);
			$permalink = stripslashes($row['permalink']);
			$bio = stripslashes($row['bio']);
			$avatar = $row['avatar'];
			$role_id = $row['role_id'];
			$active = $row['active'];	
			$email_verified = $row['email_verified'];
			$register_time = $row['register_time'];
			$register_ip = $row['register_ip'];
			$register_host = $row['register_host'];	
			$last_activity = $row['last_activity'];		
			
			$bio = strip_tags($bio);
			$bio2 = substr($bio, 0, 300);
			
        ?>	
        <tr <?php if($active==0) echo 'bgcolor="#FEEAD6"';?>>            
            <td>
            <?php
			if($avatar)
				{
				?>
                <span style="float: left; margin-right:10px;"><a target="_blank" href="<?php echo $config_site_media;?>/avatars/<?php echo $avatar;?>"><img style="max-width:30px; height:auto;" src="<?php echo $config_site_media;?>/avatars/<?php echo $avatar;?>" /></a></span>
                <?php					
				}
			?>
            <h3 style="margin-top:0;"><?php echo $name;?></h3> 
            <?php if($bio2) echo $bio2.'<div class="clear"></div>';?>
            <small>
            Email: <?php echo $email;?><br />
            Register time: <?php echo DateTimeFormat($register_time);?> | 
            Active: <?php if($active==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?> |
            Email verified: <?php if($email_verified==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?><br />
			Register IP: <?php echo $register_ip;?> | Register host: <?php echo $register_host;?><br />
			Last activity: <?php echo DateTimeFormat($last_activity);?>
            </small>
            </td>
			
            <td>
            <?php 
			$query_user_role = "SELECT title FROM ".$database_table_prefix."users_roles WHERE id = '$role_id' LIMIT 1";
			$rs_user_role = $conn->query($query_user_role);
			$row = $rs_user_role->fetch_assoc();
   	        $user_role = stripslashes($row['title']);   
			echo $user_role;                     
			?>
            </td>
                    
            <td>
            <?php
			$data2 = "SELECT id FROM ".$database_table_prefix."content WHERE user_id = '$user_id'";
			if($conn->query($data2) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR); }

			$rs2 = $conn->query($data2);
			$number_content = $rs2->num_rows;
			?>
            <a href="account.php?page=content&search_user_id=<?php echo $user_id;?>"><?php echo $number_content;?> items</a>
            </td>
           
            <td>
            <i class="fa fa-edit fa-fw"></i> <a href="account.php?page=users_edit&id=<?php echo $user_id;?>">Edit</a>
            <div class="clear"></div>
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $user_id;?>('<?php echo $user_id;?>');">Delete</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $user_id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'users_delete.php?id=<?php echo $user_id;?>&pagenum=<?php echo $pagenum;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php 
        }
        ?>
        </tbody>
		</table>

    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=users&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=users&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=users&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=users&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->