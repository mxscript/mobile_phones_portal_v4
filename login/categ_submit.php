<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$categ_id = isset($_POST['categ_id']) ? $_POST['categ_id'] : '';
$categ_id = Secure($categ_id);
$categ_id = (int)$categ_id;

$title = isset($_POST['title']) ? $_POST['title'] : '';
$title = Secure($title);
$title_encoded = urlencode($title);

$position = isset($_POST['position']) ? $_POST['position'] : '';
$position = Secure($position);
$position = (int)$position;

$description = isset($_POST['description']) ? $_POST['description'] : '';
$description = Secure($description);
$description_encoded = urlencode($description);

$active = isset($_POST['active']) ? $_POST['active'] : '';
$active = Secure($active);
$active = (int)$active;

$show_in_menu = isset($_POST['show_in_menu']) ? $_POST['show_in_menu'] : '';
$show_in_menu = Secure($show_in_menu);

$show_in_footer = isset($_POST['show_in_footer']) ? $_POST['show_in_footer'] : '';
$show_in_footer = Secure($show_in_footer);

$custom_tpl_file = isset($_POST['custom_tpl_file']) ? $_POST['custom_tpl_file'] : '';
$custom_tpl_file = Secure($custom_tpl_file);

$slider_id = isset($_POST['slider_id']) ? $_POST['slider_id'] : '';
$slider_id = Secure($slider_id);
$slider_id = (int)$slider_id;

$show_only_in_template = isset($_POST['show_only_in_template']) ? $_POST['show_only_in_template'] : '';
$show_only_in_template = Secure($show_only_in_template);

$image_type = isset($_POST['image_type']) ? $_POST['image_type'] : '';
$image_type = Secure($image_type);

$image_fa = isset($_POST['image_fa']) ? $_POST['image_fa'] : '';
$image_fa = Secure($image_fa);

$meta_title = isset($_POST['meta_title']) ? $_POST['meta_title'] : '';
$meta_title = Secure($meta_title);
$meta_title_encoded = urlencode($meta_title);

$meta_description = isset($_POST['meta_description']) ? $_POST['meta_description'] : '';
$meta_description = Secure($meta_description);
$meta_description_encoded = urlencode($meta_description);

$meta_keywords = isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : '';
$meta_keywords = Secure($meta_keywords);
$meta_keywords_encoded = urlencode($meta_keywords);

$permalink = isset($_POST['permalink']) ? $_POST['permalink'] : '';
$permalink = Secure($permalink);
if($permalink!="")
	$permalink = RewriteUrl($permalink);
else	
	$permalink = RewriteUrl($title);

if($title=="")
	{
	header("Location:account.php?page=categ_add&msg=error_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}

// check for duplicate title (in the same category)
$sql = "SELECT id FROM ".$database_table_prefix."categories WHERE title LIKE '$title' AND parent_id = '$categ_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=categ_add&msg=error_duplicate_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}

// check for duplicate permalink (in the same category)
$sql = "SELECT id FROM ".$database_table_prefix."categories WHERE permalink LIKE '$permalink' AND parent_id = '$categ_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=categ_add&msg=error_duplicate_permalink&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}


$query = "INSERT INTO ".$database_table_prefix."categories (id, parent_id, title, permalink, description, position, active, meta_title, meta_description, meta_keywords, show_in_menu, show_in_footer, custom_tpl_file, slider_id, show_only_in_template, image_type, image) VALUES (NULL, '$categ_id', '$title', '$permalink', '$description', '$position', '$active', '$meta_title', '$meta_description', '$meta_keywords', '$show_in_menu', '$show_in_footer', '$custom_tpl_file', '$slider_id', '$show_only_in_template', '$image_type', '$image_fa')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_ALL);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// get ID
$query_id = "SELECT id FROM ".$database_table_prefix."categories ORDER BY id DESC LIMIT 1";
$rs_id = $conn->query($query_id);
$row = $rs_id->fetch_assoc();
$categ_id = $row['id'];
		
// IMAGE
if($image_type=="upload" and $_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/img/".$image);

		$query = "UPDATE ".$database_table_prefix."categories SET image = '$image' WHERE id = '$categ_id' LIMIT 1"; 
		if($conn->query($query) === false) {} else { $affected_rows = $conn->affected_rows;	}

		}
	}

// form OK:
header("Location: account.php?page=categ&msg=add_ok");	
exit;
?> 