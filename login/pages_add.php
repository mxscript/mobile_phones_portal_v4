<?php 
require ("checklogin.php");
require ("check_permision.php");

if(isset($_GET["description"]))
	{
	$description = $_GET["description"];
	$description_decoded = urldecode($description);
	}
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Add new page</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
	if ($msg =='error_title')
		echo '<p class="bg-danger">Error. Input page title</p>';
	if ($msg =='error_duplicate_title')
		echo '<p class="bg-danger">Error. There is another page with this title</p>';	
	if ($msg =='error_duplicate_permalink')
		echo '<p class="bg-danger">Error. There is another page with this permalink</p>';						
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">
                             
            <form name="AddPage" action="pages_submit.php" method="post">
            
            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Page title</label>
            <input class="form-control" name="title" type="text" value="<?php echo $title;?>" />
            </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Is active?</label>
            <select name="active" class="form-control">
            <option value="1">YES</option>
            <option value="0">NO</option>
    		</select>
            </div>
            </div>


            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Show in header</label>
            <select name="show_in_header" class="form-control">
            <option value="1">YES</option>
            <option value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Show in footer</label>
            <select name="show_in_footer" class="form-control">
            <option value="1">YES</option>
            <option value="0">NO</option>
    		</select>
            </div>
            </div>

          	<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Page order</label>
            <input type="text" name="position" class="form-control" />
            </div>
            </div>
			
            <div class="clear"></div>
            	
          	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Redirect to url</label>
            <input type="text" name="redirect_url" class="form-control" placeholder="http://" value="<?php echo $redirect_url;?>" >
            </div>
            </div>			
            
          	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Custom PHP file</label>
            <input type="text" name="custom_php_file" class="form-control"  />
            </div>
            </div>
                                    
          	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Custom template file</label>
            <input type="text" name="custom_tpl_file" class="form-control"  />
            </div>
            </div>

               
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Page content</label>
            <textarea id="textarea" name="description" rows="3" class="form-control"></textarea>
            <script>
			$(document).ready(function() {
				$('#textarea').summernote({
					height: 200,
					onImageUpload: function(files, editor, welEditable) {
						sendFile(files[0], editor, welEditable);
					}
				});
				function sendFile(file, editor, welEditable) {
					data = new FormData();
					data.append("file", file);
					$.ajax({
						data: data,
						type: "POST",
						url: "texteditor_upload.php",
						cache: false,
						contentType: false,
						processData: false,
						success: function(url) {
							editor.insertImage(welEditable, url);
						}
					});
				}
			});
			</script>
            
            </div>
            </div>

            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>SEO settings:</h2>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
            <label>Meta title</label>
            <input class="form-control" name="meta_title" type="text" />
            </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
            <label>Meta description</label>
            <input class="form-control" name="meta_description" type="text" />
            </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
            <label>Meta Keywords</label>
            <input class="form-control" name="meta_keywords" type="text" />
            </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="form-group">
            <label>URL structure (leave blank to autogenerate permalink)</label>
            <input class="form-control" name="permalink" type="text" />
            </div>
            </div>
            
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input class="btn btn-primary" name="input" type="submit" value="Add page" />
            </div>  
            </div>
                      
            </form>
            <div class="clear"></div>


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->