<?php
require ("checklogin.php");
require ("check_permision.php");

$search = isset($_REQUEST['search']) ? $_REQUEST['search'] : '';
$search = Secure($search);		

$search_terms_user = isset($_REQUEST['search_terms_user']) ? $_REQUEST['search_terms_user'] : '';
$search_terms_user = Secure($search_terms_user);		

$query = "SELECT id FROM ".$database_table_prefix."contact_messages";

$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<script language="JavaScript">
function toggle(source) {
  checkboxes = document.getElementsByName('select[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Contact Messages (<?php echo $rows;?> messages)</h1> 
        </section>

        <!-- Main content -->
        <section class="content">


	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Message sent</p>';		
    if ($msg =='resend_ok')
        echo '<p class="bg-info">Message sent to recipient(s)</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Message edited</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Message deleted</p>';
	if ($msg =='error_class')
		echo '<p class="bg-danger">Error. Select class</p>';	
	if ($msg =='error_select_user')
		echo '<p class="bg-danger">Error! Select user</p>';	
	if ($msg =='send_email_ok')
		echo '<p class="bg-info">Email sent</p>';							
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}
if ($rows==0)
	{
		echo "No contact message";
	}
else
	{
		$page_rows = 50;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		
	
		$query = "SELECT id, unique_code, name, email, time, message, is_read, time_read, ip FROM ".$database_table_prefix."contact_messages ORDER BY id DESC";
			
		if($conn->query($query) === false) {trigger_error('Error: '.$conn->error, E_USER_ERROR);}		
		$rs = $conn->query($query);
		?>
		
        <form action="contact_messages_bulk_action.php" method="post"> 
        <table class="table table-bordered" id="listings">
		<thead> 
        <tr>
        <th align="center"><input type="checkbox" onClick="toggle(this)" /></th>
        <th width="130">Date</th>
        <th width="220">Sender details</th>
        <th>Message details</th>
        <th width="80">Status</th>
		<th width="60" class="no_sort">Actions</th>
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $message_id = $row['id'];
			$message_unique_code = $row['unique_code'];
			$name = $row['name'];
			$email = $row['email'];
			$time = $row['time'];		
			$message = stripslashes($row['message']);
			$is_read = $row['is_read'];	
			$time_read = $row['time_read'];
			$ip = $row['ip'];
			
			
			$message = strip_tags($message);
			if(strlen($message)>250)
				$message2 = substr($message, 0, 250)."...";
			else
				$message2 = $message;
				
        ?>	
        <tr>       
        	<td align="left" valign="top">                    
                    <input name="select[]" type="checkbox" value="<?php echo $message_id;?>" />
            </td>    

        	<td>
            <small><?php echo DateTimeFormat($time);?></small>
            </td>
            
            <td>            
            <?php echo $name;?><br />
            <small>
            <?php echo $email;?><br />
			IP: <?php echo $ip;?>
            </small>           
            </td>

            
			<td>
            <?php			
			echo "<small>".nl2br($message2)."</small>";
			?>   
            <br /> 
            <a href="account.php?page=contact_messages_details&code=<?php echo $message_unique_code;?>"> <strong>Read message</strong></a>      
            <?php 
			if($is_read==1) echo "<br><small>Read at ".DateTimeFormat($time_read)."</small>";						
			?>
            </td>
            
            <td>
			<?php
			if($is_read==0) 				
				echo '<font color="#FF3300">Unread</font>';
			if($is_read==1) 	
					echo '<font color="#3399CC">Read</font>';

			?>
            </td>                    
            
            <td>    
            <small>                                        
            <i class="fa fa-close fa-trash"></i> <a href="javascript:deleteRecord_<?php echo $message_id;?>('<?php echo $message_id;?>');">Delete</a>
            </small>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $message_id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'contact_messages_delete.php?id=<?php echo $message_id;?>';
				}
			}
			</script>  
            </td>
        </tr>
        <?php
        }
        ?>
        </tbody>
		</table>
                
	        <div class="clear"></div>
    	    With selected items:
            <select name="action" class="select">
                <option value="delete">Delete</option>    
            </select>    
            
            <input type="hidden" name="page" value="contact_messages" />
            <input type="hidden" name="pagenum" value="<?php echo $pagenum;?>" />
            <input id="submit" name="input" type="submit" value="Apply" />

			</form>

		<?php
        } // END if rows==0						
        ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->