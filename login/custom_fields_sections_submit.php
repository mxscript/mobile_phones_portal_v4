<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

$group_id = $_POST["group_id"];
$group_id = Secure($group_id);

$title = $_POST["title"];
$title = Secure($title);

$description = $_POST["description"];
$description = Secure($description);

$active = $_POST["active"];
$active = Secure($active);

$position = $_POST["position"];
$position = Secure($position);

$permalink = RewriteUrl($title);

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

if($title=="")
	{
	header("Location:account.php?page=custom_fields_cf&msg=error_name&group_id=$group_id");
	exit();
	}

// check for duplicate
$sql = "SELECT id FROM ".$database_table_prefix."cf_sections WHERE permalink LIKE '$permalink' AND group_id = '$group_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=custom_fields_cf&msg=error_duplicate&group_id=$group_id");
	exit();
	}

$query = "INSERT INTO ".$database_table_prefix."cf_sections (id, group_id, title, permalink, description, active, position) VALUES (NULL, '$group_id', '$title', '$permalink', '$description', '$active', '$position')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=custom_fields_cf&msg=add_ok&group_id=$group_id");	
exit;
?> 