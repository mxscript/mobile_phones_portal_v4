<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$slide_id = $_POST["slide_id"];
$slide_id = Secure($slide_id);

$title = $_POST["title"];
$title = Secure($title);

$content = $_POST["content"];
$content = Secure($content);

$position = $_POST["position"];
$position = Secure($position);

$url = $_POST["url"];
$url = Secure($url);

$target = $_POST["target"];
$target = Secure($target);

$active = $_POST["active"];
$active = Secure($active);

$slider_id = $_POST["slider_id"];
$slider_id = Secure($slider_id);

$query = "UPDATE ".$database_table_prefix."sliders_content SET position = '$position', title = '$title', content = '$content', url = '$url', target = '$target',  active = '$active' WHERE id = '$slide_id' LIMIT 1"; 
if($conn->query($query) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
else { $last_inserted_id = $conn->insert_id; $affected_rows = $conn->affected_rows;}


// IMAGE
if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		// delete old images
		$query_img = "SELECT image FROM ".$database_table_prefix."sliders_content WHERE id = '$slide_id' LIMIT 1";
		$rs_img = $conn->query($query_img);
		$row = $rs_img->fetch_assoc();
        $old_image = $row['image'];

		@unlink ("../content/media/large/".$old_image);
		@unlink ("../content/media/small/".$old_image);		
			
		$image_code = random_code();
		$image = $image_code."-".$_FILES['image']['name'];
		$image = RewriteFile($image);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/temp/".$image);

		// create big image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(1400, 450, "crop"); // (options: exact, portrait, landscape, auto, crop) 
		$resizeObj -> saveImage("../content/media/large/".$image);

		// create small image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(600, 300, "crop"); // (options: exact, portrait, landscape, auto, crop) 
		$resizeObj -> saveImage("../content/media/small/".$image);
		
		@unlink ("../content/media/temp/".$image);
		
		$query = "UPDATE ".$database_table_prefix."sliders_content SET image = '$image' WHERE id = '$slide_id' LIMIT 1"; 
		if($conn->query($query) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
		else { $last_inserted_id = $conn->insert_id; $affected_rows = $conn->affected_rows;}		
		}
	}


// form OK:
header("Location: account.php?page=slider_content&msg=edit_ok&slider_id=$slider_id");	
exit;
?> 