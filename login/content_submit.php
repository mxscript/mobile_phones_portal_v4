<?php 
//error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
require ("../core/core.php");
require ("../core/functions/functions.images.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$title = isset($_POST['title']) ? $_POST['title'] : '';
$title = Secure($title);
$title_encoded = urlencode($title);

$categ_id = isset($_POST['categ_id']) ? $_POST['categ_id'] : '';
$categ_id = Secure($categ_id);
$categ_id = (int)$categ_id;

$content_short = isset($_POST['content_short']) ? $_POST['content_short'] : '';
$content_short = Secure($content_short);
$content_short_encoded = urlencode($content_short);

$content = isset($_POST['content']) ? $_POST['content'] : '';
$content = Secure($content);
$content_encoded = urlencode($content);

$aditional_info = isset($_POST['aditional_info']) ? $_POST['aditional_info'] : '';
$aditional_info = Secure($aditional_info);

$status_id = isset($_POST['status_id']) ? $_POST['status_id'] : '';
$status_id = Secure($status_id);

$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
$user_id = Secure($user_id);
$user_id = (int)$user_id;
if($user_id==0) $user_id = $logged_user_id;

$cf_group_id = isset($_POST['cf_group_id']) ? $_POST['cf_group_id'] : '';
$cf_group_id = Secure($cf_group_id);

$meta_title = isset($_POST['meta_title']) ? $_POST['meta_title'] : '';
$meta_title = Secure($meta_title);
$meta_title_encoded = urlencode($meta_title);

$meta_description = isset($_POST['meta_description']) ? $_POST['meta_description'] : '';
$meta_description = Secure($meta_description);
$meta_description_encoded = urlencode($meta_description);

$meta_keywords = isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : '';
$meta_keywords = Secure($meta_keywords);
$meta_keywords_encoded = urlencode($meta_keywords);

$custom_datetime_added = isset($_POST['custom_datetime_added']) ? $_POST['custom_datetime_added'] : '';
$custom_datetime_added = Secure($custom_datetime_added);

$custom_tpl_file = isset($_POST['custom_tpl_file']) ? $_POST['custom_tpl_file'] : '';
$custom_tpl_file = Secure($custom_tpl_file);

$sticky = isset($_POST['sticky']) ? $_POST['sticky'] : '';
$sticky = Secure($sticky);
if($sticky=="on") $sticky = 1; else $sticky = 0;

$disable_comments = isset($_POST['disable_comments']) ? $_POST['disable_comments'] : '';
$disable_comments = Secure($disable_comments);
if($disable_comments=="on") $disable_comments = 1; else $disable_comments = 0;

$disable_ratings = isset($_POST['disable_ratings']) ? $_POST['disable_ratings'] : '';
$disable_ratings = Secure($disable_ratings);
if($disable_ratings=="on") $disable_ratings = 1; else $disable_ratings = 0;

$disable_ads = isset($_POST['disable_ads']) ? $_POST['disable_ads'] : '';
$disable_ads = Secure($disable_ads);
if($disable_ads=="on") $disable_ads = 1; else $disable_ads = 0;

$permalink = isset($_POST['permalink']) ? $_POST['permalink'] : '';
$permalink = Secure($permalink);
if($permalink!="")
	$permalink = RewriteUrl($permalink);
else	
	$permalink = RewriteUrl($title);

if($title=="")
	{
	header("Location:account.php?page=content_add&msg=error_title&title=$title_encoded&content=$content_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded");
	exit();
	}

$unique_code = random_code();	
$root_categ_id = getRootCategIDFromCategID($categ_id);


if($custom_datetime_added!='')
	$datetime_added = $custom_datetime_added;
else
	$datetime_added = date("Y-m-d H:i:s");

$datetime_modified = "0000-00-00 00:00:00";
$query = "INSERT INTO ".$database_table_prefix."content (id, unique_code, title, permalink, image, user_id, cf_group_id, categ_id, root_categ_id, status_id, content_short, content, aditional_info, meta_title, meta_description, meta_keywords, datetime_added, datetime_modified, sticky, disable_comments, disable_ratings, disable_ads, custom_tpl_file, hits, mobile_release_date, mobile_main_price, search_terms, cf_array, rating_value, rating_ratings) VALUES (NULL, '$unique_code', '$title', '$permalink', '', '$user_id', '$cf_group_id', '$categ_id', '$root_categ_id', '$status_id', '$content_short', '$content', '$aditional_info', '$meta_title', '$meta_description', '$meta_keywords', '$datetime_added', '$datetime_modified', '$sticky', '$disable_comments', '$disable_ratings', '$disable_ads', '$custom_tpl_file', 0, '0000-00-00', 0, '', '', 0, 0)"; 

if($conn->query($query) === false) {
	echo "ERROR ".$conn->error;
	trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// get ID
$query_id = "SELECT id FROM ".$database_table_prefix."content ORDER BY id DESC LIMIT 1";
$rs_id = $conn->query($query_id);
$row = $rs_id->fetch_assoc();
$content_id = $row['id'];


if($_FILES['image']['name'])
	{
	$f = $_FILES['image']['name'];
	$ext = strtolower(substr(strrchr($f, '.'), 1));
	if (($ext!= "jpg") && ($ext != "jpeg") && ($ext != "gif") && ($ext != "png")) 
		{
		}

	else
		{
		$code = random_code();
		$permalink2 = substr($permalink, 0, 80);
		$image = $code."-".$permalink2.".".$ext;
		$image = RewriteFile($image);
				
		move_uploaded_file($_FILES["image"]["tmp_name"], "../content/media/temp/".$image);

		// create large image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(getResizeWidth_Large($content_id), getResizeHeight_Large($content_id), getResizeType_Large($content_id));
		$resizeObj -> saveImage("../content/media/large/".$image);
	
		// create small image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(getResizeWidth_Small($content_id), getResizeHeight_Small($content_id), getResizeType_Small($content_id));
		$resizeObj -> saveImage("../content/media/small/".$image);

		// create thumb image
		$resizeObj = new resize("../content/media/temp/".$image); 
		$resizeObj -> resizeImage(getResizeWidth_Thumb($content_id), getResizeHeight_Thumb($content_id), getResizeType_Thumb($content_id));
		$resizeObj -> saveImage("../content/media/thumbs/".$image);
		
		@unlink ("../content/media/temp/".$image);
	
		$query = "UPDATE ".$database_table_prefix."content SET image = '$image' WHERE unique_code = '$unique_code' ORDER BY id DESC LIMIT 1"; 
		$rs = $conn->query($query); 
		$affected_rows = $conn->affected_rows;	
		}
	}

//updateReleaseDate($content_id);
updateSearchTerms($content_id);
//updateSpecsArray($content_id);

// form OK:
header("Location: account.php?page=content&msg=add_ok");	
exit;
