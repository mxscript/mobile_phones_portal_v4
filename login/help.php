<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Help and Support</h1> 
        </section>

        <!-- Main content -->
        <section class="content">

        
          <div class="row">
                <div class="col-lg-12">				
        
                    <div class="box box-info">                        
                        <div class="box-body">
        				<h3>Free support</h3>
						You find any bug or error in this script? We fix all bugs and errors for FREE. Please <a target="_blank" href="http://www.mxscripts.com/contact.html"><strong>contact us</strong></a>.<br />
                        You have any question about how to use this script? Please <a target="_blank" href="http://www.mxscripts.com/contact.html"><strong>contact us</strong></a>.
                        
                        <h3>Paid services</h3>
						We offer paid services at the <strong>lowest prices and high quality</strong>.
                        <div class="clear"></div>
                        What we can do:<br />

                        - modify existing templates or <strong>create new custom templates</strong> for you<br />
                        - add <strong>new features</strong> or custom <strong>modifications</strong> for this script<br />
                        - integrate online payments (like PayPal payments), create onlne shopping carts<br />
                        - integrate SMTP service for sending emails or newsletters<br />
                        - new <strong>custom scripts</strong> or software
                        <div class="clear"></div>
                        We have more than 12 years of experience and provide professional scripts and custom programming services at the lowest prices. Our skills: PHP, MySQL, HTML5, CSS3, Ajax, SEO, Bootstrap Framework, Smarty Template Engine, JavaScript, server management, payments integration (PayPal, credit card)...  <br />We can provide high quality web applications, secure and clean code. We make valid and responsive layouts. All scripts are Search Engine Optimized.
                        <div class="clear"></div>
                        Read more details about <a target="_blank" href="http://www.mxscripts.com/services.html"><strong>our services</strong></a>
                        <div class="clear"></div>
        				Please <a target="_blank" href="http://www.mxscripts.com/contact.html"><strong>contact us</strong></a> if you have any questions or requests
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
          </div>	
                                

          <div class="row">				
                
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <!-- MX Scripts Info -->
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">News and promotions from MX Scripts</h3>                      
                    </div>
                    <div class="box-body responsive">
                      <iframe frameborder="0" style="border:none;" width="100%" height="250" src="http://include.mxscripts.com/iframe_dashboard.php"></iframe>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
				</div>
                
		  </div>	


        </section><!-- /.content -->

</div><!-- /.content-wrapper -->