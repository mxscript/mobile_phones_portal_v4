<?php
session_start();
$_SESSION = array();
session_destroy();
setcookie("user_token", "", time()-60*60*24*120, "/");  // 120 days ago
header("location:index.php?msg=logout");
exit;
?>