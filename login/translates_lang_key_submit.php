<?php
require ("../core/core.php");
require ("../core/plugins/resize-class.php");

require ("checklogin.php");
require ("check_permision.php");

$sql = "SET NAMES 'utf8'";
$conn->query($sql);

$sql = "SET CHARACTER 'utf8'";
$conn->query($sql);

$lang_id = isset($_POST['lang_id']) ? $_POST['lang_id'] : '';
$lang_id = Secure($lang_id);
$lang_id = (int)$lang_id;

$lang_key = isset($_POST['lang_key']) ? $_POST['lang_key'] : '';
$lang_key = Secure($lang_key);

$translate = isset($_POST['translate']) ? $_POST['translate'] : '';
$translate = addslashes($translate);

$lang_key = RewriteLangKey($lang_key);

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

if (!$lang_key or !$translate)
	{
	header("Location:account.php?page=translates_lang&msg=error_inputs&lang_id=$lang_id");
	exit();
	}

$sql = "SELECT id FROM ".$database_table_prefix."lang_translates WHERE lang_key LIKE '$lang_key' AND lang_id = '$lang_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
		header("Location:account.php?page=translates_lang&msg=error_duplicate_lang_key&lang_id=$lang_id");
		exit();
	}
	
$query = "INSERT INTO ".$database_table_prefix."lang_translates (id, lang_id, lang_key, translate) VALUES (NULL, '$lang_id', '$lang_key', '$translate')"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

header("Location:account.php?page=translates_lang&msg=add_ok&lang_id=$lang_id");
exit();