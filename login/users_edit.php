<?php 
require ("checklogin.php");
require ("check_permision.php");

$id = $_GET["id"];
$id = Secure($id);

$query = "SELECT email, name, bio, avatar, role_id, active, email_verified FROM ".$database_table_prefix."users WHERE id = '$id' LIMIT 1";
$rs = $conn->query($query);
$exist = $rs->num_rows;

while($row = $rs->fetch_assoc())
	{
	$email = $row['email'];
	$name = stripslashes($row['name']);
	$bio = stripslashes($row['bio']);
	$avatar = $row['avatar'];
	$role_id = $row['role_id'];	
	$active = $row['active'];
	$email_verified = $row['email_verified'];
	}
?>


<div class="content-wrapper">

        <section class="content-header">
          <h1>Edit user</h1>                
        </section>

        <!-- Main content -->
        <section class="content">

<?php
	if ($msg =='error_name')
		echo '<p class="bg-danger">Input full name</p>';	
	if ($msg =='error_email')
		echo '<p class="bg-danger">Input valid email</p>';	
	if ($msg =='error_password')
		echo '<p class="bg-danger">Input password</p>';	
	if ($msg =='error_duplicate_email')
		echo '<p class="bg-danger">There is another user with this email address</p>';	
?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">


			<?php
			if($exist == 0) echo "Invalid user";
			else
			{
			?>
            <form name="EditUser" action="users_edit_submit.php" method="post" onsubmit="return ValidateEditUser()" enctype="multipart/form-data">

                        
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Full name</label>
            <input class="form-control" name="name" type="text" value="<?php echo $name;?>" />
            </div>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Email</label>
            <input class="form-control" name="email" type="text" value="<?php echo $email;?>" />
            </div>
            </div>

			<div class="clear"></div>
            
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Change password (leave empty to not change)</label>
            <input class="form-control" name="password" type="text" />
            </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>User bio</label>
            <textarea name="bio" rows="3" class="form-control"><?php echo $bio;?></textarea>
            </div>
            </div>


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
            <label>Is active?</label>
            <select name="active" class="form-control">
            <option <?php if ($active==1) echo "selected=\"selected\"";?> value="1">YES</option>
            <option <?php if ($active==0) echo "selected=\"selected\"";?> value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
            <label>Email verified?</label>
            <select name="email_verified" class="form-control">
            <option <?php if ($email_verified==1) echo "selected=\"selected\"";?> value="1">YES</option>
            <option <?php if ($email_verified==0) echo "selected=\"selected\"";?> value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group">
	        <label>Role</label>
    	    <select name="role_id" class="form-control">
                <?php
                $query_user_role = "SELECT id, title FROM ".$database_table_prefix."users_roles WHERE active = 1 ORDER BY id ASC";
				$rs_user_role = $conn->query($query_user_role);
				while ($row = $rs_user_role->fetch_assoc())
					{
					$role_id_selected = $row['id'];
	   	            $role_title_selected = stripslashes($row['title']);
					?>
		            <option <?php if($role_id==$role_id_selected) echo 'selected="selected"';?> value="<?php echo $role_id_selected;?>"><?php echo $role_title_selected;?></option>
                    <?php
					}
				?>
    		</select>
        	</div>
			</div>


            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Change avatar</label>
            <input type="file" name="image">
            </div>
            </div>

                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $id;?>" />
            <input class="btn btn-primary" name="input" type="submit" value="Edit user" />
            </div>  
            </div>                      
            </form>
			<?php
            } // END if 
            ?>

			<div class="clear"></div>            

                <?php
               	if($avatar)
					{
					?>
                    <div id="avatar_image">
                            <span style="float: left; margin-right:10px;"><img style="max-width:100px; height:auto;" src="<?php echo $config_site_media;?>/avatars/<?php echo $avatar;?>" /></span>
                            <i class="fa fa-trash-o fa-fw"></i> <a class="delete_image" id="<?php echo $id;?>" href="ajax_remove_image.php?id=<?php echo $id;?>&what=user">Remove avatar</a>
                            <script type="text/javascript">
                            $(function(){
                                $('.delete_image').click(function(){
                                    var id = $(this).attr('id');
                                    
                                    $.ajax({
                                        type: "POST",
                                        url: "ajax_remove_image.php",
                                        data: "id="+id+"&what=user",
        
                                        success: function() {
												$('#avatar_image').hide();
												$("#image_deleted_text").html("Avatar removed").css('color','red');
                                            }
                                    });
                                    return false;
                                });
                            });
                            </script>  
                    </div>  
                    <div id="image_deleted_text"></div>                      
                	<?php					
					}
				?> 

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->