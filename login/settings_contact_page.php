<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<form action="settings_submit.php" method="post">  
<div class="content-wrapper">

        <section class="content-header">
          <h1>Contact Page Settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	if($config_enable_contact_page == 0)
		echo "<p class='bg-danger'>Contact Page is DISABLED!</p>";							
	?>	
                
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">    
        
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> General settings
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                <div class="col-md-3">
	                <div class="form-group">
					<label>Enable contact page</label>
					<select name="enable_contact_page" class="form-control">
						<option <?php if ($config_enable_contact_page=='1') echo "selected=\"selected\"";?> value="1">Enabled</option>
						<option <?php if ($config_enable_contact_page=='0') echo "selected=\"selected\"";?> value="0">Disabled</option>
                    </select>
                    </div>
				</div>

                <div class="col-md-3">
	                <div class="form-group">
					<label>Contact form</label>
					<select name="enable_contact_form" class="form-control">
						<option <?php if ($config_enable_contact_form=='1') echo "selected=\"selected\"";?> value="1">Show contact form</option>
						<option <?php if ($config_enable_contact_form=='0') echo "selected=\"selected\"";?> value="0">Don't show contact form</option>
                    </select>
                    </div>
				</div>

<?php /*?>                <div class="col-md-6">
	                <div class="form-group">
					<label>Send notification email (to site email address)</label>
					<select name="contact_form_send_notification_email" class="form-control">
						<option <?php if ($config_contact_form_send_notification_email=='0') echo "selected=\"selected\"";?> value="0">Don't send email</option>
						<option <?php if ($config_contact_form_send_notification_email=='1') echo "selected=\"selected\"";?> value="1">Send notification email when contact form is submited</option>
                    </select>
                    </div>
				</div>
<?php */?>                
                                                                            
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
   
             
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Google Map
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">
				
                <div class="col-md-12">	
				Tutorial how to find a location coordinates (latitude and longitude): <a target="_blank" href="https://support.google.com/maps/answer/18539?co=GENIE.Platform%3DDesktop&hl=en">Get coordinates of a location</a>
                </div>
                
                <div class="col-md-3">
	                <div class="form-group">
					<label>Show Google Map</label>
					<select name="contact_page_show_map" class="form-control">
						<option <?php if ($config_contact_page_show_map=='1') echo "selected=\"selected\"";?> value="1">Yes</option>
						<option <?php if ($config_contact_page_show_map=='0') echo "selected=\"selected\"";?> value="0">No</option>
                    </select>
                    </div>
				</div>
                
                <div class="col-md-3">
	                <div class="form-group">
					<label>Location Latitude</label>
					<input type="text" name="contact_page_map_latitude" class="form-control" value="<?php echo $config_contact_page_map_latitude;?>">
                    </div>
                </div>    

                <div class="col-md-3">
	                <div class="form-group">
					<label>Location Longitude</label>
					<input type="text" name="contact_page_map_longitude" class="form-control" value="<?php echo $config_contact_page_map_longitude;?>">
                    </div>
                </div>                  
               
                <div class="col-md-3">
	                <div class="form-group">
					<label>Map Zoom Level</label>
					<input type="text" name="contact_page_map_zoom" class="form-control" value="<?php echo $config_contact_page_map_zoom;?>">
                    </div>
                </div>   
                				                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->


            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Custom text
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

                <div class="col-md-12">
	                <div class="form-group">
					<label>Custom text in Contact Page</label>
					<textarea id="custom_text" name="contact_page_custom_text" rows="5" class="form-control"><?php echo $config_contact_page_custom_text;?></textarea>
  					<script>
                    $(document).ready(function() {
                        $('#custom_text').summernote({
                            height: 200,
                            onImageUpload: function(files, editor, welEditable) {
                                sendFile(files[0], editor, welEditable);
                            }
                        });
                        function sendFile(file, editor, welEditable) {
                            data = new FormData();
                            data.append("file", file);
                            $.ajax({
                                data: data,
                                type: "POST",
                                url: "texteditor_upload.php",
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function(url) {
                                    editor.insertImage(welEditable, url);
                                }
                            });
                        }
                    });
                    </script>

                    </div>
				</div>
                                				                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->


		        <div class="form-group">    
            	<input type="hidden" name="settings_section" value="contact_page" />
            	<input type="hidden" name="return_page" value="settings_contact_page" />                 	            	                	
            	<button type="submit" class="btn btn-primary">Change settings</button>		    
    	        </div>

				<div class="clear"></div>            

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->

</div><!-- /.content-wrapper -->

</form>
