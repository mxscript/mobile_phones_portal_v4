<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<form action="settings_submit.php" method="post">  
<div class="content-wrapper">

        <section class="content-header">
          <h1>Locale settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>
	
                
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">    
        
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Date Settings
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">

				<div class="col-md-6">
	                <div class="form-group">
					<label>Date format (example: "d M, Y" means "01 Jan, 2015")</label> 
                    <input type="text" name="date_format" class="form-control" value="<?php echo $config_date_format;?>">	
                    <div class="clear"></div>
                    <strong>DAYS:</strong><br />
					d -	Day of the month, 2 digits with leading zeros (01 to 31)<br />
                    D - A textual representation of a day, three letters (Mon through Sun)<br />
                    j - Day of the month without leading zeros (1 to 31)
                    <div class="clear"></div>
                    <strong>MONTHS:</strong><br />
                    m - Numeric representation of a month, with leading zeros (01 through 12)<br />
                    M - A short textual representation of a month, three letters (Jan through Dec)<br />
                    F - A full textual representation of a month (January through December)<br />
                    n - Numeric representation of a month, without leading zeros (1 through 12)
                    <div class="clear"></div>
                    <strong>YEAR:</strong><br />
                    Y - A full numeric representation of a year, 4 digits (1999 or 2003)<br />
                    y - A two digit representation of a year (99 or 03)
                    
 					<div class="clear"></div>
                    More details: <a target="_blank" href="http://php.net/manual/en/function.date.php">php.net/manual/en/function.date.php</a>
                    </div>
                </div>   
                                                                            
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
   
             
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Currency settings
                    <div class="clear"></div>
					Read more about <a target="_blank" href="http://www.php.net/manual/en/function.money-format.php">PHP monre_format function</a>
                </div><!-- /.panel-heading -->
                
                <div class="panel-body">
                
				
                <div class="col-md-3">
	                <div class="form-group">
					<label>Curerncy name</small></label>
					<input type="text" name="currency_name" class="form-control" value="<?php echo $config_currency_name;?>">
                    </div>
                </div>                    
				
                <div class="col-md-3">
	                <div class="form-group">
					<label>Curerncy symbol</label>
					<input type="text" name="currency_symbol" class="form-control" value="<?php echo $config_currency_symbol;?>">
                    </div>
                </div>    
				
                <div class="clear"></div>
                
                <div class="col-md-3">
	                <div class="form-group">
					<label>Display style</label>
					<select name="currency_display_style" class="form-control">
                    <option value="" selected="selected"> - select - </option>
					<option <?php if ($config_currency_display_style=='name_value') echo "selected=\"selected\"";?> value="name_value">USD 18</option>
					<option <?php if ($config_currency_display_style=='value_name') echo "selected=\"selected\"";?> value="value_name">18 USD</option>
					<option <?php if ($config_currency_display_style=='symbol_value') echo "selected=\"selected\"";?> value="symbol_value">$ 18</option>
					<option <?php if ($config_currency_display_style=='value_symbol') echo "selected=\"selected\"";?> value="value_symbol">18 $</option>
					</select>
                    </div>
				</div>

                <div class="col-md-3">
	                <div class="form-group">
					<label>Money format (eg. for 1234.56)</label>
					<select name="currency_money_format" class="form-control">
                    <option value="" selected="selected"> - select - </option>
					<option <?php if ($config_currency_money_format=='none') echo "selected=\"selected\"";?> value="none">1234.56</option>
					<option <?php if ($config_currency_money_format=='us') echo "selected=\"selected\"";?> value="us">1,234.56</option>
					<option <?php if ($config_currency_money_format=='eu') echo "selected=\"selected\"";?> value="eu">1.234,56</option>
					</select>
                    </div>
				</div>

                                				                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->

		        <div class="form-group">    
            	<input type="hidden" name="settings_section" value="locale" />
            	<input type="hidden" name="return_page" value="settings_locale" />                 	            	                	
            	<button type="submit" class="btn btn-primary">Change settings</button>		    
    	        </div>

				<div class="clear"></div>            

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->

</div><!-- /.content-wrapper -->

</form>