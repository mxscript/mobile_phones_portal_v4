<?php 
require ("../core/core.php");
require ("../core/plugins/smtp/PHPMailerAutoload.php");

$email = $_POST['email'];
$email = Secure($email);

if (strlen($email)==0)
	{
	header("Location:reset-activation.php?msg=error");
	exit();
	}
		

$sql = "SELECT email, active FROM ".$database_table_prefix."users WHERE email LIKE '$email' LIMIT 1";
$rs = $conn->query($sql);
$exist = $rs->num_rows;
if($exist==0)
	{
	header("Location:reset-activation.php?msg=wrong_email");
	exit();
	}

$row = $rs->fetch_assoc();
$active = $row['active'];
if($active==1)
	{
	header("Location:reset-activation.php?msg=user_already_active");
	exit();
	}


$new_code = random_code();	
$new_code = md5($new_code);
		
$query = "UPDATE ".$database_table_prefix."users SET activation_code = '$new_code' WHERE email LIKE '$email' LIMIT 1"; 
$rs = $conn->query($query);
$affected_rows = $conn->affected_rows;	

// *****************************************************************************************************************************
// SEND MAIL
// *****************************************************************************************************************************
if($config_email_send_option=="smtp")
	{
		// SMTP MAILER	
		//----------------------------------------------------------------------------------------------------------		
		$mail = new PHPMailer;
		
		$mail->IsSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $config_email_smtp_server;                 // Specify main and backup server
		$mail->Port = $config_email_smtp_port;                                    // Set the SMTP port
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $config_email_smtp_user;                // SMTP username
		$mail->Password = $config_email_smtp_password;                  // SMTP password
		$mail->SMTPSecure = $config_email_smtp_encryption;                            // Enable encryption, 'ssl' also accepted

		$mail->setFrom($config_site_email, $config_site_email_name);
		$mail->addReplyTo($config_site_email);
		$mail->AddAddress($email);  
		
		$mail->IsHTML(true);                                  // Set email format to HTML
		
		$mail->Subject = 'Your activation code on '.$config_site_url;
		$mail->Body    = '
			<html>
			<head>
			  <title>New activation code</title>
			</head>
			<body>
			  <div style="font-size:12px;font-family:arial;">
			  <p>Hello!</p>
			  You choose to resend your activation code on '.$config_site_url.'<br><br>
			  Click here to activate your account: <a href="'.$config_site_url.'/login/activate_user.php?activation_code='.$new_code.'">ACTIVATE ACCOUNT</a><br><br>
			  Thank you!<br><br>
			  </div>
			</body>
			</html>
		';
		$mail->AltBody = 'You choose to resend your activation code on '.$config_site_url.'\n  Url to activate your account is: '.$config_site_url.'/login/activate_user.php?activation_code='.$new_code;
		
		if(!$mail->Send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   exit;
		}
		

	}

else
	{
		// PHP MAILER	
		//----------------------------------------------------------------------------------------------------------		
		$to      = '$email';
		$subject = 'Your activation code on '.$config_site_url;
		//$message = '';
		$message = '
		<html>
		<head>
		  <title>New activation code</title>
		</head>
		<body>
		  <div style="font-size:12px;font-family:arial;">
		  <p>Hello!</p>
		  You choose to resend your activation code on '.$config_site_url.'<br><br>
		  Click here to activate your account: <a href="'.$config_site_url.'/login/activate_user.php?activation_code='.$new_code.'">ACTIVATE ACCOUNT</a><br><br>
		  Thank you!<br><br>
		  </div>
		</body>
		</html>
		';
		
		// HTML mail
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		$headers .= 'From: '.$config_site_email."\r\n" .
			'Reply-To: '.$config_site_email."\r\n" .
			'X-Mailer: PHP/' . phpversion();
		mail($email, $subject, $message, $headers);
		//-------------------------------------------------------------------------------------------------------------
	}

session_destroy();
setcookie("user_token", "", time()-60*60*24*120, "/");  // 120 days ago
header("Location: index.php?msg=reset_activation_ok");
exit;
?>