<?php 
require ("checklogin.php");
require ("check_permision.php");

$id = isset($_GET['id']) ? $_GET['id'] : '';
$id = Secure($id);

$query = "SELECT parent_id, title, permalink, description, position, active, meta_title, meta_description, meta_keywords, show_in_menu, show_in_footer, custom_tpl_file, image_type, image FROM ".$database_table_prefix."categories WHERE id = '$id' LIMIT 1";
$rs = $conn->query($query);
$exist = $rs->num_rows;

while($row = $rs->fetch_assoc())
	{
	$parent_id_edited = $row['parent_id'];	
	$title = stripslashes($row['title']);
	$permalink = $row['permalink'];
	$description = stripslashes($row['description']);
	$position = $row['position'];	
	$active = $row['active'];
	$meta_title = stripslashes($row['meta_title']);
	$meta_description = stripslashes($row['meta_description']);
	$meta_keywords = stripslashes($row['meta_keywords']);
	$show_in_menu = $row['show_in_menu'];
	$show_in_footer = $row['show_in_footer'];
	$custom_tpl_file = $row['custom_tpl_file'];
	$image_type = $row['image_type'];
	$image = $row['image'];
	}
?>


<div class="content-wrapper">

        <section class="content-header">
          <h1>Edit category</h1>  
        </section>

        <!-- Main content -->
        <section class="content">

<?php
if ($msg =='error_title')
	echo '<p class="bg-danger">Error. Input title</p>';
if ($msg =='error_section')
	echo '<p class="bg-danger">Error. Select section</p>';
if ($msg =='error_duplicate_title')
	echo '<p class="bg-danger">Error. There is another category in this section with this title</p>';	
if ($msg =='error_duplicate_permalink')
	echo '<p class="bg-danger">Error. There is another category in this section with this permalink</p>';	
?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

			<?php
			if($exist == 0) echo "Invalid category";
			else
			{
			?>
            <form name="EditCateg" action="categ_edit_submit.php" method="post" onsubmit="return ValidateEditCateg()" enctype="multipart/form-data">

                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Title</label>
            <input class="form-control" name="title" type="text" value="<?php echo $title;?>" />
            </div>
            </div>
			 
                                      
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Description</label>
            <textarea name="description" rows="3" class="form-control"><?php echo $description;?></textarea>
            </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Select parent category</label>
         	<select name="parent_id" class="form-control">
                <option value="0" <?php if ($parent_id_edited==0) echo "selected=\"selected\"";?>>[ROOT]</option>
				<?
                $data = array();
                $index = array();
                $query2 = "SELECT id, parent_id, title FROM ".$database_table_prefix."categories ORDER BY title ASC";
                $rs2 = $conn->query($query2);
                while ($row = $rs2->fetch_assoc()) 
                    {
                    $id_select_parent = $row["id"];
                    $parent_id = $row["parent_id"] ==0 ? 0 : $row["parent_id"];
                    $data[$id_select_parent] = $row;
                    $index[$parent_id][] = $id_select_parent;
                    }
                display_child_nodes(0, $parent_id_edited, 0);
                ?>
            </select>
            </div>
            </div>


            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Is active?</label>
            <select name="active" class="form-control">
            <option <?php if ($active==1) echo "selected=\"selected\"";?> value="1">YES</option>
            <option <?php if ($active==0) echo "selected=\"selected\"";?> value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Show in header menu</label>
            <select name="show_in_menu" class="form-control">
            <option <?php if ($show_in_menu==1) echo "selected=\"selected\"";?> value="1">YES</option>
            <option <?php if ($show_in_menu==0) echo "selected=\"selected\"";?> value="0">NO</option>
    		</select>
            </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Show in footer</label>
            <select name="show_in_footer" class="form-control">
            <option <?php if ($show_in_footer==1) echo "selected=\"selected\"";?> value="1">YES</option>
            <option <?php if ($show_in_footer==0) echo "selected=\"selected\"";?> value="0">NO</option>
    		</select>
            </div>
            </div>

            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Position</label>
            <input class="form-control" name="position" type="text" value="<?php echo $position;?>" />
            </div>
            </div>
         
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Custom template file</label>
            <input class="form-control" name="custom_tpl_file" type="text" value="<?php echo $custom_tpl_file;?>" />
            </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Image type</label>
            <select name="image_type" class="form-control" onchange="showDiv(this)">
            <option <?php if ($image_type=="") echo "selected=\"selected\"";?> value="">- no image -</option>
            <option <?php if ($image_type=="upload") echo "selected=\"selected\"";?> value="upload">Upload file</option>
            <option <?php if ($image_type=="fa") echo "selected=\"selected\"";?> value="fa">Font awesome icon</option>
    		</select>
            </div>
            </div>

                <script>
				function showDiv(elem){
				if(elem.value == "")
					{
				      document.getElementById('hidden_div1').style.display = "none";	
					  document.getElementById('hidden_div2').style.display = "none";				
					}
			   	if(elem.value == "fa")
					{
				      document.getElementById('hidden_div1').style.display = "block";	
					  document.getElementById('hidden_div2').style.display = "none";				
					}
				if(elem.value == "upload")
					{
				      document.getElementById('hidden_div1').style.display = "none";
					  document.getElementById('hidden_div2').style.display = "block";
					}
				}
				</script>

            <div id="hidden_div1" class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="display:<?php if($image_type=="fa") echo 'block'; else echo 'none';?>">
            <div class="form-group">
            <label>Font awesome code</label>
            <input class="form-control" name="image_fa" type="text" value="<?php echo $image;?>" />
            </div>
            </div>

            <div id="hidden_div2" style="display:<?php if($image_type=="upload") echo 'block'; else echo 'none';?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                <label>Upload image</label>
                <input class="form-control" name="image" type="file" />
                </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <label>Image</label>
                <div class="clear"></div>
                <?php if($image) { ?>
	            <img src="../content/media/img/<?php echo $image;?>" />
                <?php } ?>
                </div>
            </div>


                                                
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>SEO settings:</h2>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Meta title</label>
            <input class="form-control" name="meta_title" type="text" value="<?php echo $meta_title;?>" />
            </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Meta description</label>
            <input class="form-control" name="meta_description" type="text" value="<?php echo $meta_description;?>" />
            </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Meta Keywords</label>
            <input class="form-control" name="meta_keywords" type="text" value="<?php echo $meta_keywords;?>" />
            </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>URL structure (leave blank to autogenerate permalink)</label>
            <input class="form-control" name="permalink" type="text" value="<?php echo $permalink;?>"  />
            </div>
            </div>
                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $id;?>" />
            <input class="btn btn-primary" name="input" type="submit" value="Edit category" />
            </div>  
            </div>                      
            </form>
			<?php
            } // END if 
            ?>

            <div class="clear2"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->