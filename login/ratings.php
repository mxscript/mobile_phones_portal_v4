<?php 
require ("checklogin.php");
require ("check_permision.php");

$today = date("Y-m-d");
$query = "SELECT id FROM ".$database_table_prefix."content_extra WHERE name = 'star_rating'";
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>
<script language="JavaScript">
function toggle(source) {
  checkboxes = document.getElementsByName('select[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<div class="content-wrapper">
        <section class="content-header">
          <h1>Content ratings (<?php echo $rows;?> ratings)</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Deleted</p>';
    if ($msg =='demo_mode')
        echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

<?php
// ------------------------------------------------------------------------------------------------------
if (!(isset($pagenum)))
	{
	$pagenum = 1;
	}

if ($rows==0)
	{
		echo "No rating";
	}

else
	{
		$page_rows = 30;
		$last = ceil($rows/$page_rows);

		if ($pagenum < 1)
		{
		$pagenum = 1;
		}
		elseif ($pagenum > $last)
		{
		$pagenum = $last;
		}

		$max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
		$query = "SELECT id, content_id, value, extra FROM ".$database_table_prefix."content_extra WHERE name = 'star_rating' ORDER BY id DESC $max";
		$rs = $conn->query($query);
		?>
		

		<form action="ratings_bulk_action.php" method="post">         
        <div class="table-responsive">
        <table class="table table-bordered" id="listings">	
		<thead> 
        <tr>
            <th width="50" align="center"><input type="checkbox" onClick="toggle(this)" /></th>
            <th>Content</th>
            <th width="220">Rating</th>
            <th width="160">Rate Date</th>
			<th width="120">Actions</th>                        
		</tr>
        </thead>
        
        <tbody>
		<?php
		while($row = $rs->fetch_assoc())
        	{
            $id = $row['id'];
			$content_id = $row['content_id'];
			$value = $row['value'];
			$extra = $row['extra'];
			
			$query_content = "SELECT categ_id, title, permalink, image FROM ".$database_table_prefix."content WHERE id = '$content_id' LIMIT 1";
			$rs_content = $conn->query($query_content);
			$row = $rs_content->fetch_assoc();
			$content_categ_id = $row['categ_id'];
			$content_title = stripslashes($row['title']);
			$content_permalink = $row['permalink'];
			$content_image = $row['image'];	

            $query3 = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$content_categ_id' LIMIT 1";
           	$rs3 = $conn->query($query3);
			$row = $rs3->fetch_assoc();
            $categ_title = stripslashes($row['title']);
			$categ_permalink = stripslashes($row['permalink']);								
			
			$rating_value = returnStarRatingValue($content_id);
			$numb_ratings = returnNumberStarRatings($content_id);

        ?>	
        <tr>            
			<td align="left" valign="top">                    
			<input name="select[]" type="checkbox" value="<?php echo $id;?>" />
            </td>        
            
			
            <td>
            <?php if($content_image!="") { ?>
            <img style="padding:0; max-width:40px; max-height:60px; height:auto; margin-right:15px; float:left;" src="<?php echo $config_site_media;?>/thumbs/<?php echo $content_image;?>" />
            <?php } ?>
			<a target="_blank" href="<?php echo $config_site_url.'/'.$categ_permalink.'/'.$content_permalink.'-'.$content_id.'.html'; ?>"><strong><?php echo $categ_title." ".$content_title;?></strong></a><br />
			TOTAL RATING: 
            <?php
			for($i=1; $i<=$rating_value;$i++) { ?>
			<i style="font-size:16px; color: #F2CA4F; margin:0; padding:0; " class="fa fa-star fa-fw"></i>
            <?php } ?> (<?php echo $numb_ratings;?> ratings)


            </td>

            <td>
            <?php
			for($i=1; $i<=$value;$i++) { ?>
			<i style="font-size:20px; color: #F2CA4F; margin:0; padding:0; " class="fa fa-star fa-fw"></i>
            <?php } ?>
            </td>
            
            <td>
            <?php echo DateTimeFormat($extra);?>
            </td>
                                    
			<td>
            <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $id;?>('<?php echo $id;?>');">Delete</a>
            <script language="javascript" type="text/javascript">
			function deleteRecord_<?php echo $id;?>(RecordId)
			{
				if (confirm('Confirm delete')) {
					window.location.href = 'ratings_delete.php?id=<?php echo $id;?>&pagenum=<?php echo $pagenum;?>&content_id=<?php echo $content_id;?>';
				}
			}
			</script>  
			</td>
            
        </tr>
        <?php
        }
        ?>
        </tbody>
		</table>
        </div>
        
        <div class="clear"></div>
        With selected items:
            <select name="action" class="select">
                <option value="delete">Delete</option>
            </select>    
            
            <input type="hidden" name="page" value="ratings" />
            <input type="hidden" name="pagenum" value="<?php echo $pagenum;?>" />
            <input id="submit" name="input" type="submit" value="Apply" />
        </form>        


    <div class="clear"></div>
    <ul class="pagination">
	<?php
	echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
		if ($pagenum == 1)
		{
		}
		else
			{
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=ratings&pagenum=1'><strong>First page</strong></a></li>";
			echo " ";
			$previous = $pagenum-1;
			echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=ratings&pagenum=$previous'>".$previous."</a></li>";
			}

			echo "";


		if ($pagenum == $last)
			{			
			}
		else {
			$next = $pagenum+1;
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=ratings&pagenum=$next'> ".$next."</a></li>";
			echo " ";
			echo "<li><a href='{$_SERVER['PHP_SELF']}?page=ratings&pagenum=$last'><strong>Last page</strong></a></li>";
		} 
		?>	
		</ul>


		<?php
        } // END if rows==0
        ?>

			<div class="clear"></div>            
			<a href="tools_clean_old_visits.php?return_page=analytics"><strong>Delete visitors log older than 30 days</strong></a> (recomended to save database space)

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->