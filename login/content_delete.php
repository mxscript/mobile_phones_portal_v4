<?php
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$content_id = $_GET['id'];
$content_id = Secure($content_id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

// **************************************************
// AUTHORS FILTERS
if (authorPermissionOK($content_id, $logged_user_id)==0)
	{
	header("Location: account.php?page=content&msg=no_permission&pagenum=$pagenum");
	exit;
	}
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************


$query = "SELECT image FROM ".$database_table_prefix."content WHERE id = '$content_id' $filter_user_role LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$image = $row['image'];
@unlink ("../content/media/large/".$image);
@unlink ("../content/media/small/".$image);
@unlink ("../content/media/thumbs/".$image);

$sql = "DELETE FROM ".$database_table_prefix."content WHERE id = '$content_id' $filter_user_role LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$sql = "DELETE FROM ".$database_table_prefix."cf_values WHERE content_id = '$content_id'"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$sql = "DELETE FROM ".$database_table_prefix."content_extra WHERE content_id = '$content_id'"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=content&msg=delete_ok&pagenum=$pagenum");
exit;
