<?php 
session_start();
require ("../core/core.php");
require ("checklogin.php");

$page = isset($_GET['page']) ? $_GET['page'] : '';
$page = Secure($page);		

$msg = isset($_GET['msg']) ? $_GET['msg'] : '';
$msg = Secure($msg);	

$pagenum = isset($_GET['pagenum']) ? $_GET['pagenum'] : '';
$pagenum = Secure($pagenum);

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."comments WHERE approved = 0"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_pending_comments = $row['numb'];

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."reports WHERE resolved = 0"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_new_reports = $row['numb'];

$sql = "SELECT COUNT(id) AS numb FROM ".$database_table_prefix."contact_messages WHERE is_read = 0"; 
$rs = $conn->query($sql);
$row = $rs->fetch_assoc();
$numb_new_contact_messages = $row['numb'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My account</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../core/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">        
    <link rel="stylesheet" href="assets/css/admin.css">
    <link rel="stylesheet" href="assets/css/admin_extra.css">
    <link rel="stylesheet" href="assets/css/skins/skin-blue.min.css">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.1.4 -->
    <script src="assets/js/jQuery-2.1.4.min.js"></script>

    <!-- Bootstrap 3.3.5 -->
    <script src="../core/assets/js/bootstrap.min.js"></script>
    
    <!-- App -->
    <script src="assets/js/app.min.js"></script>

    <!-- Morris charts -->
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="assets/plugins/morris/morris.min.js"></script>

    <!-- TEXT Editor -->
	<script src="../core/assets/js/summernote.min.js"></script>
    <link href="../core/assets/css/summernote.css" rel="stylesheet">
    
    <!-- Checkboxes style -->
    <link href="../core/assets/css/bootstrap-checkbox.css" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="assets/plugins/datatables/dataTables.bootstrap.css">
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script src="assets/js/validation.js"></script>
<style>
.no_sort::after { display: none!important; }

.no_sort { pointer-events: none!important; cursor: default!important; }
</style>

  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a title="Go to website" href="<?php echo $config_site_url;?>" target="_blank" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><i class="fa fa-laptop"></i></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><i class="fa fa-laptop"></i> View site</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              
              <?php
              if($logged_user_role=="admin")
			  {
			  ?>	

              <li class="messages-menu">
                <!-- Menu toggle button -->
                <a title="All Content" href="account.php?page=content">
                  <i class="fa fa-folder-o"></i>                  
                </a>
              </li>

              <li class="messages-menu">
                <!-- Menu toggle button -->
                <a title="Add new content" href="account.php?page=content_add">
                  <i class="fa fa-plus"></i>                  
                </a>
              </li>
                                            
              <li class="messages-menu">
                <!-- Menu toggle button -->
                <a title="Contact messages" href="account.php?page=contact_messages">
                  <i class="fa fa-envelope-o"></i>
                  <?php if($numb_new_contact_messages!=0){ ?>
                  <span class="label label-success"><?php echo $numb_new_contact_messages;?></span>
                  <?php } ?>
                </a>
              </li>
              
              <li class="notifications-menu">
                <!-- Menu toggle button -->
                <a title="Discussions" href="account.php?page=comments">
                  <i class="fa fa-comment-o"></i>
                  <?php if($numb_pending_comments!=0){ ?>
                  <span class="label label-warning"><?php echo $numb_pending_comments;?></span>
                  <?php } ?>
                </a>                
              </li>

              <li class="tasks-menu">
                <!-- Menu Toggle Button -->
                <a title="Reports" href="account.php?page=reports">
                  <i class="fa fa-flag-o"></i>
                  <?php if($numb_new_reports!=0){ ?>
                  <span class="label label-danger"><?php echo $numb_new_reports;?></span>
                  <?php } ?>
                </a>                
              </li>
              <?php
			  }
			  ?>
              
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="../content/media/avatars/<?php echo $logged_user_avatar;?>" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo $logged_user_name;?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="../content/media/avatars/<?php echo $logged_user_avatar;?>" class="img-circle" alt="User Image">
                    <p><?php echo $logged_user_name;?><small><?php echo $logged_user_email;?></small></p>
                  </li>                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="account.php?page=profile" class="btn btn-info btn-flat"><i class="fa fa-user"></i> My Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="logout.php" class="btn btn-danger btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>              
                            
            </ul>
          </div>
        </nav>
      </header>
      

		<?php	
		if($logged_user_role=="admin")
			include ("static/account_admin.php");

		if($logged_user_role=="moderator")
			include ("static/account_moderator.php");

		if($logged_user_role=="author")
			include ("static/account_author.php");

		if($logged_user_role=="user")
			include ("static/account_user.php");
		?>


      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          License for <a target="_blank" href="<?php echo $config_site_url;?>"><?php echo $config_site_title;?></a>
        </div>
        <!-- Default to the left -->
        <strong>Software by <a target="_blank" href="http://www.mxscripts.com">MX Scripts</a>.</strong>
      </footer>
      
    </div><!-- ./wrapper -->
    
  </body>
</html>
