<?php
require ("checklogin.php");
require ("check_permision.php");

$content_id = $_GET["id"];
$content_id = Secure($content_id);
$content_id = (int)$content_id;

// **************************************************
// AUTHORS FILTERS
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************

$query = "SELECT id FROM ".$database_table_prefix."prices WHERE content_id = '$content_id'";
$query_content = "SELECT title FROM ".$database_table_prefix."content WHERE id = '$content_id' $filter_user_role LIMIT 1";
					
$rs_content = $conn->query($query_content);
$row = $rs_content->fetch_assoc();
$content_title = stripslashes($row['title']);
	
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Prices for "<?php echo $content_title;?>"</h1> 
			            
          <a class="btn btn-primary" data-toggle="modal" href="#" data-target=".modal_add_price"><i class="fa fa-plus"></i> Add new price</a>
		  <?php include ("static/modal_add_price.php"); ?>  
        </section>

        <!-- Main content -->
        <section class="content">


		<?php
        if ($msg =='add_ok')
            echo '<p class="bg-info">Price added</p>';		
        if ($msg =='edit_ok')
            echo '<p class="bg-info">Price modified</p>';
        if ($msg =='delete_ok')
            echo '<p class="bg-info">Price deleted</p>';
        if ($msg =='error_seller')
            echo '<p class="bg-danger">ERROR! Input seller name</p>';
        if ($msg =='error_price')
            echo '<p class="bg-danger">ERROR! Input price</p>';
        if ($msg =='demo_mode')
            echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
        ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

			<?php
            // ------------------------------------------------------------------------------------------------------
            if (!(isset($pagenum)))
                {
                $pagenum = 1;
                }
            
            if ($rows==0)
                {
                    echo "No price";
                }
            
            else
                {
                    $page_rows = 50;
                    $last = ceil($rows/$page_rows);
            
                    if ($pagenum < 1)
                    {
                    $pagenum = 1;
                    }
                    elseif ($pagenum > $last)
                    {
                    $pagenum = $last;
                    }
            
                    $max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
                    
                    $query = "SELECT id, content_id, user_id, price, details, seller_logo, url, active FROM ".$database_table_prefix."prices WHERE content_id = '$content_id' ORDER BY id DESC $max";
            
                    $rs = $conn->query($query);
                    ?>
                    
                    <table class="table table-bordered" id="listings">
                    <thead> 
                    <tr>
                    <th width="140">Seller logo</th>
                    <th>Details</th>                    
                    <th width="150">Price</th>
                    <th width="140">Actions</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    <?php
                    while($row = $rs->fetch_assoc())
                        {
                        $price_id = $row['id'];
                        $content_id_edited = $row['content_id'];
                        $user_id = $row['user_id'];	
                        $price = $row['price'];
                        $logo = $row['seller_logo'];
						$details = stripslashes($row['details']);		
						$url = $row['url'];		
						$active = $row['active'];		
                    ?>	
                    <tr <?php if($active==0) echo 'bgcolor="#FEEAD6"';?>>            
                        <td>
                        <?php
                        if($logo!="")
                            {
                            ?>
                            <span style="float: left; margin-right:10px;"><img style="max-width:150px; max-height:130px; height:auto; width:auto;" src="<?php echo $config_site_media;?>/thumbs/<?php echo $logo;?>" /></span>
                            <?php									
                            }
							?>
                        </td>
                        
                        <td>
                        <?php    
						if ($details) echo nl2br(html_entity_decode($details));                        
						if($url!='') echo '<br /><a target="_blank" href='.$url.'>'.$url.'</a>';	
                        ?>                         
                        </td>
                                                                   
                        <td>
                        <?php
					    echo $price;
                        ?>
                        </td>
                                    
                        <td>
                        <a data-toggle="modal" href="#" data-target=".modal_edit_price_<?php echo $price_id;?>"><i class="fa fa-edit fa-fw"></i> Edit  price</a>
                        
                        <?php include ("static/modal_edit_price.php");?>  
                        <div class="clear"></div>
                        <a href="javascript:deleteRecord_<?php echo $price_id;?>('<?php echo $price_id;?>');"><i class="fa fa-trash-o fa-fw"></i> Delete price</a>
                        <script language="javascript" type="text/javascript">
                        function deleteRecord_<?php echo $price_id;?>(RecordId)
                        {
                            if (confirm('Confirm delete')) {
                                window.location.href = 'prices_delete.php?id=<?php echo $price_id;?>&pagenum=<?php echo $pagenum;?>&content_id=<?php echo $content_id;?>';
                            }
                        }
                        </script>  
                        </td>
                    </tr>
                    <?php 
                    }
                    ?>
                    </tbody>
                    </table>
            
                    <div class="clear"></div>
                    <ul class="pagination">
                    <?php
                    echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
                        if ($pagenum == 1)
                        {
                        }
                        else
                            {
                            echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=prices&pagenum=1&id=$content_id'><strong>First page</strong></a></li>";
                            echo " ";
                            $previous = $pagenum-1;
                            echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=prices&pagenum=$previous&id=$content_id'>".$previous."</a></li>";
                            }
                
                            echo "";
                
                
                        if ($pagenum == $last)
                            {			
                            }
                        else {
                            $next = $pagenum+1;
                            echo "<li><a href='{$_SERVER['PHP_SELF']}?page=prices&pagenum=$next&id=$content_id'> ".$next."</a></li>";
                            echo " ";
                            echo "<li><a href='{$_SERVER['PHP_SELF']}?page=prices&pagenum=$last&id=$content_id'><strong>Last page</strong></a></li>";
                        } 
                        ?>	
                    </ul>
                    <?php
                    } // END if rows==0
                    ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->