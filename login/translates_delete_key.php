<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$lang_id = $_GET['lang_id'];
$lang_id = Secure($lang_id);

$translate_id = $_GET['translate_id'];
$translate_id = Secure($translate_id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

$sql = "DELETE FROM ".$database_table_prefix."lang_translates WHERE id = '$translate_id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=translates_lang&msg=delete_ok&pagenum=$pagenum&lang_id=$lang_id");
exit;
?>