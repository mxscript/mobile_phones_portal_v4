<?php
require ("checklogin.php");
require ("check_permision.php");

$content_id = $_GET["id"];
$content_id = Secure($content_id);
$content_id = (int)$content_id;

$source = $_GET["source"];
$source = Secure($source);
if($source=="") $source = "content";

// **************************************************
// AUTHORS FILTERS
$filter_user_role = "";
if($logged_user_role=="author") $filter_user_role = "AND user_id = '$logged_user_id'";
// **************************************************


if($content_id==0)
	$query = "SELECT id FROM ".$database_table_prefix."media";
else	
	{	
	$query = "SELECT id FROM ".$database_table_prefix."media WHERE content_id = '$content_id' AND source = '$source'";
	
	if($source=="page")
		$query_content = "SELECT title FROM ".$database_table_prefix."pages WHERE id = '$content_id' LIMIT 1";
	else
		$query_content = "SELECT title FROM ".$database_table_prefix."content WHERE id = '$content_id' $filter_user_role LIMIT 1";
					
	$rs_content = $conn->query($query_content);
	$row = $rs_content->fetch_assoc();
	$content_title = stripslashes($row['title']);
	}
	
$rs = $conn->query($query);
$rows = $rs->num_rows;
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1><?php if($content_id!=0) {?>Media for <?php echo '"'.$content_title.'"';} else echo "All media content";?></h1> 
         
          <?php if($content_id!=0) {?><a class="btn btn-primary" data-toggle="modal" href="#" data-target=".modal_add_media"><i class="fa fa-plus"></i> Add new media</a><?php include ("static/modal_add_media.php"); } ?>          
        </section>

        <!-- Main content -->
        <section class="content">


		<?php
        if ($msg =='add_ok')
            echo '<p class="bg-info">Media added</p>';		
        if ($msg =='edit_ok')
            echo '<p class="bg-info">Media modified</p>';
        if ($msg =='delete_ok')
            echo '<p class="bg-info">Media deleted</p>';
        if ($msg =='demo_mode')
            echo '<p class="bg-danger">ERROR! This action is disabled in demo mode</p>';
        ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

			<?php
            // ------------------------------------------------------------------------------------------------------
            if (!(isset($pagenum)))
                {
                $pagenum = 1;
                }
            
            if ($rows==0)
                {
                    echo "No media";
                }
            
            else
                {
                    $page_rows = 50;
                    $last = ceil($rows/$page_rows);
            
                    if ($pagenum < 1)
                    {
                    $pagenum = 1;
                    }
                    elseif ($pagenum > $last)
                    {
                    $pagenum = $last;
                    }
            
                    $max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
                    
                    if($content_id==0)
                        $query = "SELECT id, content_id, type, title, description, file, embed_code, source, url_redirect FROM ".$database_table_prefix."media ORDER BY id DESC $max";
                    else
                        $query = "SELECT id, content_id, type, title, description, file, embed_code, source, url_redirect FROM ".$database_table_prefix."media WHERE content_id = '$content_id' ORDER BY id DESC $max";
            
                    $rs = $conn->query($query);
                    ?>
                    
                    <table class="table table-bordered" id="listings">
                    <thead> 
                    <tr>
                    <th>Media Details</th>
                    <th width="220">Content Details</th>
                    <th width="130">Type</th>
                    <th width="140">Actions</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    <?php
                    while($row = $rs->fetch_assoc())
                        {
                        $media_id = $row['id'];
                        $content_id_edited = $row['content_id'];
                        $type = $row['type'];	
                        $title = stripslashes($row['title']);
						$description = stripslashes($row['description']);
                        $file = $row['file'];
                        $embed_code = $row['embed_code'];	
						$source = $row['source'];	
						$url_redirect = stripslashes($row['url_redirect']);
						
						if($source=="page")
							$query_content = "SELECT title FROM ".$database_table_prefix."pages WHERE id = '$content_id_edited' LIMIT 1";
						else
							$query_content = "SELECT title FROM ".$database_table_prefix."content WHERE id = '$content_id_edited' LIMIT 1";
										
						$rs_content = $conn->query($query_content);
						$row = $rs_content->fetch_assoc();
						$content_title = stripslashes($row['title']);										
                    ?>	
                    <tr>            
                        <td>
                        <?php
                        if($type=="image")
                            {
                            ?>
                            <span style="float: left; margin-right:10px;"><a target="_blank" href="<?php echo $config_site_media;?>/large/<?php echo $file;?>"><img style="max-width:150px; max-height:130px; height:auto; width:auto;" src="<?php echo $config_site_media;?>/thumbs/<?php echo $file;?>" /></a></span>
                            <?php									
                            }
                        echo '<strong>'.$title.'</strong>';	
						if ($description) echo '<br />'.$description;
                        if($type=="video")
                            {
                            ?>
                            <br /><small><?php echo $embed_code;?></small>
                            <?php
                            }
						if($url_redirect!='') echo '<br />URL: <a target="_blank" href='.$url_redirect.'>'.$url_redirect.'</a>';	
                        ?>                         
                        </td>
                        
                        <td>
                        <small><?php echo $content_title;?></small>
                        <div class="clear"></div>
                        <?php if($source=="page") echo "(page)";?>
                        </td>
                                           
                        <td>
                        <?php
							if($url_redirect!='')
								echo 'URL redirect';
							else
							    echo $type;
                        ?>
                        </td>
                                    
                        <td>
                        <a data-toggle="modal" href="#" data-target=".modal_edit_media_<?php echo $media_id;?>"><i class="fa fa-edit fa-fw"></i> Edit  media</a>
                        
                        <?php include ("static/modal_edit_media.php");?>  
                        <div class="clear"></div>
                        <a href="javascript:deleteRecord_<?php echo $media_id;?>('<?php echo $media_id;?>');"><i class="fa fa-trash-o fa-fw"></i> Delete media</a>
                        <script language="javascript" type="text/javascript">
                        function deleteRecord_<?php echo $media_id;?>(RecordId)
                        {
                            if (confirm('Confirm delete')) {
                                window.location.href = 'content_media_delete.php?id=<?php echo $media_id;?>&pagenum=<?php echo $pagenum;?>&content_id=<?php echo $content_id;?>&source=<?php echo $source;?>';
                            }
                        }
                        </script>  
                        </td>
                    </tr>
                    <?php 
                    }
                    ?>
                    </tbody>
                    </table>
            
                    <div class="clear"></div>
                    <ul class="pagination">
                    <?php
                    echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
                        if ($pagenum == 1)
                        {
                        }
                        else
                            {
                            echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=content_media&pagenum=1&id=$content_id'><strong>First page</strong></a></li>";
                            echo " ";
                            $previous = $pagenum-1;
                            echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=content_media&pagenum=$previous&id=$content_id'>".$previous."</a></li>";
                            }
                
                            echo "";
                
                
                        if ($pagenum == $last)
                            {			
                            }
                        else {
                            $next = $pagenum+1;
                            echo "<li><a href='{$_SERVER['PHP_SELF']}?page=content_media&pagenum=$next&id=$content_id'> ".$next."</a></li>";
                            echo " ";
                            echo "<li><a href='{$_SERVER['PHP_SELF']}?page=content_media&pagenum=$last&id=$content_id'><strong>Last page</strong></a></li>";
                        } 
                        ?>	
                    </ul>
                    <?php
                    } // END if rows==0
                    ?>

			<div class="clear"></div>            


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->