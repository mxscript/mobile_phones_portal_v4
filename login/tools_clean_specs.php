<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");
require ("../core/plugins/simple_html_dom.php");

require ("checklogin.php");
require ("check_permision.php");

$number = 1;

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$query = "SELECT id, cf_array FROM ".$database_table_prefix."content WHERE root_categ_id = 52 AND id = 1401 ORDER BY id ASC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
	{
		
	ob_start();
	ob_implicit_flush(true);
	set_time_limit(0);

    $cf_array = Secure($row['cf_array']);
	$id = $row['id'];
	
	$test = '
	a:12:{s:6:"Launch";a:2:{s:9:"Announced";s:14:"2016, February";s:6:"Status";s:31:"Available. Released 2016, March";}s:7:"Network";a:7:{s:10:"Technology";s:16:"GSM / HSPA / LTE";s:2:"2G";s:73:"GSM 850 / 900 / 1800 / 1900 - SIM 1 &amp;amp; SIM 2 (dual-SIM model only)";s:2:"3G";s:49:"HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100 - G935F";s:2:"4G";s:203:"LTE band 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 12(700), 13(700), 17(700), 18(800), 19(800), 20(800), 25(1900), 26(850), 28(700), 38(2600), 39(1900), 40(2300), 41(2500) - G935F";s:5:"Speed";s:41:"HSPA 42.2/5.76 Mbps, LTE Cat9 450/50 Mbps";s:4:"GPRS";s:3:"Yes";s:4:"Edge";s:3:"Yes";}s:4:"Body";a:4:{s:10:"Dimensions";s:45:"150.9 x 72.6 x 7.7 mm (5.94 x 2.86 x 0.30 in)";s:6:"Weight";s:15:"157 g (5.54 oz)";s:3:"SIM";s:59:"Single SIM (Nano-SIM) or Dual SIM (Nano-SIM, dual stand-by)";s:5:"Build";s:34:"Corning Gorilla Glass 4 back panel";}s:7:"Display";a:5:{s:4:"Type";s:47:"Super AMOLED capacitive touchscreen, 16M colors";s:4:"Size";s:40:"5.5 inches (~76.1% screen-to-body ratio)";s:10:"Resolution";s:43:"1440 x 2560 pixels (~534 ppi pixel density)";s:10:"Multitouch";s:3:"Yes";s:10:"Protection";s:23:"Corning Gorilla Glass 4";}s:8:"Platform";a:4:{s:2:"OS";s:30:"Android OS, v6.0 (Marshmallow)";s:7:"Chipset";s:49:"Qualcomm MSM8996 Snapdragon 820
Exynos 8890 Octa";s:3:"CPU";s:115:"Dual-core 2.15 GHz Kryo &amp;amp; dual-core 1.6 GHz Kryo
Quad-core 2.3 GHz Mongoose + quad-core 1.6 GHz Cortex-A53";s:3:"GPU";s:26:"Adreno 530
Mali-T880 MP12";}s:6:"Memory";a:2:{s:9:"Card Slot";s:115:"microSD, up to 256 GB (dedicated slot) - single-SIM model
microSD, up to 256 GB (uses SIM 2 slot) - dual-SIM model";s:8:"Internal";s:18:"32/64 GB, 4 GB RAM";}s:6:"Camera";a:4:{s:7:"Primary";s:62:"12 MP, f/1.7, 26mm, phase detection autofocus, OIS, LED flash,";s:8:"Features";s:162:"1/2.6&amp;quot; sensor size, 1.4 µm pixel size, geo-tagging, simultaneous 4K video and 9MP image recording, touch focus, face/smile detection, Auto HDR, panorama";s:5:"Video";s:60:"2160p@30fps, 1080p@60fps, 720p@240fps, HDR, dual-video rec.,";s:9:"Secondary";s:44:"5 MP, f/1.7, 22mm, dual video call, Auto HDR";}s:5:"Sound";a:3:{s:11:"Alert types";s:29:"Vibration; MP3, WAV ringtones";s:11:"Loudspeaker";s:35:"Voice 70dB / Noise 69dB / Ring 71dB";s:10:"3.5mm Jack";s:3:"Yes";}s:5:"Comms";a:6:{s:4:"WLAN";s:57:"Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot";s:9:"Bluetooth";s:20:"v4.2, A2DP, LE, aptX";s:3:"GPS";s:29:"Yes, with A-GPS, GLONASS, BDS";s:3:"NFC";s:3:"Yes";s:5:"Radio";s:2:"No";s:3:"USB";s:23:"microUSB v2.0, USB Host";}s:8:"Features";a:4:{s:7:"Sensors";s:81:"Fingerprint, accelerometer, gyro, proximity, compass, barometer, heart rate, SpO2";s:9:"Messaging";s:45:"SMS(threaded view), MMS, Email, Push Mail, IM";s:7:"Browser";s:5:"HTML5";s:4:"Java";s:2:"No";}s:7:"Battery";a:2:{s:7:"Battery";s:37:"Non-removable Li-Ion 3600 mAh battery";s:9:"Talk time";s:15:"Up to 27 h (3G)";}s:4:"Misc";a:4:{s:6:"Colors";s:37:"Black, White, Gold, Silver, Pink Gold";s:6:"SAR US";s:93:"1.17 W/kg (head) &amp;amp;nbsp; &amp;amp;nbsp; 1.59 W/kg (body) &amp;amp;nbsp; &amp;amp;nbsp;";s:6:"SAR EU";s:93:"0.26 W/kg (head) &amp;amp;nbsp; &amp;amp;nbsp; 0.51 W/kg (body) &amp;amp;nbsp; &amp;amp;nbsp;";s:11:"Price group";s:20:"9/10 (About 800 EUR)";}}';
		
	
	$cf_array = str_replace("&amp;amp;nbsp;", "", $test);
	
	echo $cf_array;
	
	//$cf_array = serialize($cf_array);
	
	
	$query_update = "UPDATE ".$database_table_prefix."content SET cf_array = '$cf_array' WHERE id = '$id' LIMIT 1"; 
	if($conn->query($query_update) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
	else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }
	

	echo '<a name="scrolldown'.$number.'"></a>';
	echo "<strong>".$title."</strong><br>Search terms: ".$search_terms."<hr>";

	ob_flush();
	flush();
	sleep(0.1);
	ob_end_flush();
	?>
	 <script language="javascript">
	   self.location="#scrolldown<?php echo $number;?>";
	 </script>
	<?php 
	$number = $number+1;
	
	}
	
exit;
?>
