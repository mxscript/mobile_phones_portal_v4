<?php
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=error_demo_mode");
	exit();
	}

$page_id = $_POST["page_id"];
$page_id = Secure($page_id);

$title = $_POST["title"];
$title = Secure($title);
$title_encoded = urlencode($title);

$description = $_POST["description"];
$description = Secure($description);
$description_encoded = urlencode($description);

$active = $_POST["active"];
$active = Secure($active);

$show_in_header = $_POST["show_in_header"];
$show_in_header = Secure($show_in_header);

$show_in_footer = $_POST["show_in_footer"];
$show_in_footer = Secure($show_in_footer);

$slider_id = $_POST["slider_id"];
$slider_id = Secure($slider_id);

$position = $_POST["position"];
$position = Secure($position);

$enable_comments = $_POST["enable_comments"];
$enable_comments = Secure($enable_comments);

$redirect_url = $_POST["redirect_url"];
$redirect_url = Secure($redirect_url);

$custom_php_file = $_POST["custom_php_file"];
$custom_php_file = Secure($custom_php_file);

$custom_tpl_file = $_POST["custom_tpl_file"];
$custom_tpl_file = Secure($custom_tpl_file);

$meta_title = $_POST["meta_title"];
$meta_title = Secure($meta_title);
$meta_title_encoded = urlencode($meta_title);

$meta_description = $_POST["meta_description"];
$meta_description = Secure($meta_description);
$meta_description_encoded = urlencode($meta_description);

$meta_keywords = $_POST["meta_keywords"];
$meta_keywords = Secure($meta_keywords);
$meta_keywords_encoded = urlencode($meta_keywords);

$permalink = $_POST["permalink"];
$permalink = Secure($permalink);

if($permalink!="")
	$permalink = RewriteUrl($permalink);
else	
	$permalink = RewriteUrl($title);

if($title=="")
	{
	header("Location:account.php?page=pages_edit&msg=error_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded&page_id=$page_id");
	exit();
	}

// check for duplicate page
$sql = "SELECT id FROM ".$database_table_prefix."pages WHERE title LIKE '$title' AND id != '$page_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=pages_edit&msg=error_duplicate_title&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded&page_id=$page_id");
	exit();
	}

// check for duplicate permalink
$sql = "SELECT id FROM ".$database_table_prefix."pages WHERE permalink LIKE '$permalink' AND id != '$page_id' LIMIT 1";
$rs = $conn->query($sql);
$count = $rs->num_rows;
if($count!=0)
	{
	header("Location:account.php?page=pages_edit&msg=error_duplicate_permalink&title=$title_encoded&description=$description_encoded&meta_title=$meta_title_encoded&meta_description=$meta_description_encoded&meta_keywords=$meta_keywords_encoded&page_id=$page_id");
	exit();
	}


$query = "UPDATE ".$database_table_prefix."pages SET title = '$title', permalink = '$permalink', description = '$description', active = '$active', position = '$position', show_in_header = '$show_in_header', show_in_footer = '$show_in_footer', meta_title = '$meta_title', meta_description = '$meta_description', meta_keywords = '$meta_keywords', custom_tpl_file = '$custom_tpl_file', custom_php_file = '$custom_php_file', redirect_url = '$redirect_url' WHERE id = '$page_id' LIMIT 1"; 
if($conn->query($query) === false) {
  trigger_error('Error: '.$conn->error, E_USER_ERROR);
} else {
  $last_inserted_id = $conn->insert_id;
  $affected_rows = $conn->affected_rows;
}

// form OK:
header("Location: account.php?page=pages&msg=edit_ok");	
exit;
?> 