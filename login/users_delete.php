<?php 
require ("../core/core.php");

require ("checklogin.php");
require ("check_permision.php");

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$id = $_GET['id'];
$id = Secure($id);

$pagenum = $_GET['pagenum'];
$pagenum = Secure($pagenum);

$query = "SELECT protected FROM ".$database_table_prefix."users WHERE id = '$id' LIMIT 1";
$rs = $conn->query($query);
$row = $rs->fetch_assoc();
$protected = $row['protected'];

if($protected==1)
	{
	header("Location: account.php?page=users&msg=error_delete_protected&pagenum=$pagenum");
	exit;
	}

$sql = "UPDATE ".$database_table_prefix."content SET user_id = 0 WHERE user_id = '$id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$sql = "DELETE FROM ".$database_table_prefix."users_extra WHERE user_id = '$id'"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

$sql = "DELETE FROM ".$database_table_prefix."users WHERE id = '$id' LIMIT 1"; 	
if($conn->query($sql) === false) {  trigger_error('Error: '.$conn->error, E_USER_ERROR); } 
else { $affected_rows = $conn->affected_rows; }

header("Location: account.php?page=users&msg=delete_ok&pagenum=$pagenum");
exit;