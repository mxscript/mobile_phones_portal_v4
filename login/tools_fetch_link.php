<?php 
require ("../core/core.php");
require ("../core/plugins/resize-class.php");
require ("../core/plugins/simple_html_dom.php");

require ("checklogin.php");
require ("check_permision.php");

$cf_group_id = 5;
$cf_section_id_launch = 1;
$cf_section_id_network = 3;
$cf_section_id_body = 4;
$cf_section_id_display = 5;
$cf_section_id_platform = 6;
$cf_section_id_memory = 7;
$cf_section_id_camera = 8;
$cf_section_id_sound = 9;
$cf_section_id_comms = 10;
$cf_section_id_features = 13;
$cf_section_id_battery = 14;
$cf_section_id_misc = 15;

$cf_id_technology = 1;
$cf_id_2g = 2;
$cf_id_3g = 3;
$cf_id_4g = 4;
$cf_id_speed = 5;
$cf_id_gprs = 6;
$cf_id_edge = 7;
$cf_id_announced = 8;
$cf_id_status = 9;
$cf_id_dimensions = 10;
$cf_id_display_type = 18;
$cf_id_weight = 15;
$cf_id_sim = 16;
$cf_id_build = 59;
$cf_id_others_body = 17;
$cf_id_display_size = 19;
$cf_id_display_resolution = 20;
$cf_id_multitouch = 21;
$cf_id_protection = 22;
$cf_id_os = 23;
$cf_id_chipset = 24;
$cf_id_cpu = 25;
$cf_id_gpu = 27;
$cf_id_card_slot = 28;
$cf_id_internal = 29;
$cf_id_camera_primary = 30;
$cf_id_camera_features = 31;
$cf_id_camera_video = 32;
$cf_id_camera_secondary = 33;
$cf_id_alert_types = 34;
$cf_id_loudspeaker = 35;
$cf_id_jack = 36;
$cf_id_others_sound = 37;
$cf_id_others_display = 38;
$cf_id_wlan = 39;
$cf_id_bluetooth = 41;
$cf_id_gps = 42;
$cf_id_nfc = 43;
$cf_id_radio = 45;
$cf_id_usb = 46;
$cf_id_sensors = 47;
$cf_id_messaging = 48;
$cf_id_browser = 49;
$cf_id_java = 50;
$cf_id_others_features = 51;
$cf_id_battery = 52;
$cf_id_stand_by = 53;
$cf_id_talk_time = 54;
$cf_id_music_play = 55;
$cf_id_colors = 56;
$cf_id_sar_us = 57;
$cf_id_sar_eu = 58;
$cf_id_price_group = 60;

$today = date("Y-m-d");
$number = 0;

if($site_demo_mode!=0)
	{
	header("Location:account.php?page=dashboard&msg=demo_mode");
	exit();
	}

$query = "SELECT id, brand_name, brand_permalink, device_title, device_permalink, device_image_url, url FROM ".$database_table_prefix."arena_urls WHERE  is_fetched = 0 AND brand_permalink LIKE 'yezz' ORDER BY id DESC";
$rs = $conn->query($query);
while($row = $rs->fetch_assoc())
{
	

    $arena_url_id = $row['id'];
	$brand_name = Secure($row['brand_name']);
	$brand_permalink = $row['brand_permalink'];
	$device_title = Secure($row['device_title']);
	$device_permalink = $row['device_permalink'];
	$device_image_url = $row['device_image_url'];
	$url = $row['url'];

	
	// category
	$query2 = "SELECT id FROM ".$database_table_prefix."categories WHERE permalink = '$brand_permalink' ORDER BY id DESC LIMIT 1";
	$rs2 = $conn->query($query2);
	$row2 = $rs2->fetch_assoc();
	$categ_id = $row2['id'];

	// check if exist 
	$query3 = "SELECT id FROM ".$database_table_prefix."content WHERE categ_id = '$categ_id' AND permalink LIKE '$device_permalink' ORDER BY id DESC LIMIT 1";
	$rs3 = $conn->query($query3);
	$exist = $rs3->num_rows;

	if($exist==0)
	{
	
			ob_start();
			ob_implicit_flush(true);
			set_time_limit(0);
	
			$filename = str_replace("http://gsmarena.com/", "", $url);
			$filename = '../temp/fetch/'.$brand_permalink.'/'.$filename;
	
			/************************************************************************/
			//$filename = '../temp/fetch/samsung/samsung_galaxy_tab_a_9_7-7122.php';
		
			$html = file_get_html($filename);
	
		
			// get text 
			$aditional_info = "";
			foreach($ret = $html->find('div[id=specs-list]') as $text) 
				foreach($text->find('p') as $t) 
				$aditional_info = $t->plaintext;
				$aditional_info = Secure($aditional_info);
			
		
			$Data = array();
					$technology = "";
					$band_2g = "";
					$band_3g = "";
					$band_4g = "";
					$speed = "";
					$edge = "";
					$gprs = "";
					$announced = "";				
					$status = "";
					$dimensions = "";
					$weight = "";
					$build = "";
					$sim = "";
					$display_type = "";
					$display_size = "";
					$display_resolution = "";
					$multitouch = "";
					$protection = "";
					$os = "";
					$chipset = "";
					$cpu = "";
					$gpu = "";
					$card_slot = "";
					$internal_memory = "";
					$camera_primary = "";
					$camera_features = "";
					$camera_video = "";
					$camera_secondary = "";
					$alert_types = "";
					$loudspeaker = "";
					$jack = "";
					$bluetooth = "";
					$wlan = "";
					$gps = "";
					$nfc = "";
					$radio = "";
					$usb = "";
					$sensors = "";
					$messaging = "";
					$browser = "";
					$java = "";					
					$battery = "";			
					$stand_by = "";
					$talk_time = "";
					$colors = "";
					$price_group = "";
					$sar_eu = "";
					$sar_us = "";
		
			foreach($html->find('table') as $tables) 
			{
				foreach($tables->find('tr') as $row) 
				{		
					
					
					foreach($row->find('th') as $th) 
					{
						$th = $th->plaintext;
						//echo $th;
					}
						$flight = array();
						foreach($row->find('td') as $cell) 
						{		
						$flight[$th][] = $cell->plaintext;			    	   	
						}
					$Data[] = $flight;		
		
				}
						
			}

			
			/************************************************************************/
			/* GET groups:
			Array
			(
				[0] => Network
				[1] => Network
				[2] => Network
				[3] => Network
				[4] => Network
				[5] => Launch
				[6] => Launch
				.............
				[7] => Body
				.............
				[40] => Battery
				[41] => Battery
				[42] => Misc
			)
			*/
			$Group = array();
			foreach ($Data as $row => $tr)
				foreach ($tr as $row2 => $x)
					$Group[] = $row2;
					//print_r($Group);
					
			/************************************************************************/
			
			foreach ($Data as $row => $tr)
				{
				foreach ($tr as $row2)
					{				
					
					$all[] = $row2;
					
					if($row2[0]=="Technology")
							$technology = Secure($row2[1]);
							
					if($row2[0]=="2G bands")
							$band_2g = Secure($row2[1]);		
			
					if($row2[0]=="3G bands")
							$band_3g = Secure($row2[1]);		
					
					if($row2[0]=="4G bands")
							$band_4g = Secure($row2[1]);		
					
					if($row2[0]=="Speed")
							$speed = Secure($row2[1]);		
					
					if($row2[0]=="EDGE")
							$edge = Secure($row2[1]);		
					
					if($row2[0]=="GPRS")
							$gprs = Secure($row2[1]);		
					
					if($row2[0]=="Announced")
							$announced = Secure($row2[1]);		
					
					if($row2[0]=="Status")
							$status = Secure($row2[1]);		
			
					if($row2[0]=="Dimensions")
							$dimensions = Secure($row2[1]);		
					
					if($row2[0]=="Weight")
							$weight = Secure($row2[1]);		
					
					if($row2[0]=="Build")
							$build = Secure($row2[1]);		
					
					if($row2[0]=="SIM")
							$sim = Secure($row2[1]);		
					
					if($row2[0]=="Type")
							$display_type = Secure($row2[1]);		
					
					if($row2[0]=="Size")
							$display_size = Secure($row2[1]);		
					
					if($row2[0]=="Resolution")
							$display_resolution = Secure($row2[1]);		
					
					if($row2[0]=="Multitouch")
							$multitouch = Secure($row2[1]);		
					
					if($row2[0]=="Protection")
							$protection = Secure($row2[1]);		
					
					if($row2[0]=="OS")
							$os = Secure($row2[1]);		
					
					if($row2[0]=="Chipset")
							$chipset = Secure($row2[1]);		
					
					if($row2[0]=="CPU")
							$cpu = Secure($row2[1]);		
					
					if($row2[0]=="GPU")
							$gpu = Secure($row2[1]);		
					
					if($row2[0]=="Card slot")
							$card_slot = Secure($row2[1]);		
					
					if($row2[0]=="Internal")
							$internal_memory = Secure($row2[1]);
					
					if($row2[0]=="Primary")
							$camera_primary = Secure($row2[1]);	
							$camera_primary = str_replace("check quality", "", $camera_primary);			
					
					if($row2[0]=="Features")
							$camera_features = Secure($row2[1]);	
							$camera_features = str_replace("check quality", "", $camera_features);				
					
					if($row2[0]=="Video")
							$camera_video = Secure($row2[1]);	
							$camera_video = str_replace("check quality", "", $camera_video);				
					
					if($row2[0]=="Secondary")
							$camera_secondary = Secure($row2[1]);	
							$camera_secondary = str_replace("check quality", "", $camera_secondary);				
					
					if($row2[0]=="Alert types")
							$alert_types = Secure($row2[1]);		
					
					if(trim($row2[0])=="Loudspeaker")
							$loudspeaker = Secure($row2[1]);		
					
					if(trim($row2[0])=="3.5mm jack")
							$jack = Secure($row2[1]);		
					
					if($row2[0]=="WLAN")
							$wlan = Secure($row2[1]);		
			
					if($row2[0]=="Bluetooth")
							$bluetooth = Secure($row2[1]);		
					
					if($row2[0]=="GPS")
							$gps = Secure($row2[1]);		
			
					if($row2[0]=="NFC")
							$nfc = Secure($row2[1]);		
			
					if($row2[0]=="Radio")
							$radio = Secure($row2[1]);		
					
					if($row2[0]=="USB")
							$usb = Secure($row2[1]);		
					
					if($row2[0]=="Sensors")
							$sensors = Secure($row2[1]);		
			
					if($row2[0]=="Messaging")
							$messaging = Secure($row2[1]);		
			
					if($row2[0]=="Browser")
							$browser = Secure($row2[1]);		
			
					if($row2[0]=="Java")
							$java = Secure($row2[1]);		
					
					if($row2[0]=="&nbsp;")
							{ 
							for ($i=0; $i<count($Data); $i++)
								{	
								//echo "<br>".$i.": ".$Group[$i]." - ".$row2[1];	
								if($Group[$i]=="Battery") $battery = Secure($row2[1]);				
								}
							}
					
					if($row2[0]=="Stand-by")
							$stand_by = Secure($row2[1]);		
					
					if($row2[0]=="Talk time")
							$talk_time = Secure($row2[1]);		
					
					if($row2[0]=="Colors")
							$colors = Secure($row2[1]);		
			
					if($row2[0]=="Price group")
							$price_group = Secure($row2[1]);		
					
					if($row2[0]=="SAR EU")
							$sar_eu = Secure($row2[1]);						
					
					if($row2[0]=="SAR US")
							$sar_us = Secure($row2[1]);						
					
					}
				
				}
				//print_r($all);
			
			
				/************************************************************************/
				echo '<a name="scrolldown'.$number.'"></a>';
				echo "<hr><strong>".$device_title."</strong>";
				/*
				echo "<br>Technology: ".$technology;
				echo "<br>2G bands: ".$band_2g;
				echo "<br>3G bands: ".$band_3g;
				echo "<br>4G bands: ".$band_4g;	
				echo "<br>EDGE: ".$edge;
				echo "<br>GPRS: ".$gprs;
				*/
				echo "<br>Announced: ".$announced;	
				echo "<br>Status: ".$status;	
				/*
				echo "<br>Dimensions: ".$dimensions;		
				echo "<br>Weight: ".$weight;	
				echo "<br>Build: ".$build;	
				echo "<br>SIM: ".$sim;		
				echo "<br>Display type: ".$display_type;	
				echo "<br>Display size: ".$display_size;						
				echo "<br>Display resolution: ".$display_resolution;	
				echo "<br>Multitouch: ".$multitouch;	
				echo "<br>Protection: ".$protection;	
				echo "<br>OS: ".$os;	
				echo "<br>Chipset: ".$chipset;	
				echo "<br>CPU: ".$cpu;	
				echo "<br>GPU: ".$gpu;	
				echo "<br>Card slot: ".$card_slot;	
				echo "<br>Internal: ".$internal_memory;	
				echo "<br>Camera primary: ".$camera_primary;	
				echo "<br>Camera features: ".$camera_features;	
				echo "<br>Camera video: ".$camera_video;	
				echo "<br>Camera secondary: ".$camera_secondary;	
				echo "<br>Alert types: ".$alert_types;	
				echo "<br>Loudspeaker: ".$loudspeaker;	
				echo "<br>3.5mm jack: ".$jack;													
				echo "<br>WLAN: ".$wlan;													
				echo "<br>Bluetooth: ".$bluetooth;													
				echo "<br>GPS: ".$gps;													
				echo "<br>NFC: ".$nfc;													
				echo "<br>Radio: ".$radio;													
				echo "<br>USB: ".$usb;													
				echo "<br>Sensors: ".$sensors;													
				echo "<br>Messaging: ".$messaging;																					
				echo "<br>Browser: ".$browser;																					
				echo "<br>Java: ".$java;
				echo "<br>Features others: ".$others_features;		
				echo "<br>Battery: ".$battery;																					
				echo "<br>Stand-by: ".$stand_by;																					
				echo "<br>Talk time: ".$talk_time;																					
				echo "<br>Colors: ".$colors;																										
				echo "<br>Price group: ".$price_group;																											
				echo "<br>SAR EU: ".$sar_eu;																											
				echo "<br>SAR US: ".$sar_us;																													
				*/
				$now = date("Y-m-d H:i:s");
				$content_code = random_code();
				$status_id = getActiveContentStatusID();

				// IMAGE
				if($device_image_url)
				{
					$ext = strtolower(substr(strrchr($device_image_url, '.'), 1));	
					$code = random_code();	
					$permalink2 = substr($device_permalink, 0, 80);
					$image = $code."-".$permalink2.".".$ext;
					$image = RewriteFile($image);										
							
					$imageString = file_get_contents($device_image_url);
					
					$fp = fopen("../content/media/large/".$image, "w");
					fwrite($fp, $imageString);
					
					$fp = fopen("../content/media/small/".$image, "w");
					fwrite($fp, $imageString);
					
					$fp = fopen("../content/media/thumbs/".$image, "w");
					fwrite($fp, $imageString);
					
					fclose($fp);
					
					//@unlink ("../content/media/temp/".$image);				
				}
								

				$query = "INSERT INTO ".$database_table_prefix."content (id, unique_code, title, aditional_info, permalink, image, user_id, categ_id, cf_group_id, status_id, datetime_added) VALUES (NULL, '$content_code', '$device_title', '$aditional_info', '$device_permalink', '$image', '$logged_user_id', '$categ_id', '$cf_group_id', '$status_id', '$now')"; 
				if($conn->query($query) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// content ID
				$query_id = "SELECT id FROM ".$database_table_prefix."content WHERE unique_code = '$content_code' ORDER BY id DESC LIMIT 1";
				$rs_id = $conn->query($query_id);
				$row_id = $rs_id->fetch_assoc();
				$content_id = $row_id['id'];

				// ADD CUSTOM FIELDS *****************************************				
				
				// Technology
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_technology', '$cf_group_id', '$technology', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// 2g
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_2g', '$cf_group_id', '$band_2g', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// 3g
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_3g', '$cf_group_id', '$band_3g', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// 4g
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_4g', '$cf_group_id', '$band_4g', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Speed
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_speed', '$cf_group_id', '$speed', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// GPRS
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_gprs', '$cf_group_id', '$gprs', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// EDGE
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_edge', '$cf_group_id', '$edge', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Announced
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_announced', '$cf_group_id', '$announced', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Status
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_status', '$cf_group_id', '$status', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Dimensions
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_dimensions', '$cf_group_id', '$dimensions', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Weight
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_weight', '$cf_group_id', '$weight', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// SIM
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_sim', '$cf_group_id', '$sim', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Build
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_build', '$cf_group_id', '$build', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Display type
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_display_type', '$cf_group_id', '$display_type', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Display size
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_display_size', '$cf_group_id', '$display_size', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Resolution
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_display_resolution', '$cf_group_id', '$display_resolution', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Multitouch
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_multitouch', '$cf_group_id', '$multitouch', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Protection
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_protection', '$cf_group_id', '$protection', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// OS
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_os', '$cf_group_id', '$os', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Chipset
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_chipset', '$cf_group_id', '$chipset', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// CPU
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_cpu', '$cf_group_id', '$cpu', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// GPU
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_gpu', '$cf_group_id', '$gpu', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }
				
				// Card slot
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_card_slot', '$cf_group_id', '$card_slot', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Internal
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_internal', '$cf_group_id', '$internal_memory', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Camera primary
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_camera_primary', '$cf_group_id', '$camera_primary', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Camera features
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_camera_features', '$cf_group_id', '$camera_features', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Camera video
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_camera_video', '$cf_group_id', '$camera_video', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Camera secondary
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_camera_secondary', '$cf_group_id', '$camera_secondary', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Alert types
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_alert_types', '$cf_group_id', '$alert_types', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Loudspeaker
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_loudspeaker', '$cf_group_id', '$loudspeaker', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Jack
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_jack', '$cf_group_id', '$jack', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// WLAN
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_wlan', '$cf_group_id', '$wlan', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Bluetooth
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_bluetooth', '$cf_group_id', '$bluetooth', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// GPS
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_gps', '$cf_group_id', '$gps', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// NFC
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_nfc', '$cf_group_id', '$nfc', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Radio
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_radio', '$cf_group_id', '$radio', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// USB
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_usb', '$cf_group_id', '$usb', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Sensors
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_sensors', '$cf_group_id', '$sensors', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Messaging
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_messaging', '$cf_group_id', '$messaging', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Browser
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_browser', '$cf_group_id', '$browser', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Java
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_java', '$cf_group_id', '$java', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// battery
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_battery', '$cf_group_id', '$battery', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// stand by
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_stand_by', '$cf_group_id', '$stand_by', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// talk time
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_talk_time', '$cf_group_id', '$talk_time', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Music play
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_music_play', '$cf_group_id', '$music_play', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Colors
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_colors', '$cf_group_id', '$colors', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }
				
				// SAR US
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_sar_us', '$cf_group_id', '$sar_us', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// SAR EU
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_sar_eu', '$cf_group_id', '$sar_eu', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }

				// Price group
				$query_cf = "INSERT INTO ".$database_table_prefix."cf_values (id, content_id, cf_id, group_id, value, extra) VALUES (NULL, '$content_id', '$cf_id_price_group', '$cf_group_id', '$price_group', '')"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }



				
				$query_cf = "UPDATE ".$database_table_prefix."arena_urls SET is_fetched = 1, fetch_date = '$today' WHERE id = '$arena_url_id' LIMIT 1"; 
				if($conn->query($query_cf) === false) { trigger_error('Error: '.$conn->error, E_USER_ERROR);} 
				else {  $last_inserted_id = $conn->insert_id;  $affected_rows = $conn->affected_rows; }


		ob_flush();
		flush();
		sleep(1);
		ob_end_flush();
		?>
		<script language="javascript">
		   self.location="#scrolldown<?php echo $number;?>";
		 </script>
		<?php 
		$number = $number+1;

	} // end IF exist

} //end WHILE
	
exit;
?>
