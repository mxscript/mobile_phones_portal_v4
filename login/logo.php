<?php 
require ("checklogin.php");
require ("check_permision.php");
?>


<div class="content-wrapper">

        <section class="content-header">
          <h1>Website logo</h1>                
        </section>

        <!-- Main content -->
        <section class="content">

<?php
	if ($msg =='add_ok')
		echo '<p class="bg-info">Logo changed</p>';	
	if ($msg =='error_file')
		echo '<p class="bg-danger">Error. Select image file (jpg, jpeg, gif, png)</p>';	
?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">


            <form action="logo_submit.php" method="post" enctype="multipart/form-data">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Change HEADER logo</label>
            <input type="hidden" name="where" value="header" />
            <input type="file" name="image">
            </div>
            </div>
                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input class="btn btn-primary" name="input" type="submit" value="Change header logo" />
            </div>  
            </div>                      
            </form>

			<div class="clear"></div>
            
            <form action="logo_submit.php" method="post" enctype="multipart/form-data">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Change FOOTER logo</label>
            <input type="hidden" name="where" value="footer" />
            <input type="file" name="image">
            </div>
            </div>
                        
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input class="btn btn-primary" name="input" type="submit" value="Change footer logo" />
            </div>  
            </div>                      
            </form>


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->