<?php
session_start();

$_SESSION['user_token'] = isset($_SESSION['user_token']) ? $_SESSION['user_token'] : '';
$_SESSION['user_logged'] = isset($_SESSION['user_logged']) ? $_SESSION['user_logged'] : '';
$_COOKIE['user_logged'] = isset($_COOKIE['user_logged']) ? $_COOKIE['user_logged'] : '';

if((isset($_SESSION['user_token']) and $_SESSION['user_logged']==1) or (isset($_COOKIE['user_token']) and $_SESSION['user_logged']==1))	{
	// user is logged
	header("location:account.php");
	exit;
	}

require ("../core/core.php");

$msg = isset($_GET['msg']) ? $_GET['msg'] : '';
$msg = Secure($msg);

$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : '';
$redirect = Secure($redirect);
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login area</title>

    <!-- Core CSS -->
    <link href="../core/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/login.css" rel="stylesheet">

    <!-- Checkboxes style -->
    <link href="../core/assets/css/bootstrap-checkbox.css" rel="stylesheet">

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Hello. Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                    	<?php	
                    	if ($msg =='error')
							echo '<p class="text-danger">Wrong email or password</p>';
						if ($msg =='register_ok') 
							echo '<p class="text-info">You must activate your account. Please check your email (including spam folder)</p>';		
						if ($msg =='user_active')
							echo '<p class="text-info">User activated. Please login</p>';
						if ($msg =='user_already_active')
							echo '<p class="text-info">User is active. Please login</p>';
						if ($msg =='logout')
							echo '<p class="text-info">You have sign out!</p>';										
						if ($msg =='error_inactive')
							echo '<p class="text-danger"><strong>User is inactive.</strong><br />Please click on activate link in your registration email (don\'t forget to check spam folder too)!</p><i class="fa fa-undo fa-fw"></i> <a href="reset-activation.php">Resend activation code</a><hr />';	
						if ($msg =='reset_ok')
							echo '<p class="text-info">Your new password was sent in your email address!</p>';			
						if ($msg =='reset_activation_ok')
							echo '<p class="text-info">We have resend a new activation code. Please check your email address to ACTIVATE your account!</p>';			
						if ($msg =='not_logged')
							echo '<p class="text-danger">Please login in your account!</p>';			
													
	
						?>
                        <form role="form" method="post" action="login.php">
                            <fieldset>
                                <div class="input-group">
	                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                    <input class="form-control" placeholder="E-mail" name="login_email" type="email" autofocus>
                                </div>
                                <div class="input-group">
	                                <span class="input-group-addon"><i class="fa fa-unlock"></i></span>
                                    <input class="form-control" placeholder="Password" name="login_password" type="password" value="">
                                </div>

                                <div class="checkbox checkbox-primary">
                                <input id="checkbox_remember" type="checkbox" name="remember">
                                <label for="checkbox_remember"> Remember me</label>
                                </div>                                
								<input type="hidden" name="redirect" value="<?php echo $redirect;?>" />
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Login" />
                            </fieldset>
                        </form>

                        <div class="clear"></div>
                        <?php
						if($config_user_registration_enabled==1) { ?>
                        <i class="fa fa-user fa-fw"></i> No account yet? <a href="sign-up.php">Register new account</a><br />
                        <?php } ?>
                        <i class="fa fa-undo fa-fw"></i> Forget password? <a href="reset-password.php">Reset password</a>
                        
                        </div>   
                                                   
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Core Scripts -->
    <script src="../core/assets/js/jquery-1.10.2.js"></script>
    <script src="../core/assets/js/bootstrap.min.js"></script>

</body>

</html>