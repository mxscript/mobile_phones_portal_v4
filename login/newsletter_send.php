<?php 
require ("checklogin.php");
require ("check_permision.php");

$code = $_GET["code"];
$code = Secure($code);
?>

<!-- Select2 -->
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
<script src="assets/plugins/select2/select2.min.js"></script>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Select recipients</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
	if ($msg =='error_subject')
		echo '<p class="bg-danger">Error. Input subject</p>';
	if ($msg =='error_smtp')
		echo '<p class="bg-danger">Error. SMTP is required to send emails. Please <a href="account.php?page=settings_emails">setup to use SMTP mailer</a></p>';		
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">
                             
           <form action="newsletter_send_submit.php" method="post">
            
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
            <label>Send to:</label>
   	        <select id="receivers" name="receivers" class="form-control">
            <option value="all">All subscribers</option>
   	        <option value="defined">Manual select subscribers</option>
   			</select>
       	    </div>
            </div>
			
            <script>
			$("#receivers").change(function () {
		    var selected_option = $('#receivers').val();
		    if (selected_option === 'defined') {
		        $('#manual').attr('pk','all').show();
		    }
		    if (selected_option != 'defined') {
		        $("#manual").removeAttr('pk').hide();
		    }
			})
			</script>
            
			<div id="manual" hidden="hidden">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <label>Input email(s)</label>
            <select name="subscriber_ids[]" class="form-control select2" multiple="multiple" data-placeholder="Select subscriber" style="width: 100%;">
            <?php
            $query = "SELECT id, email FROM ".$database_table_prefix."nl_subscribers WHERE is_banned = 0 ORDER BY email ASC";
			$rs = $conn->query($query);
			while($row = $rs->fetch_assoc())
	        	{
    	        $id = $row['id'];
				$email = $row['email'];
				?>
                <option value="<?php echo $id;?>"><?php echo $email;?></option>
                <?php
				}
				?>
            </select>
            </div>
            </div>
			</div>

			<script type="text/javascript">
                  $(function () {
                    $(".select2").select2();
                  });
            </script>
   
                                    
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
            <input type="hidden" name="code" value="<?php echo $code;?>" />
            <input class="btn btn-danger btn-flat" name="input" type="submit" value="Send newsletter" />
            </div>  
            </div>
                      
            </form>
            <div class="clear"></div>


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->