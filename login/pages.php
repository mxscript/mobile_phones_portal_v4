<?php 
require ("checklogin.php");
require ("check_permision.php");

$query = "SELECT id FROM ".$database_table_prefix."pages";
$rs = $conn->query($query);
$rows = $rs->num_rows;

$description = isset($_GET['description']) ? $_GET['description'] : '';
$description_decoded = urldecode($description);

$title = isset($_GET['title']) ? $_GET['title'] : '';
$title_decoded = urldecode($title);

$meta_title = isset($_GET['meta_title']) ? $_GET['meta_title'] : '';
$meta_title_decoded = urldecode($meta_title);

$meta_description = isset($_GET['meta_description']) ? $_GET['meta_description'] : '';
$meta_description_decoded = urldecode($meta_description);

$meta_keywords = isset($_GET['meta_keywords']) ? $_GET['meta_keywords'] : '';
$meta_keywords_decoded = urldecode($meta_keywords); 
?>

<div class="content-wrapper">

        <section class="content-header">
          <h1>Pages (<?php echo $rows;?> pages)</h1> 
          <div class="clear"></div>  
          <a class="btn btn-primary" data-toggle="modal" href="account.php?page=pages_add"><i class="fa fa-plus"></i> Add new page</a>          
        </section>

        <!-- Main content -->
        <section class="content">

	<?php
    if ($msg =='add_ok')
        echo '<p class="bg-info">Page added</p>';		
    if ($msg =='edit_ok')
        echo '<p class="bg-info">Page modified</p>';
    if ($msg =='delete_ok')
        echo '<p class="bg-info">Page deleted</p>';
    ?>

    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">

		<?php
        // ------------------------------------------------------------------------------------------------------
        if (!(isset($pagenum)))
            {
            $pagenum = 1;
            }
        
        if ($rows==0)
            {
                echo "No page";
            }
        
        else
            {
                $page_rows = 10;
                $last = ceil($rows/$page_rows);
        
                if ($pagenum < 1)
                {
                $pagenum = 1;
                }
                elseif ($pagenum > $last)
                {
                $pagenum = $last;
                }
        
                $max = ' LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;		
                $query = "SELECT id, title, permalink, description, active, position, show_in_header, show_in_footer, meta_title, meta_description, meta_keywords, custom_tpl_file, custom_php_file, redirect_url FROM ".$database_table_prefix."pages ORDER BY position ASC, title ASC $max";
                $rs = $conn->query($query);
                ?>
                
                <table class="table table-bordered" id="listings">
                <thead> 
                <tr>
                <th width="80">Order</th>
                <th>Details</th>
                <th width="160">Actions</th>
                </tr>
                </thead>
                
                <tbody>
                <?php
                while($row = $rs->fetch_assoc())
                    {
                    $page_id = $row['id'];
                    $title = stripslashes($row['title']);
                    $permalink = $row['permalink'];
                    $description = strip_tags(html_entity_decode(stripslashes($row['description'])));
                    $active = $row['active'];
					$position = $row['position'];
                    $show_in_header = $row['show_in_header'];
					$show_in_footer = $row['show_in_footer'];
                    $meta_title = stripslashes($row['meta_title']);
                    $meta_description = stripslashes($row['meta_description']);
                    $meta_keywords = stripslashes($row['meta_keywords']);
					$custom_tpl_file = $row['custom_tpl_file'];
					$custom_php_file = $row['custom_php_file'];
					$redirect_url = $row['redirect_url'];
					
					$description2 = substr($description, 0, 500);
					
                ?>	
                <tr <?php if($active==0) echo 'bgcolor="#FFE1E1"'; ?> >
                   
                   	<td>
                    <?php echo $position; ?>
                    </td>
                    
                    <td>
                    <h4><?php echo $title; ?></h4> 
                    <?php if($description) echo $description2.'<div class="clear"></div>'; ?>
                    <small>
                    Active: <?php if($active==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?> |
                    Show in header: <?php if($show_in_header==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?> | 
                    Show in footer: <?php if($show_in_footer==0) echo "<font color='#CC0000'>NO</font>"; else echo "YES"; ?> |                                      
                    <div class="clear"></div>
                    Meta title: <?php echo $meta_title; ?>
                    <br />
                    Meta description: <?php echo $meta_description; ?>
                    <br />
                    Meta keywords: <?php echo $meta_keywords; 
					if($custom_tpl_file!='') echo '<br />Custom template file: '.$custom_tpl_file;
					if($custom_php_file!='') echo '<br />Custom PHP file: '.$custom_php_file;					
					if($redirect_url!='') echo '<br />Redirect URL: <a target="_blank" href='.$redirect_url.'>'.$redirect_url.'</a>';
					?>
                    </small>
                    </td>
                    
                    
                    <td>
                    <i class="fa fa-edit fa-fw"></i> <a href="account.php?page=pages_edit&page_id=<?php echo $page_id;?>"><small>Edit page</small></a>
                    <div class="clear"></div>
                    <i class="fa fa-trash-o fa-fw"></i> <a href="javascript:deleteRecord_<?php echo $page_id;?>('<?php echo $page_id;?>');"><small>Delete page</small></a>
                    <script language="javascript" type="text/javascript">
                    function deleteRecord_<?php echo $page_id;?>(RecordId)
                    {
                        if (confirm('Confirm delete')) {
                            window.location.href = 'pages_delete.php?id=<?php echo $page_id;?>';
                        }
                    }
                    </script>  
                    <div class="clear"></div>
					<?php
                    $query_media = "SELECT id FROM ".$database_table_prefix."media WHERE source = 'page' AND content_id = '$page_id'";
   					$rs_media = $conn->query($query_media);
					$numb_media = $rs_media->num_rows;
					?>
                    <small>
					<i class="fa fa-image fa-fw"></i> <a href="account.php?page=content_media&id=<?php echo $page_id;?>&pagenum=<?php echo $pagenum;?>&source=page"> <?php if ($numb_media==0) echo 'No media content'; else echo $numb_media." media content";?></a>
                    </small>
					<div class="clear"></div>
                    </td>
                </tr>
                <?php
                }
                ?>
                </tbody>
                </table>
        
            <div class="clear"></div>
            <ul class="pagination">
            <?php
            echo "Page <strong>$pagenum</strong> of <strong>$last</strong><br><br>";
                if ($pagenum == 1)
                {
                }
                else
                    {
                    echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=pages&pagenum=1'><strong>First page</strong></a></li>";
                    echo " ";
                    $previous = $pagenum-1;
                    echo "<li> <a href='{$_SERVER['PHP_SELF']}?page=pages&pagenum=$previous'>".$previous."</a></li>";
                    }
        
                    echo "";
        
        
                if ($pagenum == $last)
                    {			
                    }
                else {
                    $next = $pagenum+1;
                    echo "<li><a href='{$_SERVER['PHP_SELF']}?page=pages&pagenum=$next'> ".$next."</a></li>";
                    echo " ";
                    echo "<li><a href='{$_SERVER['PHP_SELF']}?page=pages&pagenum=$last'><strong>Last page</strong></a></li>";
                } 
                ?>	
                </ul>
        
        
                <?php
                } // END if rows==0
                ?>


					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                

        </section><!-- /.content -->

</div><!-- /.content-wrapper -->