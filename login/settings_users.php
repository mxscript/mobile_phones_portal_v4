<?php 
require ("checklogin.php");
require ("check_permision.php");
?>

<form action="settings_submit.php" method="post">
<div class="content-wrapper">

        <section class="content-header">
          <h1>Users and Registration Settings</h1>          
        </section>

        <!-- Main content -->
        <section class="content">

    <?php
	if ($msg =='edit_ok')
		echo '<p class="bg-info">Settings changed</p>';		
	if($msg =='demo_mode')
		echo "<p class='bg-danger'>Warning! You cant change this settings in demo mode</p>";							
	?>


	<p class="bg-info">Note: Staff members (admins, moderators, authors) can be added only in admin area <a href="account.php?page=users">here</a></p>	
              
    <div class="row">
        <div class="col-lg-12">				

        	<div class="box box-info">
                
				<div class="box-body">
                
				<div class="col-lg-6">
                <div class="form-group">
                <label>User registration:</label>
                                	
                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_user_reg" type="checkbox" name="user_registration_enabled" <?php if ($config_user_registration_enabled=='1') echo "checked";?>>
                    <label for="checkbox_user_reg"> Enable users registration</label>
                    </div>

                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_user_validate_email" type="checkbox" name="registration_need_email_validation" <?php if ($config_registration_need_email_validation=='1') echo "checked";?>>
                    <label for="checkbox_user_validate_email"> Users must validate email address</label>
                    </div>

                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_user_validate_admin" type="checkbox" name="registration_need_admin_validation" <?php if ($config_registration_need_admin_validation=='1') echo "checked";?>>
                    <label for="checkbox_user_validate_admin"> Users must be validated by admin</label>
                    </div>

                    <div class="checkbox checkbox-primary">
                    <input id="checkbox_captcha" type="checkbox" name="captcha_registration" <?php if ($config_captcha_registration=='1') echo "checked";?>>
                    <label for="checkbox_captcha"> Enable captcha in register form</label>
                    </div>                    
					                                      
                </div>
                </div>                                                                     
                                
                <div class="col-lg-6">
                <h4>Users roles description:</h4>
                <strong>Staff user roles</strong><br />
                <?php
				$sql_roles = "SELECT id, role, title, role_description FROM ".$database_table_prefix."users_roles WHERE active = '1' AND is_staff = 1 ORDER BY id ASC";
				$rs_roles = $conn->query($sql_roles);
				while($row = $rs_roles->fetch_assoc())
					{
					$role_id = $row['id'];
					$role = stripslashes($row['role']);
					$role_title = stripslashes($row['title']);
					$role_description = stripslashes($row['role_description']);
					?>
	                <em><?php echo $role_title." (".$role.")";?></em> - <?php echo $role_description;?><br />
                    <?php
					}
					?>
                <div class="clear"></div>    
                <strong>Registered members</strong><br />
                <?php
				$sql_roles = "SELECT id, role, title, role_description FROM ".$database_table_prefix."users_roles WHERE active = '1' AND is_staff = 0 ORDER BY id ASC";
				$rs_roles = $conn->query($sql_roles);
				while($row = $rs_roles->fetch_assoc())
					{
					$role_id = $row['id'];
					$role = stripslashes($row['role']);
					$role_title = stripslashes($row['title']);
					$role_description = stripslashes($row['role_description']);
					?>
	                <em><?php echo $role_title." (".$role.")";?></em> - <?php echo $role_description;?><br />
                    <?php
					}
					?>
                    
                </div>
                                				
                <div class="col-lg-12">    
                    <div class="form-group">    
		            	<input type="hidden" name="settings_section" value="users" />
        		    	<input type="hidden" name="return_page" value="settings_users" />                 	            	
                        <button type="submit" class="btn btn-primary">Change settings</button>		    
                    </div>
                </div>
                    

					</div><!-- /.box-body -->
                  </div><!-- /.box -->

				</div>
		  </div>	
                                
        </section><!-- /.content -->




</div><!-- /.content-wrapper -->
</form>   