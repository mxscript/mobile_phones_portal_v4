<?php
/*
=========================================================================================
Copyright www.mxscripts.com

Terms and Conditions:
- A single license can be used for a single location.
- You cannot sale or distribution (free or for a fee) this script or some piece of script code.
- The code of this script is forbidden to change and redistribute it (free or paid). You can change the script just for your own use and not to resell.
=========================================================================================
*/

require_once 'init.php';

$get_categ_permalink = $_GET["categ_permalink"];
$get_categ_permalink = Secure($get_categ_permalink);

$get_permalink = $_GET["permalink"];
$get_permalink = Secure($get_permalink);

$content_id = $_GET["id"];
$content_id = Secure($content_id);
$content_id = (int)$content_id;

$msg = isset($_GET['msg']) ? $_GET['msg'] : '';
$msg = Secure($msg);

$active_tab = isset($_GET['active_tab']) ? $_GET['active_tab'] : '';
$active_tab = Secure($active_tab);

// update hits
$sql = "UPDATE ".$database_table_prefix."content SET hits = hits + 1 WHERE id = '$content_id' AND status_id = '$activeContentStatusID' LIMIT 1";
$rs = $conn->query($sql);

$postDetailsArray =  postDetailsArray($content_id);

$exist = $postDetailsArray['exist'];
$title = $postDetailsArray['title'];
$permalink = $postDetailsArray['permalink'];
$image = $postDetailsArray['image'];
$categ_id = $postDetailsArray['categ_id'];
$root_categ_id = $postDetailsArray['root_categ_id'];
$cf_group_id = $postDetailsArray['cf_group_id'];
$user_id = $postDetailsArray['user_id'];
$status_id = $postDetailsArray['status_id'];
$content_short = $postDetailsArray['content_short'];
$content = $postDetailsArray['content'];
$meta_title = $postDetailsArray['meta_title'];
$meta_description = $postDetailsArray['meta_description'];
$meta_keywords = $postDetailsArray['meta_keywords'];
$datetime = $postDetailsArray['datetime_added'];
$hits = $postDetailsArray['hits'];
$aditional_info = $postDetailsArray['aditional_info'];
$disable_comments = $postDetailsArray['disable_comments'];
$disable_ads = $postDetailsArray['disable_ads'];
$disable_ratings = $postDetailsArray['disable_ratings'];
$categ_title = $postDetailsArray['categ_title'];
$categ_permalink = $postDetailsArray['categ_permalink'];

if($exist==1 and $permalink == $get_permalink and $categ_permalink == $get_categ_permalink)
	{} // valid content
else
	{			
	$smarty->display('404.tpl');
	exit;
	}

$captcha_random_code = rand(0,9999);

$smarty->assign('CONTENT_ID',$content_id);
$smarty->assign('CONTENT_TITLE',$title);
$smarty->assign('CONTENT_PERMALINK',$permalink);
$smarty->assign('META_TITLE',$meta_title);
$smarty->assign('META_DESCRIPTION',$meta_description);
$smarty->assign('META_KEYWORDS',$meta_keywords);
$smarty->assign('CATEG_TITLE',$categ_title);
$smarty->assign('CATEG_PERMALINK',$categ_permalink);
$smarty->assign('CONTENT_IMAGE',$image);
$smarty->assign('CONTENT_SHORT',$content_short);
$smarty->assign('CONTENT',$content);
$smarty->assign('CONTENT_ADITIONAL_INFO',$aditional_info);
$smarty->assign('CONTENT_HITS',$hits);
$smarty->assign('CONTENT_DATETIME',DateTimeFormat($datetime));
$smarty->assign('MSG',$msg);
$smarty->assign('CAPTCHA_RANDOM_CODE',$captcha_random_code);
$smarty->assign('DISABLE_ADS', $disable_ads);
$smarty->assign('DISABLE_COMMENTS',$disable_comments);
$smarty->assign('DISABLE_RATINGS',$disable_ratings);

$specs_array = specsArray($content_id);

// check if exist prices
$smarty->assign('EXIST_PRICES',checkExistPrices($content_id));

// check if exist media
$smarty->assign('EXIST_MEDIA',checkExistMedia($content_id));

// SPECS
$smarty->assign('specs_array', $specs_array);


// **************************************************************************************
// SUMMARY DATA
$specs_summary_array = MobileSpecsSummary($content_id);

$smarty->assign('SUMMARY_STATUS',$specs_summary_array['status']);
$smarty->assign('SUMMARY_DISPLAY',$specs_summary_array['display']);
$smarty->assign('SUMMARY_OS',$specs_summary_array['os']);
$smarty->assign('SUMMARY_CPU',html_entity_decode($specs_summary_array['cpu']));
$smarty->assign('SUMMARY_MEMORY',$specs_summary_array['memory']);
$smarty->assign('SUMMARY_CAMERA',$specs_summary_array['camera']);
$smarty->assign('SUMMARY_BATTERY',$specs_summary_array['battery']);
// **************************************************************************************

// STAR RATING
$rating_value = returnStarRatingValue($content_id);
$numb_ratings = returnNumberStarRatings($content_id);
$smarty->assign('STAR_RATING_VALUE', $rating_value);
$smarty->assign('STAR_RATING_NUMB_RATINGS', $numb_ratings);

// MEDIA - IMAGES
$smarty->assign('CONTENT_NUMBER_OF_MEDIA_IMAGES', numberImages($content_id));		

// MEDIA - VIDEOS
$smarty->assign('CONTENT_NUMBER_OF_MEDIA_VIDEOS', numberVideos($content_id));	


$smarty->assign('content_media_videos', ContentMediaVideos($content_id));
$smarty->assign('content_media_images', ContentMediaImages($content_id));

$smarty->display('post-media.tpl');	
 