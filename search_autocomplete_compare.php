<p id="searchresults">
<?php
require "core/core.php";
$status_id_active = getActiveContentStatusID();

if(isset($_POST['queryString'])) 
{
	$product_number = isset($_REQUEST['product_number']) ? $_REQUEST['product_number'] : '';
	$product_number = Secure($product_number);
	
	$id_product1 = isset($_REQUEST['id_product1']) ? $_REQUEST['id_product1'] : '';
	$id_product1 = Secure($id_product1);
	
	$id_product2 = isset($_REQUEST['id_product2']) ? $_REQUEST['id_product2'] : '';
	$id_product2 = Secure($id_product2);

	$id_product3 = isset($_REQUEST['id_product3']) ? $_REQUEST['id_product3'] : '';
	$id_product3 = Secure($id_product3);

	$queryString = isset($_POST['queryString']) ? $_POST['queryString'] : '';
	$queryString = Secure($queryString);
			
	if(strlen($queryString) >0)
	{
		$query_ls_all = "SELECT id, title, permalink, categ_id, image FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active'  AND search_terms LIKE '%" . $queryString . "%'";	
		$rs_ls_all = $conn->query($query_ls_all);
		$ls_numb_results = $rs_ls_all->num_rows;
		
		// display first 6 results
		$query_ls = "SELECT id, title, permalink, categ_id, image FROM ".$database_table_prefix."content WHERE status_id = '$status_id_active'  AND search_terms LIKE '%" . $queryString . "%' ORDER BY id DESC LIMIT 6";
		$rs_ls = $conn->query($query_ls);
		while ($row_ls = $rs_ls->fetch_assoc())
			{
				$ls_id = stripslashes($row_ls['id']);
				$ls_title = stripslashes($row_ls['title']);
				$ls_permalink = stripslashes($row_ls['permalink']);
				$ls_categ_id = stripslashes($row_ls['categ_id']);	
				$ls_image = stripslashes($row_ls['image']);	
								
				$query_ls_categ = "SELECT title, permalink FROM ".$database_table_prefix."categories WHERE id = '$ls_categ_id' LIMIT 1";
				$query_ls_categ = $conn->query($query_ls_categ);
				$row_ls = $query_ls_categ->fetch_assoc();
				$ls_categ_title = stripslashes($row_ls['title']);
				$ls_categ_permalink = stripslashes($row_ls['permalink']);
				
				if($product_number==1)
					echo '<a href="'.$config_site_url.'/compare.html?id_product1='.$ls_id.'&id_product2='.$id_product2.'&id_product3='.$id_product3.'">';
				if($product_number==2)
					echo '<a href="'.$config_site_url.'/compare.html?id_product2='.$ls_id.'&id_product1='.$id_product1.'&id_product3='.$id_product3.'">';
				if($product_number==3)
					echo '<a href="'.$config_site_url.'/compare.html?id_product3='.$ls_id.'&id_product1='.$id_product1.'&id_product2='.$id_product2.'">';
					
	         	echo '<img src="'.$config_site_media.'/thumbs/'.$ls_image.'"/>';
	         			
	         	if(strlen($ls_title) > 45) 
					{
					$ls_title = substr($ls_title, 0, 45) . "...";
	         		}	         			
	         	echo '<span class="searchheading">'.$ls_categ_title.' '.$ls_title.'</span>';
	         			

	         } // end while
   		
	}
} 
else 
{
	echo 'There should be no direct access to this script!';
}
?>
</p>